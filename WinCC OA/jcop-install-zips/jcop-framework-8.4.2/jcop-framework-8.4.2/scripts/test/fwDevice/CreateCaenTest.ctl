#uses "fwDevice/fwDevice.ctl"

// to find device models with width wider than one slot one may use the following dpQuery
//   SELECT '.model:_original.._value', '.width:_original.._value', '.slots:_original.._value' FROM '*' WHERE _DPT = "_FwDeviceModel" AND  '.width:_original.._value' > 1
//
// in particular, these are CAEN SY1527 board models such as A1526N (and P), A876, A1520PE, A1833BP (and BN), A1535SN, A3540AP 

main()
{
    fwDevice_initialize();

    dyn_string exceptionInfo;
    string crateName="Test1";
    string crateType="CAEN SY1527";
    string crateModel="SY4527";
    string vendor="CAEN";
    string boardType="CAEN SY1527 Board";
    //string boardModel="A1526N"; // these are double-width boards with 6 channels
    //string boardModel="A1536N"; // these are single-width boards with 32 channels
    //string boardModel="A2518"; // these are single-width boards with 8 channels
    string boardModel="A1932AN"; // these are double-width boards with 48 channels! Note it has a special channel type, and channel numbering starts with 1 (0 is the "primary channel")
    string channelType="CAEN Channel";
//    string channelModel="CAEN SY1527 Channel";
    string channelModel="CAEN SY1527 A1932AN Channel";

    string crateDP=vendor+"/"+crateName; // must not have system name

    // ==== cleanup ===
    if (dpExists(crateDP)) {
        DebugTN("Dropping existing crate with its children",crateDP);
        time t0=getCurrentTime();
        fwDevice_delete(getSystemName()+crateDP, exceptionInfo);

        time t1=getCurrentTime();
        if (dynlen(exceptionInfo)) {DebugTN(exceptionInfo);return;}
        time tDiff=t1-t0;
        int tSec=period(tDiff);
        int mSec=milliSecond(tDiff);
        float tElapsed=tSec+0.001*mSec;
        DebugTN("Dropping done",tElapsed);
    }
    // ================

        time t0=getCurrentTime();

    dyn_string crateDev;
    dyn_string parentObj=makeDynString(vendor,"","","");

    // NOTE! When creating, the DP_NAME should only contain name, not the full DP!!!
    crateDev[fwDevice_DP_NAME] = crateName;

    crateDev[fwDevice_COMMENT] = "A test Crate";
    crateDev[fwDevice_MODEL] = crateModel;

    string devName;
    fwDevice_getName(crateDev[fwDevice_DP_NAME],devName,exceptionInfo);
    if (dynlen(exceptionInfo)) {DebugTN(exceptionInfo);return;}
    DebugTN("Name of device to create",devName); 

    fwDevice_getDpType(crateType, crateDev[fwDevice_DP_TYPE], exceptionInfo);	

    DebugTN("Create crate",crateDev);

    fwDevice_create(crateDev, parentObj, exceptionInfo);
    if (dynlen(exceptionInfo)) {DebugTN(exceptionInfo);return;}
    crateDev[fwDevice_DP_NAME] = parentObj[fwDevice_DP_NAME] + fwDevice_HIERARCHY_SEPARATOR + crateDev[fwDevice_DP_NAME];

    fwDevice_setAddress(crateDev[fwDevice_DP_NAME], makeDynString(fwDevice_ADDRESS_DEFAULT), exceptionInfo);
    fwDevice_setAlert(crateDev[fwDevice_DP_NAME], fwDevice_ALERT_SET, exceptionInfo);
    if (dynlen(exceptionInfo)) {DebugTN(exceptionInfo);return;}

    DebugTN("Crate created/configured OK");

    dyn_dyn_string modelProperties;
    fwDevice_getModelProperties(crateDev,modelProperties, exceptionInfo);
    if (dynlen(exceptionInfo)) {DebugTN(exceptionInfo);return;}

    int slots = modelProperties[fwDevice_MODEL_SLOTS][1];
    int startingNumber = modelProperties[fwDevice_MODEL_STARTING_NUMBER][1];

    DebugTN("Crate has "+slots+" slots starting at "+startingNumber); 

    // we will loop over free slots, updating with every loop,
    // because a board may take more than one slot
    while(true) {

        dyn_int freeSlots;
        fwDevice_getFreeSlots(crateDev, freeSlots, exceptionInfo);
        if (dynlen(exceptionInfo)) {DebugTN(exceptionInfo);return;}

        if (dynlen(freeSlots)<1) break;

        int boardNum = (freeSlots[1]-1) + startingNumber;

        dyn_string boardDev;
        fwDevice_getDpType(boardType, boardDev[fwDevice_DP_TYPE], exceptionInfo);
        boardDev[fwDevice_MODEL] = boardModel;
        boardDev[fwDevice_COMMENT] = "A test Board";

        string boardName;
        fwDevice_getDefaultName(boardDev, boardNum, boardName, exceptionInfo);
        boardDev[fwDevice_DP_NAME]=boardName;
        DebugTN("Create board",boardDev);

        fwDevice_create(boardDev, crateDev, exceptionInfo);
        if (dynlen(exceptionInfo)) {DebugTN(exceptionInfo);return;}
        boardDev[fwDevice_DP_NAME] = crateDev[fwDevice_DP_NAME] + fwDevice_HIERARCHY_SEPARATOR + boardDev[fwDevice_DP_NAME];

        fwDevice_setAddress(boardDev[fwDevice_DP_NAME], makeDynString(fwDevice_ADDRESS_DEFAULT), exceptionInfo);
        fwDevice_setAlert(boardDev[fwDevice_DP_NAME], fwDevice_ALERT_SET, exceptionInfo);

        dyn_dyn_string bdModelProperties,chnModelProperties;
        fwDevice_getModelProperties(boardDev,bdModelProperties, exceptionInfo);
        if (dynlen(exceptionInfo)) {DebugTN(exceptionInfo);return;}
        int bdslots = bdModelProperties[fwDevice_MODEL_SLOTS][1];
        int bdstartingNumber = bdModelProperties[fwDevice_MODEL_STARTING_NUMBER][1];

        dyn_string channelDev;

        string channelDPT;
        fwDevice_getDpType(channelType, channelDPT, exceptionInfo);
        if (dynlen(exceptionInfo)) {DebugTN(exceptionInfo);return;}
        channelDev[fwDevice_DP_TYPE]=channelDPT;
        channelDev[fwDevice_MODEL] = channelModel;
        channelDev[fwDevice_COMMENT] = "A test Channel";
        // determine width of the channel
        fwDevice_getModelProperties(channelDev,chnModelProperties, exceptionInfo);
        if (dynlen(exceptionInfo)) {DebugTN(exceptionInfo);return;}
        int chnWidth = chnModelProperties[fwDevice_MODEL_WIDTH][1];

        for(int j = bdstartingNumber; j < bdstartingNumber + bdslots; j += chnWidth) {

            string channelName;
            channelDev[fwDevice_DP_NAME]=""; // we need to clear it before calling the next function!

            fwDevice_getDefaultName(channelDev, j, channelName, exceptionInfo);
            if (dynlen(exceptionInfo)) {DebugTN(exceptionInfo);return;}

            channelDev[fwDevice_DP_NAME]=channelName;

            //DebugTN("CREATING",j,channelDev);

            fwDevice_create(channelDev, boardDev, exceptionInfo);
            if (dynlen(exceptionInfo)) {DebugTN(exceptionInfo);return;}

            // modify to full name before it is used by configuration functions...
            channelDev[fwDevice_DP_NAME] = boardDev[fwDevice_DP_NAME] + fwDevice_HIERARCHY_SEPARATOR + channelDev[fwDevice_DP_NAME];

            fwDevice_setAddress(channelDev[fwDevice_DP_NAME], makeDynString(fwDevice_ADDRESS_DEFAULT), exceptionInfo);
            fwDevice_setAlert(channelDev[fwDevice_DP_NAME], fwDevice_ALERT_SET, exceptionInfo);
        }
    }
    time t1=getCurrentTime();
    if (dynlen(exceptionInfo)) {DebugTN(exceptionInfo);return;}
    time tDiff=t1-t0;
    int tSec=period(tDiff);
    int mSec=milliSecond(tDiff);
    float tElapsed=tSec+0.001*mSec;
    DebugTN("OK",tElapsed);
}