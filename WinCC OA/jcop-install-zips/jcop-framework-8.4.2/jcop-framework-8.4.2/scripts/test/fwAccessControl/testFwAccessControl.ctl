#uses "fwUnitTestComponentAsserts.ctl"
#uses "fwAccessControl/fwAccessControl.ctc" // The library of code being tested.

/*****************************************************************************

Layout of access rights being tested
------------------------------------

DOMAIN: TEST
DOMAIN2: TEST2, used to test for non-granted access rights
PRIVS: Visualize, Operate, Expert, Admin
GROUPS/Roles:
    --- for standard tests ---
    TestOperators: TEST:Visualize, TEST:Operate
    TestExperts: TEST:Visualize, TEST:Operate, Test:Expert
    TestAdmins: TEST:Visualize, TEST:Admin
    --- for role-inheritance (groups-in-groups):
    TestOperatorRole: TEST:Operate, "SYSTEM:Normal user level"
    TestExpertRole: includes TestOperatorRole and add Test:Expert
USERS
    test1: TestOperators
    test2: TestExperts
    test3: TestOperators+TestAdmins
    test4: TestOperatorRole
    test5: TestExpertRole + TestOperators
    test6: TestExpertRole + TestAdmins

******************************************************************************/


const string TestDomainName="TEST";
const string Test2DomainName="TEST2";

string user1="test1";
dyn_string expectedPrivs1=makeDynString("TEST:Visualize","TEST:Operate");
dyn_string deniedPrivs1=makeDynString("System:Extended user level","TEST:Admin","TEST:Expert","TEST2:Operate");

string user2="test2";
dyn_string expectedPrivs2=makeDynString("TEST:Visualize","TEST:Operate","TEST:Expert");
dyn_string deniedPrivs2=makeDynString("System:Extended user level","TEST:Admin","TEST2:Operate");

string user3="test3";
dyn_string expectedPrivs3=makeDynString("TEST:Visualize","TEST:Operate","TEST:Admin");
dyn_string deniedPrivs3=makeDynString("System:Extended user level","TEST:Expert","TEST2:Operate");

string user4="test4";
dyn_string expectedPrivs4=makeDynString("TEST:Operate","SYSTEM:Normal user level");
dyn_string deniedPrivs4=makeDynString("System:Extended user level","TEST:Admin","TEST:Expert","TEST2:Operate","TEST:Visualize");

string user5="test5";
dyn_string expectedPrivs5=makeDynString("TEST:Operate","SYSTEM:Normal user level","TEST:Expert","TEST:Visualize");
dyn_string deniedPrivs5=makeDynString("System:Extended user level","TEST:Admin", "TEST2:Operate");

string user6="test6";
dyn_string expectedPrivs6=makeDynString("TEST:Operate","SYSTEM:Normal user level","TEST:Expert","TEST:Visualize","TEST:Admin");
dyn_string deniedPrivs6=makeDynString("System:Extended user level","TEST2:Operate");

/**
@brief This routine is optional.
Prepare the appropriate global environment for all of the test cases in this suite.
setupSuite() is called once only, before any of the test cases are called.
*/
void testFwAccessControl_setupSuite()
{
DebugTN("Setting up the test suite");

//	DebugTN("testFwAccessControl_setupSuite");
	setUserId(0,"");
	dyn_string exceptionInfo;

	fwAccessControl_checkAddDomain(TestDomainName,makeDynString("Visualize","Operate","Expert","Admin"),exceptionInfo);
	fwAccessControl_checkAddDomain(Test2DomainName,makeDynString("Visualize","Operate","Expert","Admin"),exceptionInfo);
	fwAccessControl_checkAddGroup("TestOperators",makeDynString("TEST:Visualize","TEST:Operate"),exceptionInfo);
	fwAccessControl_checkAddGroup("TestExperts",  makeDynString("TEST:Visualize","TEST:Operate","TEST:Expert"),exceptionInfo);
	fwAccessControl_checkAddGroup("TestAdmins",   makeDynString("TEST:Visualize","TEST:Admin"),exceptionInfo);
	fwAccessControl_checkAddGroup("TestOperatorRole", makeDynString("TEST:Operate","SYSTEM:Normal user level"),exceptionInfo);
	fwAccessControl_checkAddGroup("TestExpertRole",   makeDynString("TEST:Expert"),exceptionInfo);
	fwAccessControl_setGroupsInGroup("TestExpertRole",makeDynString("TestOperatorRole"),exceptionInfo);

	fwAccessControl_checkAddUser("test1",makeDynString("TestOperators"),exceptionInfo);
	fwAccessControl_checkAddUser("test2",makeDynString("TestExperts"),exceptionInfo);
	fwAccessControl_checkAddUser("test3",makeDynString("TestOperators","TestAdmins"),exceptionInfo);
	fwAccessControl_checkAddUser("test4",makeDynString("TestOperatorRole"),exceptionInfo);
	fwAccessControl_checkAddUser("test5",makeDynString("TestExpertRole","TestOperators"),exceptionInfo);
	fwAccessControl_checkAddUser("test6",makeDynString("TestExpertRole","TestAdmins"),exceptionInfo);

	if (dynlen(exceptionInfo)) { DebugTN("Problem in testFwAccessControl_setupSuite",exceptionInfo);};
}

//-------------------------------------------------------

/**
@brief testExampleComponent_setup() ensures a consistent environment exists prior to calling each test case.
This routine is optional. If you declare one here, it should prepare any appropriate environment
that will be required before running each of the test cases in this suite.

*/
void test_fwAccessControl_setup()
{
    // reset our privs back to root
    setUserId(0,"");
}

//-------------------------------------------------------

/**
@brief Example of a developer's test routine.
A test*.ctl file can contain as many test routines as the developer wants.
*/
void test_fwAccessControl_testIsGrantedSystemVisualize()
{
    dyn_string exceptionInfo;
    bool granted;
    fwAccessControl_isGranted("SYSTEM:Visualize",granted,exceptionInfo);
    assertEmpty(exceptionInfo);
    assertTrue(granted);
}


void test_fwAccessControl_testIsGrantedTestVisualize()
{
    dyn_string exceptionInfo;
    bool granted;
    fwAccessControl_isGranted(TestDomainName+":Visualize",granted,exceptionInfo);
    assertEmpty(exceptionInfo);
    assertFalse(granted);
}

/*
    Parameterized test for fwAccessControl_checkUserPrivilege()
    the checkIfGranted param determines whether the privsToCheck are expected to
    be granted to the user (TRUE) or expected not to be granted.
*/
void checkUserPrivilege_test(string userName, const dyn_string& privsToCheck, bool checkIfGranted)
{
//    DebugTN("###Run "+(checkIfGranted?"allow":"deny")+" test on user "+userName,privsToCheck);

    for (int i=1;i<=dynlen(privsToCheck);i++) {
	bool granted;
	dyn_string exceptionInfo;

	// we will need to split domain and priv name
	dyn_string ds=strsplit(privsToCheck[i],":");
	string domainName=ds[1];
	string privilegeName=ds[2];
	fwAccessControl_checkUserPrivilege(userName, domainName, privilegeName, granted, exceptionInfo);
	assertEmpty(exceptionInfo);

	if (checkIfGranted)
	    assertTrue(granted,"fwAccessControl_CheckUserPrivilege for user "+userName+" did not return TRUE for "+privsToCheck[i]);
	else
	    assertFalse(granted,"fwAccessControl_CheckUserPrivilege for user "+userName+" did not return FALSE for "+privsToCheck[i]);
    }
}


/*
    Parameterized test for fwAccessControl_getUserPrivileges()
    the checkIfGranted param determines whether the privsToCheck are expected to
    be granted to the user (TRUE) or expected not to be granted.
*/
void getUserPrivileges_test(string userName, const dyn_string& privsToCheck, bool checkIfGranted)
{
//    DebugTN("###Run "+(checkIfGranted?"allow":"deny")+" test on user "+userName,privsToCheck);

    dyn_string exceptionInfo;
    dyn_string userPrivs;
    fwAccessControl_getUserPrivileges(userName,makeDynString(),userPrivs,exceptionInfo);
    assertEmpty(exceptionInfo);

    for (int i=1;i<=dynlen(privsToCheck);i++) {
	if (checkIfGranted) {
	    if (!dynContains(userPrivs,privsToCheck[i])) assertTrue(granted,"fwAccessControl_getUserPrivileges for user "+userName+" does not contain "+privsToCheck[i]);
	} else {
	    if (dynContains(userPrivs,privsToCheck[i])) assertFalse(granted,"fwAccessControl_getUserPrivilege for user "+userName+" contains unexpected "+privsToCheck[i]);
	}
    }
}


/*
    Parameterized test for fwAccessControl_isGranted()
    the checkIfGranted param determines whether the privsToCheck are expected to
    be granted to the user (TRUE) or expected not to be granted.
*/
void isGranted_test(string userName, const dyn_string& privsToCheck, bool checkIfGranted)
{

//    DebugTN("###Run "+(checkIfGranted?"allow":"deny")+" test on user "+userName,privsToCheck);


    bool sudo=setUserId(getUserId(userName));
    if (!sudo) assertTrue(false,"Could not become user "+userName+" ; "+(string)getLastError());
    if (getUserName(getUserId())!=userName) assertTrue(false,"Became wrong user; requested:"+userName+", actual:"+getUserName(getUserId()));

    for (int i=1;i<=dynlen(privsToCheck);i++) {
	bool granted;
	dyn_string exceptionInfo;

	fwAccessControl_isGranted(privsToCheck[i],granted,exceptionInfo);
	assertEmpty(exceptionInfo);

	if (checkIfGranted)
	    assertTrue(granted,"fwAccessControl_isGranted for user "+userName+" did not return TRUE for "+privsToCheck[i]);
	else
	    assertFalse(granted,"fwAccessControl_isGranted for user "+userName+" did not return FALSE for "+privsToCheck[i]);
    }

    setUserId(0,""); // return to be root for the next test.
}



void test_fwAccessControl_checkUserPrivilege()
{

    checkUserPrivilege_test(user1, expectedPrivs1, true);
    checkUserPrivilege_test(user1, deniedPrivs1, false);
    checkUserPrivilege_test(user2, expectedPrivs2, true);
    checkUserPrivilege_test(user2, deniedPrivs2, false);
    checkUserPrivilege_test(user3, expectedPrivs3, true);
    checkUserPrivilege_test(user3, deniedPrivs3, false);
    checkUserPrivilege_test(user4, expectedPrivs4, true);
    checkUserPrivilege_test(user4, deniedPrivs4, false);
    checkUserPrivilege_test(user5, expectedPrivs5, true);
    checkUserPrivilege_test(user5, deniedPrivs5, false);
    checkUserPrivilege_test(user6, expectedPrivs6, true);
    checkUserPrivilege_test(user6, deniedPrivs6, false);

}

void test_fwAccessControl_isGranted()
{

    isGranted_test(user1, expectedPrivs1, true);
    isGranted_test(user1, deniedPrivs1, false);
    isGranted_test(user2, expectedPrivs2, true);
    isGranted_test(user2, deniedPrivs2, false);
    isGranted_test(user3, expectedPrivs3, true);
    isGranted_test(user3, deniedPrivs3, false);
    isGranted_test(user4, expectedPrivs4, true);
    isGranted_test(user4, deniedPrivs4, false);
    isGranted_test(user5, expectedPrivs5, true);
    isGranted_test(user5, deniedPrivs5, false);
    isGranted_test(user6, expectedPrivs6, true);
    isGranted_test(user6, deniedPrivs6, false);

}

void test_fwAccessControl_getUserPrivileges()
{

    getUserPrivileges_test(user1, expectedPrivs1, true);
    getUserPrivileges_test(user1, deniedPrivs1, false);
    getUserPrivileges_test(user2, expectedPrivs2, true);
    getUserPrivileges_test(user2, deniedPrivs2, false);
    getUserPrivileges_test(user3, expectedPrivs3, true);
    getUserPrivileges_test(user3, deniedPrivs3, false);
    getUserPrivileges_test(user4, expectedPrivs4, true);
    getUserPrivileges_test(user4, deniedPrivs4, false);
    getUserPrivileges_test(user5, expectedPrivs5, true);
    getUserPrivileges_test(user5, deniedPrivs5, false);
    getUserPrivileges_test(user6, expectedPrivs6, true);
    getUserPrivileges_test(user6, deniedPrivs6, false);

}

//-------------------------------------------------------

/**
@brief This routine is optional.
Called after each test case routine.
*/
void test_fwAccessControl_teardown()
{
//	DebugTN("testFwAccessControl_teardown");
}

//-------------------------------------------------------

/**
@brief This routine is optional.
Called after all test case routines in this file have been run.
*/
void test_fwAccessControl_teardownSuite()
{
    DebugTN("testFwAccessControl_teardownSuite");
    dyn_string exceptionInfo;

    setUserId(0,"");

    fwAccessControl_deleteUser("test1", exceptionInfo);
    fwAccessControl_deleteUser("test2", exceptionInfo);
    fwAccessControl_deleteUser("test3", exceptionInfo);
    fwAccessControl_deleteUser("test4", exceptionInfo);
    fwAccessControl_deleteUser("test5", exceptionInfo);
    fwAccessControl_deleteUser("test6", exceptionInfo);

    fwAccessControl_deleteGroup("TestExpertRole", exceptionInfo);
    fwAccessControl_deleteGroup("TestOperatorRole", exceptionInfo);
    fwAccessControl_deleteGroup("TestAdmins", exceptionInfo);
    fwAccessControl_deleteGroup("TestExperts", exceptionInfo);
    fwAccessControl_deleteGroup("TestOperators", exceptionInfo);

    fwAccessControl_deleteDomain(TestDomainName,exceptionInfo);

    if (dynlen(exceptionInfo)) { DebugTN("Problem in testFwAccessControl_teardownSuite",exceptionInfo);};
}
