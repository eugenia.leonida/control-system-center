//==============================================================
//      HISTORY OF MODIFICATIONS
//
//      ORIGINAL AUTHOR: Bobby
//
//      19-3-2013 G.M.  cosmetic changes
//
//      10-2014   O.H.  - redundancy
//                      - improved processing of commands to PLC
//
//          2016-07-25 saved as 3.14 G.M.
//

#uses "CtrlPv2Admin"
#uses "fwRack/fwRackConnections.ctl"
#uses "fwRack/fwAuxil.ctl"

global string g_fwRACK_MAIN_NODE = "RCA"; 
global string g_fwRACK_PLC_TYPE = "FwRackPowerDistributionPLC";
global dyn_string hardwareConnected;
global dyn_string connectedStatusPLCs, dsEquipmentStatus;
bool DEBUG = false;

mapping PlcStatusConnected_mapping;

//===========================================================
main()
{
  dyn_string PLCConnections = dpNames("*",g_fwRACK_PLC_TYPE);           // this and the next seem identical 
// dyn_string dsRackPLCs = dpNames("*","FwRackPowerDistributionPLC");   // so I comment this out
  dyn_string exceptions;
  string     plcName;
  int i;
  int status;
  
  //DEBUG = true;
  
  if (DEBUG) DebugTN("Adding and initializing global variables and queues -- fwRCA.ctl/main");
  addGlobal("plcOutputSize",INT_VAR); 
  addGlobal("plcBusyMapping",MAPPING_VAR);
  addGlobal("plcQueueResponse",MAPPING_VAR);  // DOES NOT SEEM TO BE USED G.M. 21-3-2013
  addGlobal("dpConnections",DYN_STRING_VAR);  //holds all application dpConnections
  
  LF_CreateDefaultDatatypes();
  
  if (dpExists("fwRCA_Debug")==FALSE) dpCreate("fwRCA_Debug","FwIntParam");
  dpConnect("EH_setDebugFlag",TRUE,"fwRCA_Debug.count");

  
  //First find the existing PLCs
  for (i = 1; i <= dynlen(PLCConnections);i++) {              // I replaced dsRackPLC with PLCConnections,
      fwDevice_getName(PLCConnections[i],plcName,exceptions);   // as they seem to be the same thing.
      plcBusyMapping[plcName + "_BusyFlag"] = 0;                // G.M. 21-3-2013
      dpSetWait(PLCConnections[i] + ".Output.Queue", makeDynAnytype()); // O.H. 10-2014
  }    
  
  
  if (DEBUG) DebugTN("Clearing all PC busy and status flags -- fwRCA.ctl/main");
  for (i = 1; i <= dynlen(PLCConnections); i++) {
      dpSetWait(PLCConnections[i] + ".Status.Busy", TRUE, PLCConnections[i] + ".Status.PLC", -1);
  }
  
  _fwRackConnections_connectDeviceCommands("PLC");
  
  //Connect to all existing PLC connections. Monitor also if new ones are created or existing ones removed  
  if (DEBUG) DebugTN("Connectiong to all power distribution PLC datapoints (also detects new ones created) "+
                     "-- fwRCA.ctl/main");
  status = dpQueryConnectSingle("_fwRackControlApplication_PLCConnections", 
                       TRUE, 
                       "MonitorPLCDps",
                       "SELECT '.Status.PLC:_online.._value','.Command:_online.._value' "+ 
                       "FROM '*' WHERE _DPT = \""+ g_fwRACK_PLC_TYPE +"\"" );    
  if (DEBUG) DebugTN("dpQueryConnectSingle returns "+status);
  if (dpExists("FwRackShowGlobals") == FALSE) dpCreate("FwRackShowGlobals","FwIntParam");
  dpConnect("EH_ShowGlobals",FALSE,"FwRackShowGlobals.count");
 
}
//================
void EH_ShowGlobals(string dpe,int val)
{
  DebugN("SHOW_GLOBALS connectedStatusPLC:"+connectedStatusPLCs);
  DebugN("SHOW_GLOBALS hardwareConnected:"+hardwareConnected);
  DebugN("SHOW_GLOBALS equipment status:"+dsEquipmentStatus);
}
//====================================================================
void LF_CreateDefaultDatatypes() 
{ 
  fwAuxil_createListDptype();      // creates dpType FwList if it does not exist
  fwAuxil_createIntParamDptype();  // creates dpType FwIntParam if it does not exist
  fwAuxil_createParamDptype();     // creates dpType FwParam if it does not exist
  fwAuxil_createDynIntDptype();    // creates dpType FwDynInt if it does not exist
  fwAuxil_createBoolDptype();      // creates dpType FwBoolValue if it does not exist
  
}

//=================================================================
void EH_setDebugFlag(string dpe, int val)
{
  DEBUG = TRUE;
  if (val == 0) DEBUG = FALSE;
}


//=================================================================
//   This callback is triggered when the .Status.PLC or the .Command dpe of a dp of type
//   FwRackPowerDistributionPLC change. The values are not used directly; instead
//   the dp is extracted from val and identified, and relevants dpes are read inside
//   this function.
//
synchronized _fwRackControlApplication_PLCConnections(string ident, dyn_dyn_anytype val)
{
  int    ModbusNumber;
  string dp, 
         IPAddress, 
         Port, 
         Command; 
  bool   Busy;
  int Status_PLC; //G.M. 2013-7-2

  string plcName;
  dyn_string dsExceptions;
  string Pre_fix;
  int i;

  for(i = 2; i<= dynlen(val); i++) {
    dp =   val[i][1];   
    dp = dpSubStr(dp,DPSUB_DP);
    
    DebugN("_fwRackControlApplication_PLCConnections:dp="+dp,dpExists(dp));
    if (dpExists(dp)) {
      dpGet(dp + ".IPAddress",    IPAddress,
            dp + ".Port",         Port,
            dp + ".ModbusNumber", ModbusNumber,
            dp + ".Command",      Command,
            dp + ".Status.PLC",   Status_PLC,   //G.M. 2013-7-2
            dp + ".Status.Busy",  Busy);
            DebugN("Plc = " + dp, "Command = "+Command, "IP="+IPAddress, "Busy="+Busy, "Status_PLC = "+Status_PLC);
      //if ((Status_PLC == 2) || (Status_PLC == 4)) return;  //G.M. 2013-7-2
      
      //string dpPLCNoSystemName;     dp already contain the same value G.M. 21-3-2013                   
      //dpPLCNoSystemName = dpSubStr(dp,DPSUB_DP);
      if (!dpExists("_Rack_" + dp)) dpCreate("_Rack_"+dp,"_PollGroup");            
      dpSet("_Rack_"+dp +".Active",true);
      dpSet("_Rack_"+dp +".PollInterval",500);

      fwDevice_getName(dp,plcName,dsExceptions);
      
      //in case it is a new PLC created after the ctrl script was started add it to the mappings
      if (!mappingHasKey(plcBusyMapping,plcName + "_BusyFlag")) {
           dpSet(dp + ".Status.Busy", TRUE,
                 dp + ".Status.PLC", -1);
           plcBusyMapping[plcName + "_BusyFlag"] = 0;
           dpSetWait(dp + ".Output.Queue", makeDynAnytype()); // O.H. 10-2014
      }
      
      // If there is a command, take the corresponding actions
      if ((Command != "") && (Busy == TRUE)) { 
        switch (Command) {
             case "CONNECT":
                if (dynContains(hardwareConnected,getSystemName() + dp + ".Status.PLC") == 0) {
                    if (DEBUG == TRUE) DebugTN("Connecting PLC: >>" + dp + "<< on address: >>" + 
                                               IPAddress +":" + Port+"<<" + " running on driver: >>" + 
                                               ModbusNumber + "<< -- fwRCA.ctl/_fwRCA_PLCConnections");
                    _fwRackConnections_connect(IPAddress,Port,ModbusNumber,dp);
                }        
                break;        
             case "DISCONNECT":
                if (dynContains(dsEquipmentStatus,dp) > 0) {          
                    dynRemove(dsEquipmentStatus, dynContains(dsEquipmentStatus,dp));
                    //if (DEBUG == TRUE) 
                    DebugTN("BEFORE Disconnecting equipment status: " + dp + " in DISCONNECT");            
                    dpDisconnect("fwRackConnections_readEquipmentStatus", dp+".Tables.EquipmentStatus"); 
                    DebugTN("AFTER Disconnecting equipment status: " + dp + " in DISCONNECT");            
                }
                if (DEBUG) {
                    DebugTN("IN DISCONNECT: ",connectedStatusPLCs);
                    DebugTN("IN DISCONNECT: ",dp);
                }
                if (dynContains(connectedStatusPLCs,dp)> 0) {          
                    dynRemove(connectedStatusPLCs, dynContains(connectedStatusPLCs,dp));
                    //dpDisconnect("_fwRackConnections_updatePLCStatus", dp  + ".Status.PLC",
                    //             "_"+dp+"_Modbus" + ".ConnState", "_Connections.Driver.ManNums");     
                    if (DEBUG) DebugTN("Disconnecting plc update: " + dp +
                                       "-- fwRCA.ctl/_fwRCA_PLCConnections");              
                }         
                _fwRackConnections_disconnect(dp);
                break;
             case "CONFIGURE":
                fwDevice_getName(dp,plcName,dsExceptions);
                //DebugTN("Dyn contains: ");      
                //DebugTN(dsEquipmentStatus);
                //DebugTN(dp);
                if (dpExists(plcName+"_ConfigurationDpList")) { 
                     if (dynContains(dsEquipmentStatus,dp)>0) {          
                          dynRemove(dsEquipmentStatus, dynContains(dsEquipmentStatus,dp));           
                          DebugTN("BEFORE Disconnecting equipment status: " + dp + " in CONFIGURE");            
                          dpDisconnect("fwRackConnections_readEquipmentStatus", dp+".Tables.EquipmentStatus"); 
                          DebugTN("AFTER Disconnecting equipment status: " + dp + " in CONFIGURE");            
                          if (DEBUG)  DebugTN("Disconnecting equipment status: " + dp +
                                              "-- fwRCA.ctl/_fwRCA_PLCConnections");            
                     }
                }
                if (fwRackTool_ReconfigurePLC(plcName,dsExceptions)) {          
                     if(dpExists(plcName+"_ConfigurationDpList")) {                          
                          Pre_fix = "RCA_"+plcName+"_";
                          if ((dynContains(dsEquipmentStatus,dp) == 0) && dpExists(Pre_fix+"FWRack_StatusDpe.list")) {
                               dynAppend(dsEquipmentStatus,dp);
                               DebugN("---->BEFORE Connecting equipment status: " + dp + " in CONFIGURE");            
                               dpConnect ("fwRackConnections_readEquipmentStatus", dp+".Tables.EquipmentStatus");
                               DebugN("---->AFTER Connecting equipment status: " + dp + " in CONFIGURE");            
                               if(DEBUG) DebugTN("Connecting equipment status: " + dp +
                                                 "-- fwRCA.ctl/_fwRCA_PLCConnections");            
                          }
                     }
                }
                break;
             case "REMOVE_CONFIGURATION":
                break;
             case "LOAD_CONFIGURATION":
                break;
             default:
                DebugTN("Unknown command "+Command+
                        "-- fwRCA.ctl/_fwRCA_PLCConnections");
                break;
        }
        //Finished executing PLC command
        if (DEBUG) DebugTN("Setting busy flags to false, new PLC admin commands welcomed for " + dp+
                           " -- fwRCA.ctl/_fwRCA_PLCConnections");
        
        if (Command != "CONNECT") {
              if (Command == "CONFIGURE") dpSet(dp + ".Status.Busy",  FALSE, dp + ".Command", "CONNECT");
              else dpSetWait(dp + ".Status.Busy",  FALSE, dp + ".Command",  ""); 
        } else dpSetWait(dp + ".Status.Busy",  FALSE); // Command == CONNECT

      }
      
      //Connect to PLC status     
     
      if (DEBUG) {
        DebugTN("modbus exists "+ fwInstallationRedu_getReduDp("_"+dp+"_Modbus") + ".ConnState"+ "? " +
                dpExists(fwInstallationRedu_getReduDp("_"+dp+"_Modbus") + ".ConnState")  +" -- fwRCA.ctl/_fwRCA_PLCConnections");
        DebugTN("dynContains(connectedStatusPLCs,dp)? " +dynContains(connectedStatusPLCs,dp)  +
                " -- fwRCA.ctl/_fwRCA_PLCConnections");
        DebugTN("PLC command : " + Command);
      }
      if (dpExists(fwInstallationRedu_getReduDp("_"+dp+"_Modbus") + ".ConnState") && 
         (dynContains(connectedStatusPLCs,dp) == 0) && 
         (Command != "DISCONNECT") && 
         (Command != "") ) {        
            if(DEBUG) DebugTN("Connecting to plc status: "+dp+ " -- fwRCA.ctl/_fwRCA_PLCConnections");
            dynAppend(connectedStatusPLCs,dp);
            if (mappingHasKey(PlcStatusConnected_mapping,dp) == FALSE) {
               DebugN("--------------READY TO DP CONNECT "+dp);
               dpConnect("_fwRackConnections_updatePLCStatus_PLCdp", dp + ".Status.PLC");
               dpConnect("_fwRackConnections_updatePLCStatus_modDP", fwInstallationRedu_getReduDp("_"+dp+"_Modbus") + ".ConnState");
               dpConnect("_fwRackConnections_updatePLCStatus_drvDP", fwInstallationRedu_getReduDp("_Connections") + ".Driver.ManNums");
               PlcStatusConnected_mapping[dp] = "Connected";
            }
            //dpConnect("_fwRackConnections_updatePLCStatus", dp + ".Status.PLC", 
            //          "_"+dp+"_Modbus" + ".ConnState", "_Connections.Driver.ManNums");      
      }
    }  // end of "if dpExists(dp)"
  } // end of for loop
}


