/**@file testFwDevice.ctl  
  
@brief Tests for fwDevice.
  
@par Creation Date
		 17/06/2014   
 
*/
 
#uses "fwUnitTestComponentAsserts.ctl"
#uses "fwDevice/fwDevice.ctl"

/**
* Test that the get hierarchy function corresponds with the expected behaviour, as defined in the documentation.
*
* from the documentation:
* @param deviceDpName			dp name of the device (e.g. CAEN/crate003/board07/channel005)
* @param deviceHierarchy		structure containing the hierarchy from the given device going to its parent, grandparent, ect.
*							Each row in the dyn_dyn_string has the following structure:
*							<ol>
*								<li> device dp name
*								<li> device position as string (keeping trailing 0s)
*								<li> device position as int
*							</ol>
*							So for example, deviceHierarchy[3][1] will contain the device dp name of the grandparent.				
*/
void testFwDevice_getHierarchy_OK() {
  dyn_dyn_string result, exceptionInfo;
  fwDevice_getHierarchy("CAEN/crate003/board07/channel005", result, exceptionInfo);
  
  assertEmpty(exceptionInfo, "Exceptions raised");
  
  assertEqual(4, dynlen(result), "Excepted 4 levels");  
  
  for (int i = 1; i <= dynlen(result); i++) {
      assertEqual(3, dynlen(result[i]), "Excepted 3 entries in dyn_string");
  }  
  
  //level 1
  assertEqual("CAEN/crate003/board07/channel005", result[1][1], "Level 1 DP name wrong");
  assertEqual("005", result[1][2], "Level 1 position string wrong");
  assertEqual("5", result[1][3], "Level 1 position int wrong");
  
  //level 2
  assertEqual("CAEN/crate003/board07", result[2][1], "Level 2 DP name wrong");
  assertEqual("07", result[2][2], "Level 2 position string wrong");
  assertEqual("7", result[2][3], "Level 2 position int wrong");

  //level 3
  assertEqual("CAEN/crate003", result[3][1], "Level 3 DP name wrong");
  assertEqual("003", result[3][2], "Level 3 position string wrong");
  assertEqual("3", result[3][3], "Level 3 position int wrong");
      
  //level 4
  assertEqual("CAEN", result[4][1], "Level 4 DP name wrong");
  assertEqual("", result[4][2], "Level 4 position string wrong");
  assertEqual("", result[4][3], "Level 4 position int wrong"); 
}

/**
* Test for bug reported in JIRA FWCORE-2705: getHierarchy must handle numbers in the datapoint & system names.
*
* @return void
*/
void testFwDevice_getHierarchy_CheckNumbersInNames_OK() {
  dyn_dyn_string result, exceptionInfo;
  //this name was the one reported in JIRA FWCORE-2705
  fwDevice_getHierarchy("CMS_TRACKER_CAEN2:CAEN/CMS_TRACKER_SY1527_2/branchController13/easyCrate0/powerConverter44/channel000.", result, exceptionInfo);
  
  assertEmpty(exceptionInfo, "Exceptions raised");  
  
  assertEqual(6, dynlen(result), "Excepted 6 levels");  
  
  for (int i = 1; i <= dynlen(result); i++) {
      assertEqual(3, dynlen(result[i]), "Excepted 3 entries in dyn_string");
  }  
  
  //level 1
  assertEqual("CMS_TRACKER_CAEN2:CAEN/CMS_TRACKER_SY1527_2/branchController13/easyCrate0/powerConverter44/channel000.", result[1][1], "Level 1 DP name wrong");
  assertEqual("000", result[1][2], "Level 1 position string wrong");
  assertEqual("0", result[1][3], "Level 1 position int wrong");
  
  //level 2
  assertEqual("CMS_TRACKER_CAEN2:CAEN/CMS_TRACKER_SY1527_2/branchController13/easyCrate0/powerConverter44", result[2][1], "Level 2 DP name wrong");
  assertEqual("44", result[2][2], "Level 2 position string wrong");
  assertEqual("44", result[2][3], "Level 2 position int wrong");

  //level 3
  assertEqual("CMS_TRACKER_CAEN2:CAEN/CMS_TRACKER_SY1527_2/branchController13/easyCrate0", result[3][1], "Level 3 DP name wrong");
  assertEqual("0", result[3][2], "Level 3 position string wrong");
  assertEqual("0", result[3][3], "Level 3 position int wrong");
      
  //level 4
  assertEqual("CMS_TRACKER_CAEN2:CAEN/CMS_TRACKER_SY1527_2/branchController13", result[4][1], "Level 4 DP name wrong");
  assertEqual("13", result[4][2], "Level 4 position string wrong");
  assertEqual("13", result[4][3], "Level 4 position int wrong"); 
      
  //level 5
  assertEqual("CMS_TRACKER_CAEN2:CAEN/CMS_TRACKER_SY1527_2", result[5][1], "Level 5 DP name wrong");
  assertEqual("2", result[5][2], "Level 5 position string wrong");
  assertEqual("2", result[5][3], "Level 5 position int wrong"); 

  //level 6
  assertEqual("CMS_TRACKER_CAEN2:CAEN", result[6][1], "Level 6 DP name wrong");
  assertEqual("", result[6][2], "Level 6 position string wrong");
  assertEqual("", result[6][3], "Level 6 position int wrong");  
}

/**
* Test for an empty string being passed into the function.
*
* @return void
*/
void testFwDevice_getHierarchy_EmptyString_Fail() {
  dyn_dyn_string result, exceptionInfo;
  fwDevice_getHierarchy("", result, exceptionInfo);
  assertTrue(dynlen(exceptionInfo) > 0, "Exceptions not raised");
  dyn_dyn_string expectedResult;
  dynAppend(expectedResult, makeDynString(""));
  assertEqual(expectedResult, result);
}
