// The "internal" lib to be loaded dynamically by dynamicLibAutoInit

// to test that the library-scope variables will be visible in functions
float myVersion = 3.14;

// to test global variable visibility
global string g_dlInternalTest = "Test";

// to test function visibility together with the library-scope variable
bool CheckLibInternal()
{
	DebugTN(__FUNCTION__, myVersion, g_dlInternalTest);
	return myVersion == 3.14;
}