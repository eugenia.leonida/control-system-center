// The "internal" lib to be loaded dynamically by dynamicLibAutoInit2

// to test that the library-scope variables will be visible in functions
float myVersion2 = 2.78;

// to test global variable visibility
global string g_dlInternalTest2 = "Test2";

// to test function visibility together with the library-scope variable
bool CheckLibInternal2()
{
	DebugTN(__FUNCTION__, myVersion2, g_dlInternalTest2);
	return myVersion2 == 3.14;
}