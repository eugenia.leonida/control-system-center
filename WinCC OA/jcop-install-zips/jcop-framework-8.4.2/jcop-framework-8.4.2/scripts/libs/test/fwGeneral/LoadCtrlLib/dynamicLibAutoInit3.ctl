// Example of a library that dynamically loads another lib on runtime
// yet this time the initializer function for the constant is not
// the fwGeneral_loadCtrlLib, but another function that calls it

private const bool _initializeMe3 = DoInitializeMe3();

bool DoInitializeMe3()
{
	bool ok = fwGeneral_loadCtrlLib("test/fwGeneral/LoadCtrlLib/dynamicLibInternal3.ctl", true);
}

bool dynamicFunctionTest3()
{
	DebugTN("Hello from " + __FUNCTION__, _initializeMe3);
	return _initializeMe3;
}