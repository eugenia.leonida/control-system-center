// The "internal" lib to be loaded dynamically by dynamicLibAutoInit2

// to test that the library-scope variables will be visible in functions
float myVersion3 = 6.626e-34;

// to test global variable visibility
global string g_dlInternalTest3 = "Test3";

// to test function visibility together with the library-scope variable
bool CheckLibInternal3()
{
	DebugTN(__FUNCTION__, myVersion3, g_dlInternalTest3);
	return myVersion3 == 6.626e-34;
}