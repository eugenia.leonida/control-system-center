#uses "fwGeneral/fwGeneral.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

// this one will trigger the auto-loading of dynamicLibInternal.ctl
// see test_fwGeneral_loadCtrlLib_checkInternalLibLoaded
#uses "test/fwGeneral/LoadCtrlLib/dynamicLibAutoInit.ctl"

// this one is also auto-loaded, yet the initializer function
// is different than fwGeneral_loadCtrlLib()
#uses "test/fwGeneral/LoadCtrlLib/dynamicLibAutoInit3.ctl"

//const int ERR_FUNCTION_NOT_DEFINED=72;// this one is already known to CTRL
const int ERR_VARIABLE_NOT_DEFINED = 73;


void test_fwGeneral_loadCtrlLib_NoExcOnNotFound()
{
	string libName = "SomeNonExistngLib.ctl";
	bool ok;

	try {
		ok = fwGeneral_loadCtrlLib(libName, false, true);
	} catch {
		dyn_errClass exc = getLastException();
		assert(false, "Exception encountered when not expected:" + (string)exc);
	}
	assertEqual(false, ok, "Wrong return code");
}


void test_fwGeneral_loadCtrlLib_ExcOnNotFound()
{
	string libName = "SomeNonExistngLib.ctl";
	bool gotException = false;

	try {
		bool ok = fwGeneral_loadCtrlLib(libName, true, true);
	} catch {
		dyn_errClass exc = getLastException();
		gotException = true;
		string errTxt = getErrorText(exc);
		assertEqual(", fwGeneral_loadCtrlLib: library not found " + libName, errTxt, "Wrong exception encountered");
	} finally {
		if (!gotException) assert(false, "Exception not encountered when expected");
	}
}

void test_fwGeneral_loadCtrlLib_libWithError()
{
	DebugTN("[NOTE]=>" + __FUNCTION__ + ": Syntax error reported in log by fwGeneral.ctl is expected here");
	delay(0, 100);

	string libName = "test/fwGeneral/LoadCtrlLib/dynamicLibWithError.ctl";
	bool gotException = false;

	try {
		bool ok = fwGeneral_loadCtrlLib(libName, true, true);
	} catch {
		dyn_errClass exc = getLastException();
		gotException = true;
		string errTxt = getErrorText(exc);
		assertEqual(", fwGeneral_loadCtrlLib: syntax error in library " + libName, errTxt, "Wrong exception encountered");
	} finally {
		if (!gotException) assert(false, "Exception not encountered when expected");
	}
}



void test_fwGeneral_loadCtrlLib_checkInternalsLibLoaded()
{
	// the library test/fwGeneral/dynamicLibInternal.ctl should
	// have been loaded dynamically through the "test/fwGeneral/dynamicLibAutoInit.ctl"
	// which is stated in the #uses . Check that symbols are available


	try {
		bool ok1 = dynamicFunctionTest1();      // from the dynamicLibAutoInit.ctl
		bool ok2 = CheckLibInternal();          // from the dynamicLibInternal.ctl
		DebugTN("Global1:", g_dlInternalTest);  //from the dynamicLibInternal.ctl
		bool ok3 = dynamicFunctionTest3();      // from the dynamicLibAutoInit3.ctl
		bool ok4 = CheckLibInternal3();         // from the dynamicLibInternal3.ctl
		DebugTN("Global3:", g_dlInternalTest);  // from the dynamicLibInternal3.ctl
	} catch {
		dyn_errClass exc = getLastException();
		switch (getErrorCode(exc)) {
		  case ERR_FUNCTION_NOT_DEFINED:
			assert(false, "Function was not loaded: " + getErrorText(exc));
			break;
		  case ERR_VARIABLE_NOT_DEFINED:
			assert(false, "Variable was not loaded: " + getErrorText(exc));
			break;
		  default:
			assert(false, "Unexpected exception encountered: " + getErrorText(exc));
			break;
		}
	}
}

void test_fwGeneral_loadCtrlLib_nonInit()
{
	// we attempt to call the function not from the lib global init
	// this is refused unless forced

	bool force = false;
	string libName = "test/fwGeneral/LoadCtrlLib/dynamicLibAutoInit2.ctl";

	bool ok;

	try {
		ok = fwGeneral_loadCtrlLib(libName, true, force);
	} catch {
		dyn_errClass exc = getLastException();
		string errTxt = getErrorText(exc);
		assertEqual(", fwGeneral_loadCtrlLib not called from lib init script - this may not work now; refused to load the library " + libName,
					errTxt,
					"Wrong exception encountered");
	}

	// now try to force the loading of this lib
	force = true;
	try {
		ok = fwGeneral_loadCtrlLib(libName, true, force);
	} catch {
		dyn_errClass exc = getLastException();
		string errTxt = getErrorText(exc);
		assert(false, "Unexpected exception encountered while loading the lib:" + (string)exc);
	}

	// there are parts that should be working:
	try {
		bool ok1 = dynamicFunctionTest2();      // from the dynamicLibAutoInit2.ctl
//	bool ok2=CheckLibInternal2();        // from the dynamicLibInternal2.ctl
		DebugTN("Global:", g_dlInternalTest2);  // from the dynamicLibInternal2.ctl
	} catch {
		dyn_errClass exc = getLastException();
		switch (getErrorCode(exc)) {
		  case ERR_FUNCTION_NOT_DEFINED:
			assert(false, "Function was not loaded: " + getErrorText(exc));
			break;
		  case ERR_VARIABLE_NOT_DEFINED:
			assert(false, "Variable was not loaded: " + getErrorText(exc));
			break;
		  default:
			assert(false, "Unexpected exception encountered: " + getErrorText(exc));
			break;
		}
	}

	bool gotException = false;
	// and parts that are currently failing...
	// we expect the "Variable not defined" error caused by the use of "myVersion2" variable
	// which is not in the scope anymore (reason for ETM-1752)
	try {
		bool ok2 = CheckLibInternal2();        // from the dynamicLibInternal2.ctl
	} catch {
		dyn_errClass exc = getLastException();
		gotException = true;
		string errTxt = getErrorText(exc);
		if (getErrorCode(exc) != ERR_VARIABLE_NOT_DEFINED) {
			assert(false, "Unexpected exception encountered: " + errTxt);
		} else {
			assert(strpos(errTxt, "myVersion2") >= 0, "Exception on missing variable does not match: " + errTxt);
		}
	} finally {
		if (!gotException) assert(false, "Did not get an exception when expected.");
	}
}

