#uses "fwGeneral/fwException.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

/** Checks an exception looks like it should.
 *
 * @param ex	(errClass)	IN	The exception to check.
 * @param catalog	(string)	IN	The catalog it is supposed to come from.
 * @param prio	(int)	IN	The priority level it is supposed to have.
 * @param type	(int)	IN	The type it is supposed to have.
 * @param code	(int)	IN	The error code it is supposed to have.
 * @param text	(string)	IN	 is optional with default value
 *   meaning don't check the text of the exception. Otherwise, it
 *   checks the full text of the exception (that is, the message
 *   from the catalog + all three notes given at creation
 *   concatenated by ", ".
 * @return value of type 'void'
 */
void assert_exception_like(errClass ex, string catalog, int prio, int type, int code, string text = "")
{
	assertEqual(catalog, getErrorCatalog(ex), "for catalog entry");
	assertEqual(code, getErrorCode(ex), "for code entry");
	assertEqual(prio, getErrorPriority(ex), "for prio entry");
	assertEqual(type, getErrorType(ex), "for type entry");
	if (text != "")
		assertEqual(text, getErrorText(ex), "for text entry");
}

/** For debug purpose.
 */
int exception_info(errClass ex)
{
	//errClass ex = exs[1];
	DebugTN("Exception", ex);
	DebugTN("Dp name", getErrorDpName(ex));
	DebugTN("user id", getErrorUserId(ex));
	DebugTN("catalog", getErrorCatalog(ex));
	DebugTN("man id", getErrorManId(ex));
	DebugTN("text", getErrorText(ex));
	DebugTN("code", getErrorCode(ex));
	DebugTN("prio", getErrorPriority(ex));
	DebugTN("type", getErrorType(ex));
	DebugTN("stack", getErrorStackTrace(ex));
	return 0;
}

/** Tests it can create built-in exceptions.
 *
 * Covered here:
 * * Built-in exception from default catalog (_errors.cat).
 * * Built-in exception coming from a specific catalog file.
 */
void test_fwException_declare_builtin()
{
	// From default
	errClass index_out_of_range = fwException_declare("-79", BUILTIN_EXCEPTION);

	assert_exception_like(index_out_of_range, "", PRIO_SEVERE, ERR_CONTROL, 79, "Index out of range");

	// From a specific catalog file
	errClass statement_not_reachable = fwException_declare("ctrl-15", BUILTIN_EXCEPTION);
	assert_exception_like(statement_not_reachable, "ctrl", PRIO_SEVERE, ERR_CONTROL, 15, "Statement not reachable: $1");
}

/** Tests it can create custom exceptions.
 *
 * Covered here:
 * * A basic custom exception.
 * * A custom exception whose parent is another custom exception.
 * * A basic custom exception for which the name/hierarchy is directly provided.
 * * A custom exception with a parent for which the name/hierarchy is directly provided.
 */
void test_fwException_declare_custom()
{
	errClass super_duper_exception = fwException_declare("SUPER.DUPER");

	assert_exception_like(super_duper_exception, "fwException", PRIO_SEVERE, ERR_CONTROL, 4, "EX.SUPER.DUPER");

	// With hierarchy
	errClass upper_lower_exception = fwException_declare("UPPER.LOWER", super_duper_exception);
	assert_exception_like(upper_lower_exception, "fwException", PRIO_SEVERE, ERR_CONTROL, 5, "EX.SUPER.DUPER.UPPER.LOWER");

	// With the full name given directly
	errClass direct_exception = fwException_declare("EX.TEST.DIRECT");
	assert_exception_like(direct_exception, "fwException", PRIO_SEVERE, ERR_CONTROL, 6, "EX.TEST.DIRECT");

	errClass direct_with_parent_exception = fwException_declare("EX.TEST.DIRECT.WITH.PARENT", direct_exception);
	assert_exception_like(direct_with_parent_exception, "fwException", PRIO_SEVERE, ERR_CONTROL, 7, "EX.TEST.DIRECT.WITH.PARENT");
}

/** Tests it can create custom exceptions whose parent is a built-in one.
 *
 * Covered here:
 * * A custom exception whose parent is a built-in exception.
 */
void test_fwException_declare_from_builtin()
{
	errClass my_index_out_of_range = fwException_declare("MINE.IOOR", INDEX_OUT_OF_RANGE_EXCEPTION);

	assert_exception_like(my_index_out_of_range, "fwException", PRIO_SEVERE, ERR_CONTROL, 4, "EX.WCCOA.-79.MINE.IOOR");
}

/** Setup function deleting the locally created fwException.cat file.
 *
 */
void test_fwException_declare_setup()
{
	// Clean the catalog file created in the project that contains all custom exceptions
	string catalog_path = getPath(MSG_REL_PATH, "fwException.cat", 0, 1);

	if (catalog_path != "") {
		assertEqual(0, remove(catalog_path), "Could not delete local catalog file: " + catalog_path);
		_fwException_last_read_catalog = (time)0;
	}
}

