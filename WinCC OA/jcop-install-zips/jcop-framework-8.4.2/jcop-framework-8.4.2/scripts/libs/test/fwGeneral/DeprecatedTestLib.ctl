#uses "fwGeneral/fwGeneral.ctl"

// We need to trace the line numbers for the calls to FWDEPRECATED
// so that we may later analyze the stack trace texts

int lineDeprecatedTestFunction, lineParentDeprecatedTestFunction, lineDeprecatedFunctionMultiCall;
int lineDeprecatedExtraDummyLevel, lineDeprecatedTestFunctionNoArguments;

void DeprecatedTestFunction(int i, string s, bool b = true)
{
	lineDeprecatedTestFunction=__LINE__; FWDEPRECATED();
}

void ParentDeprecatedTestFunction()
{
	lineParentDeprecatedTestFunction=__LINE__; DeprecatedTestFunction(123, "A"); 
}

void DeprecatedFunctionMultiCall(int num = 5)
{
	for (int i = 1; i <= num; i++) {
		lineDeprecatedFunctionMultiCall=__LINE__; DeprecatedTestFunction(i, "Test" + i, ((num % 2) == 1) );
	}
}

void DeprecatedExtraDummyLevel(int foo)
{
	for (int i = 1; i <= 5; i++) {
		lineDeprecatedExtraDummyLevel=__LINE__; ParentDeprecatedTestFunction();
	}
}

void DeprecatedTestFunctionNoArguments()
{
	lineDeprecatedTestFunctionNoArguments=__LINE__; FWDEPRECATED();
}
