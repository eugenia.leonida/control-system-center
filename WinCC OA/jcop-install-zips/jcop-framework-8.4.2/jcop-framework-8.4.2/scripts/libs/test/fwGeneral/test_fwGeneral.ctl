#uses "fwUnitTestComponentAsserts.ctl"
#uses "fwGeneral/fwScreenShot.ctl"

/**Cases related to fwScreenShot_checkFileExtension**/
void test_checkFileExtension_addMissingExtension()
{
	string sFileName = "/tmp/test";
	string sExtension = "png";

	string sExpectedFileName = sFileName + "." + sExtension;
	dyn_string exception;

	_fwScreenShot_checkFileExtension( sFileName, sExtension, exception);

	assertEqual(sFileName, sExpectedFileName);
}

void test_checkFileExtension_noExtensionAdded()
{
	string sFileName = "/tmp/test.png";
	string sExtension = "png";

	string sExpectedFileName = sFileName;
	dyn_string exception;

	_fwScreenShot_checkFileExtension( sFileName, sExtension, exception);

	assertEqual(sFileName, sExpectedFileName);
}
