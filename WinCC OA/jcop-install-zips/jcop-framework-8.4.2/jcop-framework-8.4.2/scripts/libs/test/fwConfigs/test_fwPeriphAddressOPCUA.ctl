#uses "fwUnitTestComponentAsserts.ctl"
#uses "fwConfigs/fwPeriphAddressOPCUA.ctl"

/** 
* Tests the OPCUA address functions in fwPeriphAddressOPCUA.ctl.
*/

string     sys;               /*!< The system where the dummy OPCUA server is */
string     server;            /*!< The name of the dummy OPCUA server */
dyn_string subscriptions;     /*!< A list of subscriptions on the dummy OPCUA server */
dyn_int    subscriptionTypes; /*!< The corresponding subscription types */

/**
* Sets up the test suite by creating DPs for the OPCUA Server and OPCUA subscriptions.
* 
* The DPTs _OPCUAServer and _OPCUASubscription must exist.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_setupSuite() {
  sys    = getSystemName();
  server = "testServer" + rand();
  dpCreate("_" + server, "_OPCUAServer");
  subscriptions = makeDynString("sub1"+rand(), "sub2"+rand(), "sub3"+rand());
  subscriptionTypes = makeDynInt(1, 2, 3);
  for (int i = 1; i <= dynlen(subscriptions); i++) {
    dpCreate("_" + subscriptions[i], "_OPCUASubscription");
    dpSetWait("_" + subscriptions[i] + ".Config.SubscriptionType", subscriptionTypes[i]);
  }
  dyn_string subscriptionsWithUnderscore;
  for (int i = 1; i <= dynlen(subscriptions); i++) {
    dynAppend(subscriptionsWithUnderscore, sys + "_" + subscriptions[i]);
  }
  dpSetWait("_" + server + ".Config.Subscriptions", subscriptionsWithUnderscore);
}

/**
* Tears down the test suit by deleting the OPCUA server and subscription DPs.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_teardownSuite() {
  dpDelete("_" + server);
  for (int i = 1; i <= dynlen(subscriptions); i++) {
    dpDelete("_" + subscriptions[i]);
  }
}

/**
* The server that was created in the setup should exist in the list of servers obtained 
* from the fwPeriphAddressOPCUA_getServers(string sys, string server) function.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_getServers_OK() {  
  dyn_string exceptionInfo;
  assertTrue(dynContains(fwPeriphAddressOPCUA_getServers(sys, exceptionInfo), server));
  assertTrue(dynlen(exceptionInfo) == 0, "Exceptions raised");
}

/**
* The server that was created in the setup should exist in the list of servers obtained 
* from the fwPeriphAddressOPCUA_getServers(string sys, string server) function.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_getServers_SystemDoesNotExist_Fail() {  
  dyn_string exceptionInfo;
  assertEqual(makeDynString(), fwPeriphAddressOPCUA_getServers("asdfg345345dfg", exceptionInfo));
}



/**
* The server that was created in the setup should exist in the list of servers obtained 
* from the fwPeriphAddressOPCUA_getServers(string sys, string server) function.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_getServers_BlankSystem_Fail() {  
  dyn_string exceptionInfo;
  assertEqual(makeDynString(), fwPeriphAddressOPCUA_getServers("", exceptionInfo));
  assertFalse(dynlen(exceptionInfo) == 0, "Exceptions not raised");
}



/**
* The subscriptions that were created in the setup should exist for the server
* that was also created in the setup.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_getSubscriptions_OK() {
  dyn_string exceptionInfo;
  assertEqual(subscriptions, fwPeriphAddressOPCUA_getSubscriptions(sys, server, exceptionInfo));
  assertTrue(dynlen(exceptionInfo) == 0, "Exceptions raised");
}

/**
* An exception should be raised if the system is blank and the function should return
* and empty dyn_string.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_getSubscriptions_BlankSystem_Fail() {
  dyn_string exceptionInfo;
  assertEqual(makeDynString(), fwPeriphAddressOPCUA_getSubscriptions("", "", exceptionInfo));
  assertFalse(dynlen(exceptionInfo) == 0, "Exceptions not raised");
}

/**
* An exception should be raised if the server is blank and the function should return
* and empty dyn_string.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_getSubscriptions_BlankServer_Fail() {
  dyn_string exceptionInfo;
  assertEqual(makeDynString(), fwPeriphAddressOPCUA_getSubscriptions(sys, "", exceptionInfo));
  assertFalse(dynlen(exceptionInfo) == 0, "Exceptions not raised");
}

/**
* An exception should be raised if the server does not exist and the function should return
* and empty dyn_string.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_getSubscriptions_ServerDoesNotExist_Fail() {
  dyn_string exceptionInfo;
  assertEqual(makeDynString(), fwPeriphAddressOPCUA_getSubscriptions(sys, "asdsdssf", exceptionInfo));
  assertFalse(dynlen(exceptionInfo) == 0, "Exceptions not raised");
}

/**
* An exception should be raised if the system does not exist and the function should return
* and empty dyn_string.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_getSubscriptions_SystemDoesNotExist_Fail() {
  dyn_string exceptionInfo;
  assertEqual(makeDynString(), fwPeriphAddressOPCUA_getSubscriptions("sys123234234234", "", exceptionInfo));
  assertFalse(dynlen(exceptionInfo) == 0, "Exceptions not raised");
}

/**
* The types of the subscriptions should match the subscriptions created in
* the setup.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_getSubscriptionType_OK() {
  dyn_string exceptionInfo;
  for (int i = 1; i <= dynlen(subscriptionTypes); i++) {
    assertEqual(
      subscriptionTypes[i],
      fwPeriphAddressOPCUA_getSubscriptionType(sys, subscriptions[i], exceptionInfo)
    );   
    assertTrue(dynlen(exceptionInfo) == 0, "Exceptions raised"); 
  }
}

/**
* An exception should be thrown and the function should return -1 if a blank
* system is passed in.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_getSubscriptionType_BlankSystem_Fail() {
  dyn_string exceptionInfo;
  assertEqual(-1, fwPeriphAddressOPCUA_getSubscriptionType("", subscriptions[1], exceptionInfo));  
  assertTrue(dynlen(exceptionInfo) > 0, "Exceptions not raised");   
}

/**
* An exception should be thrown and the function should return -1 if an invalid
* system is passed in.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_getSubscriptionType_SystemDoesNotExist_Fail() {
  dyn_string exceptionInfo;
  assertEqual(-1, fwPeriphAddressOPCUA_getSubscriptionType("adasdasd", subscriptions[1], exceptionInfo));   
  assertTrue(dynlen(exceptionInfo) > 0, "Exceptions not raised"); 
}

/**
* An exception should be thrown and the function should return -1 if a blank
* subscription is passed in.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_getSubscriptionType_BlankSubscription_Fail() {
  dyn_string exceptionInfo;
  assertEqual(-1, fwPeriphAddressOPCUA_getSubscriptionType(sys, "", exceptionInfo));   
  assertTrue(dynlen(exceptionInfo) > 0, "Exceptions not raised"); 
}

/**
* An exception should be thrown and the function should return -1 if an invalid
* subscription is passed in.
*
* @return void
*/
void test_fwPeriphAddressOPCUA_getSubscriptionType_BlankDoesNotExist_Fail() {
  dyn_string exceptionInfo;
  assertEqual(-1, fwPeriphAddressOPCUA_getSubscriptionType(sys, "testest", exceptionInfo));   
  assertTrue(dynlen(exceptionInfo) > 0, "Exceptions not raised"); 
}
