#uses "fwConfigs/fwPeriphAddressS7.ctl"
#uses "fwUnitTestComponentAsserts.ctl"


const dyn_string test_dps=makeDynString("TestS7FwPeriphAddress001",
					"TestS7FwPeriphAddress002",
					"TestS7FwPeriphAddress003",
					"TestS7FwPeriphAddress004");

const string test_pollgroup="TestS7FwPeriphAddressPollGroup";
const string test_s7Connection="TestS7Connection";

// prepares a test S7-address object, with blob-datatype
private dyn_string makeS7Address(string address)
{
    dyn_string dsParameters;

    dsParameters[fwPeriphAddress_TYPE] = fwPeriphAddress_TYPE_S7;
    dsParameters[fwPeriphAddress_DRIVER_NUMBER] = 1;
    dsParameters[fwPeriphAddress_REFERENCE]=address;
    dsParameters[fwPeriphAddress_DIRECTION]=DPATTR_ADDR_MODE_INPUT_SPONT;
    dsParameters[fwPeriphAddress_DATATYPE]=fwPeriphAddress_S7_TYPE_BLOB;
    dsParameters[fwPeriphAddress_ACTIVE]="TRUE";

    dsParameters[fwPeriphAddress_S7_LOWLEVEL]="FALSE";
    dsParameters[fwPeriphAddress_S7_SUBINDEX]="0";
    dsParameters[fwPeriphAddress_S7_START]=(time)0;
    dsParameters[fwPeriphAddress_S7_INTERVAL]=(time)0;
    dsParameters[fwPeriphAddress_S7_POLL_GROUP]="";

    return dsParameters;
}


private string getErrorString(int s7ErrCode)
{
    // convert a integer error code to a 5-digit string
    // as needed by getCatStr()
    string errCodeAsString;
    sprintf(errCodeAsString,"%05d",s7ErrCode);

    string errString=getCatStr("fwPeriphAddressS7",errCodeAsString);
    return errString;
}



void test_fwPeriphAddressS7_setupSuite() 
{

    string pollGroupDp="_"+test_pollgroup;
    string s7ConnectionDp="_"+test_s7Connection;

    test_fwPeriphAddressS7_teardownSuite(); // cleanup

    delay(0,100);

    for (int i=1;i<=dynlen(test_dps);i++) dpCreate(test_dps[i], "ExampleDP_Int");
    dpCreate(pollGroupDp,"_PollGroup");
    dpCreate(s7ConnectionDp,"_S7_Conn");

    delay(0,100);
    dpSetWait(s7ConnectionDp+".DevNr",1);

}

void test_fwPeriphAddressS7_teardownSuite() 
{

    delay(0,100);
    string pollGroupDp="_"+test_pollgroup;
    string s7ConnectionDp="_"+test_s7Connection;

    for (int i=1;i<=dynlen(test_dps);i++) if (dpExists(test_dps[i])) dpDelete(test_dps[i]);
    if (dpExists(pollGroupDp)) dpDelete(pollGroupDp);
    if (dpExists(s7ConnectionDp)) dpDelete(s7ConnectionDp);

}






/**
    Note: if addr is not passed, then we assume that the error text does not contain the phrase "; while checking MY:ADDRESS:SOMETHING",
    otherwise we match it
*/
void assertS7Error(int errCode,dyn_string exceptionInfo, string param="",  string addr="", string errSeverity="ERROR")
{
    // until Paul fixes the unit-testing framework's assertError() we need this one:
    if (dynlen(exceptionInfo) <3) {
	assert(false,"No exceptionInfo while expecting to have one");
	return;
    }

    string exceptionText=getErrorString(errCode);
    string errString="fwPeriphAddress_checkS7Parameters: "+exceptionText;

    if (param!="") errString+=", "+param;
    if (addr!="") errString+="; while checking "+addr;
    assertError(errSeverity,errString,exceptionInfo);
}


void test_fwPeriphAddressS7_parseInt_OK()
{
    string testString="123";
    int expectedResult=123;
    string exceptionText;
    int res=0;
    try {
	res = fwPeriphAddressS7_parseInt(testString);
    } catch {
	exceptionText=getErrorText(getLastException());
    } finally {
	assertEqual("",exceptionText,"[Exception]");
	assertEqual(expectedResult,res,"[Wrong result]");
    }
}

void test_fwPeriphAddressS7_parseInt_WrongChars_Fail()
{
    string testString="a123";
    string exceptionText;
    try {
	fwPeriphAddressS7_parseInt(testString,EXC_S7PARSEINT);
    } catch {
	exceptionText=getErrorText(getLastException());
    } finally {
	assertEqual(getErrorString(EXC_S7PARSEINT)+", parseInt wrong format, a123",exceptionText);
    }
}


void test_fwPeriphAddressS7_parseInt_EmptyString_Fail()
{
    string testString="";
    string exceptionText;
    try {
	fwPeriphAddressS7_parseInt(testString,EXC_S7PARSEINT);
    } catch {
	exceptionText=getErrorText(getLastException());
    } finally {
	assertEqual(getErrorString(EXC_S7PARSEINT)+", parseInt empty string",exceptionText);
    }
}

void test_fwPeriphAddressS7_parseInt_Substr_OK()
{
    string testString="qwerty123asdf";
    int expectedResult=123;
    string exceptionText;
    int res=0;
    try {
	res = fwPeriphAddressS7_parseInt(testString,EXC_S7PARSEINT,6,3);
    } catch {
	exceptionText=getErrorText(getLastException());
    } finally {
	assertEqual("",exceptionText,"[Exception]");
	assertEqual(expectedResult,res,"[Wrong result]");
    }
}

void test_fwPeriphAddressS7_parseInt_SubstrStartPosNoLength_OK()
{
    string testString="qwerty123";
    int expectedResult=123;
    string exceptionText;
    int res=0;
    try {
	res = fwPeriphAddressS7_parseInt(testString,EXC_S7PARSEINT,6);
    } catch {
	exceptionText=getErrorText(getLastException());
    } finally {
	assertEqual("",exceptionText,"[Exception]");
	assertEqual(expectedResult,res,"[Wrong result]");
    }
}

void test_fwPeriphAddressS7_parseInt_SubstrFromTheEnd_OK()
{
    string testString="qwerty123asdf";
    int expectedResult=123;
    string exceptionText;
    int res=0;
    try {
	res = fwPeriphAddressS7_parseInt(testString,EXC_S7PARSEINT,-7,3);
    } catch {
	exceptionText=getErrorText(getLastException());
    } finally {
	assertEqual("",exceptionText,"[Exception]");
	assertEqual(expectedResult,res,"[Wrong result]");
    }
}
void test_fwPeriphAddressS7_parseInt_SubstrFromTheEndNoLength_OK()
{
    string testString="qwerty123";
    int expectedResult=123;
    string exceptionText;
    int res=0;
    try {
	res = fwPeriphAddressS7_parseInt(testString,EXC_S7PARSEINT,-3);
    } catch {
	exceptionText=getErrorText(getLastException());
    } finally {
	assertEqual("",exceptionText,"[Exception]");
	assertEqual(expectedResult,res,"[Wrong result]");
    }
}

void test_fwPeriphAddressS7_parseInt_WrongLength_Fail()
{
    string testString="a123";
    string exceptionText;
    try {
	fwPeriphAddressS7_parseInt(testString,EXC_S7PARSEINT,0,-5);
    } catch {
	exceptionText=getErrorText(getLastException());
    } finally {
	assertEqual(getErrorString(EXC_S7PARSEINT)+", parseInt wrong string length, -5",exceptionText);
    }
}


void test_fwPeriphAddressS7_parseInt_StartPosTooLarge_Fail()
{
    string testString="a123";
    string exceptionText;
    try {
	fwPeriphAddressS7_parseInt(testString,EXC_S7PARSEINT,4,0);
    } catch {
	exceptionText=getErrorText(getLastException());
    } finally {
	assertEqual(getErrorString(EXC_S7PARSEINT)+", parseInt wrong startPos/length, a123:4/4",exceptionText);
    }
}

void test_fwPeriphAddressS7_parseInt_NegativeStartPosTooLarge_Fail()
{
    string testString="a123";
    string exceptionText;
    try {
	fwPeriphAddressS7_parseInt(testString,EXC_S7PARSEINT,-5,0);
    } catch {
	exceptionText=getErrorText(getLastException());
    } finally {
	assertEqual(getErrorString(EXC_S7PARSEINT)+", parseInt wrong startPos/length, a123:-1/4",exceptionText);
    }
}


void test_fwPeriphAddressS7_checkS7Parameters_ParameterNumberIncorrect_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address("");
    dynRemove(dsParameters,dynlen(dsParameters));

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_WRONG_NUM_PARAM,exceptionInfo, (string)(FW_PARAMETER_FIELD_NUMBER-1));

}

void test_fwPeriphAddressS7_checkS7Parameters_AddressTypeIncorrect_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address("");
    dsParameters[FW_PARAMETER_FIELD_COMMUNICATION]="TEST";

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_WRONG_ADDRTYPE,exceptionInfo, dsParameters[FW_PARAMETER_FIELD_COMMUNICATION]);

}

void test_fwPeriphAddressS7_checkS7Parameters_DriverNumNotNumber_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address("");
    dsParameters[FW_PARAMETER_FIELD_DRIVER]="a5a";

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_WRONG_DRIVER_NUMBER,exceptionInfo, "parseInt wrong format, "+dsParameters[FW_PARAMETER_FIELD_DRIVER]);

}

void test_fwPeriphAddressS7_checkS7Parameters_DriverNumInvalid_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address("");
    dsParameters[FW_PARAMETER_FIELD_DRIVER]="500";

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_WRONG_DRIVER_NUMBER,exceptionInfo, dsParameters[FW_PARAMETER_FIELD_DRIVER]);

}


void test_fwPeriphAddressS7_checkS7Parameters_AddressEmpty_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address("");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_EMPTY,exceptionInfo);

}


void test_fwPeriphAddressS7_checkS7Parameters_AddressTooManyColons_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address("TEST:ONE:TWO:THREE");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_TOO_MANY_COLONS,exceptionInfo, "",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}



void test_fwPeriphAddressS7_checkS7Parameters_DataLenNotNumber_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address("G1.G2.G3:aa123");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertS7Error(EXC_S7ADDR_BAD_DATALEN,exceptionInfo, "parseInt wrong format, aa123",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}


void test_fwPeriphAddressS7_checkS7Parameters_WrongDataLen_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address("G1.G2.G3:100000");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertS7Error(EXC_S7ADDR_BAD_DATALEN,exceptionInfo, "100000",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}


void test_fwPeriphAddressS7_checkS7Parameters_BadNumOfGroups_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address("G1.G2.G3.G4.G5");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_BAD_NUM_OF_GROUPS,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);


}




void test_fwPeriphAddressS7_checkS7Parameters_DataTypeNotNumber_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBD15F");
    dsParameters[fwPeriphAddress_DATATYPE]="Qwerty";

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_WRONG_DATA_TYPE,exceptionInfo, "parseInt wrong format, "+dsParameters[FW_PARAMETER_FIELD_DATATYPE],dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}

void test_fwPeriphAddressS7_checkS7Parameters_DataTypeWrong_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBD15F");
    dsParameters[fwPeriphAddress_DATATYPE]=fwPeriphAddress_S7_TYPE_MAX+10;

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_WRONG_DATA_TYPE,exceptionInfo, dsParameters[FW_PARAMETER_FIELD_DATATYPE],dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}


void test_fwPeriphAddressS7_checkS7Parameters_FieldActiveNotBoolean_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBD15F");
    dsParameters[FW_PARAMETER_FIELD_ACTIVE]="Test";

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_WRONG_ACTIVE,exceptionInfo, dsParameters[FW_PARAMETER_FIELD_ACTIVE],dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}

void test_fwPeriphAddressS7_checkS7Parameters_FieldLowLevelNotBoolean_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBD15F");
    dsParameters[FW_PARAMETER_FIELD_LOWLEVEL]="Test";

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_WRONG_LOWLEVEL,exceptionInfo, dsParameters[FW_PARAMETER_FIELD_LOWLEVEL],dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}



void test_fwPeriphAddressS7_checkS7Parameters_TestGroup4_EmptyZ_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBX15.");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_DBX_BADZBIT,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}

void test_fwPeriphAddressS7_checkS7Parameters_TestGroup4_BadZ_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBX15.9");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_DBX_BADZBIT,exceptionInfo, "",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}

void test_fwPeriphAddressS7_checkS7Parameters_TestGroup4_BadDBX_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBA15.7");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertS7Error(EXC_S7ADDR_BAD_NUM_OF_GROUPS,exceptionInfo, "",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}

void test_fwPeriphAddressS7_checkS7Parameters_TestGroup4_DBXNoNumber_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBXa15.7");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertS7Error(EXC_S7ADDR_DBX_BADYNUMBER,exceptionInfo, "parseInt wrong format, a15",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}

void test_fwPeriphAddressS7_checkS7Parameters_TestGroup4_DBXEmpty_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBX.7");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertS7Error(EXC_S7ADDR_DBX_BADYNUMBER,exceptionInfo, "",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}



void test_fwPeriphAddressS7_checkS7Parameters_TestGroup4_DBXWrongNumber_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBX100000.7");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertS7Error(EXC_S7ADDR_DBX_BADYNUMBER,exceptionInfo, "",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}

void test_fwPeriphAddressS7_checkS7Parameters_TestGroup4_DBWrong_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".PB13.DBX1.7");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertS7Error(EXC_S7ADDR_BAD_NUM_OF_GROUPS,exceptionInfo, "",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}

void test_fwPeriphAddressS7_checkS7Parameters_TestGroup4_DBWithExtraTokens_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".AB13.DBX1.7");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertS7Error(EXC_S7ADDR_BAD_NUM_OF_GROUPS,exceptionInfo, "",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}


void test_fwPeriphAddressS7_checkS7Parameters_TestGroup4_DBNoNumber_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DBa13.DBX1.7");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertS7Error(EXC_S7ADDR_DBX_BADXNUMBER,exceptionInfo, "parseInt wrong format, a13",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}

void test_fwPeriphAddressS7_checkS7Parameters_TestGroup4_DBWrongNumber_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB100000.DBX1.7");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertS7Error(EXC_S7ADDR_DBX_BADXNUMBER,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}



void test_fwPeriphAddressS7_checkS7Parameters_TestGroup4_OK()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB10.DBX1.7");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_TestGroup3_OK()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB10.DBW1");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");

}



void test_fwPeriphAddressS7_checkS7Parameters_TestGroup3_DBWrong_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".AB13.DBW1");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertS7Error(EXC_S7ADDR_DBX_BADDBX,exceptionInfo, "",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}


void test_fwPeriphAddressS7_checkS7Parameters_TestGroup3_DBNoNumber_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DBa13.DBW1");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertS7Error(EXC_S7ADDR_DBX_BADXNUMBER,exceptionInfo, "parseInt wrong format, a13",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}

void test_fwPeriphAddressS7_checkS7Parameters_TestGroup3_DBWrongNumber_Fail()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB100000.DBW1");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);
    assertS7Error(EXC_S7ADDR_DBX_BADXNUMBER,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}


void test_fwPeriphAddressS7_checkS7Parameters_T_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".T5");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}

void test_fwPeriphAddressS7_checkS7Parameters_Z_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".Z7");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}

void test_fwPeriphAddressS7_checkS7Parameters_TNotNumber_Fail()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".Taaa32");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_DBX_BADTZNUMBER,exceptionInfo,"parseInt wrong format, aaa32",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);
}


void test_fwPeriphAddressS7_checkS7Parameters_T_with_Z_Fail()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".T3.5");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_BAD_NUM_OF_GROUPS,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}


void test_fwPeriphAddressS7_checkS7Parameters_WrongSingleLetter_Fail()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".V32");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_DBX_BADDBXY,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);
}

void test_fwPeriphAddressS7_checkS7Parameters_WrongSingleLetterWithZ_Fail()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".V32.4");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_DBX_BADDBXY,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);
}



void test_fwPeriphAddressS7_checkS7Parameters_DBx_DBXy_z_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB1.DBX2.3");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_DBx_DBXy_MissingZ_Fail()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB1.DBX2");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_DBX_BADZBIT,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}

void test_fwPeriphAddressS7_checkS7Parameters_My_z_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".M5.3");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}

void test_fwPeriphAddressS7_checkS7Parameters_My_z_Missing_y_Missing_z_Fail()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".M");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_DBX_BADDBXY,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}


void test_fwPeriphAddressS7_checkS7Parameters_My_z_Missing_y_Fail()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".M.3");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_DBX_BADDBXY,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}


void test_fwPeriphAddressS7_checkS7Parameters_My_z_Missing_Z_Fail()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".M5");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_DBX_BADZBIT,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}


void test_fwPeriphAddressS7_checkS7Parameters_Ey_z_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".E5.3");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}

void test_fwPeriphAddressS7_checkS7Parameters_Iy_z_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".E5.3");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_Ay_z_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".E5.3");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}

void test_fwPeriphAddressS7_checkS7Parameters_Qy_z_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".E5.3");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}

void test_fwPeriphAddressS7_checkS7Parameters_DBx_DBBy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB1.DBB2");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}

void test_fwPeriphAddressS7_checkS7Parameters_DBx_DBBy_WithZ_Fail()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB1.DBB2.5");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_BAD_NUM_OF_GROUPS,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}

void test_fwPeriphAddressS7_checkS7Parameters_DBx_DBWy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB1.DBW2");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}

void test_fwPeriphAddressS7_checkS7Parameters_DBx_DBWy_WithZ_Fail()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB1.DBW2.5");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_BAD_NUM_OF_GROUPS,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}


void test_fwPeriphAddressS7_checkS7Parameters_DBx_DBDy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB1.DBD2");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}

void test_fwPeriphAddressS7_checkS7Parameters_MBy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".MB21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_MWy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".MW21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_MDy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".MD21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}




void test_fwPeriphAddressS7_checkS7Parameters_EBy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".EB21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_EWy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".EW21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_EDy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".ED21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}



void test_fwPeriphAddressS7_checkS7Parameters_IBy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".IB21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_IWy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".IW21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_IDy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".ID21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}



void test_fwPeriphAddressS7_checkS7Parameters_ABy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".AB21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_AWy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".AW21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_ADy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".AD21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}




void test_fwPeriphAddressS7_checkS7Parameters_QBy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".QB21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_QWy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".QW21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_QDy_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".QD21");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}






void test_fwPeriphAddressS7_checkS7Parameters_DBx_DBDyF_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB1.DBD23F");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}

void test_fwPeriphAddressS7_checkS7Parameters_MDyF_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".MD21F");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

}


void test_fwPeriphAddressS7_checkS7Parameters_EDyF_Fail()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".ED21F");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_DBX_FNOTALLOWED,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}


void test_fwPeriphAddressS7_checkS7Parameters_DBx_DBXyF_z_Fail()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB15.DBX3F.3");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_DBX_FNOTALLOWED,exceptionInfo,"",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}



// some ad-hoc tests

void test_fwPeriphAddressS7_checkS7Parameters_FloatDataComponent()
{
    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBD15F");
    dsParameters[fwPeriphAddress_DATATYPE]=fwPeriphAddress_S7_TYPE_FLOAT;

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");
}

void test_fwPeriphAddressS7_checkS7Parameters_ByteDataComponent()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBB56");

    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");
}

void test_fwPeriphAddressS7_checkS7Parameters_ByteDataComponent_WithInvalidIndex_Fail()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address("TEST.DB498.DBB512:AAA200");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertS7Error(EXC_S7ADDR_BAD_DATALEN,exceptionInfo, "parseInt wrong format, AAA200",dsParameters[FW_PARAMETER_FIELD_ADDRESS]);

}

void test_fwPeriphAddressS7_checkS7Parameters_ByteDataComponent_WithIndex_OK()
{
    dyn_string exceptionInfo;
    dyn_string dsParameters=makeS7Address("TEST.DB498.DBB512:200");
    fwPeriphAddress_checkS7Parameters(dsParameters, exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");
}


void test_fwPeriphAddressS7_set_PollGroupNoSysNoUnderscore()
{

    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBD15F");

    dsParameters[fwPeriphAddress_S7_POLL_GROUP] = test_pollgroup;
    dsParameters[FW_PARAMETER_FIELD_MODE]       = DPATTR_ADDR_MODE_INPUT_POLL;

    string test_dp=test_dps[1];

    _fwPeriphAddressS7_set(getSystemName()+test_dp+".",dsParameters, exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");

    // now retrieve back and check if poll group is the same...
    dyn_string dsParameters2=dsParameters;
    // note that on return the poll group is expanded, with system name and underscore
    dsParameters[fwPeriphAddress_S7_POLL_GROUP] = getSystemName()+"_"+test_pollgroup;

    bool isActive;
    _fwPeriphAddressS7_get(getSystemName()+test_dp+".",dsParameters2, isActive,exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");

    // note: in the comparison we do not care about the START and INVERVAL, as these are only for compatibility reasons
    // (replaced by the functionality of POLL GROUPS)
    for (int i=1;i<=dynlen(dsParameters);i++)  {
        if (i==fwPeriphAddress_S7_START || i==fwPeriphAddress_S7_INTERVAL) continue;
	assertEqual(dsParameters[i],dsParameters2[i],"Mismatch of value at index "+i);
    }


}

void test_fwPeriphAddressS7_set_PollGroupNoSysUnderscore()
{

    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBD15F");

    dsParameters[fwPeriphAddress_S7_POLL_GROUP] = "_"+test_pollgroup;
    dsParameters[FW_PARAMETER_FIELD_MODE]       = DPATTR_ADDR_MODE_INPUT_POLL;

    string test_dp=test_dps[2];

    _fwPeriphAddressS7_set(getSystemName()+test_dp+".",dsParameters, exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");

    // now retrieve back and check if poll group is the same...
    dyn_string dsParameters2=dsParameters;
    // note that on return the poll group is expanded, with system name and underscore
    dsParameters[fwPeriphAddress_S7_POLL_GROUP] = getSystemName()+"_"+test_pollgroup;

    bool isActive;
    _fwPeriphAddressS7_get(getSystemName()+test_dp+".",dsParameters2, isActive,exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");

    // note: in the comparison we do not care about the START and INVERVAL, as these are only for compatibility reasons
    // (replaced by the functionality of POLL GROUPS)
    for (int i=1;i<=dynlen(dsParameters);i++)  {
        if (i==fwPeriphAddress_S7_START || i==fwPeriphAddress_S7_INTERVAL) continue;
	assertEqual(dsParameters[i],dsParameters2[i],"Mismatch of value at index "+i);
    }


}

void test_fwPeriphAddressS7_set_PollGroupRemoteSysUnderscore()
{

    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBD15F");

    dsParameters[fwPeriphAddress_S7_POLL_GROUP] = "SomeSystem:"+"_"+test_pollgroup;
    dsParameters[FW_PARAMETER_FIELD_MODE]       = DPATTR_ADDR_MODE_INPUT_POLL;

    string test_dp=test_dps[3];

    _fwPeriphAddressS7_set(getSystemName()+test_dp+".",dsParameters, exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");

    // now retrieve back and check if poll group is the same...
    dyn_string dsParameters2=dsParameters;
    // note that on return the poll group is expanded, with system name and underscore
    dsParameters[fwPeriphAddress_S7_POLL_GROUP] = getSystemName()+"_"+test_pollgroup;

    bool isActive;
    _fwPeriphAddressS7_get(getSystemName()+test_dp+".",dsParameters2, isActive,exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");

    // note: in the comparison we do not care about the START and INVERVAL, as these are only for compatibility reasons
    // (replaced by the functionality of POLL GROUPS)
    for (int i=1;i<=dynlen(dsParameters);i++)  {
        if (i==fwPeriphAddress_S7_START || i==fwPeriphAddress_S7_INTERVAL) continue;
	assertEqual(dsParameters[i],dsParameters2[i],"Mismatch of value at index "+i);
    }


}


void test_fwPeriphAddressS7_set_PollGroupMySysUnderscore()
{

    dyn_string exceptionInfo;

    dyn_string dsParameters=makeS7Address(test_s7Connection+".DB13.DBD15F");

    dsParameters[fwPeriphAddress_S7_POLL_GROUP] = getSystemName()+"_"+test_pollgroup;
    dsParameters[FW_PARAMETER_FIELD_MODE]       = DPATTR_ADDR_MODE_INPUT_POLL;

    string test_dp=test_dps[4];

    _fwPeriphAddressS7_set(getSystemName()+test_dp+".",dsParameters, exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");

    // now retrieve back and check if poll group is the same...
    dyn_string dsParameters2=dsParameters;
    // note that on return the poll group is expanded, with system name and underscore
    dsParameters[fwPeriphAddress_S7_POLL_GROUP] = getSystemName()+"_"+test_pollgroup;

    bool isActive;
    _fwPeriphAddressS7_get(getSystemName()+test_dp+".",dsParameters2, isActive,exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");

    // note: in the comparison we do not care about the START and INVERVAL, as these are only for compatibility reasons
    // (replaced by the functionality of POLL GROUPS)
    for (int i=1;i<=dynlen(dsParameters);i++)  {
        if (i==fwPeriphAddress_S7_START || i==fwPeriphAddress_S7_INTERVAL) continue;
	assertEqual(dsParameters[i],dsParameters2[i],"Mismatch of value at index "+i);
    }

}

