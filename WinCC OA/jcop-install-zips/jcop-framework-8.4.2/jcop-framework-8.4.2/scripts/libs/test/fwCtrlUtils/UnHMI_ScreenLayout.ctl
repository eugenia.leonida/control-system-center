
class UnHMI_Window
{
  private int number;
  private string fileName;
  private string panelName;
  private bool openOnStartup;

  public UnHMI_Window(int number, string fileName, string panelName, bool openOnStartup = true)
  {
    this.number = number;
    this.fileName = fileName;
    this.panelName= panelName;
    this.openOnStartup = openOnStartup;
  }

  public int getNumber() {
    return this.number;
  }
  public string getFileName()
  {
    return this.fileName;
  }

  public void setFileName(string fileName)
  {
    this.fileName = fileName;
  }

  public string getPanelName()
  {
    return this.panelName;
  }

  public void setPanelName(string panelName)
  {
    this.panelName = panelName;
  }

  public bool getOpenOnStartup()
  {
    return this.openOnStartup;
  }

  public void setOpenOnStartup(bool openOnStartup)
  {
    this.openOnStartup = openOnStartup;
  }

  public bool equals(UnHMI_Window win){
    bool equals = this.fileName == win.getFileName();
    equals = equals && (this.panelName == win.getPanelName());
    equals = equals && (this.number == win.getNumber());
    equals = equals && (this.openOnStartup== win.getOpenOnStartup());
    return equals;
  }

};

class UnHMI_ScreenLayout
{

  private vector< shared_ptr<UnHMI_Window> > windows;
  private string screenLayoutName;
  //------------------------------------------------------------------------------
  /** Default constructor.
  */
  public UnHMI_ScreenLayout(string screenLayoutName)
  {
    this.screenLayoutName = screenLayoutName;
  }

  public string getScreenLayoutName()
  {
    return this.screenLayoutName;
  }

  public vector< shared_ptr<UnHMI_Window> > getWindows()
  {
    return this.windows;
  }

  public void setWindows(vector< shared_ptr<UnHMI_Window> > windows)
  {
    this.windows = windows;
  }

  public bool equals(UnHMI_ScreenLayout sl) {
    bool equals = this.screenLayoutName == sl.getScreenLayoutName();
    vector< shared_ptr<UnHMI_Window> > slWindows = sl.getWindows();
    if (windows.count() != slWindows.count()) {
      return false;
    }
    for (int i=0; i<windows.count(); i++) {
     shared_ptr<UnHMI_Window> tempPtr = slWindows.at(i);
     equals = equals && (tempPtr.equals(windows.at(i)));
    }
    return equals;
  }

};

class UnHMI_ScreenLayoutHelper
{
  public vector< shared_ptr<UnHMI_ScreenLayout> > screenLayouts;
  const string DPE_SCREENLAYOUT = "_unApplication.screenLayout";

  private static shared_ptr<UnHMI_ScreenLayoutHelper> singleton_instance = nullptr;
  private UnHMI_ScreenLayoutHelper() {/*private constructor - Use getInstance - Singleton pattern */}

  public static shared_ptr<UnHMI_ScreenLayoutHelper>  getInstance()
  {
    if(equalPtr(singleton_instance, nullptr))
    {
      singleton_instance = new UnHMI_ScreenLayoutHelper();
    }
    return singleton_instance;
  }

  public dyn_string getScreenLayoutNames()
  {
    dyn_string screenLayoutNames;

    for (int i=0; i< screenLayouts.count(); i++)
    {
      shared_ptr<UnHMI_ScreenLayout> tempPtr = screenLayouts.at(i);
      screenLayoutNames.append(tempPtr.getScreenLayoutName());
    }
    DebugTN("names", screenLayoutNames);
    return screenLayoutNames;
  }

  public shared_ptr<UnHMI_ScreenLayout> getScreenLayout(string screenLayoutName)
  {
    shared_ptr<UnHMI_ScreenLayout> retScreenLayout;

    // ScreenLayoutName is used as a key, so no duplicates exists
    vector<int> idx = screenLayouts.indexListOf("screenLayoutName", screenLayoutName);

    if (idx.isEmpty()) {
      retScreenLayout = nullptr;
    } else {
      retScreenLayout = screenLayouts.at(idx.first());
    }
    return retScreenLayout;
  }

  public void setScreenLayout(shared_ptr<UnHMI_ScreenLayout> sl)
  {
    vector<int> idx = screenLayouts.indexListOf("screenLayoutName", sl.getScreenLayoutName());

    // Check if already exists
    if (!idx.isEmpty())
    {
      // Overwrite it if it does
      screenLayouts.replaceAt(idx.first(), sl);
    } else {
      // Otherwise append
      screenLayouts.append(sl);
    }
  }

  public bool equals(vector< shared_ptr<UnHMI_ScreenLayout> > slayouts){
    bool equals = slayouts.count() == screenLayouts.count();
    if (!equals){
      return false;
    }

    for(int i=0; i<slayouts.count(); i++) {
      shared_ptr<UnHMI_ScreenLayout> tempPtr =  screenLayouts.at(i);
      equals = equals && (tempPtr.equals(slayouts.at(i)));
    }
    return equals;

  }

};
