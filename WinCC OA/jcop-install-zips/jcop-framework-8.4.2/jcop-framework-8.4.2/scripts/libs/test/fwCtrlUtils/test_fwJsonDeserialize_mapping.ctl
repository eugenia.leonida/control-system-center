#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"


void test_fwJsonDeserialize_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonDeserialize_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonDeserialize_setup(){}
//void test_fwJsonDeserialize_teardown(){}


void test_fwJsonDeserialize_map_double(){
  const string sampleFile = "mapOfSimple/map_double.json";
  string jsonString = readSample(sampleFile);
  mapping deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to mapping variable");
}

void test_fwJsonDeserialize_map_boolean(){
  const string sampleFile = "mapOfSimple/map_boolean.json";
  string jsonString = readSample(sampleFile);
  mapping deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to mapping variable");
}

void test_fwJsonDeserialize_map_string(){
  const string sampleFile = "mapOfSimple/map_string.json";
  string jsonString = readSample(sampleFile);
  mapping deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to mapping variable");
}

void test_fwJsonDeserialize_map_empty(){
  const string sampleFile = "mapOfSimple/map_empty.json";
  string jsonString = readSample(sampleFile);
  mapping deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to mapping variable");
}

void test_fwJsonDeserialize_map_null(){
  const string sampleFile = "mapOfSimple/map_null.json";
  string jsonString = readSample(sampleFile);
  mapping deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to mapping variable");
}

void test_fwJsonDeserialize_map_array(){
  const string sampleFile = "mapComplex/map_dyn_anytype.json";
  string jsonString = readSample(sampleFile);
  mapping deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to mapping variable");
}

void test_fwJsonDeserialize_map_mixed(){
  const string sampleFile = "mapOfSimple/map_mixed_limited.json";
  string jsonString = readSample(sampleFile);
  mapping deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to mapping variable");
}

void test_fwJsonDeserialize_anytype_to_map_mixed(){
  const string sampleFile = "mapOfSimple/map_mixed_limited.json";
  string jsonString = readSample(sampleFile);
  anytype deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to mapping variable");
}

void test_fwJsonDeserialize_map_nested_simple(){
  const string sampleFile = "mapComplex/map_nested_simple_limited.json";
  string jsonString = readSample(sampleFile);
  mapping deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to mapping variable");
}
