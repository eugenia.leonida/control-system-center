/** Set of helper functions for testing jsonSerializer/Deserializer Ctrl Extension **/

#uses "fwUnitTestComponentAsserts.ctl"
#uses "CtrlJsonSerialization"
#uses "test/fwCtrlUtils/UnHMI_ScreenLayout.ctl"

// Code for exception checking

checkException(errClass expected, dyn_errClass _actual, string message){
  if(dynlen(_actual) == 0){
    assertFalse(true, "No error. " + message);
  }
  errClass actual = _actual[1];
  dyn_string errDiffElements, expectedErrElems, actualErrElems;
  int expectedErrPrio = getErrorPriority(expected);
  int actualErrPrio = getErrorPriority(actual);
  dynAppend(expectedErrElems, getErrorPrioStr(expectedErrPrio));
  dynAppend(actualErrElems, getErrorPrioStr(actualErrPrio));
  if(expectedErrPrio != actualErrPrio){
    dynAppend(errDiffElements, "priority");
  }
  int expectedErrType = getErrorType(expected);
  int actualErrType = getErrorType(actual);
  dynAppend(expectedErrElems, getErrorTypeStr(expectedErrType));
  dynAppend(actualErrElems, getErrorTypeStr(actualErrType));
  if(expectedErrType != actualErrType){
    dynAppend(errDiffElements, "type");
  }
  string expectedErrCatalog = getErrorCatalog(expected);
  string actualErrCatalog = getErrorCatalog(actual);
  dynAppend(expectedErrElems, expectedErrCatalog);
  dynAppend(actualErrElems, actualErrCatalog);
  if(expectedErrCatalog != actualErrCatalog){
    dynAppend(errDiffElements, "catalog");
  }
  int expectedErrCode = getErrorCode(expected);
  int actualErrCode = getErrorCode(actual);
  dynAppend(expectedErrElems, (string)expectedErrCode);
  dynAppend(actualErrElems, (string)actualErrCode);
  if(expectedErrCode != actualErrCode){
    dynAppend(errDiffElements, "code");
  }
  
  assertEqual(strjoin(expectedErrElems, ", "), strjoin(actualErrElems, ", "),
              strjoin(errDiffElements, ", ") + " does not match. " + message);
}

string getErrorPrioStr(int prio){
  switch(prio){
    case PRIO_INFO: return "PRIO_INFO";
    case PRIO_WARNING: return "PRIO_WARNING";
    case PRIO_SEVERE: return "PRIO_SEVERE";
  }
  return "unknown (" + (string)prio + ")";
}

string getErrorTypeStr(int type){
  switch(type){
    case ERR_IMPL: return "ERR_IMPL";
    case ERR_PARAM: return "ERR_PARAM";
    case ERR_SYSTEM: return "ERR_SYSTEM";
    case ERR_CONTROL: return "ERR_CONTROL";
    case ERR_REDUNDANCY: return "ERR_REDUNDANCY";
  }
  return "unknown (" + (string)type + ")";
}

const string JSON_SERIALIZATION_MSG_CAT = "CtrlJsonSerialization";

enum JsonSerializationErrCode{
  UNKNOWN_ERROR = 0,
  PARSE_ERROR,
  UNSUPPORTED_TYPE,
  UNSUPPORTED_USER_TYPE,
  WRONG_JSON_VALUE_TYPE,
  MISSING_OBJ_PROPERTY,
  CTRL_VAR_CAST_ERROR,
  INVALID_VALUE,
  ALLOCATION_ERROR,
  CYCLE_DETECTED_ERROR,
  WRONG_INLINE_TYPE,
  INCOMPATIBLE_INLINE_TYPE
};

/////////////////////////////////////////

const string COMPLEX_VECTOR_JSON = "[{\"screenLayoutName\":\"testing\",\"windows\":[{\"fileName\":\"file_0\",\"number\":0,\"openOnStartup\":false,\"panelName\":\"panel_0\"},{\"fileName\":\"file_1\",\"number\":1,\"openOnStartup\":false,\"panelName\":\"panel_1\"},{\"fileName\":\"file_2\",\"number\":2,\"openOnStartup\":true,\"panelName\":\"panel_2\"},{\"fileName\":\"file_3\",\"number\":3,\"openOnStartup\":false,\"panelName\":\"panel_3\"}]}]" ;

const anytype EMPTY_ANYTYPE;
// mapping:
// key -> relative path to sample file in data/ directory
// value -> value of deserialized sample
const mapping SAMPLES_MAPPING = makeMapping(
    "simpleTypes/null.json", EMPTY_ANYTYPE,
    "simpleTypes/integer_negative.json", -397234,
    "simpleTypes/integer_positive.json", 2652423,
    "simpleTypes/double_positive.json", 1.4533e22,
    "simpleTypes/double_negative.json", -1.8431e-15,
    "simpleTypes/boolean.json", true,
    "simpleTypes/string.json", "dummy string",
    "simpleTypes/string_empty.json", "",
    "simpleTypes/char.json", (char)100,
    "simpleTypes/time.json", makeTime(2020,06,30,12,45,30,513),
    "simpleTypes/bit32.json", (bit32)0xAAAAAAAAU,
    "simpleTypes/bit64.json", (bit64)0xAAAAAAAAAAAAAAAAUL,
    "simpleTypes/blob.json", (blob)"66AA00FF810F3AC4B1",
    "simpleTypes/atime.json", makeATime(makeTime(1988,12,31,11,23,7,295), 9, "ExampleDP_Arg1."),
    "simpleTypes/function_ptr_member.json", "Test_Function::testFunction",
    //==========================
    "arraysOfSimple/arr_empty.json", makeDynAnytype(),
    "arraysOfSimple/arr_null.json", makeDynAnytype(EMPTY_ANYTYPE, EMPTY_ANYTYPE, EMPTY_ANYTYPE),
    "arraysOfSimple/arr_integer.json", makeDynInt(-3045,60593),
    "arraysOfSimple/arr_integer_positive.json", makeDynUInt(0, 3252, 6573, 74),
    "arraysOfSimple/arr_double.json", makeDynFloat(0, 3.3462, -1.84e5, 1.563e-34),
    "arraysOfSimple/arr_double_indented.json", makeDynFloat(0, 3.3462, -1.84e5, 1.563e-34),
    "arraysOfSimple/arr_boolean.json", makeDynBool(true, false, false),
    "arraysOfSimple/arr_string.json", makeDynString("one", "two"),
    "arraysOfSimple/arr_char.json", makeDynChar((char)0, (char)255, 'a', '\n'),
    "arraysOfSimple/arr_time.json", makeDynTime(makeTime(1998,12,30,12,45,30,513), makeTime(2029,3,8,23,29,0,999)),
    "arraysOfSimple/arr_bit64.json", makeDynBit64((bit64)0, (bit64)0xFFFFFFFFFFFFFFFFUL, (bit64)0x6666666666666666UL),
    "arraysOfSimple/arr_blob.json", (dyn_blob)makeDynString("", "00", "12CE0353", "356769AFFB"),
    "arraysOfSimple/arr_atime.json", makeDynATime(makeATime(makeTime(1988,12,31,11,23,7,295), 9, "ExampleDP_Arg1."),
                                                  makeATime(makeTime(2022,8,1,13,51,44,999), 0, "ExampleDP_DDE.f1")),
    "arraysOfSimple/arr_mixed_limited.json", makeDynAnytype(EMPTY_ANYTYPE, 3.7657, "test", false),
    "arraysOfSimple/arr_mixed_all.json", makeDynAnytype(-645, 89734U, -79268053654L, 952786345212UL,
                                          7.5437e75, true, "text", (char)0x11, makeTime(2034,11,5,17,0,0,864),
                                          (bit32)0x47FA24B6U, (bit64)0xA259B42E64CD1244UL, EMPTY_ANYTYPE, (blob)"34A08044FF00"),
    //==========================
    "mapOfSimple/map_integer.json", makeMapping("a", 34),
    "mapOfSimple/map_double.json", makeMapping("b", 5.78),
    "mapOfSimple/map_boolean.json", makeMapping("c", true),
    "mapOfSimple/map_string.json", makeMapping("d", "value"),
    "mapOfSimple/map_string_indented.json", makeMapping("d", "value", "e", "value2"),
    "mapOfSimple/map_empty.json", makeMapping(),
    "mapOfSimple/map_null.json", makeMapping("e", EMPTY_ANYTYPE),
    "mapOfSimple/map_char.json", makeMapping("f", (char)167),
    "mapOfSimple/map_time.json", makeMapping("g", makeTime(1981,5,31,0,37,54,34)),
    "mapOfSimple/map_mixed_limited.json", makeMapping( // limited to simple types supported by json (null, double, bool, string) - for deserializer
                                          "_double", 2.947e-49, "_bool", true, "_string", "text", "_anytype", EMPTY_ANYTYPE),
    "mapOfSimple/map_mixed_limited_with_inline_type.json", makeMapping("@class", "Test_Derived2", "_double", 2.947e-49, "_bool", true,
                                          "_string", "text", "_anytype", EMPTY_ANYTYPE),
    "mapOfSimple/map_mixed_all.json", makeMapping("_int", -34, "_uint", 45738U, "_long", -87635894346L, "_ulong", 748976015UL,
                                          "_double", 3.6834256e87, "_bool", true, "_string", "text", "_char", (char)0x04,
                                          "_time", makeTime(2000,04,1,1,2,52,5), "_bit32", (bit32)0x47A5B1FBU,
                                          "_bit64", (bit64)0xD39A42B56F502A53UL, "_anytype", EMPTY_ANYTYPE, "_blob", (blob)"0345E501",
                                          "_atime", makeATime(makeTime(2008,3,14,15,9,32), 1, "ExampleDP_Arg1.")),
    "mapOfSimple/map_mixed_all_with_inline_type.json", makeMapping("_int", -34, "_uint", 45738U, "_long", -87635894346L, "_ulong", 748976015UL,
                                          "_double", 3.6834256e87, "_bool", true, "_string", "text", "_char", (char)0x04,
                                          "_time", makeTime(2000,04,1,1,2,52,5), "_bit32", (bit32)0x47A5B1FBU,
                                          "_bit64", (bit64)0xD39A42B56F502A53UL, "_anytype", EMPTY_ANYTYPE, "_blob", (blob)"0345E501",
                                          "_atime", makeATime(makeTime(2008,3,14,15,9,32), 1, "ExampleDP_Arg1.")),
    //==========================
    "arrayComplex/arr_arr_double.json", (dyn_dyn_float)makeDynAnytype(
        makeDynFloat(0, 435.435), makeDynFloat(), makeDynFloat(3e-8), makeDynFloat(5.525e99, 2.865e-76)),
    "arrayComplex/arr_arr_arr_bool.json", makeDynAnytype(
        (dyn_dyn_bool)makeDynAnytype(makeDynBool(true, false), makeDynBool(false)),
        (dyn_dyn_bool)makeDynAnytype(makeDynBool(), makeDynBool(false), makeDynBool(true))),
    "arrayComplex/arr_arr_mixed.json", (dyn_dyn_anytype)makeDynAnytype(
        makeDynAnytype("text", false), makeDynAnytype(EMPTY_ANYTYPE), makeDynAnytype(), makeDynAnytype(9.452e-55, "text")),
    "arrayComplex/arr_map_same.json", makeDynMapping(makeMapping("d", ""), makeMapping("d", "sample text")),
    "arrayComplex/arr_map_same_with_inline_type.json", makeDynMapping(makeMapping("d", ""), makeMapping("d", "sample text")),
    //==========================
    "mapComplex/map_dyn_int.json", makeMapping("di", makeDynInt(32423,62,-626)),
    "mapComplex/map_dyn_anytype.json", makeMapping("ds", makeDynAnytype("a", EMPTY_ANYTYPE, 5.633, true)),
    "mapComplex/map_nested_simple_limited.json", makeMapping("tf", makeMapping("b", 345.76),
                                                             "ts", makeMapping("d", "dummy text"),
                                                             "tb", makeMapping("c", false),
                                                             "ta", makeMapping("e", EMPTY_ANYTYPE))
    );

const mapping TEMPLATE_PARAMS_MAP = makeMapping(
    "<:sys_name:>", getSystemName()
);

const string TEST_SAMPLES_PATH = "test/fwCtrlUtils/jsonSamples/";

string readSample(string sampleKey){
  string samplePath = TEST_SAMPLES_PATH + sampleKey;
  string absoluteSamplePath = getPath(DATA_REL_PATH, samplePath);
  if(absoluteSamplePath == "" || access(absoluteSamplePath, R_OK) != 0){
    breakTest("JSON sample file: " + samplePath + " does not exist in data/ directory or is not readable");
  }
  string jsonSample;
  if(!fileToString(absoluteSamplePath, jsonSample)){
    breakTest("Could not read JSON sample from file: " + absoluteSamplePath);
  }
  for(int i=0;i<TEMPLATE_PARAMS_MAP.count();i++){
    string templateParam = mappingGetKey(TEMPLATE_PARAMS_MAP, i + 1);
    if(jsonSample.contains(templateParam)){
      strreplace(jsonSample, templateParam, TEMPLATE_PARAMS_MAP[templateParam]);
    }
  }
  return jsonSample;
}

vector<void> dyn2vec(const dyn_anytype &arr){
  vector<void> vec;
  for(int i=0;i<arr.count();i++){
    vec.append(arr.at(i));
  }
  return vec;
}

vector<shared_ptr<double> > dyn2vec_ptr_double(const dyn_anytype &arr){
  vector<shared_ptr<double> > vec_ptr_double;
  for(int i=0;i<arr.count();i++){
    vec_ptr_double.append(new double(arr.at(i)));
  }
  return vec_ptr_double;
}

vector<vector<double> > dyn_dyn2vec_vec_double(const dyn_dyn_anytype &arr){
  vector<vector<double> > vec;
  for(int i=0;i<arr.count();i++){
    vector<double> vf = dyn2vec(arr.at(i));
    vec.append(vf);
  }
  return vec;
}

bool areVectorsEqual(vector<void> v1, vector<void> v2){
  if(getTypeName(v1) != getTypeName(v2)){
    DebugTN("Vector types are different", getTypeName(v1), getTypeName(v2));
    return false;
  }
  int v1Len = v1.count();
  int v2Len = v2.count();
  if(v1Len != v2Len){
    DebugTN("Vectors have different number of elements", v1Len, v2Len);
    return false;
  }
  for(int i=0;i<v1Len;i++){
    if(v1.at(i) != v2.at(i)){
      DebugTN("Vectors have different element at position " + i, v1.at(i), v2.at(i));
      return false;
    }
  }
  return true;
}

/**
  * Test User Types (enum, class, struct)
  */

enum Test_enum{
  default_val,
  di_1 = -626,
  di_2 = 62,
  di_3 = 32423,
  map_integer = 34,
  integer_positive = 2652423,
  next_val
};

class Test_Empty{};

class Test_Int{
  int a;

  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_Int> obj = new Test_Int();
    obj.a = m["a"];
    return obj;
  }
};

struct Test_Time{
  time g;

  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_Time> obj = new Test_Time();
    obj.g = m["g"];
    return obj;
  }
};

class Test_String{
  string d;

  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_String> obj = new Test_String();
    obj.d = m["d"];
    return obj;
  }

  public static vector<void>arrMap2vec(const dyn_anytype &arr){
    vector<Test_String> vec;
    for(int i=1;i<=dynlen(arr);i++){
      Test_String obj = map2obj(arr[i]);
      vec.append(obj);
    }
    return vec;
  }

  public static vector<void>arrMap2vecPtr(const dyn_anytype &arr){
    vector<shared_ptr<Test_String> > vec;
    for(int i=1;i<=dynlen(arr);i++){
      shared_ptr<Test_String> obj = map2obj(arr[i]);
      vec.append(obj);
    }
    return vec;
  }
};

struct Test_Bool{
  bool c;

  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_Bool> obj = new Test_Bool();
    obj.c = m["c"];
    return obj;
  }
};

class Test_Anytype{
  anytype e;

  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_Anytype> obj = new Test_Anytype();
    obj.e = m["e"];
    return obj;
  }

  public void setE(anytype _e){e = _e;}
};

class Test_Pointer_Char{
  shared_ptr <char> f;

  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_Anytype> obj = new Test_Anytype();
    obj.f = new char(m["f"]);
    return obj;
  }
};

class Test_Float{
  float b;

  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<void> obj = new Test_Float();
    obj.b = new double(m["b"]);
    return obj;
  }
};

class Test_Nested_Simple_Limited{
  Test_Float tf;
  Test_String ts;
  Test_Bool tb;
  Test_Anytype ta;

  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_Nested_Simple_Limited> obj = new Test_Nested_Simple_Limited();
    obj.tf = Test_Float::map2obj(m["tf"]);
    obj.ts = Test_String::map2obj(m["ts"]);
    obj.tb = Test_Bool::map2obj(m["tb"]);
    obj.ta = Test_Anytype::map2obj(m["ta"]);
    return obj;
  }
};

class Test_AllSimple{
  int _int;
  uint _uint;
  public long _long;
  ulong _ulong;
  public double _double;
  bool _bool;
  string _string;
  char _char;
  public time _time;
  bit32 _bit32;
  protected bit64 _bit64;
  anytype _anytype;
  protected blob _blob;
  atime _atime;

  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_AllSimple> obj = new Test_AllSimple();
    obj._int = m["_int"];
    obj._uint = m["_uint"];
    obj._long = m["_long"];
    obj._ulong = m["_ulong"];
    obj._double = m["_double"];
    obj._bool = m["_bool"];
    obj._string = m["_string"];
    obj._char = m["_char"];
    obj._time = m["_time"];
    obj._bit32 = m["_bit32"];
    obj._bit64 = m["_bit64"];
    obj._anytype = m["_anytype"];
    obj._blob = m["_blob"];
    obj._atime = m["_atime"];
    return obj;
  }
};

class Test_DynInt{
  dyn_int di;

  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_DynInt> obj = new Test_DynInt();
    obj.di = m["di"];
    return obj;
  }
};

class Test_VecEnum{
  vector<Test_enum> di;
  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_VecEnum> obj = new Test_VecEnum();
    vector<Test_enum> tmp_vec;
    for(int i=0;i<m["di"].count();i++){
      Test_enum tmp_enum = m["di"].at(i);
      tmp_vec.append(tmp_enum);
    }
    obj.di = tmp_vec;
    return obj;
  }
};

class Test_Base{
  protected double _double;
  protected bool _bool;
};

class Test_Derived:Test_Base{
  protected string _string;
  protected anytype _anytype;

  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_Derived> obj = new Test_Derived();
    obj._string = m["_string"];
    obj._anytype = m["_anytype"];
    obj._double = m["_double"];
    obj._bool = m["_bool"];
    return obj;
  }
};

class Test_Derived2:Test_Derived{
  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_Derived2> obj = new Test_Derived2();
    obj._string = m["_string"];
    obj._anytype = m["_anytype"];
    obj._double = m["_double"];
    obj._bool = m["_bool"];
    return obj;
  }
};

class Test_DerivedExt:Test_Derived{
  uint _uint;
  int _int;
  dyn_string _ds;
  vector<shared_ptr<char> > vpc;
  Test_VecEnum tve;
  shared_ptr<Test_Float> ptf;

  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_DerivedExt> obj = new Test_DerivedExt();
    obj._string = m["_string"];
    obj._anytype = m["_anytype"];
    obj._double = m["_double"];
    obj._bool = m["_bool"];
    return obj;
  }
};

class Test_StaticMembersOnly{
  static int a;
  static string s;
  static dyn_float df;
  static vector<bool> vb;
};

class Test_InstanceStaticMembersMixed{
  bool c;
  public static int a;
  static vector<float> vf;

  public static shared_ptr<void>map2obj(const mapping &m){
    shared_ptr<Test_InstanceStaticMembersMixed> obj = new Test_InstanceStaticMembersMixed();
    obj.c = m["c"];
    return obj;
  }
};

class Test_Function{
  public static void testFunction(){}
};

class Test_Recurence{
  public shared_ptr<Test_Recurence> selfPtr;
};
