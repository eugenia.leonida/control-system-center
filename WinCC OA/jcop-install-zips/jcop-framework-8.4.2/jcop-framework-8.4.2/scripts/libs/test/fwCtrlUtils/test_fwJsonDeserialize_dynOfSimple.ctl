#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"


void test_fwJsonDeserialize_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonDeserialize_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonDeserialize_setup(){}
//void test_fwJsonDeserialize_teardown(){}


void test_fwJsonDeserialize_dyn_int(){
  const string sampleFile = "arraysOfSimple/arr_integer.json";
  string jsonString = readSample(sampleFile);
  dyn_int deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_int variable");
}

void test_fwJsonDeserialize_dyn_uint(){
  const string sampleFile = "simpleTypes/integer_positive.json";
  string jsonString = readSample(sampleFile);
  dyn_uint deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(makeDynUInt(SAMPLES_MAPPING[sampleFile]), deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_uint variable");
}

void test_fwJsonDeserialize_dyn_long(){
  const string sampleFile = "simpleTypes/integer_negative.json";
  string jsonString = readSample(sampleFile);
  dyn_long deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(makeDynLong(SAMPLES_MAPPING[sampleFile]), deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_long variable");
}

void test_fwJsonDeserialize_dyn_ulong(){
  const string sampleFile = "arraysOfSimple/arr_integer_positive.json";
  string jsonString = readSample(sampleFile);
  dyn_ulong deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual((dyn_ulong)SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_ulong variable");
}

 // Note: boolean value must be always lowercase (otherwise parsing fails)
void test_fwJsonDeserialize_dyn_bool(){
  const string sampleFile = "arraysOfSimple/arr_boolean.json";
  string jsonString = readSample(sampleFile);
  dyn_bool deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_bool variable");
}

// Note: double is synonym to float in WinCC OA world, hence no separate test case for double here
void test_fwJsonDeserialize_dyn_float(){
  const string sampleFile = "arraysOfSimple/arr_double.json";
  string jsonString = readSample(sampleFile);
  dyn_float deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  // Note: not optimal - rounding error may occur - in such case modify assertion below
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_float variable");
}

void test_fwJsonDeserialize_dyn_string(){
  const string sampleFile = "arraysOfSimple/arr_string.json";
  string jsonString = readSample(sampleFile);
  dyn_string deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_string variable");
}

void test_fwJsonDeserialize_dyn_char(){
  const string sampleFile = "arraysOfSimple/arr_char.json";
  string jsonString = readSample(sampleFile);
  dyn_char deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_char variable");
}

void test_fwJsonDeserialize_dyn_bit32(){
  const string sampleFile = "simpleTypes/bit32.json";
  string jsonString = readSample(sampleFile);
  dyn_bit32 deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(makeDynBit32(SAMPLES_MAPPING[sampleFile]), deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_bit32 variable");
}

void test_fwJsonDeserialize_dyn_bit64(){
  const string sampleFile = "arraysOfSimple/arr_bit64.json";
  string jsonString = readSample(sampleFile);
  dyn_bit64 deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_bit64 variable");
}

void test_fwJsonDeserialize_dyn_time(){
  const string sampleFile = "arraysOfSimple/arr_time.json";
  string jsonString = readSample(sampleFile);
  dyn_time deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_time variable");
}

void test_fwJsonDeserialize_dyn_blob(){
  const string sampleFile = "arraysOfSimple/arr_blob.json";
  string jsonString = readSample(sampleFile);
  dyn_blob deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_blob variable");
}

void test_fwJsonDeserialize_dyn_atime(){
  const string sampleFile = "arraysOfSimple/arr_atime.json";
  string jsonString = readSample(sampleFile);
  dyn_atime deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_atime variable");
}

void test_fwJsonDeserialize_dyn_anytype_empty(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  dyn_anytype deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_anytype variable");
}

void test_fwJsonDeserialize_dyn_anytype_null(){
  const string sampleFile = "arraysOfSimple/arr_null.json";
  string jsonString = readSample(sampleFile);
  dyn_anytype deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_anytype variable");
}

void test_fwJsonDeserialize_dyn_mixed_null(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  dyn_mixed deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual((dyn_mixed)SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_mixed variable");
}

void test_fwJsonDeserialize_dyn_anytype_mixed(){
  const string sampleFile = "arraysOfSimple/arr_mixed_limited.json";
  string jsonString = readSample(sampleFile);
  dyn_anytype deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to dyn_anytype variable");
}
