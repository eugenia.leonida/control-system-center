#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"


void test_fwJsonDeserialize_userType_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonDeserialize_userType_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonDeserialize_setup(){}
//void test_fwJsonDeserialize_teardown(){}


void test_fwJsonDeserialize_userType_classOneSimple(){
  const string sampleFile = "mapOfSimple/map_integer.json";
  string jsonString = readSample(sampleFile);
  Test_Int sampleVal = Test_Int::map2obj(SAMPLES_MAPPING[sampleFile]);
  Test_Int deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to simple class");
}

void test_fwJsonDeserialize_userType_classAnytypeMem_checkCleaning(){
  const string sampleFile = "mapOfSimple/map_null.json";
  string jsonString = readSample(sampleFile);
  Test_Anytype sampleVal = Test_Anytype::map2obj(SAMPLES_MAPPING[sampleFile]);
  Test_Anytype deserializedVar;
  deserializedVar.setE(0);
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to class with one memeber " +
              "of type anytype having different value assigned");
}

void test_fwJsonDeserialize_userType_structOneSimple(){
  const string sampleFile = "mapOfSimple/map_boolean.json";
  string jsonString = readSample(sampleFile);
  Test_Bool sampleVal = Test_Bool::map2obj(SAMPLES_MAPPING[sampleFile]);
  Test_Bool deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to simple struct");
}

void test_fwJsonDeserialize_userType_classAllSimple(){
  const string sampleFile = "mapOfSimple/map_mixed_all.json";
  string jsonString = readSample(sampleFile);
  Test_AllSimple sampleVal = Test_AllSimple::map2obj(SAMPLES_MAPPING[sampleFile]);
  Test_AllSimple deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to class containing simple properties");
}

void test_fwJsonDeserialize_userType_classOneDyn(){
  const string sampleFile = "mapComplex/map_dyn_int.json";
  string jsonString = readSample(sampleFile);
  Test_DynInt sampleVal = Test_DynInt::map2obj(SAMPLES_MAPPING[sampleFile]);
  Test_DynInt deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to class containing members of simple type");
}

void test_fwJsonDeserialize_userType_classNested(){
  const string sampleFile = "mapComplex/map_nested_simple_limited.json";
  string jsonString = readSample(sampleFile);
  Test_Nested_Simple_Limited sampleVal = Test_Nested_Simple_Limited::map2obj(SAMPLES_MAPPING[sampleFile]);
  Test_Nested_Simple_Limited deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to class containing members of another user defined type");
}

void test_fwJsonDeserialize_userType_enum(){
  const string sampleFile = "simpleTypes/integer_positive.json";
  string jsonString = readSample(sampleFile);
  Test_enum sampleVal = SAMPLES_MAPPING[sampleFile];
  Test_enum deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to enum");
}

void test_fwJsonDeserialize_userType_classVecEnum(){
  const string sampleFile = "mapComplex/map_dyn_int.json";
  string jsonString = readSample(sampleFile);
  Test_VecEnum sampleVal = Test_VecEnum::map2obj(SAMPLES_MAPPING[sampleFile]);
  Test_VecEnum deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to class containing vector of enums");
}

void test_fwJsonDeserialize_userType_classWithInstanceAndStaticMembers(){
  const string sampleFile = "mapOfSimple/map_boolean.json";
  string jsonString = readSample(sampleFile);
  Test_InstanceStaticMembersMixed::a = 99;
  Test_InstanceStaticMembersMixed sampleVal = Test_InstanceStaticMembersMixed::map2obj(SAMPLES_MAPPING[sampleFile]);
  Test_InstanceStaticMembersMixed deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to class that has instance and static members");
}
