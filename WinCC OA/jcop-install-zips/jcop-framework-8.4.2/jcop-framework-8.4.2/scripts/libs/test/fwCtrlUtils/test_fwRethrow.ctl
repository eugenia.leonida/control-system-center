#uses "CtrlOOUtils"
#uses "test/fwCtrlUtils/classes/FwTestClass01.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

#uses "test/fwCtrlUtils/testUtils.ctl"


void testRethrow(bool useFwRethrow)
{
	try {
		FwTestClass01 obj;
		obj.methodWithException();
	} catch {
		if (useFwRethrow) {
			fwRethrow(); 
		} else {
			dyn_errClass exc=getLastException();
			throw (exc[1]);
		}
	} 
}


void test_fwRethrow_withThrow() 
{
	/* We expect that the exception from the testRethrow() will not preserve
	   the origin of exception (FwTestClass01::methodWithException()), and 
	   will put itself on the top of the stack trace, e.g.
	   	1: "testRethrow(bool useFwRethrow = FALSE) at test_fwRethrow.ctl:20"
	   	2: "test_fwRethrow_withThrow() at test_fwRethrow.ctl:32"
	   	3: "main(string args = \"\") at [Script: FwUnitTestComponent/fwUnitTestComponentTestRunnerScript.ctl]:95"
	*/	
	
	string expectedExceptionText="Example exception, Additional Diagnostic Information";
	bool gotException=false;
	try {
		testRethrow(false);
	} catch {
		dyn_errClass exc=getLastException();
		assertTrue(dynlen(exc)>=1, "Received exception is empty");
		gotException=true;
		dyn_string stkTrace=getErrorStackTrace(exc);
		assertException(exc, PRIO_INFO, ERR_IMPL, 0, expectedExceptionText);
		if (dynlen(stkTrace)<2) {
			DebugTN("EXCEPTION ENCOUNTERED IN "+__FUNCTION__,stkTrace);
			assert(false , "Invalid Stack trace. See log.");
		} else {
			// verify the stack trace
			assertSubstr("testRethrow(bool useFwRethrow = FALSE)", stkTrace[1], "Wrong stack trace(1)");
			assertSubstr(__FUNCTION__                            , stkTrace[2], "Wrong stack trace(2)");
		}
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}

void test_fwRethrow_withFwThrow() 
{
	/* We expect that the exception from the testRethrow() WILL BE PRESERVED
	   and the origin of exception (FwTestClass01::methodWithException()) will be on top
	   of the stack trace
	   will put itself on the top of the stack trace, e.g.
	    1: "FwTestClass01::methodWithException() at FwTestClass01.ctl:58"
	   	2: "testRethrow(bool useFwRethrow = TRUE) at test_fwRethrow.ctl:20"
	   	3: "test_fwRethrow_withThrow() at test_fwRethrow.ctl:32"
	   	4: "main(string args = \"\") at [Script: FwUnitTestComponent/fwUnitTestComponentTestRunnerScript.ctl]:95"
	*/	
	
	string expectedExceptionText="Example exception, Additional Diagnostic Information";
	bool gotException=false;
	try {
		testRethrow(true);
	} catch {
		dyn_errClass exc=getLastException();
		assertTrue(dynlen(exc)>=1, "Received exception is empty");
		gotException=true;
		dyn_string stkTrace=getErrorStackTrace(exc);
		assertException(exc, PRIO_INFO, ERR_IMPL, 0, expectedExceptionText);
		if (dynlen(stkTrace)<3) {
			DebugTN("EXCEPTION ENCOUNTERED IN "+__FUNCTION__,stkTrace);
			assert(false , "Invalid Stack trace. See log.");
		} else {
			// verify the stack trace
			assertSubstr("FwTestClass01::methodWithException()",  stkTrace[1], "Wrong stack trace(1)");
			assertSubstr("testRethrow(bool useFwRethrow = TRUE)", stkTrace[2], "Wrong stack trace(2)");
			assertSubstr(__FUNCTION__,                            stkTrace[3], "Wrong stack trace(3)");
		}
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}

void test_fwRethow_outOfScope()
{
	bool execAterFwRethrow=false;
	bool execCatch=false;
	bool execFinally=false;
	try {
		fwRethrow();
		execAterFwRethrow=true;
	} catch {
		assert(false, "Catch block should not have been executed");
	} finally {
		execFinally=true;
	}
	assertTrue(execAterFwRethrow,"Block after FwRethrow has not been executed");
	assertTrue(execFinally,"Block in finally has not been executed");
}
/**
	Test the nested try/catch block with rethrows
*/
void test_fwRethrow_multiLayer()
{
	dyn_string trace;
	const dyn_string expectedTrace=makeDynString("Before00","=>Try01", "==>Try02", "===>Try03", "===>Catch03", "===>Finally03", "==>Catch02", "==>Finally02", "=>Catch01", "=>Finally01", "After00");

	dynAppend(trace,"Before00");
	try {
		dynAppend(trace,"=>Try01");
		try {
			dynAppend(trace,"==>Try02");
			try {
				dynAppend(trace,"===>Try03");
				FwTestClass01 obj;
				obj.methodWithException();
				dynAppend(trace,"===>Try03AfterException");
			} catch {
					dynAppend(trace,"===>Catch03");
					fwRethrow(); 	
					dynAppend(trace,"===>Catch03AfterRethrow");
			} finally {
				dynAppend(trace,"===>Finally03");
			}
			dynAppend(trace,"===>After03");
		} catch {
				dynAppend(trace,"==>Catch02");
				fwRethrow(); 			
				dynAppend(trace,"==>Catch02AfterRethrow");
		} finally {
			dynAppend(trace,"==>Finally02");
		}
		dynAppend(trace,"==>After02");
	} catch {
		dynAppend(trace,"=>Catch01");
		//DebugTN(getErrorStackTrace(getLastException()));
	} finally {
		dynAppend(trace,"=>Finally01");
	}
	dynAppend(trace,"After00");
	
	assertEqual(expectedTrace, trace, "Unexpected sequence of calls");
}

/**
	Check that fwRethrow from the finally block does not do anything.
*/
void test_fwRethrow_fromFinally()
{
	dyn_string trace;
	const dyn_string expectedTrace=makeDynString("Before00","=>Try01", "==>Try02", "==>Catch02", "==>Finally02", "==>Finally02AfterFwRethrow", "==>After02", "=>Finally01", "After00");

	dynAppend(trace,"Before00");

	try {
		dynAppend(trace,"=>Try01");
		try {
			dynAppend(trace,"==>Try02");
			FwTestClass01 obj;
			obj.methodWithException();
			dynAppend(trace,"==>Try02AfterException");
		} catch {
				dynAppend(trace,"==>Catch02");
		} finally {
			dynAppend(trace,"==>Finally02");
			fwRethrow(); 	
			dynAppend(trace,"==>Finally02AfterFwRethrow");
		}
		dynAppend(trace,"==>After02");
	} catch {
			dynAppend(trace,"=>Catch01");
			fwRethrow(); 			
			dynAppend(trace,"=>Catch01AfterRethrow");
	} finally {
		dynAppend(trace,"=>Finally01");
	}
	dynAppend(trace,"After00");

	assertEqual(expectedTrace, trace, "Unexpected sequence of calls");
}