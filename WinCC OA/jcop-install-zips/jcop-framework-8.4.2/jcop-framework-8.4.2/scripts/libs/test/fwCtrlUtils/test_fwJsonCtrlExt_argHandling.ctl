#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"


void test_fwJsonCtrlExt_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonCtrlExt_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonCtrlExt_setup(){}
//void test_fwJsonCtrlExt_teardown(){}

//==================================
// Correct arguments

// fwJsonSerialize()

void test_fwJsonSerialize_objOnly(){
  assertTrue("" != fwJsonSerialize(0),
             "Serialization failed when only 'obj' argument is given");
}

void test_fwJsonSerialize_objAndCompact(){
  assertTrue("" != fwJsonSerialize(0, false),
             "Serialization failed when 'obj' and 'compact' as 2nd argument are given");
}

void test_fwJsonSerialize_objAndCompactConverted(){
  assertTrue("" != fwJsonSerialize(0, 0),
             "Serialization failed when 'obj' and 'compact' as 2nd argument (it needs to be converted to bool) are given");
}

void test_fwJsonSerialize_objAndOptions(){
  assertTrue("" != fwJsonSerialize(0, makeMapping()),
             "Serialization failed when 'obj' and 'options' arguments are given");
}

void test_fwJsonSerialize_objOptionsAndCompact(){
  assertTrue("" != fwJsonSerialize(0, makeMapping(), true),
             "Serialization failed when 'obj', 'options' and 'compact' arguments are given");
}

void test_fwJsonSerialize_objOptionsAndCompactConverted(){
  assertTrue("" != fwJsonSerialize(0, makeMapping(), 0),
             "Serialization failed when 'compact' argument agument requires conversion to type bool");
}

void test_fwJsonSerialize_objOptionsAndSharedPtrCompactConverted(){
  assertTrue("" != fwJsonSerialize(0, makeMapping(), new int(0)),
             "Serialization failed when 'compact' argument agument requires conversion to type bool");
}

void test_fwJsonSerialize_tooMuchArgs(){
  assertTrue("" != fwJsonSerialize(0, makeMapping(), true, "extra"),
             "Serialization failed when additional argument is given (should be discarded)");
}

// fwJsonDeserialize()

void test_fwJsonDeserialize_jsonAndObj(){
  string deserializedVar;
  string jsonString = readSample("simpleTypes/string.json");
  assertEqual(0, fwJsonDeserialize(jsonString, deserializedVar),
              "Deserialization failed when correct 'json' and 'obj' arguments are given");
}

void test_fwJsonDeserialize_sharedPtrJsonAndObj(){
  string deserializedVar;
  shared_ptr<string> jsonString = new string(readSample("simpleTypes/string.json"));
  assertEqual(0, fwJsonDeserialize(jsonString, deserializedVar),
              "Deserialization failed when correct 'json' and 'obj' arguments are given");
}

void test_fwJsonDeserialize_jsonObjAndOptions(){
  string deserializedVar;
  string jsonString = readSample("simpleTypes/string.json");
  assertEqual(0, fwJsonDeserialize(jsonString, deserializedVar, makeMapping("acceptIncompleteJson", true)),
              "Deserialization failed when correct 'json', 'obj' and options arguments are given");
}

void test_fwJsonDeserialize_tooMuchArgs(){
  string deserializedVar;
  string jsonString = readSample("simpleTypes/string.json");
  assertEqual(0, fwJsonDeserialize(jsonString, deserializedVar, makeMapping(), "extra"),
              "Deserialization failed when additional argument is given (should be discarded)");
}

//==================================
// Invalid arguments

// fwJsonSerialize()

void test_fwJsonSerialize_noArgs(){
  dyn_errClass funcException;
  try{
    fwJsonSerialize();
  }catch{
    funcException = getLastException();
  }
  checkException(makeError("", PRIO_SEVERE, ERR_PARAM, 75), funcException,
                 "Exception should be thrown when no arguments are passed");
}

void test_fwJsonSerialize_optionsWrongType(){
  dyn_errClass funcException;
  try{
    fwJsonSerialize(0, "text");
  }catch{
    funcException = getLastException();
  }
  checkException(makeError("", PRIO_SEVERE, ERR_PARAM, 50), funcException,
                 "Exception should be thrown when options argument is of wrong type");
}

void test_fwJsonSerialize_compactWrongType(){
  dyn_errClass funcException;
  try{
    fwJsonSerialize(0, makeMapping(), "text");
  }catch{
    funcException = getLastException();
  }
  checkException(makeError("", PRIO_SEVERE, ERR_PARAM, 50), funcException,
                 "Exception should be thrown when compact argument is of wrong type and cannot be converted to bool");
}

void test_fwJsonSerialize_compactArgRedundantWithOption(){
  dyn_errClass funcWarning;
  fwJsonSerialize(0, makeMapping("compactFormat", true), false);
  dyn_errClass funcWarning = getLastError();
  checkException(makeError("", PRIO_WARNING, ERR_PARAM, 76), funcWarning,
                 "Warning should be thrown when compact argument duplicates compactFormat option");
}

// fwJsonDeserialize()

void test_fwJsonDeserialize_noArgs(){
  dyn_errClass funcException;
  try{
    fwJsonDeserialize();
  }catch{
    funcException = getLastException();
  }
  checkException(makeError("", PRIO_SEVERE, ERR_PARAM, 75), funcException,
                 "Exception should be thrown when no arguments are passed");
}

void test_fwJsonDeserialize_noObj(){
  dyn_errClass funcException;
  string jsonString = readSample("simpleTypes/string.json");
  try{
    fwJsonDeserialize(jsonString);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError("", PRIO_SEVERE, ERR_PARAM, 75), funcException,
                 "Exception should be thrown when obj argument is missing");
}

void test_fwJsonDeserialize_jsonWrongType(){
  dyn_errClass funcException;
  anytype deserializedVar;
  try{
    fwJsonDeserialize(true, deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError("", PRIO_SEVERE, ERR_PARAM, 50), funcException,
                 "Exception should be thrown when json argument is of wrong type");
}

void test_fwJsonDeserialize_sharedPtrJsonEmpty(){
  dyn_errClass funcException;
  shared_ptr<string> jsonString;
  anytype deserializedVar;
  try{
    fwJsonDeserialize(jsonString, deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError("", PRIO_SEVERE, ERR_PARAM, 51), funcException,
                 "Exception should be thrown when an uninitialized pointer is passed when a json string is expected");
}

void test_fwJsonDeserialize_objNotAssignable(){
  dyn_errClass funcException;
  string jsonString = readSample("simpleTypes/string.json");
  const anytype a;
  try{
    fwJsonDeserialize(jsonString, a);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError("", PRIO_SEVERE, ERR_PARAM, 78), funcException,
                 "Exception should be thrown when cannot assign output to obj argument");
}
