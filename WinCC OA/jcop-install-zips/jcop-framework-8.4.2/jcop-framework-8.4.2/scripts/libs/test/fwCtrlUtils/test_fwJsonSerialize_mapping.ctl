#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"

void test_fwJsonSerialize_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonSerialize_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonSerialize_setup(){}
//void test_fwJsonSerialize_teardown(){}


void test_fwJsonSerialize_map_int(){
  const string sampleFile = "mapOfSimple/map_integer.json";
  string jsonString = readSample(sampleFile);
  mapping serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize mapping with single integer value to json string");
}

void test_fwJsonSerialize_map_double(){
  const string sampleFile = "mapOfSimple/map_double.json";
  string jsonString = readSample(sampleFile);
  mapping serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize mapping with single double value to json string");
}

void test_fwJsonSerialize_map_boolean(){
  const string sampleFile = "mapOfSimple/map_boolean.json";
  string jsonString = readSample(sampleFile);
  mapping serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize mapping with single boolean value to json string");
}

void test_fwJsonSerialize_map_string(){
  const string sampleFile = "mapOfSimple/map_string.json";
  string jsonString = readSample(sampleFile);
  mapping serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize mapping with single string value to json string");
}

void test_fwJsonSerialize_map_empty(){
  const string sampleFile = "mapOfSimple/map_empty.json";
  string jsonString = readSample(sampleFile);
  mapping serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize empty mapping to json string");
}

void test_fwJsonSerialize_map_null(){
  const string sampleFile = "mapOfSimple/map_null.json";
  string jsonString = readSample(sampleFile);
  mapping serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize mapping with single empty antype value to json string");
}

void test_fwJsonSerialize_map_array(){
  const string sampleFile = "mapComplex/map_dyn_anytype.json";
  string jsonString = readSample(sampleFile);
  mapping serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize mapping with array value");
}

void test_fwJsonSerialize_map_mixed(){
  const string sampleFile = "mapOfSimple/map_mixed_all.json";
  string jsonString = readSample(sampleFile);
  mapping serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize mapping with different values");
}

void test_fwJsonSerialize_map_mixed_in_mixed(){
  const string sampleFile = "mapOfSimple/map_mixed_all.json";
  string jsonString = readSample(sampleFile);
  mixed serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize mixed variable containing mapping with different values");
}

void test_fwJsonSerialize_map_nested_simple(){
  const string sampleFile = "mapComplex/map_nested_simple_limited.json";
  string jsonString = readSample(sampleFile);
  mixed serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize nested mapping");
}
