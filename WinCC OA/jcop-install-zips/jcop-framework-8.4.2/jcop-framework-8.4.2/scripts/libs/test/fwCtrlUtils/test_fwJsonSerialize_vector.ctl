#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"

void test_fwJsonSerialize_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonSerialize_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonSerialize_setup(){}
//void test_fwJsonSerialize_teardown(){}


void test_fwJsonSerialize_vector_int(){
  const string sampleFile = "arraysOfSimple/arr_integer.json";
  string jsonString = readSample(sampleFile);
  vector<int> serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize vector<int> to json string");
}

void test_fwJsonSerialize_vector_float(){
  const string sampleFile = "arraysOfSimple/arr_double.json";
  string jsonString = readSample(sampleFile);
  vector<double> serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize vector<double> to json string");
}

void test_fwJsonSerialize_vector_string(){
  const string sampleFile = "arraysOfSimple/arr_string.json";
  string jsonString = readSample(sampleFile);
  vector<string> serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize vector<string> to json string");
}

void test_fwJsonSerialize_vector_bool(){
  const string sampleFile = "arraysOfSimple/arr_boolean.json";
  string jsonString = readSample(sampleFile);
  vector<bool> serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize vector<bool> with single string value to json string");
}

void test_fwJsonSerialize_vector_bit32(){
  const string sampleFile = "simpleTypes/bit32.json";
  string jsonString = readSample(sampleFile);
  vector<bit32> serializedVar = makeVector(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize vector<bit32> with single string value to json string");
}

void test_fwJsonSerialize_vector_time(){
  const string sampleFile = "arraysOfSimple/arr_time.json";
  string jsonString = readSample(sampleFile);
  vector<time> serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize vector<time> with single string value to json string");
}

void test_fwJsonSerialize_vector_long_empty(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  vector<long> serializedVar;// = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize empty vector<long> mapping to json string");
}

void test_fwJsonSerialize_vector_void_empty(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  vector<void> serializedVar;// = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize empty vector<void> to json string");
}

void test_fwJsonSerialize_vector_void_null(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  vector<void> serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize vector<void> with null values to json string");
}

void test_fwJsonSerialize_vector_void_mixed(){
  const string sampleFile = "arraysOfSimple/arr_mixed_all.json";
  string jsonString = readSample(sampleFile);
  vector<void> serializedVar = dyn2vec(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize vector<void> with mixed values to json string");
}

void test_fwJsonSerialize_vector_anytype_mixed(){
  const string sampleFile = "arraysOfSimple/arr_mixed_all.json";
  string jsonString = readSample(sampleFile);
  vector<anytype> serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize vector<anytype> with mixed values to json string");
}

void test_fwJsonSerialize_vector_mixed_empty(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  vector<mixed> serializedVar;// = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize empty vector<mixed> to json string");
}

void test_fwJsonSerialize_vector_mixed_null(){
  const string sampleFile = "arraysOfSimple/arr_null.json";
  string jsonString = readSample(sampleFile);
  vector<mixed> serializedVar = (dyn_mixed)SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize vector<mixed> with null values to json string");
}

void test_fwJsonSerialize_vector_userType(){
  const string sampleFile = "arrayComplex/arr_map_same.json";
  string jsonString = readSample(sampleFile);
  vector<Test_String> serializedVar = Test_String::arrMap2vec(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize vector<userType> to json string");
}

void test_fwJsonSerialize_vector_userType_empty(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  vector<Test_String> serializedVar;
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize empty vector<userType> to json string");
}

void test_fwJsonSerialize_vector_shared_ptr_double(){
  const string sampleFile = "arraysOfSimple/arr_double.json";
  string jsonString = readSample(sampleFile);
  vector<shared_ptr<double> > serializedVar = dyn2vec_ptr_double(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize empty vector<shared_ptr<double> > to json string");
}

void test_fwJsonSerialize_vector_shared_ptr_userType(){
  const string sampleFile = "arrayComplex/arr_map_same.json";
  string jsonString = readSample(sampleFile);
  vector<shared_ptr<Test_String> > serializedVar = Test_String::arrMap2vecPtr(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize empty vector<shared_ptr<userType> > to json string");
}

void test_fwJsonSerialize_vector_vector_double(){
  const string sampleFile = "arrayComplex/arr_arr_double.json";
  string jsonString = readSample(sampleFile);
  vector<vector<double> > serializedVar = dyn_dyn2vec_vec_double(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize empty vector<vector<double> > to json string");
}
