struct FwTestStruct01 {

	int id;
	string name;
	
	void print(string header="") {
		DebugTN(header,id,name);
	}
};

using StructPtr = shared_ptr<FwTestStruct01>;

class FwTestClass01 {

	private static StructPtr s = new FwTestStruct01;

	public int method01(int input) {
		return 2*input;
	}

	public static int staticMethod01(int input) {
		return 2*input;
	}
	
	public void voidMethod() {
		return;
	}
	
	public StructPtr structMethod() {
		return s;
	}
	
	private void privVoidMethod() {
		return;
	}
	
	private string privStringMethod() {
		return "OK";
	}
	
	private int privMethodWithParam(int i) {
		return 2*i;
	}

	public shared_ptr<void> getVoid() {
		return s;
	}
	
	public static shared_ptr<void> getNullPtr() {
		return nullptr;
	}

	public static StructPtr getTypedNullPtr() {
		return nullptr;
	}
	
	public void methodWithException() {
		throw(makeError("", PRIO_INFO, ERR_IMPL, 0, "Example exception", "Additional Diagnostic Information"));
	}
	

};