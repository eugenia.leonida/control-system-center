#uses "CtrlOOUtils"
#uses "test/fwCtrlUtils/classes/FwTestClass01.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

#uses "test/fwCtrlUtils/testUtils.ctl"

void throwTestException()
{
    errClass err=makeError("", PRIO_WARNING, ERR_IMPL, 0, "Example exception", "Additional Diagnostic Information");
    fwThrowWithStackTrace(err);
}

bool excToExcInfo(dyn_string &exceptionInfo, dyn_errClass exc)
{
	if (dynlen(exc)<0) return false;
	string fwPrio;
	switch (getErrorPriority(exc)) {
		case PRIO_FATAL: fwPrio="FATAL"; break;
		case PRIO_FATAL_NO_RESTART: fwPrio="FATAL"; break;
		case PRIO_SEVERE: fwPrio="ERROR"; break;
		case PRIO_WARNING: fwPrio="WARNING"; break;
		case PRIO_INFO: fwPrio="INFO"; break;
		default: fwPrio="UNKNOWN";break;
	}

	string funcName;
	dyn_string stk=getErrorStackTrace(exc);
	if (dynlen(stk)>=1) {
		int pos=uniStrPos(stk[1],"(");
		funcName=uniSubStr(stk[1],0,pos);
	}

	string errText=getErrorText(exc);
	errText=strltrim(errText," ,");
	errText=strrtrim(errText," ,");

	dynAppend(exceptionInfo, fwPrio);
	if (funcName!="") {
		dynAppend(exceptionInfo, funcName+"():"+errText);
	} else {
		dynAppend(exceptionInfo, errText);
	}
	dynAppend(exceptionInfo, getErrorCode(exc));
	return true;
}

void exampleFwFunction(dyn_string &exceptionInfo)
{
	try {
		throwTestException();
	} catch {
		dyn_errClass exc=getLastException();
		excToExcInfo(exceptionInfo,exc);
	}
}

void test_exceptionInfo_FromException()
{
	dyn_string expectedExcInfo=makeDynString("WARNING","throwTestException():Example exception, Additional Diagnostic Information","0");
	dyn_string exceptionInfo;

	exampleFwFunction(exceptionInfo);

	assertEqual(expectedExcInfo,exceptionInfo);

}