#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"

/** Testing serialization and deserialization of errClass variable cannot be done separately
  * as it is not possible to set one property of errClass - error time.
  * When creating an errClass object always current time is assigned.
  *
  * errClass tests will do the following:
  *  a) sourceErrClass <serialization>-> json <deserialization>-> targetErrClass
  *     and check equality of sourceErrClass and targetErrClass
  *  b) sourceJson <deserialization>-> errClass <serialization>-> targetJson
  *     and check equality of sourceJson and targetJson
  */

//================================================
// Global indicating if global const errClass ERR_WITH_ST was initialized correctly
global bool gInitOk = false;

//================================================
// Helper function and structure

private errClass getErrClassWithStackTrace(){
  dyn_errClass runtimeErr;
  clearLastError();
  try{
    throw(makeError("ctrl", PRIO_SEVERE, ERR_CONTROL, 4, "CtrlJsonSerialization", "", "dummy"));
  }catch{
    runtimeErr = getLastException();
  }
  if(runtimeErr.count() != 1){
    return ERR_MIN_INFO;
  }
  gInitOk = true;
  return runtimeErr.at(0);
}

struct TestErr{
  errClass _err;
  dyn_errClass _dynerr;

  static shared_ptr<TestErr> make(errClass _err, dyn_errClass _dynerr){
    shared_ptr<TestErr> testErr = new TestErr();
    testErr._err = _err;
    testErr._dynerr = _dynerr;
    return testErr;
  }
};

//================================================
// Sample obj definition

const errClass ERR_MIN_INFO = makeError("uim", PRIO_SEVERE, ERR_PARAM, 25);
const errClass ERR_EXT_INFO = makeError(0, PRIO_INFO, ERR_IMPL, 10, "ExampleDP_Arg1.", convManIdToInt(UI_MAN, 99, 1578, 1),
                                        1024, "custom desc 1", "custom desc2", "!@#$%^\"'`~qwerty");
const errClass ERR_WITH_ST = getErrClassWithStackTrace();
const dyn_errClass ERR_ARRAY = makeDynAnytype(ERR_MIN_INFO, ERR_EXT_INFO, ERR_WITH_ST);

const mapping ERR_MAP = makeMapping("_err", ERR_MIN_INFO,
                                    "_dynerr", ERR_ARRAY);

const TestErr ERR_OBJ = TestErr::make(ERR_MIN_INFO, ERR_ARRAY);

//================================================
// Setup and teardown

void test_fwJsonSerialize_setupSuite(){
  if(!gInitOk){
    breakTest("Failed to initialize example errClass objects, cannot proceed with testing");
  }
}

void test_fwJsonSerialize_teardownSuite(){
  // No need for suite teardown currently
}

//================================================
/** JSON samples (data/test/fwCtrlUtils/):
  * errorClass/errClass_extInfo.json
  * errorClass/errClass_minInfo.json
  * errorClass/errClass_withST.json
  * errorClass/arr_errClass.json
  * errorClass/map_errClass.json
  */

/** TODO:
void test_fwJsonSerializeDeserialize_errClass_minInfo(){}
void test_fwJsonSerializeDeserialize_errClass_extInfo(){}
void test_fwJsonSerializeDeserialize_errClass_withST(){}
void test_fwJsonSerializeDeserialize_dyn_errClass(){}
  */

void test_fwJsonSerialize_vector_errClass_empty(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  vector<errClass> serializedVar;// = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize empty vector<errClass> to json string");
}

void test_fwJsonDeserialize_vector_errClass_empty(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  vector<errClass> sampleVal;// = SAMPLES_MAPPING[sampleFile];
  vector<errClass> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to empty vector<errClass> variable");
}

void test_fwJsonSerializeDeserialize_vector_errClass(){
  vector<errClass> sourceErrVec = ERR_ARRAY;
  string jsonString = fwJsonSerialize(sourceErrVec);
  vector<errClass> targetErrVec;
  int retVal = fwJsonDeserialize(jsonString, targetErrVec);
  assertEqual(sourceErrVec, targetErrVec, "Serialization and deserialization of vector<errClass> " +
              "produces different vector");
}

void test_fwJsonDeserializeSerialize_vector_errClass(){
  const string sampleFile = "errorClass/arr_errClass.json";
  string sourceJsonString = readSample(sampleFile);
  vector<errClass> deserializedVar;
  int retVal = fwJsonDeserialize(sourceJsonString, deserializedVar);
  string targetJsonString = fwJsonSerialize(deserializedVar);
  assertEqual(sourceJsonString, targetJsonString, "Deserialization and serialization of JSON encoding " +
              "vector<errClass> produces different JSON string");
}

void test_fwJsonSerializeDeserialize_obj_errClass(){
  TestErr sourceErrObj = ERR_OBJ; // intermediate object needed as const variables are currently not accepted by serializer
  string jsonString = fwJsonSerialize(sourceErrObj);
  TestErr targetErrObj;
  int retVal = fwJsonDeserialize(jsonString, targetErrObj);
  //DebugTN(sourceErrObj);
  //DebugTN(targetErrObj);
  assertEqual(ERR_OBJ, targetErrObj, "Serialization and deserialization of struct containing errClass " +
              "and dyn_errClass produces different object");
}

void test_fwJsonDeserializeSerialize_obj_errClass(){
  const string sampleFile = "errorClass/map_errClass.json";
  string sourceJsonString = readSample(sampleFile);
  TestErr deserializedVar;
  int retVal = fwJsonDeserialize(sourceJsonString, deserializedVar);
  string targetJsonString = fwJsonSerialize(deserializedVar);
  assertEqual(sourceJsonString, targetJsonString, "Deserialization and serialization of JSON encoding " +
              "object with errClass and dyn_errClass produces different JSON string");
}


