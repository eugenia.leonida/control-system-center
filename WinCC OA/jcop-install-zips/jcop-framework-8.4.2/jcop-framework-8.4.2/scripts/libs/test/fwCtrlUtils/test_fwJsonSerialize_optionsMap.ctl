#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"

const dyn_string NO_SERIALIZE_EMPTY;

void test_fwJsonSerialize_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonSerialize_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonSerialize_setup(){}
//void test_fwJsonSerialize_teardown(){}

void test_fwJsonSerialize_optionsMap_vector_float_indented_by_arg(){
  const string sampleFile = "arraysOfSimple/arr_double_indented.json";
  string jsonString = readSample(sampleFile);
  vector<double> serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar, false),
              "Failed to serialize vector<double> to indented json string (indentation requested in argument)");
}

void test_fwJsonSerialize_optionsMap_map_string_indented_by_option(){
  const string sampleFile = "mapOfSimple/map_string_indented.json";
  string jsonString = readSample(sampleFile);
  mapping serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar, makeMapping("compactFormat", false)),
              "Failed to serialize mapping with single string value to indented json string (indentation requested in option)");
}

void test_fwJsonSerialize_optionsMap_map_string_compact_by_option(){
  const string sampleFile = "mapOfSimple/map_string.json";
  string jsonString = readSample(sampleFile);
  mapping serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar, makeMapping("compactFormat", true)),
              "Failed to serialize mapping with single string value to compact json string (compact format requested in option)");
}

void test_fwJsonSerialize_optionsMap_map_string_indented_by_arg_overwrite_option(){
  const string sampleFile = "mapOfSimple/map_string_indented.json";
  string jsonString = readSample(sampleFile);
  mapping serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar, makeMapping("compactFormat", true), false),
              "Failed to serialize mapping with single string value to indented json string " +
              "(indentation requested in argument, that overwrites option)");
}

void test_fwJsonSerialize_optionsMap_addObjectTypeProperty(){
  const string sampleFile = "mapOfSimple/map_mixed_all_with_inline_type.json";
  string jsonString = readSample(sampleFile);
  Test_AllSimple serializedVar = Test_AllSimple::map2obj(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar, makeMapping("addObjectTypeProperty", true)),
              "Failed to serialize class to json string with inline type property");
}
