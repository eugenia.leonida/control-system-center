#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"

void test_fwJsonDeserialize_errHandling_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonDeserialize_errHandling_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonDeserialize_errHandling_setup(){}
//void test_fwJsonDeserialize_errHandling_teardown(){}


void test_fwJsonDeserialize_errHandling_parsingError(){
  dyn_errClass funcException;
  anytype deserializedVar;
  try{
    fwJsonDeserialize("{", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::PARSE_ERROR), funcException,
                 "Exception should be thrown when trying to parse incorrect JSON");
}

void test_fwJsonDeserialize_errHandling_emptyArrayAtRootWhenOneElementNeeded(){
  dyn_errClass funcException;
  int deserializedVar;
  try{
    fwJsonDeserialize("[]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when passed JSON with an empty array at root when one-element array expected");
}

void test_fwJsonDeserialize_errHandling_twoElementArrayAtRootWhenOneElementNeeded(){
  dyn_errClass funcException;
  int deserializedVar;
  try{
    fwJsonDeserialize("[3,5]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when passed JSON with two-element array at root when one-element array expected");
}

void test_fwJsonDeserialize_errHandling_unsupportedType(){
  dyn_errClass funcException;
  va_list deserializedVar;
  try{
    fwJsonDeserialize("[\"test\"]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::UNSUPPORTED_TYPE), funcException,
                 "Exception should be thrown when unsupported type passed as an 'obj' argument");
}

void test_fwJsonDeserialize_errHandling_functionPtrClassScopeNotFound(){
  dyn_errClass funcException;
  function_ptr deserializedVar;
  try{
    fwJsonDeserialize("[\"AClassThatDoNotExist9631::func\"]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when class/struct that function_ptr points to does not exist");
}

void test_fwJsonDeserialize_errHandling_functionPtrFunctionNotFoundInClassScope(){
  dyn_errClass funcException;
  function_ptr deserializedVar;
  try{
    fwJsonDeserialize("[\"Test_Function::testFunction2\"]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when function that function_ptr points to is not defined in given class/struct");
}

void test_fwJsonDeserialize_errHandling_functionPtrEmptyClassScope(){
  dyn_errClass funcException;
  function_ptr deserializedVar;
  try{
    fwJsonDeserialize("[\"::testFunction2\"]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when function that function_ptr points to is not defined in given class/struct");
}

void test_fwJsonDeserialize_errHandling_functionPtrFunctionNotFoundInGlobalScope(){
  dyn_errClass funcException;
  function_ptr deserializedVar;
  try{
    fwJsonDeserialize("[\"aFunctionThatDoNotExist9631\"]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when function that function_ptr points to is not defined");
}

void test_fwJsonDeserialize_errHandling_invalidAtimeJson_wrongFieldValue(){
  dyn_errClass funcException;
  atime deserializedVar;
  try{
    fwJsonDeserialize("[{\"count\":9,\"dpid\":\"" + getSystemName() + "ExampleDP_Arg1.\",\"time\":\"wrong\"}]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when json has invalid atime data (invalid field value: time)");
}

void test_fwJsonDeserialize_errHandling_invalidErrClassJson_missingField(){
  dyn_errClass funcException;
  errClass deserializedVar;
  try{
    fwJsonDeserialize("[{\"catalog\":\"uim\",\"prio\":1,\"time\":\"2020-08-05T08:55:36.948Z\",\"type\":1}]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when json has invalid errClass data (missing field: code)");
}

void test_fwJsonDeserialize_errHandling_invalidEnumValue(){
  dyn_errClass funcException;
  Test_enum deserializedVar;
  try{
    fwJsonDeserialize("[10]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when json has invalid enum value");
}

void test_fwJsonDeserialize_errHandling_invalidTimeValue(){
  dyn_errClass funcException;
  time deserializedVar;
  try{
    fwJsonDeserialize("[\"2020\"]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when json has invalid value, that cannot be converted to time type");
}

void test_fwJsonDeserialize_errHandling_invalidCharValue(){
  dyn_errClass funcException;
  char deserializedVar;
  try{
    fwJsonDeserialize("[\"256\"]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when json has invalid value, that cannot be converted to char type");
}

void test_fwJsonDeserialize_errHandling_invalidBit32Value(){
  dyn_errClass funcException;
  bit32 deserializedVar;
  try{
    fwJsonDeserialize("[\"2\"]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when json has invalid value, that cannot be converted to bit32 type");
}

void test_fwJsonDeserialize_errHandling_invalidBit64Value(){
  dyn_errClass funcException;
  bit64 deserializedVar;
  try{
    fwJsonDeserialize("[\"0xAF\"]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when json has invalid value, that cannot be converted to bit64 type");
}

void test_fwJsonDeserialize_errHandling_invalidBlobValue(){
  dyn_errClass funcException;
  blob deserializedVar;
  try{
    fwJsonDeserialize("[\"0xA\"]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INVALID_VALUE), funcException,
                 "Exception should be thrown when json has invalid value, that cannot be converted to blob type");
}

void test_fwJsonDeserialize_errHandling_invalidValueType(){
  dyn_errClass funcException;
  int deserializedVar;
  try{
    fwJsonDeserialize("[\"5\"]", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::WRONG_JSON_VALUE_TYPE), funcException,
                 "Exception should be thrown when json has invalid value type");
}

void test_fwJsonDeserialize_errHandling_missingObjectProperty(){
  dyn_errClass funcException;
  Test_Int deserializedVar;
  try{
    fwJsonDeserialize("{}", deserializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::MISSING_OBJ_PROPERTY), funcException,
                 "Exception should be thrown when object property is missing in json");
}

void test_fwJsonDeserialize_errHandling_objectTypePropertyIncompatible(){
  dyn_errClass funcException;
  shared_ptr<Test_String> deserializedVar;
  try{
    fwJsonDeserialize("{\"@class\":\"Test_Bool\",\"d\":\"test\"}", deserializedVar, makeMapping("useObjectTypeProperty", true));
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::INCOMPATIBLE_INLINE_TYPE), funcException,
                 "Exception should be thrown when object inline type is incompatible with shared_ptr type and useObjectTypeProperty option is set");
}

void test_fwJsonDeserialize_errHandling_objectTypePropertyWrong_unknown(){
  dyn_errClass funcException;
  anytype deserializedVar;
  try{
    fwJsonDeserialize("{\"@class\":\"DummyClassThatDoesNotExist\"}", deserializedVar, makeMapping("useObjectTypeProperty", true));
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::WRONG_INLINE_TYPE), funcException,
                 "Exception should be thrown when object inline type is not an existing user type");
}

void test_fwJsonDeserialize_errHandling_objectTypePropertyWrong_enum(){
  dyn_errClass funcException;
  shared_ptr<void> deserializedVar;
  try{
    fwJsonDeserialize("{\"@class\":\"Test_enum\"}", deserializedVar, makeMapping("useObjectTypeProperty", true));
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::WRONG_INLINE_TYPE), funcException,
                 "Exception should be thrown when object inline type is an existing user type, but not a class/struct");
}
