#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"

void test_fwJsonSerialize_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonSerialize_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonSerialize_setup(){}
//void test_fwJsonSerialize_teardown(){}


void test_fwJsonSerialize_shared_ptr_int(){
  const string sampleFile = "simpleTypes/integer_negative.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<int> serializedVar = new int(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize shared_ptr<int> to json string");
}

void test_fwJsonSerialize_shared_ptr_uint(){
  const string sampleFile = "simpleTypes/integer_positive.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<uint> serializedVar = new uint(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize shared_ptr<uint> to json string");
}

void test_fwJsonSerialize_shared_ptr_char(){
  const string sampleFile = "simpleTypes/char.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<char> serializedVar = new char(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize shared_ptr<char> to json string");
}

void test_fwJsonSerialize_shared_ptr_bit64(){
  const string sampleFile = "simpleTypes/bit64.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<bit64> serializedVar = new bit64(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize shared_ptr<bit64> to json string");
}

void test_fwJsonSerialize_shared_ptr_bool_nullptr(){
  const string sampleFile = "simpleTypes/null.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<bool> serializedVar;
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize uninitialized shared_ptr<bool> to json string");
}

void test_fwJsonSerialize_shared_ptr_void_nullptr(){
  const string sampleFile = "simpleTypes/null.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<void> serializedVar;
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize uninitialized shared_ptr<void> to json string");
}

void test_fwJsonSerialize_shared_ptr_void(){
  const string sampleFile = "simpleTypes/double_positive.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<void> serializedVar = new double(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize shared_ptr<void> poining to double to json string");
}

void test_fwJsonSerialize_shared_ptr_anytype_nullptr(){
  const string sampleFile = "simpleTypes/null.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<anytype> serializedVar;
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize uninitialized shared_ptr<anytype> to json string");
}

void test_fwJsonSerialize_shared_ptr_mixed(){
  const string sampleFile = "simpleTypes/boolean.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<mixed> serializedVar = new mixed(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize shared_ptr<mixed> to json string");
}

void test_fwJsonSerialize_shared_ptr_mapping_nullptr(){
  const string sampleFile = "simpleTypes/null.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<mapping> serializedVar;
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize uninitialized shared_ptr<mapping> to json string");
}

void test_fwJsonSerialize_shared_ptr_mapping(){
  const string sampleFile = "mapOfSimple/map_string.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<mapping> serializedVar = new mapping(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize shared_ptr<mapping> to json string");
}

void test_fwJsonSerialize_shared_ptr_class_simple(){
  const string sampleFile = "mapOfSimple/map_integer.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<Test_Int> serializedVar = Test_Int::map2obj(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize shared_ptr<class> to json string");
}

void test_fwJsonSerialize_shared_ptr_struct_simple(){
  const string sampleFile = "mapOfSimple/map_time.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<Test_Time> serializedVar = Test_Time::map2obj(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize shared_ptr<struct> to json string");
}

void test_fwJsonSerialize_shared_ptr_class_nullptr(){
  const string sampleFile = "simpleTypes/null.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<Test_Int> serializedVar;
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize uninitialized shared_ptr<class> to json string");
}

void test_fwJsonSerialize_shared_ptr_enum(){
  const string sampleFile = "simpleTypes/integer_positive.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<Test_enum> serializedVar = new Test_enum(SAMPLES_MAPPING[sampleFile]);;
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize shared_ptr<enum> to json string");
}
