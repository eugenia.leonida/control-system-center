#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"

void test_fwJsonSerialize_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonSerialize_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonSerialize_setup(){}
//void test_fwJsonSerialize_teardown(){}


void test_fwJsonSerialize_complexVectorSharedPtr(){
  shared_ptr<UnHMI_ScreenLayoutHelper> helper = UnHMI_ScreenLayoutHelper::getInstance();
  vector<void> v = helper.screenLayouts;
  v.clear();
  shared_ptr<UnHMI_ScreenLayout> sl = new UnHMI_ScreenLayout("testing");
  vector< shared_ptr<UnHMI_Window> > windows;
  for (int i=0; i<4; i++) {
    shared_ptr<UnHMI_Window> win =  new UnHMI_Window(i, "file_" + i, "panel_" + i, i==2);
    windows.append(win);
  }
  sl.setWindows(windows);
  helper.setScreenLayout(sl);
  string json = fwJsonSerialize(helper.screenLayouts);
  assertTrue(json == COMPLEX_VECTOR_JSON, "Failed to serialize complex vector object to validated COMPLEX_VECTOR_JSON");
}
