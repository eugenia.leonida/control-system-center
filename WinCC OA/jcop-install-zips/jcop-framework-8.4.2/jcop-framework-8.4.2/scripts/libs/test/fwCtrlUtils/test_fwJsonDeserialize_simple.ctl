#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"


void test_fwJsonDeserialize_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonDeserialize_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonDeserialize_setup(){}
//void test_fwJsonDeserialize_teardown(){}


void test_fwJsonDeserialize_int(){
  const string sampleFile = "simpleTypes/integer_negative.json";
  string jsonString = readSample(sampleFile);
  int deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to integer variable");
}

void test_fwJsonDeserialize_uint(){
  const string sampleFile = "simpleTypes/integer_positive.json";
  string jsonString = readSample(sampleFile);
  uint deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to unsigned integer variable");
}

void test_fwJsonDeserialize_long(){
  const string sampleFile = "simpleTypes/integer_negative.json";
  string jsonString = readSample(sampleFile);
  long deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to long integer variable");
}

void test_fwJsonDeserialize_ulong(){
  const string sampleFile = "simpleTypes/integer_positive.json";
  string jsonString = readSample(sampleFile);
  ulong deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to unsigned long integer variable");
}

 // Note: boolean value must be always lowercase (otherwise parsing fails)
void test_fwJsonDeserialize_bool(){
  const string sampleFile = "simpleTypes/boolean.json";
  string jsonString = readSample(sampleFile);
  bool deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to boolean variable");
}

// Note: double is synonym to float in WinCC OA world, hence no separate test case for double here
void test_fwJsonDeserialize_float(){
  const string sampleFile = "simpleTypes/double_negative.json";
  string jsonString = readSample(sampleFile);
  float deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  // Note: not optimal - rounding error may occur - in such case modify assertion below
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to floating point variable");
}

void test_fwJsonDeserialize_string(){
  const string sampleFile = "simpleTypes/string.json";
  string jsonString = readSample(sampleFile);
  string deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to string variable");
}

void test_fwJsonDeserialize_char(){
  const string sampleFile = "simpleTypes/char.json";
  string jsonString = readSample(sampleFile);
  char deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to character variable");
}

void test_fwJsonDeserialize_bit32(){
  const string sampleFile = "simpleTypes/bit32.json";
  string jsonString = readSample(sampleFile);
  bit32 deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to bit pattern (32bit) variable");
}

void test_fwJsonDeserialize_bit64(){
  const string sampleFile = "simpleTypes/bit64.json";
  string jsonString = readSample(sampleFile);
  bit64 deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to bit pattern (64bit) variable");
}

void test_fwJsonDeserialize_time(){
  const string sampleFile = "simpleTypes/time.json";
  string jsonString = readSample(sampleFile);
  time deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to timestamp variable");
}

void test_fwJsonDeserialize_blob(){
  const string sampleFile = "simpleTypes/blob.json";
  string jsonString = readSample(sampleFile);
  blob deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to blob variable");
}

void test_fwJsonDeserialize_atime(){
  const string sampleFile = "simpleTypes/atime.json";
  string jsonString = readSample(sampleFile);
  atime deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(SAMPLES_MAPPING[sampleFile], deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to atime variable");
}

void test_fwJsonDeserialize_function_ptr_nullptr(){
  const string sampleFile = "simpleTypes/string_empty.json";
  string jsonString = readSample(sampleFile);
  function_ptr deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  function_ptr fp_null = nullptr; // workaround as comparison nullptr == function_ptr throws an exception
  assertEqual(fp_null, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to nullptr function_ptr");
}

void test_fwJsonDeserialize_function_ptr_classScope(){
  const string sampleFile = "simpleTypes/function_ptr_member.json";
  string jsonString = readSample(sampleFile);
  function_ptr deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(Test_Function::testFunction, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to a function_ptr to class member function");
}

/** TODO: cases for anytype and mixed uninitialized, antype with simple types (check also if it gets expected type)
  so far not implemented as they would fail anyway
  */


void test_fwJsonDeserialize_complexVectorSharedPtr(){
  shared_ptr<UnHMI_ScreenLayoutHelper> helper = UnHMI_ScreenLayoutHelper::getInstance();
  vector<void> v = helper.screenLayouts;
  v.clear();
  shared_ptr<UnHMI_ScreenLayout> sl = new UnHMI_ScreenLayout("testing");
  vector< shared_ptr<UnHMI_Window> > windows;
  for (int i=0; i<4; i++) {
    shared_ptr<UnHMI_Window> win =  new UnHMI_Window(i, "file_" + i, "panel_" + i, i==2);
    windows.append(win);
  }
  sl.setWindows(windows);
  helper.setScreenLayout(sl);
  string json = fwJsonSerialize(helper.screenLayouts, makeMapping(), false);

  //Deserialize
  vector< shared_ptr<UnHMI_ScreenLayout> >  vector_check;
  fwJsonDeserialize(json, vector_check);
  assertTrue(helper.equals(vector_check), "Failed to deserialize -> serialize -> deserialize complex vector variable");


}

// template for simple type
/*void test_fwJsonDeserialize_<type>(){
  const <type> exampleVar = ;
  string jsonString = getJsonSerializedSimpleType(exampleVar);
  <type> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(exampleVar, deserializedVar, "Failed to deserialize JSON string " + jsonString + " to <type_desc> variable");
}*/
