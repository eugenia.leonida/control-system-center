#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"

void test_fwJsonDeserialize_optionsMap_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonDeserialize_optionsMap_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonDeserialize_optionsMap_setup(){}
//void test_fwJsonDeserialize_optionsMap_teardown(){}

void test_fwJsonDeserialize_optionsMap_acceptIncompleteJson_disabled(){
  dyn_errClass funcException;
  Test_Int deserializedVar;
  try{
    fwJsonDeserialize("{}", deserializedVar, makeMapping("acceptIncompleteJson", false));
  }catch{
    funcException = getLastException();
  }
  assertEqual(1, dynlen(funcException), "Exception should be thrown when JSON is incomplete and flag to accept this is set to false");
}

void test_fwJsonDeserialize_optionsMap_acceptIncompleteJson_singleParameterMissing(){
  Test_Bool sampleVal;
  Test_Bool deserializedVar;
  dyn_errClass funcException;
  try{
    fwJsonDeserialize("{}", deserializedVar, makeMapping("acceptIncompleteJson", true));
  }catch{
    funcException = getLastException();
  }
  assertEqual(0, dynlen(funcException), "An exception should not be thrown when JSON is incomplete but flag to accept this is set to true");
  assertEqual(sampleVal.c, deserializedVar.c, "Object property missing in JSON should be initialized with default value when acceptIncompleteJson=true");
}

void test_fwJsonDeserialize_optionsMap_acceptIncompleteJson_multipleParametersMissing(){
  const string sampleFile = "mapOfSimple/map_mixed_limited.json";
  string jsonString = readSample(sampleFile);
  Test_DerivedExt sampleVal = Test_DerivedExt::map2obj(SAMPLES_MAPPING[sampleFile]);
  Test_DerivedExt deserializedVar;
  dyn_errClass funcException;
  try{
    fwJsonDeserialize(jsonString, deserializedVar, makeMapping("acceptIncompleteJson", true));
  }catch{
    funcException = getLastException();
  }
  assertEqual(0, dynlen(funcException), "An exception should not be thrown when JSON is incomplete but flag to accept this is set to true");
  assertEqual(sampleVal, deserializedVar, "Object properties missing in JSON should be initialized with default value when acceptIncompleteJson=true");
}

void test_fwJsonDeserialize_optionsMap_useObjectTypeProperty_shared_ptr_class(){
  const string sampleFile = "mapOfSimple/map_mixed_all_with_inline_type.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<Test_AllSimple> sampleVal = Test_AllSimple::map2obj(SAMPLES_MAPPING[sampleFile]);
  shared_ptr<Test_AllSimple> deserializedVar;
  fwJsonDeserialize(jsonString, deserializedVar, makeMapping("useObjectTypeProperty", true));
  assertEqual(sampleVal, deserializedVar, "Object is not serialized properly to a shared_ptr<> " +
              "with useObjectTypeProperty flag set");
}

void test_fwJsonDeserialize_optionsMap_useObjectTypeProperty_shared_ptr_base_class(){
  const string sampleFile = "mapOfSimple/map_mixed_limited_with_inline_type.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<Test_Base> sampleVal = Test_Derived2::map2obj(SAMPLES_MAPPING[sampleFile]);
  shared_ptr<Test_Base> deserializedVar;
  fwJsonDeserialize(jsonString, deserializedVar, makeMapping("useObjectTypeProperty", true));
  assertEqual(sampleVal, deserializedVar, "Object is not serialized properly to a shared_ptr<> " +
              "of base class with useObjectTypeProperty flag set");
}

void test_fwJsonDeserialize_optionsMap_useObjectTypeProperty_shared_ptr_void(){
  const string sampleFile = "mapOfSimple/map_mixed_limited_with_inline_type.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<void> sampleVal = Test_Derived2::map2obj(SAMPLES_MAPPING[sampleFile]);
  shared_ptr<void> deserializedVar;
  fwJsonDeserialize(jsonString, deserializedVar, makeMapping("useObjectTypeProperty", true));
  assertEqual(sampleVal, deserializedVar, "Object is not serialized properly to a shared_ptr<void> " +
              "with useObjectTypeProperty flag set");
}

void test_fwJsonDeserialize_optionsMap_useObjectTypeProperty_shared_ptr_initialized(){
  const string sampleFile = "mapOfSimple/map_mixed_limited_with_inline_type.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<Test_Derived> sampleVal = Test_Derived::map2obj(SAMPLES_MAPPING[sampleFile]);
  shared_ptr<Test_Derived> deserializedVar = new Test_Derived();
  fwJsonDeserialize(jsonString, deserializedVar, makeMapping("useObjectTypeProperty", true));
  assertEqual(sampleVal, deserializedVar, "Initialized shared_ptr<> should block usage of inline " +
              "type property when useObjectTypeProperty flag set");
}

void test_fwJsonDeserialize_optionsMap_useObjectTypeProperty_vector_void(){
  const string sampleFile = "arrayComplex/arr_map_same_with_inline_type.json";
  string jsonString = readSample(sampleFile);
  vector<void> sampleVal = Test_String::arrMap2vec(SAMPLES_MAPPING[sampleFile]);
  vector<void> deserializedVar;
  fwJsonDeserialize(jsonString, deserializedVar, makeMapping("useObjectTypeProperty", true));
  assertEqual(sampleVal, deserializedVar, "Array of objects is not serialized correctly to a vector<void>" +
              "with useObjectTypeProperty flag set");
}

void test_fwJsonDeserialize_optionsMap_useObjectTypeProperty_anytype(){
  const string sampleFile = "mapOfSimple/map_mixed_limited_with_inline_type.json";
  string jsonString = readSample(sampleFile);
  anytype sampleVal = Test_Derived2::map2obj(SAMPLES_MAPPING[sampleFile]);
  anytype deserializedVar;
  fwJsonDeserialize(jsonString, deserializedVar, makeMapping("useObjectTypeProperty", true));
  assertEqual(sampleVal, deserializedVar, "Object is not serialized properly to an anytype variable " +
              "with useObjectTypeProperty flag set");
}

void test_fwJsonDeserialize_optionsMap_useObjectTypeProperty_noInlineType_anytype(){
  const string sampleFile = "mapOfSimple/map_mixed_limited.json";
  string jsonString = readSample(sampleFile);
  anytype sampleVal = SAMPLES_MAPPING[sampleFile];
  anytype deserializedVar;
  fwJsonDeserialize(jsonString, deserializedVar, makeMapping("useObjectTypeProperty", true));
  assertEqual(sampleVal, deserializedVar, "Object is not serialized properly to an anytype variable " +
              "when inline type info is missing but with useObjectTypeProperty flag set");
}

void test_fwJsonDeserialize_optionsMap_useObjectTypeProperty_disabled(){
  const string sampleFile = "mapOfSimple/map_mixed_limited_with_inline_type.json";
  string jsonString = readSample(sampleFile);
  anytype sampleVal = SAMPLES_MAPPING[sampleFile];
  anytype deserializedVar;
  fwJsonDeserialize(jsonString, deserializedVar, makeMapping("useObjectTypeProperty", false));
  assertEqual(sampleVal, deserializedVar, "Object is not serialized properly to an anytype variable " +
              "with useObjectTypeProperty flag unset");
}
