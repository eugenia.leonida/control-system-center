#uses "CtrlOOUtils"
#uses "test/fwCtrlUtils/classes/FwTestClass01.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

#uses "test/fwCtrlUtils/testUtils.ctl"

//void test_fwInvokeMethod_setupSuite(){}
//void test_fwInvokeMethod_teardownSuite(){}
//void test_fwInvokeMethod_setup(){}
//void test_fwInvokeMethod_teardown(){}

void test_fwInvokeMethod_test01() 
{
	FwTestClass01 obj;
	int inputValue=234;
	try {
		int rc=fwInvokeMethod(obj,"method01", inputValue);
		// method01() returns twice the inputValue
		assertEqual(2*inputValue, rc , "Wrong value returned");
	} catch {
		DebugTN("EXCEPTION ENCOUNTERED IN "+__FUNCTION__,getLastException());
		assert(false,"Unexpected exception encountered - see the log");
	}
}

void test_fwInvokeMethod_nonExistingMethod()
{

	FwTestClass01 obj;
	string methodName="NotExistingMethod";
	bool gotException=false;
	try {
		int rc=fwInvokeMethod(obj,methodName);
	} catch {
		gotException=true;
		dyn_errClass exc = getLastException();
		assertWrongParamException(exc, "No such method FwTestClass01:"+methodName);
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}
