#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"

void test_fwJsonSerialize_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonSerialize_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonSerialize_setup(){}
//void test_fwJsonSerialize_teardown(){}

void test_fwJsonSerialize_dyn_int(){
  const string sampleFile = "arraysOfSimple/arr_integer.json";
  string jsonString = readSample(sampleFile);
  dyn_int serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_int to json string");
}

void test_fwJsonSerialize_dyn_uint(){
  const string sampleFile = "simpleTypes/integer_positive.json";
  string jsonString = readSample(sampleFile);
  dyn_uint serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_uint to json string");
}

void test_fwJsonSerialize_dyn_long(){
  const string sampleFile = "simpleTypes/integer_negative.json";
  string jsonString = readSample(sampleFile);
  dyn_long serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_long to json string");
}

void test_fwJsonSerialize_dyn_ulong(){
  const string sampleFile = "arraysOfSimple/arr_integer_positive.json";
  string jsonString = readSample(sampleFile);
  dyn_ulong serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_ulong to json string");
}

 // Note: boolean value must be always lowercase (otherwise parsing fails)
void test_fwJsonSerialize_dyn_bool(){
  const string sampleFile = "arraysOfSimple/arr_boolean.json";
  string jsonString = readSample(sampleFile);
  dyn_bool serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_bool to json string");
}

// Note: double is synonym to float in WinCC OA world, hence no separate test case for double here
void test_fwJsonSerialize_dyn_float(){
  const string sampleFile = "arraysOfSimple/arr_double.json";
  string jsonString = readSample(sampleFile);
  dyn_float serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_float to json string");
}

void test_fwJsonSerialize_dyn_string(){
  const string sampleFile = "arraysOfSimple/arr_string.json";
  string jsonString = readSample(sampleFile);
  dyn_string serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_string to json string");
}

void test_fwJsonSerialize_dyn_char(){
  const string sampleFile = "arraysOfSimple/arr_char.json";
  string jsonString = readSample(sampleFile);
  dyn_char serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_char to json string");
}

void test_fwJsonSerialize_dyn_bit32(){
  const string sampleFile = "simpleTypes/bit32.json";
  string jsonString = readSample(sampleFile);
  dyn_bit32 serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_bit32 to json string");
}

void test_fwJsonSerialize_dyn_bit64(){
  const string sampleFile = "arraysOfSimple/arr_bit64.json";
  string jsonString = readSample(sampleFile);
  dyn_bit64 serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_bit64 to json string");
}

void test_fwJsonSerialize_dyn_time(){
  const string sampleFile = "arraysOfSimple/arr_time.json";
  string jsonString = readSample(sampleFile);
  dyn_time serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_time to json string");
}

void test_fwJsonSerialize_dyn_blob(){
  const string sampleFile = "arraysOfSimple/arr_blob.json";
  string jsonString = readSample(sampleFile);
  dyn_blob serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_blob to json string");
}

void test_fwJsonSerialize_dyn_atime(){
  const string sampleFile = "arraysOfSimple/arr_atime.json";
  string jsonString = readSample(sampleFile);
  dyn_atime serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_atime to json string");
}

void test_fwJsonSerialize_dyn_anytype_empty(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  dyn_anytype serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar),
              "Failed to serialize dyn_anytype (empty) to json string");
}

void test_fwJsonSerialize_dyn_anytype_null(){
  const string sampleFile = "arraysOfSimple/arr_null.json";
  string jsonString = readSample(sampleFile);
  dyn_anytype serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_anytype to json string");
}

void test_fwJsonSerialize_dyn_mixed_null(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  dyn_mixed serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_mixed to json string");
}

void test_fwJsonSerialize_dyn_mixed_mixed(){
  const string sampleFile = "arraysOfSimple/arr_mixed_limited.json";
  string jsonString = readSample(sampleFile);
  dyn_mixed serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize dyn_mixed to json string");
}
