#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"


void test_fwJsonDeserialize_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonDeserialize_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonDeserialize_setup(){}
//void test_fwJsonDeserialize_teardown(){}


void test_fwJsonDeserialize_vector_int(){
  const string sampleFile = "arraysOfSimple/arr_integer.json";
  string jsonString = readSample(sampleFile);
  vector<int> sampleVal = SAMPLES_MAPPING[sampleFile];
  vector<int> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<int> variable");
}

void test_fwJsonDeserialize_vector_float(){
  const string sampleFile = "arraysOfSimple/arr_double.json";
  string jsonString = readSample(sampleFile);
  vector<double> sampleVal = SAMPLES_MAPPING[sampleFile];
  vector<double> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<double> variable");
}

void test_fwJsonDeserialize_vector_string(){
  const string sampleFile = "arraysOfSimple/arr_string.json";
  string jsonString = readSample(sampleFile);
  vector<string> sampleVal = SAMPLES_MAPPING[sampleFile];
  vector<string> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<string> variable");
}

void test_fwJsonDeserialize_vector_bool(){
  const string sampleFile = "arraysOfSimple/arr_boolean.json";
  string jsonString = readSample(sampleFile);
  vector<bool> sampleVal = SAMPLES_MAPPING[sampleFile];
  vector<bool> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<bool> variable");
}

void test_fwJsonDeserialize_vector_bit32(){
  const string sampleFile = "simpleTypes/bit32.json";
  string jsonString = readSample(sampleFile);
  vector<bit32> sampleVal = makeVector(SAMPLES_MAPPING[sampleFile]);
  vector<bit32> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<bit32> variable");
}

void test_fwJsonDeserialize_vector_time(){
  const string sampleFile = "arraysOfSimple/arr_time.json";
  string jsonString = readSample(sampleFile);
  vector<time> sampleVal = SAMPLES_MAPPING[sampleFile];
  vector<time> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<time> variable");
}

void test_fwJsonDeserialize_vector_char_empty(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  vector<char> sampleVal;// = SAMPLES_MAPPING[sampleFile];
  vector<char> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<char> variable");
}

void test_fwJsonDeserialize_vector_void_empty(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  vector<void> sampleVal;// = SAMPLES_MAPPING[sampleFile];
  vector<void> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<void> variable");
}

void test_fwJsonDeserialize_vector_void_null(){
  const string sampleFile = "arraysOfSimple/arr_null.json";
  string jsonString = readSample(sampleFile);
  vector<void> sampleVal = SAMPLES_MAPPING[sampleFile];
  vector<void> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<void> variable");
}

void test_fwJsonDeserialize_vector_void_mixed(){
  const string sampleFile = "arraysOfSimple/arr_mixed_limited.json";
  string jsonString = readSample(sampleFile);
  vector<void> sampleVal = dyn2vec(SAMPLES_MAPPING[sampleFile]);
  vector<void> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<void> variable");
}

void test_fwJsonDeserialize_vector_anytype_mixed(){
  const string sampleFile = "arraysOfSimple/arr_mixed_limited.json";
  string jsonString = readSample(sampleFile);
  vector<anytype> sampleVal = SAMPLES_MAPPING[sampleFile];
  vector<anytype> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<anytype> variable");
}

void test_fwJsonDeserialize_vector_mixed_empty(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  vector<mixed> sampleVal;// = SAMPLES_MAPPING[sampleFile];
  vector<mixed> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<mixed> variable");
}

void test_fwJsonDeserialize_vector_mixed_null(){
  const string sampleFile = "arraysOfSimple/arr_null.json";
  string jsonString = readSample(sampleFile);
  vector<mixed> sampleVal = (dyn_mixed)SAMPLES_MAPPING[sampleFile];
  vector<mixed> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<mixed> variable");
}

void test_fwJsonDeserialize_vector_userType(){
  const string sampleFile = "arrayComplex/arr_map_same.json";
  string jsonString = readSample(sampleFile);
  vector<Test_String> sampleVal = Test_String::arrMap2vec(SAMPLES_MAPPING[sampleFile]);
  vector<Test_String> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to vector<userType> variable");
}

void test_fwJsonDeserialize_vector_userType_empty(){
  const string sampleFile = "arraysOfSimple/arr_empty.json";
  string jsonString = readSample(sampleFile);
  vector<Test_String> sampleVal;// = Test_String::arrMap2vec(SAMPLES_MAPPING[sampleFile]);
  vector<Test_String> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to empty vector<userType> variable");
}

void test_fwJsonDeserialize_vector_shared_ptr_double(){
  const string sampleFile = "arraysOfSimple/arr_double.json";
  string jsonString = readSample(sampleFile);
  vector<shared_ptr<double> > sampleVal = dyn2vec_ptr_double(SAMPLES_MAPPING[sampleFile]);
  vector<shared_ptr<double> > deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertTrue(areVectorsEqual(sampleVal, deserializedVar),
             "Failed to deserialize JSON: " + jsonString + " to vector<shared_ptr<double> > variable");
}

void test_fwJsonDeserialize_vector_shared_ptr_userType(){
  const string sampleFile = "arrayComplex/arr_map_same.json";
  string jsonString = readSample(sampleFile);
  vector<shared_ptr<Test_String> > sampleVal = Test_String::arrMap2vecPtr(SAMPLES_MAPPING[sampleFile]);
  vector<shared_ptr<Test_String> > deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertTrue(areVectorsEqual(sampleVal, deserializedVar),
             "Failed to deserialize JSON: " + jsonString + " to vector<shared_ptr<userType> > variable");
}

void test_fwJsonDeserialize_vector_vector_double(){
  const string sampleFile = "arrayComplex/arr_arr_double.json";
  string jsonString = readSample(sampleFile);
  vector<vector<double> > sampleVal = dyn_dyn2vec_vec_double(SAMPLES_MAPPING[sampleFile]);
  vector<vector<double> > deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertTrue(areVectorsEqual(sampleVal, deserializedVar),
             "Failed to deserialize JSON: " + jsonString + " to vector<vector<double> > variable");
}
