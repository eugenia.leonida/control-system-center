#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"


void test_fwJsonSerialize_userType_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonSerialize_userType_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonDeserialize_setup(){}
//void test_fwJsonDeserialize_teardown(){}


void test_fwJsonSerialize_userType_classOneSimple(){
  const string sampleFile = "mapOfSimple/map_integer.json";
  string jsonString = readSample(sampleFile);
  Test_Int serializedVar = Test_Int::map2obj(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize simple class to json string");
}

void test_fwJsonSerialize_userType_structOneSimple(){
  const string sampleFile = "mapOfSimple/map_boolean.json";
  string jsonString = readSample(sampleFile);
  Test_Bool serializedVar = Test_Bool::map2obj(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize simple struct to json string");
}

void test_fwJsonSerialize_userType_classAllSimple(){
  const string sampleFile = "mapOfSimple/map_mixed_all.json";
  string jsonString = readSample(sampleFile);
  Test_AllSimple serializedVar = Test_AllSimple::map2obj(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize class with members of simple " +
              "type and different access specifieres to json string");
}

void test_fwJsonSerialize_userType_classOneDyn(){
  const string sampleFile = "mapComplex/map_dyn_int.json";
  string jsonString = readSample(sampleFile);
  Test_DynInt serializedVar = Test_DynInt::map2obj(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize class with an array member " +
              "to json string");
}

void test_fwJsonSerialize_userType_classNested(){
  const string sampleFile = "mapComplex/map_nested_simple_limited.json";
  string jsonString = readSample(sampleFile);
  Test_Nested_Simple_Limited serializedVar = Test_Nested_Simple_Limited::map2obj(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize class with members of other " +
              "user defined type (nested) to json string");
}

void test_fwJsonSerialize_userType_enum(){
  const string sampleFile = "simpleTypes/integer_positive.json";
  string jsonString = readSample(sampleFile);
  Test_enum serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize enum to json string");
}

void test_fwJsonSerialize_userType_classVecEnum(){
  const string sampleFile = "mapComplex/map_dyn_int.json";
  string jsonString = readSample(sampleFile);
  Test_VecEnum serializedVar = Test_VecEnum::map2obj(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize class with vector of " +
              "enums member to json string");
}

void test_fwJsonSerialize_userType_classEmpty(){
  const string sampleFile = "mapOfSimple/map_empty.json";
  string jsonString = readSample(sampleFile);
  Test_Empty serializedVar;
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize class with no members");
}

void test_fwJsonSerialize_userType_classDerived(){
  const string sampleFile = "mapOfSimple/map_mixed_limited.json";
  string jsonString = readSample(sampleFile);
  Test_Derived serializedVar = Test_Derived::map2obj(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize a class that inherits members from a base one");
}

void test_fwJsonSerialize_userType_classWithStaticMembersOnly(){
  const string sampleFile = "mapOfSimple/map_empty.json";
  string jsonString = readSample(sampleFile);
  Test_StaticMembersOnly serializedVar;
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize class with static members only");
}

void test_fwJsonSerialize_userType_classWithInstanceAndStaticMembers(){
  const string sampleFile = "mapOfSimple/map_boolean.json";
  string jsonString = readSample(sampleFile);
  Test_InstanceStaticMembersMixed serializedVar = Test_InstanceStaticMembersMixed::map2obj(SAMPLES_MAPPING[sampleFile]);
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize class with mixed static and instance members");
}
