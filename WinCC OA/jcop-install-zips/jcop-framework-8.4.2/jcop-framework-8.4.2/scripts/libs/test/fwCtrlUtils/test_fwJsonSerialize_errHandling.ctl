#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"

void test_fwJsonSerialize_errHandling_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonSerialize_errHandling_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonSerialize_errHandling_setup(){}
//void test_fwJsonSerialize_errHandling_teardown(){}


void test_fwJsonSerialize_errHandling_circularReference1st(){
  dyn_errClass funcException;
  shared_ptr<Test_Recurence> serializedVar = new Test_Recurence();
  serializedVar.selfPtr = serializedVar;
  try{
    fwJsonSerialize(serializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::CYCLE_DETECTED_ERROR), funcException,
                 "Exception should be thrown when trying serialize an object that contains a pointer to itself");
}

void test_fwJsonSerialize_errHandling_circularReference2nd(){
  dyn_errClass funcException;
  shared_ptr<Test_Recurence> serializedVar = new Test_Recurence();
  shared_ptr<void> middlePtr = new Test_Recurence();
  serializedVar.selfPtr = middlePtr;
  middlePtr.selfPtr = serializedVar;
  try{
    fwJsonSerialize(serializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::CYCLE_DETECTED_ERROR), funcException,
                 "Exception should be thrown when trying serialize an object with circular reference");
}

void test_fwJsonSerialize_errHandling_unsupportedType(){
  dyn_errClass funcException;
  va_list serializedVar;
  try{
    fwJsonSerialize(serializedVar);
  }catch{
    funcException = getLastException();
  }
  checkException(makeError(JSON_SERIALIZATION_MSG_CAT, PRIO_WARNING, ERR_CONTROL, (int)JsonSerializationErrCode::UNSUPPORTED_TYPE), funcException,
                 "Exception should be thrown when trying serialize a mapping with unsupported key type");
}
