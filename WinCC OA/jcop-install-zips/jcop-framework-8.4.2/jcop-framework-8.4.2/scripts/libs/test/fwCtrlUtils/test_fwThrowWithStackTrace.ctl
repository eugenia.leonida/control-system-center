#uses "CtrlOOUtils"
#uses "test/fwCtrlUtils/classes/FwTestClass01.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

#uses "test/fwCtrlUtils/testUtils.ctl"

void throwTestExceptionWithCut(int cutFrames)
{
	dyn_errClass err=makeError("", PRIO_INFO, ERR_IMPL, 0, "Example exception", "Additional Diagnostic Information");
	fwThrowWithStackTrace(err,cutFrames);
}

void throwTestExceptionWithEmptyStackTrace()
{
	dyn_errClass err=makeError("", PRIO_INFO, ERR_IMPL, 0, "Example exception", "Additional Diagnostic Information");
	fwThrowWithStackTrace(err);
}

// functions to build a stack of calls
void throwTestException2(int cutFrames) { throwTestExceptionWithCut(cutFrames);}
void throwTestException3(int cutFrames) { throwTestException2(cutFrames);}
void throwTestException4(int cutFrames) { throwTestException3(cutFrames);}


void test_fwThrowWithStackTrace_throwWithCutStackTrace() 
{
	bool gotException=false;
	
	string expectedExceptionText="Example exception, Additional Diagnostic Information";
	
	try {
		throwTestException4(2);
	} catch {
		gotException=true;
		dyn_errClass exc=getLastException();
		assertException(exc, PRIO_INFO, ERR_IMPL, 0, expectedExceptionText);
		dyn_string stkTrace=getErrorStackTrace(exc);
		if (dynlen(stkTrace)<3) {
			DebugTN("EXCEPTION ENCOUNTERED IN "+__FUNCTION__,stkTrace);
			assert(false , "Invalid stack trace. See log.");
		} else {
			// verify the stack trace
			assertSubstr("throwTestException3(int cutFrames = 2) at test_fwThrowWithStackTrace.ctl", stkTrace[1], "Wrong stack trace(1)");
			assertSubstr("throwTestException4(int cutFrames = 2) at test_fwThrowWithStackTrace.ctl", stkTrace[2], "Wrong stack trace(2)");
			assertSubstr(__FUNCTION__,                            					stkTrace[3], "Wrong stack trace(3)");
		}

	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}


void test_fwThrowWithStackTrace_truncateStackTrace() 
{
	bool gotException=false;
	
	string expectedExceptionText="Example exception, Additional Diagnostic Information";
	
	try {
		throwTestException3(99);
	} catch {
		gotException=true;
		dyn_errClass exc=getLastException();
		assertException(exc, PRIO_INFO, ERR_IMPL, 0, expectedExceptionText);
		dyn_string stkTrace=getErrorStackTrace(exc);
		if (dynlen(stkTrace)!=0) {
			DebugTN("EXCEPTION ENCOUNTERED IN "+__FUNCTION__,stkTrace);
			assert(false , "Empty stack trace was expected. See log.");
		}
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}



void test_fwThrowWithStackTrace_throwWithEmptyStackTrace() 
{
	bool gotException=false;
	string expectedExceptionText="Example exception, Additional Diagnostic Information";
	
	try {
		throwTestExceptionWithEmptyStackTrace();
	} catch {
		gotException=true;
		dyn_errClass exc=getLastException();
		assertException(exc, PRIO_INFO, ERR_IMPL, 0, expectedExceptionText);
		dyn_string stkTrace=getErrorStackTrace(exc);
		if (dynlen(stkTrace)<3) {
			DebugTN("EXCEPTION ENCOUNTERED IN "+__FUNCTION__,stkTrace);
			assert(false , "Invalid stack trace. See log.");
		} else {
			// verify the stack trace
			assertSubstr("throwTestExceptionWithEmptyStackTrace() at test_fwThrowWithStackTrace.ctl", stkTrace[1], "Wrong stack trace(1)");
			assertSubstr(__FUNCTION__,                            					stkTrace[2], "Wrong stack trace(2)");
		}

	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}

void test_fwThrowWithStackTrace_throwWithSpecifiedStackTraceDynErrclass() 
{
	bool gotException=false;
	string expectedExceptionText="Example exception, Additional Diagnostic Information";
	dyn_string customStackTrace=makeDynString("Hello","From()","Custom_StackTrace()");	
	try {
		dyn_errClass err=makeError("", PRIO_INFO, ERR_IMPL, 0, "Example exception", "Additional Diagnostic Information");
		fwThrowWithStackTrace(err,customStackTrace);
	} catch {
		gotException=true;
		dyn_errClass exc=getLastException();
		assertException(exc, PRIO_INFO, ERR_IMPL, 0, expectedExceptionText);
		dyn_string stkTrace=getErrorStackTrace(exc);
		if (dynlen(stkTrace)<3) {
			DebugTN("EXCEPTION ENCOUNTERED IN "+__FUNCTION__,stkTrace);
			assert(false , "Invalid stack trace. See log.");
		} else {
			// verify the stack trace
			for (int i=1;i<=dynlen(customStackTrace);i++) {
				assertSubstr(customStackTrace[i], stkTrace[i], "Wrong stack trace("+i+")");
			}
		}
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}

void test_fwThrowWithStackTrace_throwWithSpecifiedStackTraceErrClass() 
{
	bool gotException=false;
	string expectedExceptionText="Example exception, Additional Diagnostic Information";
	dyn_string customStackTrace=makeDynString("Hello","From()","Custom_StackTrace()");	
	try {
		errClass err=makeError("", PRIO_INFO, ERR_IMPL, 0, "Example exception", "Additional Diagnostic Information");
		fwThrowWithStackTrace(err,customStackTrace);
	} catch {
		gotException=true;
		dyn_errClass exc=getLastException();
		assertException(exc, PRIO_INFO, ERR_IMPL, 0, expectedExceptionText);
		dyn_string stkTrace=getErrorStackTrace(exc);
		if (dynlen(stkTrace)<3) {
			DebugTN("EXCEPTION ENCOUNTERED IN "+__FUNCTION__,stkTrace);
			assert(false , "Invalid stack trace. See log.");
		} else {
			// verify the stack trace
			for (int i=1;i<=dynlen(customStackTrace);i++) {
				assertSubstr(customStackTrace[i], stkTrace[i], "Wrong stack trace("+i+")");
			}
		}
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}



void test_fwThrowWithStackTrace_callWithWrongExcType() 
{
	bool gotException=false;
	try {
		int wrongErr;
		fwThrowWithStackTrace(wrongErr);
	} catch {
		gotException=true;
		dyn_errClass exc=getLastException();
		assertWrongParamException(exc, "exception is of wrong type, expected ERROR_VAR got INTEGER_VAR");
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}

void test_fwThrowWithStackTrace_callWithWrongExcTypeDyn() 
{
	bool gotException=false;
	try {
		dyn_int wrongErr;
		fwThrowWithStackTrace(wrongErr);
	} catch {
		gotException=true;
		dyn_errClass exc=getLastException();
		assertWrongParamException(exc, "exception is of wrong type, expected ERROR_VAR got DYNINTEGER_VAR");
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}

void test_fwThrowWithStackTrace_callWithMissingParams() 
{
	bool gotException=false;
	try {
		fwThrowWithStackTrace();
	} catch {
		gotException=true;
		dyn_errClass exc=getLastException();
		assertWrongParamException(exc, "Argument missing in function, fwThrowWithStackTrace, exception",ERR_ARG_MISSING);
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}
