const int ERR_ARG_MISSING = 75;
const int ERR_ILLEGAL_ARG = 76;

bool assertSubstr(string subString, string searchString, string assertText)
{
	assertTrue(strpos(searchString, subString)>=0, assertText+";expected:"+subString+";got:"+searchString);
}

void assertException(const dyn_errClass &exc, int errPrio, int errType, int errCode, string errText)
{
	assertEqual( errType, getErrorType(exc),          "Wrong error type in exception");
    assertEqual( errCode, getErrorCode(exc) ,         "Wrong error code in exception");
    assertEqual( errPrio, getErrorPriority(exc),      "Wrong error priority in exception");
    assertSubstr(errText, getErrorText(exc),          "Wrong text in exception");
//    assertTrue(strpos(getErrorText(exc), errText)>=0, "Wrong text in exception: "+getErrorText(exc));
}

void assertWrongParamException(const dyn_errClass &exc, string errTxt, int errCode=ERR_ILLEGAL_ARG)
{
	assertException(exc,PRIO_SEVERE,ERR_PARAM,errCode, errTxt);
}
