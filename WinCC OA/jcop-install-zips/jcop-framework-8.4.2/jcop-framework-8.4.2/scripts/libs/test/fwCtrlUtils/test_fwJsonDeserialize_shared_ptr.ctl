#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"


void test_fwJsonDeserialize_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonDeserialize_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonDeserialize_setup(){}
//void test_fwJsonDeserialize_teardown(){}


void test_fwJsonDeserialize_shared_ptr_int(){
  const string sampleFile = "simpleTypes/integer_negative.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<int> sampleVal = new int(SAMPLES_MAPPING[sampleFile]);
  shared_ptr<int> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to shared_ptr<int> variable");
}

void test_fwJsonDeserialize_shared_ptr_char(){
  const string sampleFile = "simpleTypes/char.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<char> sampleVal = new char(SAMPLES_MAPPING[sampleFile]);
  shared_ptr<char> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to shared_ptr<char> variable");
}


void test_fwJsonDeserialize_shared_ptr_bit32_initialized(){
  const string sampleFile = "simpleTypes/bit32.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<bit32> sampleVal = new bit32(SAMPLES_MAPPING[sampleFile]);
  shared_ptr<bit32> deserializedVar = new bit32();
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to shared_ptr<bit32> variable (already initialized)");
}

void test_fwJsonDeserialize_shared_ptr_bool_nullptr(){
  const string sampleFile = "simpleTypes/null.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<bool> sampleVal;// = SAMPLES_MAPPING[sampleFile];
  shared_ptr<bool> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to uninitialized shared_ptr<bool> variable");
}

void test_fwJsonDeserialize_shared_ptr_void_nullptr(){
  const string sampleFile = "simpleTypes/null.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<void> sampleVal;// = SAMPLES_MAPPING[sampleFile];
  shared_ptr<void> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to uninitialized shared_ptr<void> variable");
}

void test_fwJsonDeserialize_shared_ptr_void_to_nullptr(){
  const string sampleFile = "simpleTypes/null.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<void> sampleVal;// = SAMPLES_MAPPING[sampleFile];
  shared_ptr<void> deserializedVar = new anytype();
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to uninitialized shared_ptr<void> variable");
}

void test_fwJsonDeserialize_shared_ptr_void(){
  const string sampleFile = "simpleTypes/double_positive.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<void> sampleVal = new dyn_anytype(SAMPLES_MAPPING[sampleFile]);
  shared_ptr<void> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to shared_ptr<void> variable");
}

void test_fwJsonDeserialize_shared_ptr_anytype_nullptr(){
  const string sampleFile = "simpleTypes/null.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<anytype> sampleVal;// = SAMPLES_MAPPING[sampleFile];
  shared_ptr<anytype> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to uninitialized shared_ptr<anytype> variable");
}

void test_fwJsonDeserialize_shared_ptr_dyn_anytype(){
  // as primitive value at JSON root is enclosed in square brackets it is treated as an array
  const string sampleFile = "simpleTypes/boolean.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<dyn_anytype> sampleVal = new dyn_anytype(makeDynAnytype(SAMPLES_MAPPING[sampleFile]));
  shared_ptr<dyn_anytype> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  DebugTN(sampleVal, deserializedVar, getTypeName(sampleVal), getTypeName(deserializedVar));
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to shared_ptr<dyn_anytype> variable");
}

void test_fwJsonDeserialize_shared_ptr_mixed_to_anytypeArray(){
  const string sampleFile = "arraysOfSimple/arr_mixed_limited.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<mixed> sampleVal = new mixed(SAMPLES_MAPPING[sampleFile]);
  shared_ptr<mixed> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  // this is not compared correctly in direct comparison, need to make it in steps here
  assertEqual(getTypeName(sampleVal), getTypeName(deserializedVar),
              "Pointer type mismatch - failed to deserialize JSON: " + jsonString + " to shared_ptr<mixed> variable");
  assertEqual((dyn_anytype)sampleVal, (dyn_anytype)deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to shared_ptr<mixed> variable");
}

void test_fwJsonDeserialize_shared_ptr_mapping_nullptr(){
  const string sampleFile = "simpleTypes/null.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<mapping> sampleVal;// = SAMPLES_MAPPING[sampleFile];
  shared_ptr<mapping> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to uninitialized shared_ptr<mapping> variable");
}

void test_fwJsonDeserialize_shared_ptr_mapping(){
  const string sampleFile = "mapOfSimple/map_string.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<mapping> sampleVal = new mapping(SAMPLES_MAPPING[sampleFile]);
  shared_ptr<mapping> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to shared_ptr<mixed> variable");
}

void test_fwJsonDeserialize_shared_ptr_class_simple(){
  const string sampleFile = "mapOfSimple/map_integer.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<Test_Int> sampleVal = Test_Int::map2obj(SAMPLES_MAPPING[sampleFile]);
  shared_ptr<Test_Int> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to shared_ptr<class> variable");
}

void test_fwJsonDeserialize_shared_ptr_struct_simple(){
  const string sampleFile = "mapOfSimple/map_time.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<Test_Time> sampleVal = Test_Time::map2obj(SAMPLES_MAPPING[sampleFile]);
  shared_ptr<Test_Time> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to shared_ptr<struct> variable");
}

void test_fwJsonDeserialize_shared_ptr_class_to_nullptr(){
  const string sampleFile = "simpleTypes/null.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<Test_Int> sampleVal;
  shared_ptr<Test_Int> deserializedVar = new Test_Int();
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to uninitialized shared_ptr<class> variable");
}

void test_fwJsonDeserialize_shared_ptr_enum(){
  const string sampleFile = "simpleTypes/integer_positive.json";
  string jsonString = readSample(sampleFile);
  shared_ptr<Test_enum> sampleVal = new Test_enum(SAMPLES_MAPPING[sampleFile]);
  shared_ptr<Test_enum> deserializedVar;
  int retVal = fwJsonDeserialize(jsonString, deserializedVar);
  assertEqual(sampleVal, deserializedVar,
              "Failed to deserialize JSON: " + jsonString + " to shared_ptr<enum> variable");
}
