#uses "test/fwCtrlUtils/jsonSerializeDeserializeTestUtils.ctl"

void test_fwJsonSerialize_setupSuite(){
  // No need for site setup currently
}

void test_fwJsonSerialize_teardownSuite(){
  // No need for suite teardown currently
}

//void test_fwJsonSerialize_setup(){}
//void test_fwJsonSerialize_teardown(){}

void test_fwJsonSerialize_int(){
  const string sampleFile = "simpleTypes/integer_negative.json";
  string jsonString = readSample(sampleFile);
  int serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize int to json string");
}

void test_fwJsonSerialize_uint(){
  const string sampleFile = "simpleTypes/integer_positive.json";
  string jsonString = readSample(sampleFile);
  uint serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize uint to json string");
}

void test_fwJsonSerialize_long(){
  const string sampleFile = "simpleTypes/integer_negative.json";
  string jsonString = readSample(sampleFile);
  long serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize long to json string");
}

void test_fwJsonSerialize_ulong(){
  const string sampleFile = "simpleTypes/integer_positive.json";
  string jsonString = readSample(sampleFile);
  ulong serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize ulong to json string");
}

void test_fwJsonSerialize_bool(){
  const string sampleFile = "simpleTypes/boolean.json";
  string jsonString = readSample(sampleFile);
  bool serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to erialize bool to json string");
}

// Note: double is synonym to float in WinCC OA world, hence no separate test case for double here
void test_fwJsonSerialize_float(){
  const string sampleFile = "simpleTypes/double_negative.json";
  string jsonString = readSample(sampleFile);
  float serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize float to json string");
}

void test_fwJsonSerialize_string(){
  const string sampleFile = "simpleTypes/string.json";
  string jsonString = readSample(sampleFile);
  string serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize string to json string");
}

void test_fwJsonSerialize_char(){
  const string sampleFile = "simpleTypes/char.json";
  string jsonString = readSample(sampleFile);
  char serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize char to json string");
}

void test_fwJsonSerialize_bit32(){
  const string sampleFile = "simpleTypes/bit32.json";
  string jsonString = readSample(sampleFile);
  bit32 serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize bit32 to json string");
}

void test_fwJsonSerialize_bit64(){
  const string sampleFile = "simpleTypes/bit64.json";
  string jsonString = readSample(sampleFile);
  bit64 serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize bit64 to json string");
}

void test_fwJsonSerialize_time(){
  const string sampleFile = "simpleTypes/time.json";
  string jsonString = readSample(sampleFile);
  time serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize time to json string");
}

void test_fwJsonSerialize_blob(){
  const string sampleFile = "simpleTypes/blob.json";
  string jsonString = readSample(sampleFile);
  blob serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize blob to json string");
}

void test_fwJsonSerialize_atime(){
  skipTest("Serialized as an object, expected is one-element array containing an object - TODO");
  const string sampleFile = "simpleTypes/atime.json";
  string jsonString = readSample(sampleFile);
  atime serializedVar = SAMPLES_MAPPING[sampleFile];
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize atime to json string");
}

void test_fwJsonSerialize_function_ptr_nullptr(){
  const string sampleFile = "simpleTypes/string_empty.json";
  string jsonString = readSample(sampleFile);
  function_ptr serializedVar = nullptr;
  DebugTN(jsonString, fwJsonSerialize(serializedVar));
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize nullptr function_ptr to json string");
}

void test_fwJsonSerialize_function_ptr_classScope(){
  const string sampleFile = "simpleTypes/function_ptr_member.json";
  string jsonString = readSample(sampleFile);
  function_ptr serializedVar = Test_Function::testFunction;
  DebugTN(jsonString, fwJsonSerialize(serializedVar));
  assertEqual(jsonString, fwJsonSerialize(serializedVar), "Failed to serialize member function function_ptr to json string");
}
