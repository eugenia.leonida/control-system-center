#uses "CtrlOOUtils"
#uses "test/fwCtrlUtils/classes/FwTestClass01.ctl"
#uses "fwUnitTestComponentAsserts.ctl"
#uses "test/fwCtrlUtils/testUtils.ctl"


//void test_fwClassExists_setupSuite()
//void test_fwClassExists_teardownSuite()
//void test_fwClassExists_setup(){}
//void test_fwClassExists_teardown(){}



void test_fwClassExists_positive() 
{
	try {
		bool exists=fwClassExists("FwTestClass01");
		assertEqual(true, exists, "Wrong value returned");
	} catch {
		DebugTN("EXCEPTION ENCOUNTERED IN "+__FUNCTION__,getLastException());
		assert(false,"Unexpected exception encountered - see the log");
	}
}

void test_fwClassExists_negative() 
{
	try {
		bool exists=fwClassExists("NonExistingClass");
		assertEqual(false, exists, "Wrong value returned");
	} catch {
		DebugTN("EXCEPTION ENCOUNTERED IN "+__FUNCTION__,getLastException());
		assert(false,"Unexpected exception encountered - see the log");
	}
}

void test_fwClassExists_positive_sharedPtr() 
{
	try {
		shared_ptr<string> className = new string("FwTestClass01");
		bool exists=fwClassExists(className);
		assertEqual(true, exists, "Wrong value returned");
	} catch {
		DebugTN("EXCEPTION ENCOUNTERED IN "+__FUNCTION__,getLastException());
		assert(false,"Unexpected exception encountered - see the log");
	}
}

void test_fwClassExists_positive_sharedPtrVoid() 
{
	try {
		shared_ptr<void> className = new string("FwTestClass01");
		bool exists=fwClassExists(className);
		assertEqual(true, exists, "Wrong value returned");
	} catch {
		DebugTN("EXCEPTION ENCOUNTERED IN "+__FUNCTION__,getLastException());
		assert(false,"Unexpected exception encountered - see the log");
	}
}

void test_fwClassExists_missingArg() 
{
	FwTestClass01 obj;
	bool gotException=false;
	try {
		bool exists=fwClassExists();	
	} catch {
		gotException=true;
		dyn_errClass exc = getLastException();
		assertWrongParamException(exc, "Argument missing in function, fwClassExists, className",ERR_ARG_MISSING);
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}


void test_fwClassExists_wrongArgType() 
{
	FwTestClass01 obj;
	bool gotException=false;
	try {
		bool exists=fwClassExists(123);	
	} catch {
		gotException=true;
		dyn_errClass exc = getLastException();
		assertWrongParamException(exc, "Invalid argument in function, fwClassExists, className is of wrong type, expected TEXT_VAR got INTEGER_VAR");
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}

void test_fwClassExists_wrongArgType_sharedPtr() 
{
	FwTestClass01 obj;
	bool gotException=false;
	try {
		shared_ptr<int> param=new int();
		bool exists=fwClassExists(param);	
	} catch {
		gotException=true;
		dyn_errClass exc = getLastException();
		assertWrongParamException(exc, "Invalid argument in function, fwClassExists, className is of wrong type, expected TEXT_VAR got INTEGER_VAR");
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}

void test_fwClassExists_wrongArgType_sharedPtrVoid() 
{
	FwTestClass01 obj;
	bool gotException=false;
	try {
		shared_ptr<void> param=new int();
		bool exists=fwClassExists(param);	
	} catch {
		gotException=true;
		dyn_errClass exc = getLastException();
		assertWrongParamException(exc, "Invalid argument in function, fwClassExists, className is of wrong type, expected TEXT_VAR got INTEGER_VAR");
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}

void test_fwClassExists_wrongArgType_sharedPtr_nullPtr() 
{
	FwTestClass01 obj;
	bool gotException=false;
	try {
		shared_ptr<int> param=nullptr;
		bool exists=fwClassExists(param);	
	} catch {
		gotException=true;
		dyn_errClass exc = getLastException();
		assertWrongParamException(exc, "Invalid argument in function, fwClassExists, className is a nullptr");
	} finally {
		assertTrue(gotException,"Expected exception was not encountered");
	}
}
