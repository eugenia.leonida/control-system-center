#uses "fwTree/fwTree.ctl"
#uses "fwFSM/fwFsmUtil.ctl"
#uses "fwUnitTestComponentAsserts.ctl"
void test_fwFsmUtil_setupSuite()
{

}

void test_fwFsmUtil_teardownSuite()
{

}


void test_fwFsmUtil_extractSystem()
{
	assertEqual("",fwFsm_extractSystem(""),"Failed for empty string");
	assertEqual("",fwFsm_extractSystem("sys:"),"Failed for sys:");
	assertEqual("dp",fwFsm_extractSystem("sys:dp"),"Failed for sys:dp");
	assertEqual("dp",fwFsm_extractSystem(":dp"),"Failed for :dp");
	assertEqual("dp:something",fwFsm_extractSystem(":dp:something"),"Failed for :dp:something");
	assertEqual("dp:something",fwFsm_extractSystem("sys:dp:something"),"Failed for sys:dp:something");
}


void test_fwFsmUtil_getSystem()
{
	assertEqual("",fwFsm_getSystem(""),"Failed for empty string");
	assertEqual("sys",fwFsm_getSystem("sys:"),"Failed for sys:");
	assertEqual("sys",fwFsm_getSystem("sys:dp"),"Failed for sys:dp");
	assertEqual("",fwFsm_getSystem(":dp"),"Failed for :dp");
	assertEqual("",fwFsm_getSystem(":dp:something"),"Failed for :dp:something");
	assertEqual("sys",fwFsm_getSystem("sys:dp:something"),"Failed for sys:dp:something");
}

void test_fwFsmUtil_getSystemName()
{
	string referenceSysName=strrtrim(getSystemName(),":");
	assertEqual(referenceSysName,fwFsm_getSystemName());
}

void test_fwFsmUtil_getProjPath()
{
	assertEqual(makeNativePath(strrtrim(PROJ_PATH,"/\\")),fwFsm_getProjPath());
}

void test_fwFsmUtil_getPvssPath()
{
	assertEqual(makeNativePath(strrtrim(PVSS_PATH,"/\\")),fwFsm_getPvssPath());
}

void test_fwFsmUtil_getProjVersion()
{
	assertEqual(VERSION_DISP,fwFsm_getProjVersion());
}