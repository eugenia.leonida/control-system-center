#uses "fwUnitTestComponentAsserts.ctl"
#uses "fwTrending/fwTrending.ctl"


 // assertFalse(fwInstallationManager_command("START","WCCOAui","-p fwTrending/fwTrending.pnl -iconBar -menuBar") < 0);

/* Appends  "trend." to the refName because of a multiple plot and the ref will be ref + "trend." */
void test_fwTrending_adjustRefName()
{
  int iLen, iRef;
  string sOutput;
  dyn_string dsRef;

  // Initialize test
  dynAppend(dsRef, "example");
  dynAppend(dsRef, "");


  iLen = dynlen(dsRef);
  for( iRef = 1 ; iRef <= iLen ; iRef++ )
  {
    sOutput = _fwTrending_adjustRefName(dsRef[iRef]);
    if( dsRef[iRef] != "" )
    {
      if( sOutput != (dsRef[iRef] + "trend.") )
      {
        assertTrue(FALSE, "Error output of function should be: " + dsRef[iRef] + "trend. and it is: " + sOutput);
        return;
      }
    }
    else
    {
      if( sOutput == (dsRef[iRef] + "trend.") )
      {
        assertTrue(FALSE, "Error output of function should be: trend. and it is: " + sOutput);
        return;
      }
    }
  }

  // If no errors, all it is OK
  assertTrue(TRUE);
}






/* Appends a dot "." to the refName because this function is called from the fwTrending/fwTrendingTrend.pnl */
void test_fwTrending_appendDotTo()
{
  int iLen, iRef;
  string sOutput;
  dyn_string dsRef;


  // Initialize test
  dynAppend(dsRef, "example");
  dynAppend(dsRef, "");


  iLen = dynlen(dsRef);
  for( iRef = 1 ; iRef <= iLen ; iRef++ )
  {
    sOutput = _fwTrending_appendDotTo(dsRef[iRef]);

    if( dsRef[iRef] != "" )
    {
      if( sOutput != (dsRef[iRef] + ".") )
      {
        assertTrue(FALSE, "Error, output from the function is: " + sOutput + " and expected is: " + dsRef[iRef] + "." );
        return;
      }
    }
    else
    {
      if( sOutput == (dsRef[iRef] + ".") )
      {
        assertTrue(FALSE, "Error, output from the function is: " + sOutput + " and expected is: .");
        return;
      }
    }
  }

  // If no errors, all it is OK
  assertTrue(TRUE);

}




/* Removes the "@" from the provided string to hide the alias prefix from the UI */
void test_fwTrending_cleanAliasRepresentation()
{
  int iLen, iLoop;
  string sOutput;
  dyn_string dsMyDP;


  // Preparing test cases
  dynAppend(dsMyDP, getSystemName() + "fwTrending_DP_random");
  dynAppend(dsMyDP, getSystemName() + "@fwTrending_DP_random.value");
  dynAppend(dsMyDP, "@fwTrending_DP_random.value");


  iLen = dynlen(dsMyDP);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++)
  {
    sOutput = fwTrending_cleanAliasRepresentation(dsMyDP[iLoop]);
    if( strpos(sOutput, "@") >= 0 )
    {
      assertTrue(FALSE, "Error, expected a string without @, but it returned: " + sOutput);
    }
  }

  // If no errors, all it is OK
  assertTrue(TRUE);

}





/* Adds the "@" to the provided string so WinCC can process an alias, what if DP name is @DPname? */
void test_fwTrending_createAliasRepresentation()
{
  int iLen, iLoop;
  string sOutput;
  dyn_string dsMyDP;


  // Preparing test cases
  dynAppend(dsMyDP, getSystemName() + "fwTrending_DP_random");
  dynAppend(dsMyDP, getSystemName() + "@fwTrending_DP_random.value");
  dynAppend(dsMyDP, "fwTrending_DP_random.value");

  iLen = dynlen(dsMyDP);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++)
  {
    sOutput = fwTrending_createAliasRepresentation(dsMyDP[iLoop]);
    if( strpos(sOutput, "@") < 0 )
    {
      assertTrue(FALSE, "Error, we expected @, an alias in the output and the output is: " + sOutput);
    }
  }


  // If no errors, all it is OK
  assertTrue(TRUE);

}




/* Encodes time as seconds */
void test_fwTrending_encodeTime()
{
  int iDays, iHours, iMinutes, iDayToSeconds, iHourToSeconds, iMinuteToSeconds, iSeconds, iCalculatedSeconds;


  // Preparing test cases
  iDays            = 1;
  iHours           = 1;
  iMinutes         = 1;
  iDayToSeconds    = 86400;
  iHourToSeconds   = 3600;
  iMinuteToSeconds = 60;


  fwTrending_encodeTime(iSeconds, iDays, iHours, iMinutes);

  iCalculatedSeconds = (iDayToSeconds * iDays) + (iHourToSeconds * iHours) + (iMinuteToSeconds * iMinutes);
  if( iSeconds != iCalculatedSeconds )
  {
    assertTrue(FALSE, "Error, conversion to seconds not correct, expected " + iCalculatedSeconds + " seconds and returned: " + iSeconds);
    return;
  }


  // If no errors, all it is OK
  assertTrue(TRUE);

}




/* Transforms special characters to be compatible with CSV file format standard. */
void test_fwTrending_modify_CSV_characters()
{
  int iLen, iLoop;
  string sResult;
  dyn_string dsTextInput;


  // Preparing test cases: quotes, comma, CRLF especial characters
  dynAppend(dsTextInput, ",test_comma");
  dynAppend(dsTextInput, "test\"_slash");
  dynAppend(dsTextInput, "test_new_line\n");
  dynAppend(dsTextInput, "NormalTest");


  iLen = dynlen(dsTextInput);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    sResult = fwTrending_modify_CSV_characters(dsTextInput[iLoop]);

    if( (strpos(dsTextInput[iLoop], "\"") >= 0) ||
        (strpos(dsTextInput[iLoop], "\n") >= 0) ||
        (strpos(dsTextInput[iLoop], ",")  >= 0)  )
    {
      // Check it has \" at the begin and end
      if( (strpos(sResult, "\"")   < 0) )
      {
        // Should contain at least \" at the beginning and end
        assertTrue(FALSE, "Error, it was expected \" at the beggining or the end of output: " + sResult);
        return;
      }

      // If original text has \", output should have it.
      if( (strpos(dsTextInput[iLoop], "\"") >= 0) &&
          (strpos(sResult, "\"\"")           < 0)   )
      {
        assertTrue(FALSE, "Error, original text included \", but output text didn't converted to \"\" : " + sResult);
        return;
      }
    }
    else
    {
      // If a text without \", \n, or commas has these modifiers, then it is wrong
      if( (strpos(sResult, "\"")   >= 0) &&
          (strpos(sResult, "\"\"") >= 0)   )
      {
        assertTrue(FALSE, "Error, original text: " + dsTextInput[iLoop] + " doesn't need to be converted: " + sResult);
        return;
      }
    }
  }

  // If no errors, all it is OK
  assertTrue(TRUE);

}





/* Checks if a line value is a constant number. */
void test_fwTrending_isConstantLineValue()
{
  bool bResult;
  int iLen, iLoop;
  dyn_string dsValue;


  // Prepare the tests
  dynAppend(dsValue, "#5");
  dynAppend(dsValue, "5");
  dynAppend(dsValue, "5#");


  iLen = dynlen(dsValue);
  for( iLoop = 1; iLoop <= iLen; iLoop++ )
  {
    bResult = _fwTrending_isConstantLineValue(dsValue[iLoop]);

    if( strtok(dsValue[iLoop], "#") == 0 )
    {
      // It is a constant line value, but function returns FALSE
      if( bResult == FALSE)
      {
        assertTrue(FALSE, "Error, first character is # and then, it should be constant line value, function returned FALSE");
        return;
      }
    }
    else
    {
      // It is not a constant line value, but function returns TRUE
      if ( bResult == TRUE)
      {
        assertTrue(FALSE, "Error, first character is not # and then, it should not be constant line value, function returned TRUE");
        return;
      }
    }
  }

  // If no errors, all it is OK
  assertTrue(TRUE);

}






/* Gets a constant line value. */
void test_fwTrending_getConstantLineValue()
{
  int iLoop, iLen;
  string sResult, sSource;
  dyn_string dsValue;


  // Prepare the tests
  dynAppend(dsValue, "#5");
  dynAppend(dsValue, "5");  // Invalid argument, but function doesn't detect it. It should return FALSE
  dynAppend(dsValue, "5#"); // Invalid argument, but function doesn't detect it. It should return FALSE


  iLen = dynlen(dsValue);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    sResult = _fwTrending_getConstantLineValue(dsValue[iLoop]);

    if( strtok(sResult, "#") == 0)
    {
      assertTrue(FALSE, "Error, it should return a float and not a symbol: " + sResult);
      return;
    }

    if( strtok(dsValue[iLoop], "#") == 0 )
    {
      sSource = substr(dsValue[iLoop], 1);

      if( ((float)sResult) != ((float)sSource) )
      {
        assertTrue(FALSE, "Error, source value was: " + sSource + " and returned value is: " + sResult);
        return;
      }
    }

  }

  // If no errors, all it is OK
  assertTrue(TRUE);

}







/* Checks if the provided info is an alias. */
void test_fwTrending_isAlias()
{
  string sMyDP, sDPType, sAlias;
  dyn_errClass deError;


  sMyDP   = "testDP_isAlias";
  sDPType = "FwTrendingTest";
  sAlias  = "myDP";


  // Create DP
  if( dpExists(sMyDP) == FALSE )
  {
    if( dpCreate(sMyDP, sDPType) != 0 )
    {
      deError =  getLastError();
      if( dynlen(deError) > 0 )
      {
        assertTrue(FALSE, "Error creating DP: " + sMyDP + "due to: " + getErrorText(deError) );
        return;
      }
    }
  }


  // Set alias
  if( dpSetAlias(sMyDP + ".name", sAlias) == 0 )
  {
    if( fwTrending_isAlias(sAlias) == FALSE )
    {
      assertTrue(FALSE, "Invalid value, fwTrending_isAlias() should return TRUE, and returned FALSE");
      return;
    }
  }
  else
  {
    deError =  getLastError();
    if( dynlen(deError) > 0 )
    {
      assertTrue(FALSE, "Error adding alias to DPE: " + sMyDP + ".name due to: " + getErrorText(deError) );
      return;
    }
  }


  // Remove test DP
  if( dpExists(sMyDP) == TRUE )
  {
    if( dpDelete(sMyDP) != 0 )
    {
      deError =  getLastError();
      if( dynlen(deError) > 0 )
      {
        assertTrue(FALSE, "Error removing test DP: " + sMyDP + " due to: " + getErrorText(deError) );
        return;
      }
    }
  }

  // All OK no exit error
  assertTrue(TRUE);

}





/* Checks if the provided info is a datapoint name. */
void test_fwTrending_isDpName()
{
  string sMyDP, sDPType, sAlias;
  dyn_errClass deError;


  // Prepare tests
  sMyDP   = "testDP_isDpName";
  sDPType = "FwTrendingTest";
  sAlias  = "myDP";


  // Create DP
  if( dpExists(sMyDP) == FALSE )
  {
    if( dpCreate(sMyDP, sDPType) != 0 )
    {
      deError =  getLastError();
      if( dynlen(deError) > 0 )
      {
        assertTrue(FALSE, "Error creating DP: " + sMyDP + "due to: " + getErrorText(deError) );
        return;
      }
    }
  }


  // Set alias
  if( dpSetAlias(sMyDP + ".name", sAlias) == 0 )
  {
    if( fwTrending_isDpName(sAlias) == TRUE )
    {
      assertTrue(FALSE, "Invalid value, fwTrending_isDpName() should return FALSE, and returned TRUE");
      return;
    }
  }
  else
  {
    deError =  getLastError();
    if( dynlen(deError) > 0 )
    {
      assertTrue(FALSE, "Error adding alias to DPE: " + sMyDP + ".name due to: " + getErrorText(deError) );
      return;
    }
  }


  // Remove test DP
  if( dpExists(sMyDP) == TRUE )
  {
    if( dpDelete(sMyDP) != 0 )
    {
      deError =  getLastError();
      if( dynlen(deError) > 0 )
      {
        assertTrue(FALSE, "Error removing test DP: " + sMyDP + " due to: " + getErrorText(deError) );
        return;
      }
    }
  }


  // All OK no exit error
  assertTrue(TRUE);

}






/* Checks if passed DPEs have no invalid values. */
//    supposed to: Check for DPEs storing numbers that their values are indeed numbers (finite values).
//    only checks if the value of DPE that exists is not a Nan.
//    what is DPE does not exist?

//void test_fwTrending_valuesAreValid()
//{
//  bool bResult;
//  int iLoop, iLen;
//  string sDPType;
//  dyn_int diValues;
//  dyn_string dsDPEs;
//  dyn_errClass deError;
//
//
//  // Prepare test
//  sDPType = "FwTrendingTest";
//  dynAppend(dsDPEs, "testDP1_valuesAreValid");
//  dynAppend(dsDPEs, "testDP2_valuesAreValid");
//  dynAppend(dsDPEs, "testDP3_valuesAreValid");
//  dynAppend(dsDPEs, "testDP4_valuesAreValid");
//  dynAppend(dsDPEs, "testDP5_valuesAreValid");
//
//  iLen = dynlen(dsDPEs);
//  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
//  {
//    // Create test DPs
//    if( dpExists(dsDPEs[iLoop]) == FALSE )
//    {
//      if( dpCreate(dsDPEs[iLoop], sDPType) != 0 )
//      {
//        deError =  getLastError();
//        if( dynlen(deError) > 0 )
//        {
//          assertTrue(FALSE, "Error creating DP: " + dsDPEs[iLoop] + " due to: " + getErrorText(deError) );
//          return;
//        }
//      }
//    }
//  }
//
//
//  // Set test DPs
//  if( dpSet(dsDPEs[1] + ".floatValue", log(-1),
//            dsDPEs[2] + ".floatValue", sqrt(-1),
//            dsDPEs[3] + ".floatValue", pow(-2, 0.5),
//            dsDPEs[4] + ".floatValue", -4.5,
//            dsDPEs[5] + ".floatValue", 4.5 ) != 0 )
//  {
//    deError =  getLastError();
//    if( dynlen(deError) > 0 )
//    {
//      assertTrue(FALSE, "Error setting DPE: " + dsDPEs[iLoop] + ".value" + " due to: " + getErrorText(deError) );
//      return;
//    }
//  }
//
//
//  // Checking function
//  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
//  {
//    bResult = fwTrending_valuesAreValid(makeDynString(dsDPEs[iLoop]));
//    if( iLoop < 4 )
//    {
//      if( bResult == TRUE )
//      {
//        assertTrue(FALSE, "Error, NaN value stored in DPE: " + dsDPEs[iLoop] + " reported as valid value");
//        return;
//      }
//    }
//    else
//    {
//      if( bResult == FALSE )
//      {
//        assertTrue(FALSE, "Error, valid value stored in DPE: " + dsDPEs[iLoop] + " reported as invalid value");
//        return;
//      }
//    }
//  }
//
//
//  // Remove test DPs
//  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
//  {
//    if( dpExists(dsDPEs[iLoop]) == TRUE )
//    {
//      if( dpDelete(dsDPEs[iLoop]) != 0 )
//      {
//        deError =  getLastError();
//        if( dynlen(deError) > 0 )
//        {
//          assertTrue(FALSE, "Error removing test DP: " + dsDPEs[iLoop] + " due to: " + getErrorText(deError) );
//          return;
//        }
//      }
//    }
//  }
//
//
//  // All OK no exit error
//  assertTrue(TRUE);
//}





/* Checks if the index corresponding to a specific item of  Trending Tool Plot configuration attributes (i.e. dpe names) can be exported or not.
   It accepts all the constants like fwTrending_PLOT_OBJECT_...
   Needed by unTree during import/export of trending settings. */
void test_fwTrending_isPlotTreeTagExportable()
{
  bool bResult;
  int iLen, iLoop;
  dyn_int diTagInd;


  // Preparing test
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_MODEL);                      //  1
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_TITLE);                      //  2
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_LEGEND_ON);                  //  3
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_BACK_COLOR);                 //  4
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_FORE_COLOR);                 //  5
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_DPES_X);                     //  6
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_DPES);                       //  7
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_LEGENDS_X);                  //  8
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_LEGENDS);                    //  9
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_COLORS);                     // 10
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_AXII_X);                     // 11
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_AXII);                       // 12
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_IS_TEMPLATE);                // 13
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_CURVES_HIDDEN);              // 14
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_RANGES_MIN_X);               // 15
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_RANGES_MAX_X);               // 16
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_RANGES_MIN);                 // 17
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_RANGES_MAX);                 // 18
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_TYPE);                       // 19
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_TIME_RANGE);                 // 20
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_TEMPLATE_NAME);              // 21
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_IS_LOGARITHMIC);             // 22
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_GRID);                       // 23
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_CURVE_TYPES);                // 24
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_MARKER_TYPE);                // 25
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_ACCESS_CONTROL_SAVE);        // 26
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_CONTROL_BAR_ON);             // 27
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_AXII_POS);                   // 28
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_AXII_LINK);                  // 29
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_DEFAULT_FONT);               // 30
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_CURVE_STYLE);                // 31
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_AXII_X_FORMAT);              // 32
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_AXII_Y_FORMAT);              // 33
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_LEGEND_VALUES_FORMAT);       // 34
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_LEGEND_DATE_FORMAT);         // 35
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_LEGEND_DATE_ON);             // 36
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_ALARM_LIMITS_SHOW);          // 37
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_EXT_MIN_FOR_LOG);            // 38
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_EXT_MAX_PERCENTAGE_FOR_LOG); // 39
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_EXT_UNITS_X);                // 40
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_EXT_UNITS);                  // 41
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_EXT_TOOLTIPS_X);             // 42
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_EXT_TOOLTIPS);               // 43
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_EXT_MIN_MAX_RANGE_X);        // 44
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_EXT_MIN_MAX_RANGE);          // 45
  dynAppend(diTagInd, fwTrending_PLOT_OBJECT_SAVE_TO_USER_FOLDER);        // 46
  dynAppend(diTagInd, 47);
  dynAppend(diTagInd, 48);
  dynAppend(diTagInd, 49);
  dynAppend(diTagInd, 0);
  dynAppend(diTagInd, -1);
  dynAppend(diTagInd, -2);


  iLen = dynlen(diTagInd);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    bResult = fwTrending_isPlotTreeTagExportable(diTagInd[iLoop]);

    if( (diTagInd[iLoop] > fwTrending_SIZE_PLOT_OBJECT) )
    {
      if( bResult == TRUE )
      {
        assertTrue(FALSE, "Property with index value: " + diTagInd[iLoop] + " -> Out of index(" + fwTrending_SIZE_PLOT_OBJECT + "), and function return TRUE");
        return;
      }
      else
      {
        continue;
      }
    }
    else
    {
      if( diTagInd[iLoop] <  1 )
      {
        if( bResult == TRUE )
        {
          assertTrue(FALSE, "Property with negative index value: " + diTagInd[iLoop] + " -> Invalid index, and function return TRUE");
          return;
        }
        else
        {
          continue;
        }
      }
      else
      {
        switch( diTagInd[iLoop] )
        {
          case fwTrending_PLOT_OBJECT_ACCESS_CONTROL_SAVE:
            if( bResult == TRUE )
            {
              assertTrue(FALSE, "Error property: fwTrending_PLOT_OBJECT_ACCESS_CONTROL_SAVE ("+ diTagInd[iLoop] + "), it is exportable and should not be");
              return;
            }
            break;


          default:
            if( bResult == FALSE )
            {
              assertTrue(FALSE, "Error property: " + diTagInd[iLoop] + ", should be exportable, and it is not");
              return;
            }
            break;
        }
      }
    }
  }


  // All OK no exit error
  assertTrue(TRUE);

}





/* Checks if the index corresponding to a specific item of Trending Tool Page configuration attributes (i.e. dpe names) can be exported or not.
   It accepts all the constants like fwTrending_PAGE_OBJECT_...
   Needed by unTree during import/export of trending settings. */
void test_fwTrending_isPageTreeTagExportable()
{
  bool bResult;
  int iLoop, iLen;
  dyn_int diTagInd;


  // Preparing test
  dynAppend(diTagInd, fwTrending_PAGE_OBJECT_MODEL);                    // 1
  dynAppend(diTagInd, fwTrending_PAGE_OBJECT_TITLE);                    // 2
  dynAppend(diTagInd, fwTrending_PAGE_OBJECT_NCOLS);                    // 3
  dynAppend(diTagInd, fwTrending_PAGE_OBJECT_NROWS);                    // 4
  dynAppend(diTagInd, fwTrending_PAGE_OBJECT_PLOTS);                    // 5
  dynAppend(diTagInd, fwTrending_PAGE_OBJECT_CONTROLS);                 // 6
  dynAppend(diTagInd, fwTrending_PAGE_OBJECT_PLOT_TEMPLATE_PARAMETERS); // 7
  dynAppend(diTagInd, fwTrending_PAGE_OBJECT_ACCESS_CONTROL_SAVE);      // 8
  dynAppend(diTagInd, 9);
  dynAppend(diTagInd, 10);
  dynAppend(diTagInd, 11);
  dynAppend(diTagInd, 0);
  dynAppend(diTagInd, -1);
  dynAppend(diTagInd, -2);


  iLen = dynlen(diTagInd);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    bResult = fwTrending_isPageTreeTagExportable(diTagInd[iLoop]);

    if( (diTagInd[iLoop] > fwTrending_SIZE_PAGE_OBJECT) )
    {
      if( bResult == TRUE )
      {
        assertTrue(FALSE, "Property with index value: " + diTagInd[iLoop] + " -> Out of index(" + fwTrending_SIZE_PAGE_OBJECT + "), and function return TRUE");
        return;
      }
      else
      {
        continue;
      }
    }
    else
    {
      if( diTagInd[iLoop] <  1 )
      {
        if( bResult == TRUE )
        {
          assertTrue(FALSE, "Property with negative index value: " + diTagInd[iLoop] + " -> Invalid index, and function return TRUE");
          return;
        }
        else
        {
          continue;
        }
      }
      else
      {
        switch( diTagInd[iLoop] )
        {
          case fwTrending_PAGE_OBJECT_ACCESS_CONTROL_SAVE:
            if( bResult == TRUE )
            {
              assertTrue(FALSE, "Error property: fwTrending_PAGE_OBJECT_ACCESS_CONTROL_SAVE ("+ diTagInd[iLoop] + "), it is exportable and should not be");
              return;
            }
            break;


          default:
            if( bResult == FALSE )
            {
              assertTrue(FALSE, "Error property: " + diTagInd[iLoop] + ", should be exportable, and it is not");
              return;
            }
            break;
        }
      }
    }
  }


  // All OK no exit error
  assertTrue(TRUE);

}





/* Returns the list of Trending Tool Plot configuration attributes (i.e. dpe names) corresponding to the indexes as defined in the constants definitions.
   Needed by unTree during import/export of trending settings. */
void test_fwTrending_getPlotTreeTagNames()
{
  const int iLENGTH_BASIC_OBJECT = 37;


  int iLen, iLoop;
  dyn_int diItems;
  dyn_string dsReturn, dsItems;


  // Prepare test
  dsItems = makeDynString(fwTrending_PLOT_MODEL,
                          fwTrending_PLOT_TITLE,
                          fwTrending_PLOT_LEGEND_ON,
                          fwTrending_PLOT_BACK_COLOR,
                          fwTrending_PLOT_FORE_COLOR,
                          fwTrending_PLOT_DPES_X,
                          fwTrending_PLOT_DPES,
                          fwTrending_PLOT_LEGENDS_X,
                          fwTrending_PLOT_LEGENDS,
                          fwTrending_PLOT_COLORS,
                          fwTrending_PLOT_AXII_X,
                          fwTrending_PLOT_AXII,
                          fwTrending_PLOT_IS_TEMPLATE,
                          fwTrending_PLOT_CURVES_HIDDEN,
                          fwTrending_PLOT_RANGES_MIN_X,
                          fwTrending_PLOT_RANGES_MAX_X,
                          fwTrending_PLOT_RANGES_MIN,
                          fwTrending_PLOT_RANGES_MAX,
                          fwTrending_PLOT_TYPE,
                          fwTrending_PLOT_TIME_RANGE,
                          fwTrending_PLOT_TEMPLATE_NAME,
                          fwTrending_PLOT_IS_LOGARITHMIC,
                          fwTrending_PLOT_GRID,
                          fwTrending_PLOT_CURVE_TYPES,
                          fwTrending_PLOT_MARKER_TYPE,
                          fwTrending_PLOT_ACCESS_CONTROL_SAVE,
                          fwTrending_PLOT_CONTROL_BAR_ON,
                          fwTrending_PLOT_AXII_POS,
                          fwTrending_PLOT_AXII_LINK,
                          fwTrending_PLOT_DEFAULT_FONT,
                          fwTrending_PLOT_CURVE_STYLE,
                          fwTrending_PLOT_AXII_X_FORMAT,
                          fwTrending_PLOT_AXII_Y_FORMAT,
                          fwTrending_PLOT_LEGEND_VALUES_FORMAT,
                          fwTrending_PLOT_LEGEND_DATE_FORMAT,
                          fwTrending_PLOT_LEGEND_DATE_ON,
                          fwTrending_PLOT_ALARM_LIMITS_SHOW);

  diItems = makeDynInt( 1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
                       11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                       21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                       31, 32, 33, 34, 35, 36, 37);


  // Starting the test
  iLen = dynlen(dsItems);
  if( iLen != iLENGTH_BASIC_OBJECT )
  {
    assertTrue(FALSE, "Error preparing elements for the test, number of string elements wrong: " + iLen + ", it should be: " + iLENGTH_BASIC_OBJECT);
    return;
  }

  iLen = dynlen(diItems);
  if( iLen != iLENGTH_BASIC_OBJECT )
  {
    assertTrue(FALSE, "Error preparing elements for the test, number of integer elements wrong: " + iLen + ", it should be: " + iLENGTH_BASIC_OBJECT);
    return;
  }


  dsReturn = fwTrending_getPlotTreeTagNames();


  // Check number of returned items
  iLen = dynlen(dsReturn);
  if( iLen != iLENGTH_BASIC_OBJECT )
  {
    assertTrue(FALSE, "Error, expected at least " + iLENGTH_BASIC_OBJECT + " entries in the plot object, and function returned: " + iLen + " items");
    return;
  }


  // Check returned values
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    if( dsReturn[diItems[iLoop]] != dsItems[iLoop] )
    {
      assertTrue(FALSE, "Returned item (" + diItems[iLoop] + ") with value: " + dsReturn[diItems[iLoop]] + " should have a value: " + dsItems[iLoop]);
      return;
    }
  }


  // All OK no exit error
  assertTrue(TRUE);

}





/* Returns the list of Trending Tool Page configuration  attributes (i.e. dpe names) corresponding to the indexes
   as defined in the constants definitions.
   Needed by unTree during import/export of trending settings. */
void test_fwTrending_getPageTreeTagNames()
{
  const int iLENGTH_BASIC_OBJECT = 8;


  int iLoop, iLen;
  dyn_int diItems;
  dyn_string dsReturn, dsItems;


  //Prepare test
  dsItems = makeDynString(fwTrending_PAGE_MODEL,
                          fwTrending_PAGE_TITLE,
                          fwTrending_PAGE_NCOLS,
                          fwTrending_PAGE_NROWS,
                          fwTrending_PAGE_PLOTS,
                          fwTrending_PAGE_CONTROLS,
                          fwTrending_PAGE_PLOT_TEMPLATE_PARAMETERS,
                          fwTrending_PAGE_ACCESS_CONTROL_SAVE);

  diItems = makeDynInt(1, 2, 3, 4, 5, 6, 7, 8);


  // Starting the test
  iLen = dynlen(dsItems);
  if( iLen != iLENGTH_BASIC_OBJECT )
  {
    assertTrue(FALSE, "Error preparing elements for the test, number of string elements wrong: " + iLen + ", it should be: " + iLENGTH_BASIC_OBJECT);
    return;
  }

  iLen = dynlen(diItems);
  if( iLen != iLENGTH_BASIC_OBJECT )
  {
    assertTrue(FALSE, "Error preparing elements for the test, number of integer elements wrong: " + iLen + ", it should be: " + iLENGTH_BASIC_OBJECT);
    return;
  }


  dsReturn = fwTrending_getPageTreeTagNames();


  // Check number of returned items
  iLen = dynlen(dsReturn);
  if( iLen != iLENGTH_BASIC_OBJECT )
  {
    assertTrue(FALSE, "Error, expected at least " + iLENGTH_BASIC_OBJECT + " entries in the page object, and function returned: " + iLen + " items");
    return;
  }


  // Check returned values
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    if( dsReturn[diItems[iLoop]] != dsItems[iLoop] )
    {
      assertTrue(FALSE, "Returned item (" + diItems[iLoop] + ") with value: " + dsReturn[diItems[iLoop]] + " should have a value: " + dsItems[iLoop]);
      return;
    }
  }


  // All OK no exit error
  assertTrue(TRUE);

}





/* Return the element from the list with the maximum value. */
void test_fwTrending_getMax()
{
  int iLoop, iLoopInt, iLen;
  float fRet, fMaxValue;
  dyn_dyn_float ddfList;


  int iListLen = 10;
  int iRandLen = 10;


  // Prepare the tests
  for( iLoop = 1 ; iLoop <= iListLen ; iLoop++ )
  {
    for( iLoopInt = 1 ; iLoopInt <= iRandLen ; iLoopInt++ )
    {
      dynAppend(ddfList[iLoopInt], rand());
    }
  }


  // Start the tests
  fRet = _fwTrending_getMax(ddfList);


  for( iLoop = 1 ; iLoop <= iListLen ; iLoop++ )
  {
    fMaxValue = dynMax(ddfList[iLoop]);
    if( fMaxValue > fRet )
    {
      assertTrue(FALSE, "Error, max value found by the function was: " + fRet + ", test found following max value: " + fMaxValue);
      return;
    }
  }


  // All OK, no exit error
  assertTrue(TRUE);

}





/* Return the element from the list with the minimum value. */
void test_fwTrending_getMin()
{
  int iLoop, iLoopInt, iLen;
  float fRet, fMinValue;
  dyn_dyn_float ddfList;


  int iListLen = 10;
  int iRandLen = 10;


  // Prepare the tests
  for( iLoop = 1 ; iLoop <= iListLen ; iLoop++)
  {
    for( iLoopInt = 1 ; iLoopInt <= iRandLen ; iLoopInt++ )
    {
      dynAppend(ddfList[iLoopInt], rand());
    }
  }


  // Start the tests
  fRet = _fwTrending_getMin(ddfList);


  for( iLoop = 1 ; iLoop <= iListLen ; iLoop++ )
  {
    fMinValue = dynMin(ddfList[iLoop]);
    if( fMinValue < fRet )
    {
      assertTrue(FALSE, "Error, min value found by the function was: " + fRet + ", test found following min value: " + fMinValue);
      return;
    }
  }


  // All OK, no exit error
  assertTrue(TRUE);

}




/* Converts a string which is seperated by the fwTrending_CONTENT_DIVIDER into a dyn_string.
   The resulting dyn_string will always have at least fwTrending_TRENDING_MAX_CURVE entries. */
void test_fwTrending_convertStringToDyn()
{
  int iLoop, iList, iLenSplit, iInputLen;
  string sValue;
  dyn_string dsValueList, dsValueInputs, dsSplit, exceptionInfo;


  // Prepare tests
  dynAppend(dsValueInputs, "one" + fwTrending_CONTENT_DIVIDER + "two" + fwTrending_CONTENT_DIVIDER + "3");
  dynAppend(dsValueInputs, "one" + fwTrending_CONTENT_DIVIDER + fwTrending_CONTENT_DIVIDER + "two");
  dynAppend(dsValueInputs, "");


  // Start the tests
  iInputLen = dynlen(dsValueInputs);
  for( iLoop = 1 ; iLoop <= iInputLen ; iLoop++ )
  {
    try
    {
      fwTrending_convertStringToDyn(dsValueInputs[iLoop], dsValueList, exceptionInfo);
    }

    finally
    {
      if( dynlen(exceptionInfo) > 0 )
      {
        assertTrue(FALSE, "Error calling fwTrending_convertStringToDyn(), due to: " + exceptionInfo);
        return;
      }
    }


    if( dynlen(dsValueList) != fwTrending_TRENDING_MAX_CURVE )
    {
      assertTrue(FALSE, "Error number of output entries invalid: " + dynlen(dsValueList) + ", and we expected: " + fwTrending_TRENDING_MAX_CURVE);
      return;
    }


    dsSplit   = strsplit(dsValueInputs[iLoop], fwTrending_CONTENT_DIVIDER);
    iLenSplit = dynlen(dsSplit);
    for( iList = 1 ; iList <= iLenSplit ; iList++ )
    {
      if( dsSplit[iList] != dsValueList[iList] )
      {
        assertTrue(FALSE, "Error comparing values lists. Function: " + dsValueList[iList] + ", test: " + dsSplit[iList]);
        return;
      }
    }


    for( iList = iLenSplit + 1 ; iList <= fwTrending_TRENDING_MAX_CURVE ; iList++ )
    {
      if( dsValueList[iList] != "" )
      {
        assertTrue(FALSE, "Error, function returned more not empty arguments: " + dsValueList[iList]);
        return;
      }
    }

  }


  // All OK, no exit error
  assertTrue(TRUE);

}





/* Converts a dyn_string to a string which is seperated by the fwTrending_CONTENT_DIVIDER.
   The resulting string will always have at least fwTrending_TRENDING_MAX_CURVE dividers.

   if dyn_string has only one element, it does not return 8 size array? */
void test_fwTrending_convertDynToString()
{
  int iLoop, iLoopInt, iLen, iInputLen;
  string sValue;
  dyn_string dsSplit, dsValueInputs, exceptionInfo;
  dyn_dyn_string ddsValueList;


  // Prepare the test
  dynAppend(ddsValueList[1], "one");
  dynAppend(ddsValueList[1], "two");
  dynAppend(ddsValueList[1], "3");

  dynAppend(ddsValueList[2], "one");
  dynAppend(ddsValueList[2], "");
  dynAppend(ddsValueList[2], "two");

  dynAppend(ddsValueList[3], "one");


  // Start test
  iInputLen = dynlen(ddsValueList);
  for( iLoop = 1; iLoop <= iInputLen; iLoop++ )
  {
    try
    {
      fwTrending_convertDynToString(ddsValueList[iLoop], sValue, exceptionInfo);
    }

    finally
    {
      if( dynlen(exceptionInfo) > 0 )
      {
        assertTrue(FALSE, "Error calling function fwTrending_convertDynToString(), returned following error: " + exceptionInfo);
        return;
      }
    }

    // Check output arguments number
    dsSplit = strsplit(sValue, fwTrending_CONTENT_DIVIDER);
    iLen    = dynlen(dsSplit);
    if( iLen > fwTrending_TRENDING_MAX_CURVE )
    {
      assertTrue(FALSE, "Error more output arguments: " + iLen + " than expected: " + fwTrending_TRENDING_MAX_CURVE);
      return;
    }


    // Check same arguments in the test items and the output
    iLen = dynlen(ddsValueList[iLoop]);
    for( iLoopInt = 1 ; iLoopInt <= iLen ; iLoopInt++ )
    {
      if( dsSplit[iLoopInt] != ddsValueList[iLoop][iLoopInt] )
      {
        assertTrue(FALSE, "Error checking the defined vs the output. Item: " + iLoopInt + " in the output is different from the input: " + ddsValueList[iLoop]);
        return;
      }
    }

    // Check that arguments not defined are empty
    for( iLoopInt = iLen + 1 ; iLoopInt <= fwTrending_TRENDING_MAX_CURVE ; iLoopInt++ )
    {
      if( (iLen > 1)                &&
          (dsSplit[iLoopInt] != "")   )
      {
        assertTrue(FALSE, "Error output field: " + iLoopInt + " should be an empty string, but it is this value: " + dsSplit[iLoopInt]);
        return;
      }
    }
  }


  // All OK, no exit error
  assertTrue(TRUE);

}





/* Checks a given curve name is a legal data point name. Template parameter markers are allowed.
   dpIsLegalName() changed to nameCheck because it is not used anymore */
void test_fwTrending_checkCurveName()
{
  bool bValid;
  int iLen, iLoop;
  string sMyDP, sDPType, sModified;
  dyn_string dsDPEs, exceptionInfo;
  dyn_errClass deError;



  // Preparing the test
  sMyDP     = "testDP";
  sDPType   = "FwTrendingTest";
  sModified = getSystemName();

  strreplace(sModified, "_", fwTrending_TEMPLATE_OPEN);
  dynAppend(dsDPEs, getSystemName() + sMyDP + ".name");                                                        // OK
  dynAppend(dsDPEs, getSystemName() + fwTrending_TEMPLATE_OPEN + sMyDP + fwTrending_TEMPLATE_CLOSE + ".name"); // OK
  dynAppend(dsDPEs, getSystemName() + fwTrending_TEMPLATE_OPEN + sMyDP + ".name");                             // Error
  dynAppend(dsDPEs, getSystemName() + sMyDP + fwTrending_TEMPLATE_CLOSE + ".name");                            // Error
  dynAppend(dsDPEs, sModified       + sMyDP + ".name");                                                        // Error


  // Create DP
  if( dpExists(sMyDP) == FALSE )
  {
    if( dpCreate(sMyDP, sDPType) != 0 )
    {
      deError =  getLastError();
      if( dynlen(deError) > 0 )
      {
        assertTrue(FALSE, "Error creating DP: " + sMyDP + "due to: " + getErrorText(deError) );
        return;
      }
    }
  }


  // Start the test
  iLen = dynlen(dsDPEs);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    try
    {
      fwTrending_checkCurveName(dsDPEs[iLoop], bValid, exceptionInfo);
    }

    finally
    {
      switch( iLoop )
      {
        case 1:
        case 2:
          if( dynlen(exceptionInfo) > 0 )
          {
            assertTrue(FALSE, "Error folling curve name is valid and failled: " + dsDPEs[iLoop] + " due to: " + exceptionInfo);
            dynClear(exceptionInfo);
            return;
          }
          break;


        case 3:
        case 4:
        case 5:
          if( dynlen(exceptionInfo) == 0 )
          {
            assertTrue(FALSE, "Error following curve name is invalid, and should create an exception in the funcion: " + dsDPEs[iLoop]);
            return;
          }
          break;


        default:
          assertTrue(FALSE, "Error, this part of code of the unit test, should never run");
          return;
          break;
      }
    }
  }


  // All OK, no exit error
  assertTrue(TRUE);

}





/* Converts framework marker type ((un)filled circle/none) to a PVSS marker type. */
void test_fwTrending_convertFrameworkToPvssMarkerType()
{
  int iLoop, iLen, iPvssType;
  dyn_int diFrameworks;
  dyn_string exceptionInfo;


  // Prepare test
  dynAppend(diFrameworks, fwTrending_MARKER_TYPE_FILLED_CIRCLE);
  dynAppend(diFrameworks, fwTrending_MARKER_TYPE_UNFILLED_CIRCLE);
  dynAppend(diFrameworks, fwTrending_MARKER_TYPE_NONE);
  dynAppend(diFrameworks, 1231231); // random number


  // Start test
  iLen = dynlen(diFrameworks);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    try
    {
      _fwTrending_convertFrameworkToPvssMarkerType(diFrameworks[iLoop], iPvssType, exceptionInfo);
    }

    finally
    {
      if( dynlen(exceptionInfo) > 0 )
      {
        if( iLoop != 4 ) //Error test case
        {
          assertTrue(FALSE, "Error calling function _fwTrending_convertFrameworkToPvssMarkerType() with argument: " + diFrameworks[iLoop] + " returned an exception: " + exceptionInfo);
          return;
        }
      }
    }

  }

  // All OK, no exit error
  assertTrue(TRUE);

}





/* Creates a page data point. The page configuration
   is initialised to default values. */
void test_fwTrending_newPage()
{
  string sPageName;
  dyn_errClass deError;


  // Prepare test
  sPageName = "test";


  // Start test
  if( dpExists(sPageName) == FALSE )
	 {
    fwTrending_newPage(sPageName);

    if( dpExists(sPageName) == FALSE )
    {
      assertTrue(FALSE, "Error, created Trend Page: " + sPageName + " doesn't exist");
      return;
    }
    else
    {
      if( dpDelete(sPageName) != 0 )
      {
        deError =  getLastError();
        if( dynlen(deError) > 0 )
        {
          assertTrue(FALSE, "Error removing test DP: " + sPageName + " due to: " + getErrorText(deError) );
          return;
        }
      }
    }

  }


  // All OK, no exit error
  assertTrue(TRUE);

}





/* Creates a page data point.
   The page configuration is initialised to default values.
   Checks are carried out if the name is valid or if it already exists.
   The only option is to write DPE as ":name"?, otherwise doesnt pass nameCheck function moreover, function dpIsIlegalName replaced by nameCheck */
void test_fwTrending_createPage()
{
  int iLoop, iLen;
  dyn_string dsPageNames, exceptionInfo;
  dyn_errClass deError;


  // Prepare test
  dynAppend(dsPageNames, "test");        // OK test
  dynAppend(dsPageNames, ":test_page");   // ERROR test
  dynAppend(dsPageNames, ":****");        // ERROR test


  // Start tests
  iLen = dynlen(dsPageNames);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    try
    {
      fwTrending_createPage(dsPageNames[iLoop], exceptionInfo);
    }

    finally
    {
      switch( iLoop )
      {
        case 1: // OK tests
          if( dynlen(exceptionInfo) > 0 )
          {
            assertTrue(FALSE, "Error calling function fwTrending_createPage() with argument: " + dsPageNames[iLoop] + " returned exception: " + exceptionInfo);
            return;
          }
          else
          {
            // Remove the DP
            if( dpDelete(dsPageNames[iLoop]) != 0 )
            {
              deError =  getLastError();
              if( dynlen(deError) > 0 )
              {
                assertTrue(FALSE, "Error removing test DP: " + dsPageNames[iLoop] + " due to: " + getErrorText(deError) );
                return;
              }
            }
          }
          break;


        case 2: // Error tests
        case 3:
          if( dynlen(exceptionInfo) == 0 )
          {
            assertTrue(FALSE, "Error creating this test page name: " + dsPageNames[iLoop] + " should trigger an exception due to an invalid name");
            return;
          }
          break;

        default:
          assertTrue(FALSE, "Error, this part of code of the unit test, should never run");
          return;
          break;
      }
    }
  }


  // All OK, no exit error
  assertTrue(TRUE);

}






/* Creates a plot data point.
   The plot configuration is initialised to default values. */
void test_fwTrending_newPlot()
{
  int iLoop, iLen;
  dyn_string dsPlotNames, exceptionInfo;
  dyn_errClass deError;


  // Prepare test
  dynAppend(dsPlotNames, "test");         // OK test
  dynAppend(dsPlotNames, ":test_plot");   // ERROR test
  dynAppend(dsPlotNames, ":****");        // ERROR test


  // Start tests
  iLen = dynlen(dsPlotNames);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    try
    {
      fwTrending_newPlot(dsPlotNames[iLoop]);
    }

    finally
    {
      switch( iLoop )
      {
        case 1: // OK tests
          if( dpExists(dsPlotNames[iLoop]) == FALSE )
          {
            assertTrue(FALSE, "Error creating new plot: " + dsPlotNames[iLoop] + " unknown reasons, check the log");
            return;
          }
          else
          {
            // Remove the DP
            if( dpDelete(dsPlotNames[iLoop]) != 0 )
            {
              deError =  getLastError();
              if( dynlen(deError) > 0 )
              {
                assertTrue(FALSE, "Error removing test DP: " + dsPlotNames[iLoop] + " due to: " + getErrorText(deError) );
                return;
              }
            }
          }
          break;


        case 2: // Error tests
        case 3:
          if( dpExists(dsPlotNames[iLoop]) == TRUE )
          {
            assertTrue(FALSE, "Error creating this test plot name: " + dsPlotNames[iLoop] + " should trigger an exception due to an invalid name");
            return;
          }
          break;

        default:
          assertTrue(FALSE, "Error, this part of code of the unit test, should never run");
          return;
          break;
      }
    }
  }


  // All OK, no exit error
  assertTrue(TRUE);

}






/* Creates a plot data point.
   Checks are carried out if the name is valid or if it already exists.
   The only option is to write DPE as ":name"?, otherwise doesnt pass nameCheck function
   moreover, function dpIsIlegalName replaced by nameCheck */
void test_fwTrending_createPlot()
{
  int iLoop, iLen;
  dyn_string dsPlotNames, exceptionInfo;


  // Prepara the tests
  dynAppend(dsPlotNames, "test");        // OK test
  dynAppend(dsPlotNames, ":test_plot");  // Error test
  dynAppend(dsPlotNames, ":****");       // Error test


  // Start tests
  iLen = dynlen(dsPlotNames);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    try
    {
      fwTrending_createPlot(dsPlotNames[iLoop], exceptionInfo);
    }

    finally
    {
      switch( iLoop )
      {
        case 1: // OK tests
          if( dynlen(exceptionInfo) > 0 )
          {
            assertTrue(FALSE, "Error calling function fwTrending_createPlot() with argument: " + dsPlotNames[iLoop] + " returned exception: " + exceptionInfo);
            return;
          }
          else
          {
            // Remove the DP
            if( dpDelete(dsPlotNames[iLoop]) != 0 )
            {
              deError =  getLastError();
              if( dynlen(deError) > 0 )
              {
                assertTrue(FALSE, "Error removing test DP: " + dsPlotNames[iLoop] + " due to: " + getErrorText(deError) );
                return;
              }
            }
          }
          break;


        case 2: // Error tests
        case 3:
          if( dynlen(exceptionInfo) == 0 )
          {
            assertTrue(FALSE, "Error creating this test plot name: " + dsPlotNames[iLoop] + " should trigger an exception due to an invalid name");
            return;
          }
          break;

        default:
          assertTrue(FALSE, "Error, this part of code of the unit test, should never run");
          return;
          break;
      }
    }
  }


  // All OK, no exit error
  assertTrue(TRUE);

}







/* Decodes time from seconds to days, hours and minutes. */
void test_fwTrending_decodeTime()
{
  const int iDAY_TO_SECONDS    = 86400;
  const int iHOUR_TO_SECONDS   = 3600;
  const int iMINUTE_TO_SECONDS = 60;


  int iRetDays, iRetHours, iRetMinutes;
  int iDays, iHours, iMinutes, iSeconds;


  // Prepare the test
  iDays    = 1;
  iHours   = 1;
  iMinutes = 1;

  iSeconds = (iDAY_TO_SECONDS    * iDays)  +
             (iHOUR_TO_SECONDS   * iHours) +
             (iMINUTE_TO_SECONDS * iMinutes);

  // Start the test
  fwTrending_decodeTime(iSeconds, iRetDays, iRetHours, iRetMinutes);

  if( (iRetDays    == iDays)    &&
      (iRetHours   == iHours)   &&
      (iRetMinutes == iMinutes)   )
  {
    assertTrue(TRUE);
  }
  else
  {
    assertTrue(FALSE, "Error calculated value in the function: " + iRetDays + "d " + iRetHours + "h " + iRetMinutes + "m doesn't match the expected one: 1d 1h 1m");
    return;
  }

}





/* Deletes a given page data point. */
void test_fwTrending_deletePage()
{
  string sMyDP, sDPType;
  dyn_string exceptionInfo;
  dyn_errClass deError;


  // Prepare test
  sMyDP   = "testDeleteDP";
  sDPType = "FwTrendingPage";


  // Start test
  // Create page to remove
  try
  {
    fwTrending_createPage(sMyDP, exceptionInfo);
  }

  finally
  {
    if( dynlen(exceptionInfo) > 0 )
    {
      assertTrue(FALSE, "Error creating page: " + sMyDP + " due to: " + exceptionInfo);
      return;
    }
  }


  // Remove page
  if( dpExists(sMyDP) )
  {
    try
    {
      fwTrending_deletePage(sMyDP, exceptionInfo);
    }

    finally
    {
      if( dynlen(exceptionInfo) > 0 )
      {
        assertTrue(FALSE, "Error removing DP page: " + sMyDP + " due to: " + exceptionInfo);
        return;
      }
      if( dpExists(sMyDP) == FALSE )
      {
        assertTrue(TRUE);
      }
      else
      {
        assertTrue(FALSE, "Error, page DP: " + sMyDP + " was removed, but still exists, no error in the exceptionInfo");
        return;
      }
    }
  }
  else
  {
    assertTrue(FALSE, "Error page DP doesn't exist: " + sMyDP + " and should exist");
    return;
  }

}





/* Deletes a given plot data point. */
void test_fwTrending_deletePlot()
{
  string sMyDP, sDPType;
  dyn_string exceptionInfo;


  // Prepare the test
  sMyDP   = "testDeleteDP";
  sDPType = "FwTrendingPlot";


  // Start test
  // Create plot to remove
  try
  {
    fwTrending_createPlot(sMyDP, exceptionInfo);
  }

  finally
  {
    if( dynlen(exceptionInfo) > 0 )
    {
      assertTrue(FALSE, "Error creating plot: " + sMyDP + " due to : " + exceptionInfo);
      return;
    }
  }

  // Remove plot
  if( dpExists(sMyDP) )
  {
    try
    {
      fwTrending_deletePlot(sMyDP, exceptionInfo);
    }

    finally
    {
      if( dynlen(exceptionInfo) > 0 )
      {
        assertTrue(FALSE, "Error removing DP plot: " + sMyDP + " due to: " + exceptionInfo);
        return;
      }

      if( dpExists(sMyDP) == FALSE )
      {
        assertTrue(TRUE);
      }
      else
      {
        assertTrue(FALSE, "Error, plot DP: " + sMyDP + " was removed, but still exists, no error in the exceptionInfo");
        return;
      }
    }
  }
  else
  {
    assertTrue(FALSE, "Error plot DP doesn't exist: " + sMyDP + " and should exist");
    return;
  }

}





/* Gets all the page DP names  that match the given pattern. */
void test_fwTrending_getPageDpNames()
{
  int iLoop, iLen, iLenOutput;
  string sPattern;
  dyn_string dsPageDpNames, dsOutput, exceptionInfo;
  dyn_errClass deError;


  // Prepare test
  dynAppend(dsPageDpNames, "testA_B-1");    // OK test
  dynAppend(dsPageDpNames, "testA_B-2");    // OK test
  dynAppend(dsPageDpNames, "testA_B-3");    // OK test
  dynAppend(dsPageDpNames, "testA_B-4");    // OK test
  dynAppend(dsPageDpNames, "testA_B-5");    // OK test
  dynAppend(dsPageDpNames, "testA_B-6");    // OK test


  // Create test DPs
  sPattern = "*testA_B-*";
  iLen     = dynlen(dsPageDpNames);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    try
    {
      fwTrending_createPage(dsPageDpNames[iLoop], exceptionInfo);
    }

    finally
    {
      if( dynlen(exceptionInfo) > 0 )
      {
        assertTrue(FALSE, "Error calling function fwTrending_createPlot() with argument: " + dsPageDpNames[iLoop] + " returned exception: " + exceptionInfo);
        return;
      }
    }
  }


  // Start the test
  try
  {
    fwTrending_getPageDpNames(dsOutput, exceptionInfo, sPattern);
  }

  finally
  {
    iLenOutput = dynlen(dsOutput);
    if( dynlen(exceptionInfo) > 0 )
    {
      assertTrue(FALSE, "Error calling the function fwTrending_getPageDpNames() with pattern argument: " + sPattern + " returned exception: " + exceptionInfo);
      return;
    }
    else
    {
      if( iLenOutput == iLen )
      {
        // Checking arguments
        for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
        {
          if( dynContains(dsOutput, getSystemName() + dsPageDpNames[iLoop] ) < 1 )
          {
            assertTrue(FALSE, "Error checking output result. Expected following DP in the output: " + dsPageDpNames[iLoop] + " and it is not present");
            return;
          }
        }
      }
      else
      {
        assertTrue(FALSE, "Error function returned " + iLenOutput + " arguments, it was expected: " + iLen);
        return;
      }
    }
  }


  // Remove test DPs
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    // Remove the DP
    if( dpDelete(dsPageDpNames[iLoop]) != 0 )
    {
      deError =  getLastError();
      if( dynlen(deError) > 0 )
      {
        assertTrue(FALSE, "Error removing test DP: " + dsPageDpNames[iLoop] + " due to: " + getErrorText(deError) );
        return;
      }
    }
  }


  // All OK, no exit error
  assertTrue(TRUE);

}





/* Gest all the page DP names on all systems. */
void test_fwTrending_getAllPages()
{
  int iLoop, iLen, iAlreadyLen, iLenExpected;
  dyn_string dsPageDpNames, dsPageTest, dsPagesAlready, exceptionInfo;
  dyn_errClass deError;


  // Prepare test
  // Get current test pages
  dsPagesAlready = dpNames("*", "FwTrendingPage");
  iAlreadyLen    = dynlen(dsPagesAlready);

  // Add custom pages
  dynAppend(dsPageTest, "testA_B_1");    // OK test
  dynAppend(dsPageTest, "testA_B_2");    // OK test
  dynAppend(dsPageTest, "testA_B_3");    // OK test
  dynAppend(dsPageTest, "testA_B_4");    // OK test
  dynAppend(dsPageTest, "testA_B_5");    // OK test
  dynAppend(dsPageTest, "testA_B_6");    // OK test

  iLen = dynlen(dsPageTest);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    try
    {
      fwTrending_createPage(dsPageTest[iLoop], exceptionInfo);
    }

    finally
    {
      if( dynlen(exceptionInfo) > 0 )
      {
        assertTrue(FALSE, "Error calling function fwTrending_createPage() with argument: " + dsPageTest[iLoop] + " returned exception: " + exceptionInfo);
        return;
      }
      else
      {
        dynAppend(dsPagesAlready, getSystemName() + dsPageTest[iLoop]);
      }
    }
  }


  // Start test
  try
  {
    fwTrending_getAllPages(dsPageDpNames, exceptionInfo);
  }

  finally
  {
    if( dynlen(exceptionInfo) > 0 )
    {
      assertTrue(FALSE, "Error calling function fwTrending_getAllPages(), it returned an exception: " + exceptionInfo);
      return;
    }
    else
    {
      // Check number of results
      iLen         = dynlen(dsPageDpNames);
      iLenExpected = dynlen(dsPagesAlready);
      if( iLenExpected != iLen )
      {
        assertTrue(FALSE, "Error number of trend page DPs found: " + iLen + " different from expected: " + iLenExpected);
        return;
      }

      // Check content of the results
      for( iLoop = 1 ; iLoop <= iLenExpected ; iLoop++ )
      {
        if( dynContains(dsPageDpNames, dsPagesAlready[iLoop]) < 1 )
        {
          assertTrue(FALSE, "Error trend page expected: " + dsPagesAlready[iLoop] + " doesn't exist on the array returned by the function: " + dsPageDpNames);
          return;
        }
      }
    }
  }


  // Remove the test page DPs
  iLen = dynlen(dsPageTest);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    if( dpDelete(dsPageTest[iLoop]) != 0 )
    {
      deError =  getLastError();
      if( dynlen(deError) > 0 )
      {
        assertTrue(FALSE, "Error removing test trending page DP: " + dsPageTest[iLoop] + " due to: " + getErrorText(deError) );
        return;
      }
    }
  }


  // All OK, no exit error
  assertTrue(TRUE);

}




// /* Gets all the plot DP names
//    that match the given pattern. */
// void test_fwTrending_getPlotDpNames()
// {
//   dyn_string dsExceptionInfo;
//   string sPattern = "*test*";
//   dyn_string dsPlotDpNames;
//
//   fwTrending_getPlotDpNames(dsPlotDpNames, dsExceptionInfo, sPattern);
//   assertTrue(dsPlotDpNames == dpNames(sPattern, fwTrending_PLOT));
// }
//
//
// /* Gets all the plot DP names on all systems. */
// void test_fwTrending_getAllPlots()
// {
//   dyn_string dsExceptionInfo;
//   string sPattern = "*:*";
//   dyn_string dsPlotDpNames;
//
//   fwTrending_getAllPlots(dsPlotDpNames, dsExceptionInfo);
//   assertTrue(dynlen(dsPlotDpNames) == dynlen(dpNames(sPattern, fwTrending_PLOT)));
// }
//
//
// /* Reads the relevant plot name from the dyn_string
//    containing a list of the plots on a page by
//    giving the column and row of the plot. */
// void test_fwTrending_getColRow()
// {
//   string sRetPlot;
//   dyn_string dsPlots;
//
//   for (int iPlot = 1; iPlot <= fwTrending_MAX_ROWS*fwTrending_MAX_COLS; iPlot++)
//   {
//     dynAppend(dsPlots, "Plot_" +  iPlot);
//   }
//   assertTrue(strlen(fwTrending_getColRow(dsPlots, fwTrending_MAX_COLS/2, fwTrending_MAX_ROWS/2)) > 0);
//   assertTrue(strlen(fwTrending_getColRow(dsPlots, fwTrending_MAX_COLS + 1, fwTrending_MAX_ROWS + 1)) == 0);
// }
//
//
// /* Returns two lists of integers representing certain
//    indexes of the curveData object and the plotData object.
//    All the curveData object indexes are returned,
//    and the corresponding indexes of the plotData
//    object where the curveData should be stored are
//    also returned. */
// void test_fwTrending_getCurveObjectIndexes()
// {
//   int iRetLen = 8;
//   dyn_int diCurveObjectIndices;
//   dyn_int diPlotObjectIndices;
//   dyn_string dsExceptionInfo;
//
//   int iRet = _fwTrending_getCurveObjectIndexes(diCurveObjectIndices, diPlotObjectIndices, dsExceptionInfo);
//
//   assertTrue((iRet == iRetLen) && (dynlen(diPlotObjectIndices) == iRetLen));
// }
//
//
// /* Gets the amount of days, hours minutes from
//    a specified time range (in s) to be shown
//    on the trend. */
// void test_fwTrending_getDaysHoursMinutesFromTimeRange()   //the same as fwTrending_decodeTime?
// {
//   int iRetDays;
//   int iRetHours;
//   int iRetMinutes;
//   dyn_string dsExceptionInfo;
//   int iDays = 1;
//   int iHours = 1;
//   int iMinutes = 1;
//
//   int iTimeRange = fwTrending_SECONDS_IN_ONE_DAY*iDays + fwTrending_SECONDS_IN_ONE_HOUR*iHours + fwTrending_SECONDS_IN_ONE_MINUTE*iMinutes;
//
//   fwTrending_getDaysHoursMinutesFromTimeRange(iTimeRange, iRetDays, iRetHours, iRetMinutes, dsExceptionInfo);
//
//   assertTrue(iRetDays == iDays && iRetHours == iHours && iRetMinutes == iMinutes);
// }
//
//
// /* Reads all the page configuration
//    from a page datapoint. */
// void test_fwTrending_getPage()
// {
//   string sPageDp = "test_page";
//   dyn_dyn_string ddsPageData;
//   dyn_string dsExceptionInfo;
//   int iRetLen = 8;
//
//   fwTrending_getPage(sPageDp, ddsPageData, dsExceptionInfo);
//   assertTrue(dynlen(dsExceptionInfo) == 0 && dynlen(ddsPageData) == iRetLen);
//
//   sPageDp = "non_existing_dp";
//   fwTrending_getPage(sPageDp, ddsPageData, dsExceptionInfo);
//   assertTrue(dynlen(dsExceptionInfo) > 0);
// }
//
//
// /* Reads all the plot configuration
//    from a plot datapoint. */
// void test_fwTrending_getPlot()
// {
//   string sPlotDp = "test_plot";
//   dyn_dyn_string ddsPlotData;
//   dyn_string dsExceptionInfo;
//   int iRetLen = 37;
//
//   fwTrending_getPlot(sPlotDp, ddsPlotData, dsExceptionInfo);
//   assertTrue(dynlen(dsExceptionInfo) == 0 && dynlen(ddsPlotData) == iRetLen);
//
//   sPlotDp = "non_existing_dp";
//   fwTrending_getPage(sPlotDp, ddsPlotData, dsExceptionInfo);
//   assertTrue(dynlen(dsExceptionInfo) > 0);
// }
//
//
//
// /*  Gets the number of curves that
//     are defined in a plot configuration
//     data point. */
// void test_fwTrending_getNumberOfCurves()
//what is dp does not exist? function fwTrending_getNumberOfCurves()
//does not cover that and an error occurs.
// {
//   string sPlotDp = "test_plot";
//   int iNumberOfCurves;
//   dyn_string dsExceptionInfo;
//   fwTrending_getNumberOfCurves(sPlotDp, iNumberOfCurves, dsExceptionInfo);
//   assertTrue((dynlen(dsExceptionInfo) == 0) && (iNumberOfCurves >= 0));
//
//   sPlotDp = "no_test_plot";
//   dynClear(dsExceptionInfo);
//   if (dpExists(sPlotDp))
//   {
//     fwTrending_getNumberOfCurves(sPlotDp, iNumberOfCurves, dsExceptionInfo);
//     assertTrue(dynlen(dsExceptionInfo) > 0);
//   }
//   else
//   {
//     assertTrue(TRUE);
//   }
// }
//
//
// /* Gets the list of plot dps that belong to a page. */
// void test_fwTrending_getPagePlotDps()
//what if page dp does not exist? error occurs and the function
//fwTrending_getPagePlotDps() does not cover the case.
// {
//   dyn_string dsPageNames;
//   dyn_string dsPlotDps;
//   dyn_string dsExceptionInfo;
//   dyn_dyn_string ddsPageData;
//   int iLen;
//
//   dynAppend(dsPageNames, "CaVPage");
//   dynAppend(dsPageNames, "test_page");
//   dynAppend(dsPageNames, "no_test_page");
//
//   iLen = dynlen(dsPageNames);
//   for (int iPage = 1; iPage <= iLen; iPage++)
//   {
//     if (dpExists(dsPageNames[iPage]))
//     {
//       dynClear(dsExceptionInfo);
//       dynClear(dsPlotDps);
//       dynClear(ddsPageData);
//       fwTrending_getPagePlotDps(dsPageNames[iPage], dsPlotDps);
//       fwTrending_getPage(dsPageNames[iPage], ddsPageData, dsExceptionInfo);
//       assertTrue(dynlen(dsPlotDps) == (int)ddsPageData[fwTrending_PAGE_OBJECT_NROWS][1] * (int)ddsPageData[fwTrending_PAGE_OBJECT_NCOLS][1]);
//     }
//     else
//     {
//       assertTrue(TRUE);
//     }
//   }
// }
//
//
// /* Gets the title of the given page data point. */
// void test_fwTrending_getPageTitle()
//what if page dp does not exist? error occurs and the function
//fwTrending_getPageTitle() does not cover the case.
// if title is not defined, takes the dp name
// {
//   dyn_string dsPageNames;
//   string sPageTitle;
//   int iLen;
//   dyn_string dsExceptionInfo;
//   dyn_dyn_string ddsPageData;
//
//   dynAppend(dsPageNames, "CaVPage");
//   dynAppend(dsPageNames, "test_page");
//   dynAppend(dsPageNames, "no_test_page");
//
//   iLen = dynlen(dsPageNames);
//   for (int iPage = 1; iPage <= iLen; iPage++)
//   {
//     if (dpExists(dsPageNames[iPage]))
//     {
//       fwTrending_getPageTitle(dsPageNames[iPage], sPageTitle);
//       fwTrending_getPage(dsPageNames[iPage], ddsPageData, dsExceptionInfo);
//       assertTrue(strlen(sPageTitle) == strlen(ddsPageData[fwTrending_PAGE_OBJECT_TITLE][1]) || sPageTitle == dsPageNames[iPage]);
//     }
//     else
//     {
//       assertTrue(TRUE);
//     }
//   }
// }
//
//
// /* Gets the title of the given plot data point. */
// void test_fwTrending_getPlotTitle()
//what if page dp does not exist? error occurs and the function
//fwTrending_getPlotTitle() does not cover the case.
// if title is not defined, takes the dp name
// {
//   dyn_string dsPlotNames;
//   string sPlotTitle;
//   int iLen;
//   dyn_string dsExceptionInfo;
//   dyn_dyn_string ddsPlotData;
//
//   dynAppend(dsPlotNames, "CaVPlot");
//   dynAppend(dsPlotNames, "test_plot");
//   dynAppend(dsPlotNames, "no_test_plot");
//
//   iLen = dynlen(dsPlotNames);
//   for (int iPlot = 1; iPlot <= iLen; iPlot++)
//   {
//     if (dpExists(dsPlotNames[iPlot]))
//     {
//       fwTrending_getPlotTitle(dsPlotNames[iPlot], sPlotTitle);
//       fwTrending_getPlot(dsPlotNames[iPlot], ddsPlotData, dsExceptionInfo);
//       assertTrue(strlen(sPlotTitle) == strlen(ddsPlotData[fwTrending_PLOT_OBJECT_TITLE][1]) || sPlotTitle == dsPlotNames[iPlot]);
//     }
//     else
//     {
//       assertTrue(TRUE);
//     }
//   }
// }
//
//
// /* Gets the template status of the given plot data point. */
// void test_fwTrending_getPlotIsTemplate()
// {
//   dyn_string dsPlotNames;
//   string sIsTemplate;
//   int iLen;
//   dyn_string dsExceptionInfo;
//   dyn_dyn_string ddsPlotData;
//
//   dynAppend(dsPlotNames, "CaVPlot");
//   dynAppend(dsPlotNames, "test_plot");
//   dynAppend(dsPlotNames, "no_test_plot");
//
//   iLen = dynlen(dsPlotNames);
//   for (int iPlot = 1; iPlot <= iLen; iPlot++)
//   {
//     if (dpExists(dsPlotNames[iPlot]) + fwTrending_PLOT_IS_TEMPLATE)
//     {
//       fwTrending_getPlotIsTemplate(dsPlotNames[iPlot], sIsTemplate);
//       fwTrending_getPlot(dsPlotNames[iPlot], ddsPlotData, dsExceptionInfo);
//       assertTrue(sIsTemplate == ddsPlotData[fwTrending_PLOT_OBJECT_IS_TEMPLATE][1]);
//     }
//     else
//     {
//       assertTrue(TRUE);
//     }
//   }
// }
//
//
// /* Gets the title of the given plot or page data point. */
// void test_fwTrending_getTitle()
//what if DP does not exist? an error is thrown. should check it a priori
// {
//   dyn_string dsDPNames;
//   string sTitle;
//   dyn_string dsExceptionInfo;
//   int iLen;
//
//   dynAppend(dsDPNames, "CaVPlot");
//   dynAppend(dsDPNames, "CaVPage");
//   dynAppend(dsDPNames, "test_plot");
//   dynAppend(dsDPNames, "no_test_page");
//
//   iLen = dynlen(dsDPNames);
//   for (int iDP = 1; iDP <= iLen; iDP++)
//   {
//     if (dpExists(dsDPNames[iDP]))
//     {
//       fwTrending_getTitle(dsDPNames[iDP], sTitle, dsExceptionInfo);
//       assertTrue(strlen(sTitle) > 0 && dynlen(dsExceptionInfo) == 0);
//     }
//     else
//     {
//       assertTrue(TRUE);
//     }
//   }
// }
//
//
// /* Checks if the Trending Tool is running in
//    a project where UNICOS FW is installed. */
// void test_fwTrending_isUnicosEnvironment()
// {
  //UNICOS is not installed.
//   bool  bRetValue;
//   dyn_string dsExceptionInfo;
//
//   bRetValue = _fwTrending_isUnicosEnvironment(dsExceptionInfo);
//   assertTrue(!bRetValue && dynlen(dsExceptionInfo) == 0);
// }
//
//
// /* Writes the given plot name to the dyn_string
//    containing a list of the plots on a page by
//    giving the column and row of the plot. */
// void test_fwTrending_setColRow()
// {
//what if col/row exceeds limits? it does not cover the case.
//   dyn_string dsPlotsList;
//   int iColumn = 3;
//   int iRow = 2;
//   string sPlotName = "test_name";
//
//   fwTrending_setColRow(dsPlotsList, iColumn, iRow, sPlotName);
//   assertTrue(dsPlotsList[fwTrending_MAX_ROWS * (iRow - 1) + iColumn] == sPlotName);
// }
//
//
// /* Split the template parameter definition string
//    into a list of template parameter names and values. */
// void test_fwTrending_splitParameters()
// {
//
//   dyn_dyn_string ddsParameters;
//   string sParameters = "first = 1, second = 2, third, fourth = 4";
//
//   _fwTrending_splitParameters(sParameters, ddsParameters);
//   assertTrue(dynlen(ddsParameters[1]) == dynlen(ddsParameters[2]) && dynlen(ddsParameters[1]) == dynlen(strsplit(sParameters, ",")));
// }
//
//
// /* Switches between  two states
//    (curve visibility is used or curve hidden state is given)
//    by inverting all the entries in the given list. */
// void test_fwTrending_switchCurvesHiddenVisible()
// {
//what if lenght of input dyn_string exceeds max length? the case is no covered.
//   dyn_string dsCurrentCurveStatus;
//   dyn_string dsExceptionInfo;
//   int iLen = fwTrending_TRENDING_MAX_CURVE/2;
//   for (int iCurve = 1; iCurve <= iLen; iCurve++)
//   {
//     dynAppend(dsCurrentCurveStatus, "TRUE");
//   }
//   _fwTrending_switchCurvesHiddenVisible(dsCurrentCurveStatus, dsExceptionInfo);
//
//   assertTrue(dynCount(dsCurrentCurveStatus, "TRUE") == 0);
// }
//
//
// /* Gets a list of the IDs of all connected
//    systems (including local system). */
// void test_fwTrending_getConnectedSystemIds()
// {
//   dyn_int diSystemIds;
//   dyn_string dsExceptionInfo;
//   _fwTrending_getConnectedSystemIds(diSystemIds, dsExceptionInfo);
//   assertTrue(dynContains(diSystemIds,getSystemId()) > 0);
// }
//
//
// /* Checks if the given system ID is of
//    a system which is connected or not. */
// void test_fwTrending_isSystemConnected()
// {
//   string sSystemName = getSystemName();
//   bool bIsConnected;
//   dyn_string dsExceptionInfo;
//
//   _fwTrending_isSystemConnected(sSystemName, bIsConnected, dsExceptionInfo);
//   assertTrue(bIsConnected);
//
//   dynClear(dsExceptionInfo);
//    sSystemName = "dist_0";
//   _fwTrending_isSystemConnected(sSystemName, bIsConnected, dsExceptionInfo);
//   assertFalse(bIsConnected);
// }
//
//
// /* Checks if the system that a dpe
//    is on is connected or not. */
// void test_fwTrending_isSystemForDpeConnected()
// {
  //what if DP does not exist? the case is not covered.
//   string sCurveDpe = "test_plot2";
//   bool bIsConnected;
//   dyn_string dsExceptionInfo;
//
//   if (dpExists(sCurveDpe))
//   {
//     _fwTrending_isSystemForDpeConnected(sCurveDpe, bIsConnected, dsExceptionInfo);
//     assertTrue(bIsConnected);
//   }
//   else //if (dynlen(dsExceptionInfo) > 0)
//   {
//     assertTrue(TRUE);
//   }
  //else
 // {
  //  assertTrue(FALSE);
 // }
// }
//
//
// /* Prepares the elements of the plot object
//    dpe (X and Y) arrays for internal
//    alias representation. */
// void test_fwTrending_preparePlotObjectDPES()
// {
//   dyn_string dsReturn;
//   dyn_string dsDpes;
//   string sDP = "test_plot";
//   dpGet(sDP + ".dpes", dsDpes);
//   dsReturn = _fwTrending_preparePlotObjectDPES(dsDpes);
//   assertTrue(dynlen(dsDpes) > 0);
// }
//
//
//
// /*
// UNICOS not installed:
// fwTrending_convertPvssDpeListToUnicosDpeList
// fwTrending_convertUnicosDpeListToPvssDpeList
// fwTrending_convertUnicosDpeStringToPvssDpeString
//
//
// _fwTrending_syncDifferentRangeFormats
//
// fwTrending_eventsSort
//
//
// */
//
