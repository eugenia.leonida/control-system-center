#uses "fwViewer/fwViewer_documentOpener.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

class testFwViewer_documentOpener : fwViewer_documentOpener
{
  protected string getPdfFile(string url)
  {
    string path = "myFilePath";
    return path;
  }
};


void test_fwViewer_documentOpener_setupSuite()
{
}

void test_fwViewer_documentOpener_teardownSuite()
{
}

void test_fwViewer_documentOpener_setup()
{
}

void test_fwViewer_documentOpener_teardown()
{
}


void test_fwViewer_isPdfFile_returnTrue() {
	string url = "http://www.pdf995.com/samples/test.pdf";
	fwViewer_documentOpener systemUnderTest = fwViewer_documentOpener();
	assertTrue(systemUnderTest.isPdfFile(url));
}

void test_fwViewer_isPdfFile_returnFalse() {
	string url = "http://www.pdf995.com/samples/test.doc";
	fwViewer_documentOpener systemUnderTest = fwViewer_documentOpener();
	assertFalse(systemUnderTest.isPdfFile(url));
}

/*
@callwithparam test_fwViewer_getPageNumber("http://www.pdf995.com/samples/test.pdf", 1)
@callwithparam test_fwViewer_getPageNumber("https://edms.cern.ch/file/1302367/last_released/Analyse_Fonctionelle_UW65_new_docx_cpdf.pdf?page=104&pagemode=bookmarks", 104)
@callwithparam test_fwViewer_getPageNumber("https://edms.cern.ch/file/1302367/last_released/Analyse_Fonctionelle_UW65_new_docx_cpdf.pdf?zoom=23&page=33", 33)
*/
void test_fwViewer_getPageNumber(dyn_mixed params)
{
  const int noParams = 2;
  	string url;
	int pageNumber, expectedPageNumber;
	fwViewer_documentOpener systemUnderTest = fwViewer_documentOpener();

  assertEqual(dynlen(params), noParams, "Function " + __FUNCTION__ + " expects " + noParams + " parameters.");

  url = params[1];
  expectedPageNumber = params[2];

	pageNumber = systemUnderTest.getPageNumber(url);

  	assertEqual(expectedPageNumber,pageNumber, "Parsing error! Got back wrong page number.");
}

/*
@callwithparam test_fwViewer_getNamedDestination_empty("https://site.url")
@callwithparam test_fwViewer_getNamedDestination_empty("https://edms.cern.ch/file/document_id/version/filename?param1=value1&param2=value")
@callwithparam test_fwViewer_getNamedDestination_empty("https://some.url/path/filename.pdf?param1=value1&param2=value&param3=value3")
*/
void test_fwViewer_getNamedDestination_empty(dyn_mixed params)
{
  const int noParams = 1;
	string url;
	string expectedNamedDest = "" ;
	fwViewer_documentOpener systemUnderTest = fwViewer_documentOpener();

  assertEqual(dynlen(params), noParams, "Function " + __FUNCTION__ + " expects " + noParams + " parameters.");

  url = params[1];

	string namedDest = systemUnderTest.getNamedDestination(url);
	assertEqual(expectedNamedDest, namedDest, "Parsing error! Got back non-empty named destination.");
}

/*
@callwithparam test_fwViewer_getNamedDestination("https://edms.cern.ch/file/document_id/version/filename.pdf?nameddest=someDest", "someDest")
@callwithparam test_fwViewer_getNamedDestination("https://edms.cern.ch/file/document_id/version/filename.pdf?page=3&nameddest=dest1", "dest1")
@callwithparam test_fwViewer_getNamedDestination("https://edms.cern.ch/file/document_id/version/filename.pdf?page=4&param=value&nameddest=dest2", "dest2")
@callwithparam test_fwViewer_getNamedDestination("https://edms.cern.ch/file/document_id/version/filename.pdf?nameddest=anotherDest&page=5&param=value", "anotherDest")
*/
void test_fwViewer_getNamedDestination(dyn_mixed params)
{
  const int noParams = 2;
	string url;
	string expectedNamedDest;
	fwViewer_documentOpener systemUnderTest = fwViewer_documentOpener();

  assertEqual(dynlen(params), noParams, "Function " + __FUNCTION__ + " expects " + noParams + " parameters.");

  url = params[1];
  expectedNamedDest = params[2];

	string namedDest = systemUnderTest.getNamedDestination(url);
	assertEqual(expectedNamedDest, namedDest, "Parsing error! Got back wrong named destination.");
}

void test_getPdfFile_returnPath()
{
  string url = "http://www.pdf995.com/samples/test.pdf";
  testFwViewer_documentOpener systemUnderTest = testFwViewer_documentOpener();
  string expectedFilePath = "myFilePath";
  string filePath = systemUnderTest.getPdfFilePath(url);

  assertEqual(expectedFilePath, filePath);
}
