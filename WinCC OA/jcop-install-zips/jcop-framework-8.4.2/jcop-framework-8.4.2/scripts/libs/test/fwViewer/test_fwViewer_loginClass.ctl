#uses "fwViewer/fwViewer_loginClass.ctl"
#uses "fwUnitTestComponentAsserts.ctl"
class testClass:fwViewer_loginClass {
	string passwordd, userNamee ;

	public testClass(string user ="", string pass = "") {
		passwordd = pass;
		userNamee = user;
	}

	protected mapping getLoginData() {
		return makeMapping("userName", userNamee, "password", passwordd);
	}
};

void test_login_returnFalse_if_loginAndPasswordEmpty() {
	testClass systemUnderTest = testClass();
	assertFalse(systemUnderTest.login());
}

void test_login_returnFalse_if_loginEmpty() {
	string login = "";
	string password = "password";
	testClass systemUnderTest = testClass(login, password);
	assertFalse(systemUnderTest.login());
}

void test_login_returnFalse_if_passwordEmpty() {
	string login = "login";
	string password = "";
	testClass systemUnderTest = testClass(login, password);
	assertFalse(systemUnderTest.login());
}
void test_login_returnTrue() {
	string login = "login";
	string password = "password";
	testClass systemUnderTest = testClass(login, password);
	assertTrue(systemUnderTest.login());
}
void test_isUserActive_returnTrue() {
	string login = "login";
	string password = "password";
	testClass systemUnderTest = testClass(login, password);
	assertTrue(systemUnderTest.login());
	assertTrue(systemUnderTest.isUserActive());
}

void test_isUserActive_returnFalse() {
	testClass systemUnderTest = testClass();
	assertFalse(systemUnderTest.isUserActive());
}

void test_getKey_returnKey() {
	string login = "login";
	string password = "password";
	string expectedValue = login + ":" + password;
	testClass systemUnderTest = testClass(login, password);
	assertTrue(systemUnderTest.login());
	string key = systemUnderTest.getKey();
	string decodedKey;
	base64Decode(key, decodedKey);
	assertEqual(expectedValue,decodedKey);
}

void test_getKey_returnFalse() {
	string login = "login";
	string password = "password";
	string expectedValue = login + ":" + password;
	bool status = false;
	testClass systemUnderTest = testClass(login, password);
	string key = systemUnderTest.getKey();
	string decodedKey;
	base64Decode(key, decodedKey);
	if (decodedKey == expectedValue) {
		status = true;
	}
	assertFalse(status);
}

void test_getActiveUser_returnUserName() {
	string login = "login";
	string password = "password";
	string expectedValue = login;
	testClass systemUnderTest = testClass(login, password);
	assertTrue(systemUnderTest.login());
	assertEqual(expectedValue,systemUnderTest.getActiveUser());

}

