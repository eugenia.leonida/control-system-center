#uses "fwRDBArchiver/fwRDBArchiver.ctl"
#uses "fwGeneral/fwException.ctl"
// fwGeneral.ctl required for FWDEPRECATED()
#uses "fwGeneral/fwGeneral.ctl" 

/** This function allows to add an option for RDB in the config file.


RDB recognizes the following options:
- DbType
- Db
- DbUser
- writeWithBulk
- alertUpdateDelay
- bufferToDisk
- bufferToDiskDir
- delayAfterDBRestart
- queryOverId
- initialEntriesInBlock
- lostConnectionReportInterval
- maxRequestLineCount 
- maxRequestThreads
- openConnOnDemand
- updateConnCloseDelay
- oracleClientVersion
- queryOverBounds
- queryTimeout
- queryFunction
- sendMaxTS
- SQLPreFetchCount
- writeTimeout
- APMDbUser 
- APMDbType
- APMDbDb

@param options        All the options to set (mapping type, key:value).
@param exceptionInfo  Details of any errors are returned here.

@return               INTEGER - 0 if no error, a negative number in case of error (check exceptionInfo).

@deprecated 2018-06-22 

*/
int fwRDBArchiver_setRDBOptions(mapping options, dyn_string &exceptionInfo) {
  FWDEPRECATED();
  return _fwRDBArchiver_setConfigFileMultipleValues("ValueArchiveRDB", options, true, exceptionInfo);
}
