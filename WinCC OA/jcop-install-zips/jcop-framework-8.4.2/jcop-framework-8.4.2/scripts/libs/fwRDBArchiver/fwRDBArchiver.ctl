// $License: NOLICENSE

#uses "CtrlPv2Admin"
#uses "CtrlRDBArchive"
#uses "CtrlRDBCompr"
#uses "CtrlRDBAccess"
#uses "rdb.ctl"
#uses "fwGeneral/fwException.ctl"
#uses "fwRDBArchiver/fwRDBArchiverDeprecated.ctl"

/**
@file fwRDBArchiver.ctl
Library which provides a set of functions in order to manage RDB configuration

@author CERN BE-ICS
@version 8.2.1
@date June 2018
*/

const int fwRDBArchiver_timerange_1hour = 1;
const int fwRDBArchiver_timerange_1day = 2;
const int fwRDBArchiver_timerange_1week = 3;
const int fwRDBArchiver_timerange_1month = 4;
const int fwRDBArchiver_timerange_explicit = 5;

const string fwRDBArchiver_DPE_RDBPassword="_RDBArchive.db.password:_original.._value";
const string fwRDBArchiver_DPE_RDBUsername="_RDBArchive.db.user:_original.._value";
const string fwRDBArchiver_DPE_RDBDatabase="_RDBArchive.db.host:_original.._value";
const string fwRDBArchiver_DPE_RDBDBType="_RDBArchive.db.dbType:_original.._value";

//---------------------------------------------------------------------------------------

/** Reconfigures the project to use RDB+Valarch parallel archiving

@return INTEGER - 0 if no error, a negative number in case of error

@reviewed 2018-06-21 @whitelisted{StandaloneTool} used in the fwRDBSettingsParallelSCADAR component of SAS

The status of this function should be reviewed with SCADA Application Service, to determine if it is still
needed. In particular, with NextGenArchiver coming in near future, parallel archiving will be achieved
in a different way.

*/
int fwRDBArchiver_convertToParallelArchiving(string db, string user, string password, dyn_string &exceptionInfo)
{
  int err;
  string sFunction = "fwRDBArchiver_convertToParallel";

  if(fwRDBArchiver_isParallelArchive()) {
    DebugTN(sFunction + " - INFO: WinCC OA project already configured with parallel archiving - nothing has to be done.");
    return 0;
  }
  else
  {
    DebugTN(sFunction + " - INFO: Updating WinCC OA project to parallel archiving. Please wait...");
  }

  err = fwRDBArchiver_configRDB(db, user, password, exceptionInfo);
  if(err < 0)
  {
    return err;
  }

  dyn_string dsVADPNames;
  dyn_int diVANumbers = makeDynInt();
  dyn_string dsTmp;

  dsVADPNames = dpNames("_ValueArchive_*","_ValueArchive");
  for(int i=1 ; i <= dynlen(dsVADPNames) ; i++) {
    string dpname=dpSubStr(dsVADPNames[i],DPSUB_DP);
    if( !patternMatch("*_*_2",dpname ) || dpname=="_ValueArchive_2"  ) // eliminate REDU datapoints, but not the _ValueArchive_2 !
    {
      dsTmp = strsplit(dpname, "_");
      dynAppend(diVANumbers, dsTmp[3]);
    }
  }
  err = fwRDBArchiver_enableMultipleVAForwarding(diVANumbers, exceptionInfo);
  if(err < 0)
  {
    return err;
  }

  err = _fwRDBArchiver_addRDBManager(exceptionInfo);
  if(err < 0)
  {
    return err;
  }

  return 0;
}


//---------------------------------------------------------------------------------------

/**
fwRDBArchiver_convertToOracle

@return INTEGER - 0 if no error, a negative number in case of error.
*/
int fwRDBArchiver_convertToOracle(string db, string user, string password, dyn_string &exceptionInfo) {
  int err;
  string sFunction = "fwRDBArchiver_convertToOracle";

  dyn_string dsArchive;

  if(fwRDBArchiver_isRDBArchive()) {
    DebugTN(sFunction + " - INFO: WinCC OA project already configured with RDB archiving - nothing has to be done.");
    return 0;
  }
  else
  {
    DebugTN(sFunction + " - INFO: Updating WinCC OA project to RDB archiving. Please wait...");
  }

  //kszkudla: 08.09.2014: we can assume that installation of core component updated the config file already
  err = fwRDBArchiver_configRDB(db, user, password, exceptionInfo);
  if(err < 0)
    return err;

  //convert archive dpe configuration
  DebugTN(sFunction + " - INFO: Converting archive DPE configuration.");
  rdbConvertHdbToRdb();

  //remove internal _ValueArchive dpN
  //TODO: check if deletion necessary
  DebugTN(sFunction + " - INFO: Removing internal _ValueArchive dpN.");
  dsArchive = dpNames("_ValueArchive_*","_ValueArchive");
  for(int i=1 ; i <= dynlen(dsArchive) ; i++) {
    DebugTN(sFunction + " - INFO: Deletion of " + dsArchive[i] + ".");

    err = dpDelete(dsArchive[i]);
    if(err < 0) {
      fwException_raise(exceptionInfo, "ERROR", sFunction + " - Unable to delete datapoint.", dsArchive[i]);
      return err;
    }
  }

  // Turn down value archive managers and add the RDB manager
  err = _fwRDBArchiver_manageManagers(exceptionInfo);
  if(err < 0)
    return err;

  // Deactivate file archive
  err = _fwRDBArchiver_deactivateFileArchive("_Alert", exceptionInfo);
  if(err < 0)
    return err;

  return 0;
}

//---------------------------------------------------------------------------------------

/**
fwRDBArchiver_isRDBArchive

@return Return true if the project is already configured with RDB archiving.
*/
bool fwRDBArchiver_isRDBArchive() {
  int iRes;
  bool bRDB=FALSE;

  dyn_string exceptionInfo = makeDynString();

  iRes = dpGet("_DataManager.UseRDBArchive", bRDB);

  //Check if the RDB is running as well
  iRes = _fwRDBArchiver_hasRDBManager(exceptionInfo);
  bRDB = bRDB && (iRes == 1);

	return bRDB;
}

//---------------------------------------------------------------------------------------

/**
fwRDBArchiver_isRDBArchiveParallel

@return Return true if the project is already configured with parallel archiving.
*/
bool fwRDBArchiver_isParallelArchive() {
  bool bReturn = false;
  int iRes;
  bool bTmpVal = false;

  dyn_string dsVADPNames;

  dsVADPNames = dpNames("_ValueArchive_?","_ValueArchive");

  for(int i = 1; i <= dynlen(dsVADPNames); ++ i)
  {
    iRes = dpGet(dsVADPNames[i]+".general.forwardToRDB", bTmpVal);
    bReturn = bReturn || bTmpVal;
  }

  return bReturn && fwRDBArchiver_isRDBArchive();
}

//---------------------------------------------------------------------------------------

/**
fwRDBArchiver_checkCredentials

@return Return true if DB credentials are correct.
*/
int fwRDBArchiver_checkCredentials(string db, string user, string password, dyn_string &exceptionInfo) {
  string sFunction = "fwRDBArchiver_checkCredentials";

  string FW_RDB_ARCHIVE_DB_CONNECTION_NAME = "fwRDBArchiverTestConnection";
  dbConnection fwRDBArchiveDBConn;

  string connectionString = "driver=QOCI8;database="+db+";uid="+user+";enc_pwd="+password;


  int rc = rdbOpenConnection(connectionString, fwRDBArchiveDBConn, FW_RDB_ARCHIVE_DB_CONNECTION_NAME);
  if (rc==-1){
    fwException_raise(exceptionInfo, "ERROR", sFunction + " - DB credentials incorrect!", "");
    return rc;
  }

  rdbCloseConnection(fwRDBArchiveDBConn);

  rc = rdbIsConnectionOpen(fwRDBArchiveDBConn);
  if (rc==0){
    fwException_raise(exceptionInfo, "ERROR", sFunction + " - failed to disconnect from DB!", "");
    return rc;
  }

  return 0;
}

//---------------------------------------------------------------------------------------

/**
This function converts RDB password from binary to hexadecimal encoding.

The password is retrieved from _RDBArchive.db.password DPE and written back to it after the conversion.
If the password is already encoded in hexadecimal form, by default the function doesn't do anything.
As there is a possibility, albeit extremely low, that a binary-encoded password may only consist of hex characters,
an option to force the conversion is provided if the caller is sure that the password is in fact binary-encoded.

Depending on 3.15 -> 3.16 project migration method used, binary data stored in the encrypted password string can be
left unchanged, as it is the case when ASCII export/import is used, or changed, when Project Upgrade tool in
Project Administrator is used. If the binary form of the password has been converted to UTF-8, passwordConvertedToUnicode
argument should be set to true in order for the conversion process to work correctly.

Rule of thumb for using the function:
- In 3.15 project that uses ISO-8859-1 encoding: call with passwordConvertedToUnicode == false.
- In 3.16 project, upgraded from 3.15 project using Project Upgrade tool in Project Administrator: call with
  passwordConvertedToUnicode == true.
- In 3.16 project, into which _RDBArchive.db.password DPE was imported using ASCII import: call with
  passwordConvertedToUnicode == false.
- In 3.16 project, created from scratch (_RDBArchive.db.password DPE not ASCII imported from an ISO-encoded project): call
  with passwordConvertedToUnicode == false.

Note that after the conversion all old clients (older than P018 for 3.15 and P008 for 3.16) will not be able
to establish the connection to RDB, as they expect the encrypted password to be binary-encoded.

More information on RDB password encryption mechanism change and ISO to UTF-8 conversion can be found at
https://indico.cern.ch/event/802974/contributions/3361422/attachments/1814373/2964787/PasswordEncodingJcopFwwg.pdf and
https://indico.cern.ch/event/822983/contributions/3459591/attachments/1859873/3056230/ISO_to_UTF8.pdf

@param passwortConvertedToUnicode Should be set to true if encrypted password was converted to UTF-8 and it has to be re-encoded as ISO-8859-1 before the conversion.
@param exceptionInfo  Details of any errors are returned here.
@param forceConversion  Perform the conversion also if the password is already hex-encoded.

@return               INTEGER - 0 if conversion was successful, 1 - if no conversion was necessary, a negative number in case of error (check exceptionInfo).
*/
int fwRDBArchiver_convertPasswordEncodingToHexadecimal(bool passwordConvertedToUnicode, dyn_string &exceptionInfo, bool forceConversion = false)
{
  string encodedPassword;
  int rc = dpGet(fwRDBArchiver_DPE_RDBPassword, encodedPassword);
  if (rc != 0 || dynlen(getLastError()) > 0) {
    fwException_raise(exceptionInfo, "ERROR", __FUNCTION__ + " failed to get encrypted RDB password from the DPE: " + fwRDBArchiver_DPE_RDBPassword, "");
    return -1;
  }

  if (passwordConvertedToUnicode) {
    encodedPassword = recode(encodedPassword, "UTF-8", "ISO-8859-1");
  }

  if (_fwRDBArchiver_isPasswordHexadecimal(encodedPassword) && !forceConversion) {
    return 1;
  }

  string hexEncodedPassword;
  for (int i = 0; i < strlen(encodedPassword); ++i) {
    string hexByte;
    sprintf(hexByte, "%02X", (unsigned)encodedPassword[i]);
    hexEncodedPassword += hexByte;
  }

  rc = dpSetWait(fwRDBArchiver_DPE_RDBPassword, hexEncodedPassword);
  if (rc != 0 || dynlen(getLastError()) > 0) {
    fwException_raise(exceptionInfo, "ERROR", __FUNCTION__ + " failed to write hex-encoded RDB password to the DPE: " + fwRDBArchiver_DPE_RDBPassword, "");
    return -1;
  }

  return 0;
}
//---------------------------------------------------------------------------------------

/**
This function checks if encrypted password string only contains hex characters.

@param encryptedPassword  string to be checked.

@return               bool - true if the string contains only hexadecimal characters, false otherwise.
*/
bool _fwRDBArchiver_isPasswordHexadecimal(const string &encryptedPassword)
{
  const dyn_char hexChars = makeDynChar('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
  for (int i = 0; i < strlen(encryptedPassword); ++i) {
    if (!dynContains(hexChars, encryptedPassword[i])) {
      return false;
    }
  }
  return true;
}

//---------------------------------------------------------------------------------------

/**
fwRDBArchiver_configRDB

This function configure PVSS to use the RDB archiver.

The config file will be completed with the following entries:
[general]
useRDBArchive = 1
useRDBGroups = 1

[ui]
CtrlDLL = "CtrlRDBArchive"
CtrlDLL = "CtrlRDBCompr"

[ctrl]
CtrlDLL = "CtrlRDBArchive"
CtrlDLL = "CtrlRDBCompr"

[ValueArchiveRDB]
DbType = "ORACLE"
writeWithBulk = 1
Db = "TheSpecifiedDP"
DbUser = "TheSpecifiedUser"

The RDB configuration will be completed with the given password for the database (encrypted).
The DataManager configuration will be completed by setting the relevant DPEs.

@param db             The Oracle TNS name for the database.
@param user           The user name (or schema name) for the database.
@param password       The password in clear text associated to the database account.
@param exceptionInfo  Details of any errors are returned here.

@return               INTEGER - 0 if no error, a negative number in case of error (check exceptionInfo).
*/
int fwRDBArchiver_configRDB(string db, string user, string password, dyn_string &exceptionInfo) {
  int err;
  string sFunction = "fwRDBArchiver_configRDB";

  DebugTN(sFunction + " - INFO: Configuring the project to use RDB. Set the config file, password and internal DPEs.");


  // [general]

  //kszkudla: 08.09.2014: configuration moved to fwRDBArchiver.config file
  // [ui]
  err = _fwRDBArchiver_setConfigFileValue("ui", "queryRDBdirect", 1, true, exceptionInfo);
  if(err < 0)
  {
    return err;
  }

  // [ctrl]
  err = _fwRDBArchiver_setConfigFileValue("ctrl", "queryRDBdirect", 1, true, exceptionInfo);
  if(err < 0)
  {
    return err;
  }


  if(db != "" && user != "" && password != "")
  {
    err = fwRDBArchiver_checkCredentials(db, user, password, exceptionInfo);
    if(err < 0)
    {
      return err;
    }
  }

  // [ValueArchiveRDB]
  mapping rdbOptions;
  rdbOptions["DbType"] = "ORACLE";
  rdbOptions["Db"] = db;
  rdbOptions["DbUser"] = user;
  // rdbOptions["writeWithBulk"] = 1; By default now
  err = _fwRDBArchiver_setConfigFileMultipleValues("ValueArchiveRDB", rdbOptions, true, exceptionInfo);
  if(err < 0)
  {
    return err;
  }

  // Set password
  string cryptedPwd = "";
  cryptAESRDB(password, cryptedPwd);
  dpSetWait(fwRDBArchiver_DPE_RDBPassword, cryptedPwd, fwRDBArchiver_DPE_RDBUsername, user, fwRDBArchiver_DPE_RDBDatabase, db, fwRDBArchiver_DPE_RDBDBType, "ORACLE");

  // Set the DataManager DPE
  dpSetWait("_DataManager.UseValueArchive", TRUE,
            "_DataManager.UseRDBArchive", TRUE); // force to TRUE - normally done when the project restarts

  // Check Oracle environment is set
  // if(getenv("ORACLE_HOME") == "")
  //   fwException_raise(exceptionInfo, "WARNING", "Environment variable \"ORACLE_HOME\" is not set.", "RDB might not be working correctly.");
  // if(getenv("TNS_ADMIN") == "")
  //   fwException_raise(exceptionInfo, "WARNING", "Environment variable \"TNS_ADMIN\" is not set.", "RDB might not be working correctly.");
  // if(getenv("NLS_LANG") != "AMERICAN_AMERICA.WE8ISO8859P1")
  //   fwException_raise(exceptionInfo, "WARNING", "Environment variable \"NLS_LANG\" is not \"AMERICAN_AMERICA.WE8ISO8859P1\".", "RDB strings might not be working correctly.");

  return 0;
}

//---------------------------------------------------------------------------------------

/**
fwRDBArchiver_manageManagers.

@param exceptionInfo  Details of any errors are returned here.

@return INTEGER - 0 if no error, a negative number in case of error (check exceptionInfo).
*/
int _fwRDBArchiver_manageManagers(dyn_string &exceptionInfo) {
  int err;
  string sFunction = "_fwRDBArchiver_manageManagers";

  dyn_dyn_mixed ddmanagersInfo;

  //stop all valarch
  DebugTN(sFunction + " - INFO: Stop all managers of types WCCOAvalarch");

  err = fwInstallationManager_getAllInfoFromPvss(ddmanagersInfo);
  if(err < 0) {
    fwException_raise(exceptionInfo, "ERROR", sFunction + " - Failed to get managers info!", "");
    return err;
  }
  for(int i=1 ; i <= dynlen(ddmanagersInfo) ; i++) {
    if(dynlen(ddmanagersInfo[i]) > 6) {
      if(ddmanagersInfo[i][1] == "WCCOAvalarch")
        fwInstallationManager_setMode(ddmanagersInfo[i][1], ddmanagersInfo[i][6], "manual", "", 0, "", "");
    }
  }

  err = fwInstallationManager_stopAllOfTypes(makeDynString("WCCOAvalarch"), "", 0, "", "",true, true);
  if(err < 0) {
    fwException_raise(exceptionInfo, "ERROR", sFunction + " - Failed to stop the WCCOAvalarch managers in the Console!", "");
    return err;
  }

  delay(10);

  // Add the RDB manager
  err = _fwRDBArchiver_addRDBManager(exceptionInfo);
  if(err < 0)
    return err;

  return 0;
}


int fwRDBArchiver_restartRDB(dyn_string &exceptionInfo)
{
  int err;
  string sFunction = "fwRDBArchiver_restartRDB";

  err = fwInstallationManager_command("START", "WCCOArdb", "", "", 0, "", "");
  if(err < 0)
  {
    fwException_raise(exceptionInfo, "ERROR", sFunction + " - Failed to start the WCCOArdb!", "");
    return err;
  }

  return 0;
}

//---------------------------------------------------------------------------------------

/**
_fwRDBArchiver_addRDBManager.

@param exceptionInfo  Details of any errors are returned here.

@return INTEGER - 0 if no error, a negative number in case of error (check exceptionInfo).
*/
int _fwRDBArchiver_addRDBManager(dyn_string &exceptionInfo) {
  int err;
  string sFunction = "_fwRDBArchiver_addRDBManager";

  err = _fwRDBArchiver_hasRDBManager(exceptionInfo);
  if(err < 0)
  {
    return err;
  }

  if(err == 0) {
    // Add the RDB manager (According to the documentation it should be between data and event. But no easy way to do that, and seems to not be critical.
    DebugTN(sFunction + " - INFO: Add WCCOArdb manager");
    err = fwInstallationManager_add("WCCOArdb", "manual", 2, 2, 30, "-num 99", "", 0, "", "");
    if(err != 1) {
      fwException_raise(exceptionInfo, "ERROR", sFunction + " - Failed to add the WCCOArdb manager in the Console! rc="+err, "");
      return 1;
    }
  }

  return 0;
}

//---------------------------------------------------------------------------------------

/**
_fwRDBArchiver_hasRDBManager

@param exceptionInfo  Details of any errors are returned here.

@return BOOLEN - 1 if manager already exists, 0 if there is no manager, a negative value in case of error.
*/
int _fwRDBArchiver_hasRDBManager(dyn_string &exceptionInfo)
{
  int err;
  string sFunction = "_fwRDBArchiver_hasRDBManager";

  dyn_dyn_mixed ddmanagersInfo;

  err = fwInstallationManager_getAllInfoFromPvss(ddmanagersInfo);
  if(err < 0) {
    fwException_raise(exceptionInfo, "ERROR", sFunction + " - Failed to get managers info!", "");
    return err;
  }

  for(int i=1 ; i <= dynlen(ddmanagersInfo) ; i++)
  {
    if(dynlen(ddmanagersInfo[i]) > 6)
    {
      if(ddmanagersInfo[i][1] == "WCCOArdb")
      {
        return 1;
      }
    }
  }

  return 0;
}

//---------------------------------------------------------------------------------------

/**
_fwRDBArchiver_deactivateFileArchive

@param fileType Designate the type of file archive ("_Event" or "_Alert" for instance).
@param exceptionInfo  Details of any errors are returned here.

@return INTEGER - 0 if no error, a negative number in case of error.
*/
int _fwRDBArchiver_deactivateFileArchive(string fileType, dyn_string &exceptionInfo) {
  int err;
  string sFunction = "_fwRDBArchiver_deactivateFileArchive";

  DebugTN(sFunction + " - INFO: Deactivating file archive.");

  // Deactivate -> cf. vision/ArchivControlSub1
  time t;
  err = dpSetWait(fileType + "ArchivControl.StatusSystem:_original.._value", 0,
                  fileType + "FileChange.validFrom:_original.._value", setPeriod(t, 0),
                  fileType + "FileChange.validUntil:_original.._value", setPeriod(t, 1));
  if(err < 0) {
    fwException_raise(exceptionInfo, "ERROR", sFunction + " - dpSetWait failed!", "");
    return err;
  }

  // Remove non active file archive -> cf. vision/ArchivControlSub2
  string dataSet;
  if(fileType == "_Event")
    dataSet = "_DataSet";
  else
    dataSet = fileType + "DataSet";

  dyn_time startTimes;
  err = dpGet(dataSet + ".List.StartTimes:_online.._value", startTimes);
  if(err < 0) {
    fwException_raise(exceptionInfo, "ERROR", sFunction + " - dpGet failed!", "");
    return err;
  }

  dyn_string pars = makeDynString();
  pars[1] = fileType;
  pars[2] = "";

  dyn_int parf = makeDynInt();
  for(int i=1 ; i < dynlen(startTimes) ; i++)
    parf[i] = period(startTimes[i]);

  if(dynlen(parf) > 0) {
    err = dpSetWait("_EventArchivControl.ExecuteAction.num:_original.._value",  6,
                    "_EventArchivControl.ExecuteAction.pars:_original.._value", pars,
                    "_EventArchivControl.ExecuteAction.pari:_original.._value", makeDynInt(),
                    "_EventArchivControl.ExecuteAction.parf:_original.._value", parf );
    if(err < 0) {
      fwException_raise(exceptionInfo, "ERROR", sFunction + " - dpSetWait failed!", "");
      return err;
    }
  }

  return 0;
}

//---------------------------------------------------------------------------------------

/**
fwRDBArchiver_fetchStatistics

This function get statistics from a RDB schema about the usage of all the archived
datapoints.

Time range mode is one of the constants fwRDBArchiver_timerange_* (defined at the
beginning of this file).
- "1hour", "1day", "1week" and "1month" are fairly self-explained. They are using SQL
functions. Thus you don't need to specify valid start and stop parameters. Reference
point is taken at the moment of execution of the query.
- "explicit" means that you explicitly give the start and stop parameters to define
the time range.

In all cases time zone and daylight saving are properly handled by the function.

The results are available in the output parameters stats. It is an array of array.
Each of these nested array contains two elements:
- The datapoint name.
- The number of value archived.

@param timerangeMode One of the fwRDBArchiver_timerange_* constant.
@param group         The RDB table group you want to fetch ("EVENT" for normal DPE, "ALERT" for alerts).
@param start         The starting time of the range if timerangeMode is "explicit".
@param stop          The ending time of the range if timerangeMode is "explicit".
@param schema        Specify the RDB schema (you must have read rights on it). Blank ("") equal to the schema locally used by the project.
@param stats         OUTPUT - The statistics returned by the database.
@param exceptionInfo OUTPUT - Details of any error are returned here.

@return INTEGER - 0 if no error, a negative number in case of error (check exceptionInfo).
*/
int fwRDBArchiver_fetchStatistics(int timerangeMode, string group, time start, time stop, string schema, dyn_dyn_anytype &stats, dyn_string &exceptionInfo) {
  int err;

  if(schema != "")
    schema += ".";

  // Check start/stop
  if((timerangeMode == fwRDBArchiver_timerange_explicit) && (start >= stop)) {
    fwException_raise(exceptionInfo, "ERROR",
                      "Start time is greater than or equal to stop time",
                      "");
    return -1;
  }

  string sqlPeriod = _fwRDBArchiver_getSQLPeriod(timerangeMode, start, stop);
  string sql =
      "SELECT elm.element_name, count(eh.ts) " +
      "  FROM " + schema + "elements elm, " + schema + group + "HISTORY eh " +
      " WHERE elm.element_id= eh.element_id " +
      "   AND eh.ts " + sqlPeriod + " " +
      " GROUP BY elm.element_name " +
      " ORDER BY count(eh.ts) DESC";

  err = runRealSQLQuery(sql, stats);
  if(err < 0) {
    fwException_raise(exceptionInfo, "ERROR",
                      "Unable to execute the following query with runRealSQLQuery(): " + sql,
                      sql);
    return -1;
  }

  return 0;
}

//---------------------------------------------------------------------------------------

/**
fwRDBArchiver_fetchDPEStatistics

This function get statistics from a RDB schema about the usage of one archived
datapoint.

Time range mode is one of the constants fwRDBArchiver_timerange_* (defined at the
beginning of this file).
- "1hour", "1day", "1week" and "1month" are fairly self-explained. They are using SQL
functions. Thus you don't need to specify valid start and stop parameters. Reference
point is taken at the moment of execution of the query.
- "explicit" means that you explicitly give the start and stop parameters to define
the time range.

In all cases time zone and daylight saving are properly handled by the function.

The result is available in the output parameters stats. It is an array of array.
The length of the outer array is zero if no result (datapoint is not archived or no
value has been archived over the period for instance),
or one (one nested array) if results. This nested array contains five elements:
- The minimum value archived over the period (blank if the DPE is a string).
- The maximum value archived over the period (blank if the DPE is a string).
- The average value archived over the period (blank if the DPE is a string).
- The first time a value has been archived over the period (in UTC!).
- The last time a value has been archived over the period (in UTC!).

@param dpe           The DPE for which you want detailed statistics.
@param timerangeMode One of the fwRDBArchiver_timerange_* constant.
@param group         The RDB table group you want to fetch ("EVENT" for normal DPE, "ALERT" for alerts).
@param start         The starting time of the range if timerangeMode is "explicit".
@param stop          The ending time of the range if timerangeMode is "explicit".
@param schema        Specify the RDB schema (you must have read rights on it). Blank ("") equals to the schema locally used by the project.
@param stats         OUTPUT - The statistics returned by the database.
@param exceptionInfo OUTPUT - Details of any error are returned here.

@return INTEGER - 0 if no error, a negative number in case of error (check exceptionInfo).
*/
int fwRDBArchiver_fetchDPEStatistics(string dpe, int timerangeMode, string group, time start, time stop, string schema, dyn_dyn_anytype &stats, dyn_string &exceptionInfo) {
  int err;

  if(schema != "")
    schema += ".";

  string sqlPeriod = _fwRDBArchiver_getSQLPeriod(timerangeMode, start, stop);
  string sql =
      "SELECT min(eh.value_number), max(eh.value_number), avg(eh.value_number), " +
      "       min(eh.ts), max(eh.ts) " +
      "  FROM " + schema + "elements elm, " +
      "       " + schema + group + "HISTORY eh " +
      " WHERE elm.element_name='" + dpe + "' " +
      "   AND elm.element_id=eh.element_id " +
      "   AND eh.ts " + sqlPeriod;

  err = runRealSQLQuery(sql, stats);
  if(err < 0) {
    fwException_raise(exceptionInfo, "ERROR",
                      "Unable to execute the following query with runRealSQLQuery(): " + sql,
                      sql);
    return -1;
  }

  return 0;
}

//---------------------------------------------------------------------------------------

/**
_fwRDBArchiver_getSQLPeriod

@param timerangeMode One of the fwRDBArchiver_timerange_* constant.
@param start The starting time of the range if timerangeMode is "explicit".
@param stop The ending time of the range if timerangeMode is "explicit".

@return The piece of SQL that correspond to the given time range.
*/
string _fwRDBArchiver_getSQLPeriod(int timerangeMode, time start, time stop) {
  string sqlPeriod;

  switch(timerangeMode) {
    case fwRDBArchiver_timerange_1hour:
      sqlPeriod = "between sys_extract_utc(systimestamp)-(1/24) and sys_extract_utc(systimestamp)";
    break;
    case fwRDBArchiver_timerange_1day:
      sqlPeriod = "between sys_extract_utc(systimestamp)-1 and sys_extract_utc(systimestamp)";
    break;
    case fwRDBArchiver_timerange_1week:
      sqlPeriod = "between sys_extract_utc(systimestamp)-7 and sys_extract_utc(systimestamp)";
    break;
    case fwRDBArchiver_timerange_1month:
      sqlPeriod = "between add_months(sys_extract_utc(systimestamp), -1) and sys_extract_utc(systimestamp)";
    break;
   default: // Explicit
      sqlPeriod = "between " + _fwRDBArchiver_timeToOracle(start) +
                  " and " + _fwRDBArchiver_timeToOracle(stop);
  }

  return sqlPeriod;
}

//---------------------------------------------------------------------------------------

/**
_fwRDBArchiver_timeToOracle

@param t The time variable to convert to Oracle SQL format

@return The piece of SQL that correspond to the given time.
*/
string _fwRDBArchiver_timeToOracle(time t) {
  return formatTime("sys_extract_utc(to_timestamp('%Y-%m-%d %H:%M:%S','YYYY-MM-DD HH24:MI:SS'))", t);
}

//---------------------------------------------------------------------------------------

/**
fwRDBArchiver_listRDBUsers

This function will give you the list of RDB users in the database,
which can be roughly considered as the list of RDB schemas.

You need to configure the connection to RDB in PVSS first to use this function.
You also need to be connected with a user who has right to access the SYS.ALL_TABLES
table in the database.

@param userList       OUTPUT - An array with RDB users.
@param exceptionInfo  OUTPUT - Details of any error are returned here.

@return INTEGER - 0 if no error, a negative number in case of error (check exceptionInfo).
*/
int fwRDBArchiver_listRDBUsers(dyn_string &userList, dyn_string &exceptionInfo) {
  int err;

  string sql = "SELECT OWNER FROM SYS.ALL_TABLES WHERE TABLE_NAME='ARC_LOG' ORDER BY OWNER";
  dyn_dyn_anytype data;
  err = runRealSQLQuery(sql, data);
  if(err < 0) {
    fwException_raise(exceptionInfo, "ERROR",
                      "Unable to execute the following query with runRealSQLQuery(): " + sql,
                      sql);
    return -1;
  }

  userList = makeDynString();
  for(int i=1 ; i <= dynlen(data) ; i++) {
    if((data[i][1] == "SYS") || (data[i][1] == "SYSTEM"))
      continue;
    userList[i] = data[i][1];
  }

  return 0;
}

//---------------------------------------------------------------------------------------

/**
fwRDBArchiver_getLocalSchemaName

This function gives you the locally configured user/schema name for RDB.

@param schemaName     OUTPUT - The user/schema name.
@param exceptionInfo  OUTPUT - Details of any error are returned here.

@return INTEGER - 0 if no error, a negative number in case of error (check exceptionInfo).
*/
int fwRDBArchiver_getLocalSchemaName(string &schemaName, dyn_string &exceptionInfo) {
  int err = dpGet("_RDBArchive.db.user", schemaName);
  if(err < 0) {
    fwException_raise(exceptionInfo, "ERROR",
                      "Unable to do dpGet on _RDBArchive.db.user",
                      "");
    return -1;
  }

  return 0;
}

//---------------------------------------------------------------------------------------

/**
fwRDBArchiver_getArchiveGroups

This function will give you the list of RDB archive groups in a specific schema.

You need to configure the connection to RDB in PVSS first to use this function.
You also need to be connected with a user who has right to access the tables of
other RDB schemas if you want to look inside foreign schemas.

@param schema         Specify the RDB schema (you must have read rights on it). Blank ("") equals to the schema locally used by the project.
@param archiveGroups  OUTPUT - An array with the archive groups of the schema.
@param exceptionInfo  OUTPUT - Details of any error are returned here.

@return INTEGER - 0 if no error, a negative number in case of error (check exceptionInfo).
*/
int fwRDBArchiver_getArchiveGroups(string schema, dyn_string &archiveGroups, dyn_string &exceptionInfo) {
  int err;

  if(schema != "")
    schema += ".";

  string sql = "SELECT GROUP_NAME FROM " + schema + "ARC_GROUP ORDER BY GROUP_NAME";
  dyn_dyn_anytype data;
  err = runRealSQLQuery(sql, data);
  if(err < 0) {
    fwException_raise(exceptionInfo, "ERROR",
                      "Unable to execute the following query with runRealSQLQuery(): " + sql,
                      sql);
    return -1;
  }

  archiveGroups = makeDynString();
  for(int i=1 ; i <= dynlen(data) ; i++) {
    archiveGroups[i] = data[i][1];
  }

  return 0;
}

//---------------------------------------------------------------------------------------

/**
This function set a value for a key in the config file of the project.
It allows the replacement of all existing keys before, or allows to add it as a value list (parameter "replace").
If the (key,value) pair already exists, nothing will be done.

@param section        The section under which the key will be added.
@param key            The key to add.
@param value          The value associated to the key.
@param replace        If true, delete all previous occurrences of the key before adding the new value. If false, just add the value.
@param exceptionInfo  Details of any errors are returned here.

@return               INTEGER - 0 if no error, a negative number in case of error (check exceptionInfo).
*/
int _fwRDBArchiver_setConfigFileValue (string section, string key, mixed value, bool replace, dyn_string &exceptionInfo) {
  int err;
  string sFunction = "_fwRDBArchiver_setConfigFileValue";

  string configFile = getPath(CONFIG_REL_PATH, "config");

  // Check if already exists
  dyn_string values=makeDynString();
  paCfgReadValueList(configFile, section, key, values);

  // If already exists with the same value, just return
  if(dynContains(values, value))
    return 0;

  if(replace == true) {
    // Delete previous value if exists
    if(dynlen(values) >= 1) {
      err = paCfgDeleteValue (configFile, section, key);
      if(err < 0) {
        fwException_raise(exceptionInfo, "ERROR",
                          sFunction + " - Unable to delete a config entry.",
                          configFile + ", " + section + ", " + key);
        return err;
      }
    }
  }

  // Eventually, add the value
  err = paCfgInsertValue(configFile, section, key, value);
  if(err < 0) {
    fwException_raise(exceptionInfo, "ERROR",
                      sFunction + " - Unable to insert config entry.",
                      configFile + ", " + section + ", " + key + "," + value);
    return err;
  }

  return 0;
}

//---------------------------------------------------------------------------------------

/**
This function allows to set multiple values to a section of the config file of the project.
It uses a mapping as input.
It allows the replacement of all existing keys before, or allows to add them as a value list (parameter "replace").
If a (key,value) pair already exists, it will not be inserted.

@param section        The section under which the keys will be added.
@param options        All the options to set (mapping type, key:value).
@param replace        If true, delete all previous occurences of the keys before adding the new value. If false, just add the value.
@param exceptionInfo  Details of any errors are returned here.

@return               INTEGER - 0 if no error, a negative number in case of error (check exceptionInfo).
*/
int _fwRDBArchiver_setConfigFileMultipleValues(string section, mapping options, bool replace, dyn_string &exceptionInfo) {
  int err;
  dyn_anytype keys = mappingKeys(options);

  for(int i=1 ; i <= dynlen(keys) ; i++) {
    err = _fwRDBArchiver_setConfigFileValue(section, keys[i], options[keys[i]], replace, exceptionInfo);
    if(err < 0)
      return err;
  }

  return 0;
}

//---------------------------------------------------------------------------------------

/**
This function compares which arc groups are defined in the DB and in the project and complete the
setup if there are differences

@param exceptionInfo  Details of any errors are returned here.

@return               INTEGER - 0 if no error, a negative number in case of error (check exceptionInfo).
*/

int fwRDBArchiver_compareAndCompleteArchiveGroups(dyn_string &exceptionInfo)
{
  int err;
  string sFunction = "fwRDBArchiver_compareAndCompleteArchiveGroups";

  // oracle db - archive groups
  dyn_dyn_anytype ddaDbArc;
  dynClear(ddaDbArc);

  string sDbSqlSelect = "SELECT GROUP_NAME, TEMPLATE_NAME, UDAG FROM ARC_GROUP";// WHERE STATUS = 'CURRENT'";
  runRealSQLQuery(sDbSqlSelect, ddaDbArc);

  dyn_string dsDbArc;
  for(int i=1;i<=dynlen(ddaDbArc);i++)
  {
    dsDbArc[i] = ddaDbArc[i][1];
  }

  // system db - archive groups
  dyn_string dsDp, dsSysArc;

  dsDp=dpNames("*","_RDBArchiveGroups");
  for(int i=1;i<=dynlen(dsDp);i++)
  {
    dpGet(dsDp[i] + ".groupName", dsSysArc[i]);
  }
  dynSortAsc(dsSysArc);


  //IM106848 create DPs in WinCC_OA
  if (dynlen(dsDbArc) > dynlen(dsSysArc))
  {
    for (int i = dynlen(dsDbArc); i >= 1; i--)
    {
      if (dynContains(dsSysArc, dsDbArc[i]) <= 0)
      {
        string idp = "_" + dsDbArc[i];
        DebugN(ddaDbArc);
        dpCreate(idp, "_RDBArchiveGroups");
        err = dpSetWait(idp+".templateName:_original.._value",  ddaDbArc[i][2],
                        idp+".groupName:_original.._value",     dsDbArc[i],
                        idp+".managerNr:_original.._value",     97,
                        idp+".isAlert:_original.._value",       "ALERT" == ddaDbArc[i][2],
                        idp+".isUserDefined:_original.._value", ddaDbArc[i][3]);
        if (err)
        {
          fwException_raise(exceptionInfo, "ERROR",
                             sFunction + " - Cannot create DP for arc group: " + idp,"");
          return - 1;
        }
        fwException_raise(exceptionInfo, "INFO",
                          sFunction + " - Creating DP for arc group: " + idp,"");
      }
    }
  }


  return 0;
}

//---------------------------------------------------------------------------------------

/**
This function creates if necessary arc groups for the parallel archiving of value archivers

@param exceptionInfo  Details of any errors are returned here.

@return               INTEGER - 0 if no error, a negative number in case of error (check exceptionInfo).
*/

int fwRDBArchiver_enableMultipleVAForwarding(dyn_int VANumbers, dyn_string &exceptionInfo)
{
  int err;
  string sFunction = "fwRDBArchiver_enableMultipleVAForwarding";

  string configFile = getPath(CONFIG_REL_PATH, "config");
  dyn_string ValueArchiveMapping = makeDynString("*");

  paCfgReadValueList(configFile, "ValueArchiveRDB", "redirectArcGroup", ValueArchiveMapping);

  if(dynlen(ValueArchiveMapping) == 0)
  {
    fwInstallation_throw( "WARNING", sFunction + " - No mapping defined, adding global mapping VA*:EVENT","");
    err = _fwRDBArchiver_setConfigFileValue("ValueArchiveRDB", "redirectArcGroup", "VA*:EVENT", true, exceptionInfo);
    if(err < 0)
    {
      return err;
    }
  }

  string sPattern;

  dyn_string dsMatchResult;

  for (int i = 1; i <= dynlen(VANumbers); i++)
  {

    sPattern = "VA["+VANumbers[i]+"]:VA["+VANumbers[i]+"]";

    dsMatchResult = dynPatternMatch(sPattern, ValueArchiveMapping);
    if(dynlen(dsMatchResult) > 0)
    {
      err = fwRDBArchiver_enableVAForwarding(VANumbers[i], exceptionInfo);
      if(err < 0)
      {
        return err;
      }
      continue;
    }

    sPattern = "VA["+ VANumbers[i] +"*]*";
    DebugN(sPattern);

    dsMatchResult = dynPatternMatch(sPattern, ValueArchiveMapping);

    if(dynlen(dsMatchResult) > 0)
    {
      DebugTN(sFunction + " - INFO: Mapping found: " + dsMatchResult + " no need to create arc groups","");
      dpSet("_ValueArchive_" + VANumbers[i] + ".general.forwardToRDB:_original.._value", TRUE);
      dpSet("_ValueArchive_" + VANumbers[i] + "_2" + ".general.forwardToRDB:_original.._value", TRUE);
    }
  }


  return 0;
}

//---------------------------------------------------------------------------------------

/**
This function enables forwarding for the value archiver

@param VANum number of the archiver to enable the forwarding
@param exceptionInfo  Details of any errors are returned here.

@return               INTEGER - 0 if no error or arc group already exits, a negative number in case of error (check exceptionInfo).
*/

int fwRDBArchiver_enableVAForwarding(int VANum, dyn_string &exceptionInfo)
{
  int err;
  string sFunction = "fwRDBArchiver_enableVAForwarding";
  DebugTN(sFunction + " - INFO: Enabling Parallel Archiving for value archive "+VANum);

  int    i, iManNum = 99;
  string sDp, sPath, sName, sGroup, sTemplate;
  bool   isAlert = false, isUserDefined = false;

  dyn_string      dsRDB;
  dyn_dyn_anytype ddaTable;

  dsRDB = dpNames("*","_RDBArchiveGroups");

  sTemplate = "EVENT";

  // group name
  sGroup = "VA" + VANum;

  sDp = "_" + sGroup;

  for(i = 1; i<= dynlen(dsRDB);i++)
  {
    dpGet(dsRDB[i] + ".groupName:_online.._value", sName);
    if (sGroup == sName && dpExists(sDp))
    {
      return 0;
    }
  }

  // db path
  err = runRealSQLQuery("select value from arc_config where name = 'def_dbfile_path'", ddaTable);
  if(err < 0)
  {
    fwException_raise(exceptionInfo, "ERROR",
                          sFunction + " - Cannot read def_dbfile_path from the DB","");
    return -1;
  }

  if (dynlen(ddaTable) > 0 && ddaTable[1][1] != "")
  {
    sPath = ddaTable[1][1];
  }



  // create group
  sGroup = strtoupper(sGroup);
  err = ag_createGroup(iManNum, sTemplate, sGroup, sPath, isAlert, isUserDefined, /*show message if already exists */ false);

  if ( err < 0 )
  {
    if(err != -99)  // -99 is nothing was created because it already exists in DB and User has canceled
    {
      fwException_raise(exceptionInfo, "INFO",
                          sFunction + " - Arc group already exists in the DB!","");
      return 0;
    }
    fwException_raise(exceptionInfo, "ERROR",
                          sFunction + " - Cannot create arc group in the DB!","");
    return - 1;
  }

  // set dpe ForwardToRDB=TRUE from dp _ValueArchive_# - after the RDBArchiveGroup was created. Otherwise the DATA will send the archived elements to the RDB.
  //  but the RDB can not add the elements, because the ArchiveGroup doesn't exist
  dpSet("_ValueArchive_" + VANum + ".general.forwardToRDB:_original.._value", TRUE);
  dpSet("_ValueArchive_" + VANum + "_2" + ".general.forwardToRDB:_original.._value", TRUE);

  return 0;
}

//@}
