//=================================================================================
// this library enables the developer to build and fill blob dpes from a simple
// text, line-oriented "command file".
// 
//==============================================================================
//         HISTORY OF MODIFICATIONS
//         
//         ORIGINAL AUTHOR: Giulio
//
//          2016-07-25 saved as 3.14 G.M.
//

//==============================================================================
/*
int fwBlobFill_processFile(string PLC, string filename)
int fwBlobFill_Config_CheckLine(string buf)
int fwBlobFill_Config_ProcessLine(string buf)
int fwBlobFill_ProcessPLC(string buf)
int fwBlobFill_ProcessWrite(string buf)
int fwBlobFill_ProcessRead(string buf)
int fwBlobFill_ProcessString(string buf)
int fwBlobFill_ProcessFreeString(string buf)
int fwBlobFill_ProcessNString(string buf)
int fwBlobFill_ProcessInt(string buf)
int fwBlobFill_ProcessByte(string buf)
void fwBlobFill_Clean()
int fwBlobFill_IntDebug(string buf)
*/
blob L_blob;
global string LL_plc;
//=======================================
// SYNTAX
//
// #blablabla this is a comment line
// CLEAN                      : will clean L_blob
// WRITE <blob dpe>           : will write L_blob into <blob_dpe>
// READ <blob_dpe>            : will read from <blob_dpe> into L_blob
// STRING <string>            : will read a free string and addd it to L_blob
// NSTRING <nchar> <string>   : will read a string and add it to L_blob. 
//                            : If the string is too long, it will be cut; 
//                            : if it is too short, spaces will be added
// INT <value>                : will add an integer to L_blob
// BYTE <value>               : will add a byte to L_blob
// PLC <plcname>              : ad-hoc for the Rack Control application
//=======================================

#uses "fwRack/fwRackDeprecated.ctl"


int fwBlobFill_processFile(string PLC, string filename)
{

int i;
file f;
int status;
int check_status;
int process_status;
string buf;

LL_plc = PLC;

f=fopen(filename,"r");
if (f == 0) {DebugTN("Cannot open file");return(-1);}
for(;;) {
  if (feof(f)) break;
  i++;
  fgets(buf,200,f);
  status = fwBlobFill_Config_CheckLine(buf);
  if (status != 0) {
     check_status = -1;
     DebugTN("PROBLEM AT LINE "+i+": ");
     DebugTN("The line is : "+substr(buf,0,strlen(buf)-1));
  }
}
fclose(f);

if (check_status == -1) {
  DebugTN("Your file is crappy and full of bugs. Fix it before bothering me again !");
  return(-1);
}


i = 0;
f = fopen(filename,"r");
for(;;) {
  if (feof(f)) break;
  i++;
  fgets(buf,200,f);
  status = fwBlobFill_Config_ProcessLine(buf);
  if (status != 0) {
     process_status = -2;
     DebugTN("PROBLEM AT LINE "+i+": ");
     DebugTN("The line is : "+buf);
  }
}
fclose(f);
DebugTN("in  fwBlobFill_processFile: bloblen ="+bloblen(L_blob));
return(process_status);

}

//=======================
int fwBlobFill_Config_CheckLine(string buf)
{
string cmd;

if (strlen(buf) < 2) return(0); //ignore short lines
sscanf(buf,"%s",cmd);
if (cmd[0] == '#') return(0); //REMARK

switch(cmd) {
  case "CLEAN":
  case "WRITE":
  case "READ":
  case "STRING":
  case "NSTRING":
  case "INT":
  case "BYTE":
  case "DEBUG_INT":
  case "PLC":
  return(0);break;
  default: return(-1);
}
  
}

//=======================
int fwBlobFill_Config_ProcessLine(string buf)
{
string cmd;

if (strlen(buf) < 2) return(0); //ignore short lines 
sscanf(buf,"%s",cmd);
if (cmd[0] == '#') return(0); //REMARK

switch(cmd) {
  case "CLEAN":       fwBlobFill_Clean(); return(0);break;
  case "WRITE":       return(fwBlobFill_ProcessWrite(buf));break;
  case "READ":        return(fwBlobFill_ProcessRead(buf));break;
  case "STRING":      return(fwBlobFill_ProcessString(buf));break;
  case "FREESTRING":  return(fwBlobFill_ProcessFreeString(buf));break;
  case "NSTRING":     return(fwBlobFill_ProcessNString(buf));break;
  case "INT":         return(fwBlobFill_ProcessInt(buf));break;
  case "BYTE":        return(fwBlobFill_ProcessByte(buf));break;
  case "DEBUG_INT":   return(fwBlobFill_IntDebug(buf));break;
  case "PLC":         return(fwBlobFill_ProcessPLC(buf));break;
  return(0);break;
  default: return(-1);
}
  
}

//====================
int fwBlobFill_ProcessPLC(string buf)
{
  string cmd,val;
  sscanf(buf,"%s %s",cmd,val);
  
  if (dpExists("RCA/"+val)==FALSE) {DebugTN("PLC does not exist");return(-1);}
  if (dpTypeName("RCA/"+val) != "FwRackPowerDistribution") {DebugTN("not a PLC");return(-1);}
  LL_plc = val;
  return(0);
}
//====================
int fwBlobFill_ProcessWrite(string buf)
{
  string cmd,dpe;

  sscanf(buf,"%s %s",cmd,dpe);
  if (dpe == "") return(-1);
  dpe = "RCA/"+LL_Plc+".Tables."+dpe;
  if (dpElementType(dpe)!= 46) {
      DebugTN(dpe+" is not a blob");return(-1);
  }
  dpSetWait(dpe,L_blob);
  return(0);
}
//=====================
int fwBlobFill_ProcessRead(string buf)
{
  string cmd,dpe;
  
  sscanf(buf,"%s %s",cmd,dpe);
  if (dpe == "") return(-1);
  dpe = "RCA/"+LL_Plc+".Tables."+dpe;
  if (dpElementType(dpe)!= 46) {
      DebugTN(dpe+" is not a blob");return(-1);
  }
  dpGet(dpe,L_blob);
  return(0);
}
//=====================
int fwBlobFill_ProcessString(string buf)
{
  string cmd,sval;
  
  sscanf(buf,"%s %s",cmd,sval);
  if (sval == "") return(-1);
  blobAppendValue(L_blob,sval,strlen(sval));
  return(0); 
}
//=====================
int fwBlobFill_ProcessFreeString(string buf)
{
  string cmd,sval;
  
  sscanf(buf,"%s %[^\n]",cmd,sval);
  if (sval == "") return(-1);
  blobAppendValue(L_blob,sval,strlen(sval));
  return(0); 
}
//=====================
int fwBlobFill_ProcessNString(string buf)
{
  string cmd,sval;
  int n;
  
  sscanf(buf,"%s %d %s",cmd,n,sval);
  if (n <= 0) return(-1);
  if (sval == "") return(-1);
  if (strlen(sval) > n) sval = substr(sval,0,n);
  if (strlen(sval) < n) for(;;) {sval = sval+" "; if (strlen(sval) == n) break;}
  blobAppendValue(L_blob,sval,n);
  return(0); 
}

//=====================
int fwBlobFill_ProcessInt(string buf)
{
  int i;
  string cmd;
  int val;
  
  i=sscanf(buf,"%s %d",cmd,val);
  if (i < 2) return(-1);

  blobAppendValue(L_blob,val,2,TRUE);
  return(0); 
}

//=====================
int fwBlobFill_ProcessByte(string buf)
{
  int i;
  string cmd;
  char cval;
  int val;
  
  i=sscanf(buf,"%s %d",cmd,val);
  if (i < 2) return(-1);
  if ((val > 255) || (val < 0)) return(-1);
  cval = val;
  blobAppendValue(L_blob,cval,1);
  return(0); 
}


//=====================
void fwBlobFill_Clean()
{
  blobZero(L_blob,0);
}
//========================
int fwBlobFill_IntDebug(string buf)
{
  dyn_int iarr;
  string cmd;
  int pos,len;
  
  sscanf(buf,"%s %d %d",cmd,pos,len);
  DebugTN("bloblen=",bloblen(L_blob));
  iarr=fwAuxil_blobGetValue_int(L_blob,pos,len);
  DebugTN("iarr=",iarr);
  return(0);
}


