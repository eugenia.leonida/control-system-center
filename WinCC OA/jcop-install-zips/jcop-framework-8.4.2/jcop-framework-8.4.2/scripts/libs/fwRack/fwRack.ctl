//===================================================================
//         MODIFICATION HISTORY
//
//         ORIGINAL AUTHOR: Bobby
//
//         18-3-2013 G.M. cosmetic changes
//
//          2016-07-25 saved as 3.14 G.M.
//

//
//         2016-07-25 G.M  added some default replacements in fwRack_formatString

#uses "fwRack/fwRackDeprecated.ctl"

#uses "fwConfigs/fwConfigs.ctl"
#uses "fwDevice/fwDevice.ctl"
#uses "fwFSM/fwFsmTreeDisplay.ctl"

//=========================================================
string fwRack_deleteChars(string sOriginal, string sIlegalChars)
{											
	dyn_char  cOriginal;
	string	  sTrimed = "";
	
	return fwRack_formatString(sOriginal);
}

//=========================================================
// New function included by Bobby. 11-2-2009. fwRack 10.1
string fwRack_formatString (string sStringToBeFormat)
{
	dyn_string dsParameters;
	dyn_string dsReplacements;
  
	dpGet("fwRackSettings.Characters.parametersToBeReplaced", dsParameters);
	dpGet("fwRackSettings.Characters.replacementParameters", dsReplacements);
	
  //--G.M. 2016-07-25 added some default replacements
  if (dynContains(dsParameters,".") < 1) {
        dynAppend(dsParameters,"."); 
        dynAppend(dsReplacements,"_");
  }
  if (dynContains(dsParameters," ") < 1) {
        dynAppend(dsParameters," "); 
        dynAppend(dsReplacements,"");
  }
  
	for (int i=1; i <= dynlen(dsParameters);i++) {
		if (dynlen(dsReplacements)>=i) strreplace(sStringToBeFormat,dsParameters[i],dsReplacements[i]);
		else strreplace(sStringToBeFormat,dsParameters[i],"");
	}
	return sStringToBeFormat; 
}

//=========================================================
void fwRack_deleteCrate(string dpName)
{
  dyn_string dsExceptions;
  fwDevice_delete(dpName, dsExceptions);
}

//=========================================================
void fwRack_setAlarm (dyn_string 		dpList,
                      int		        iCode,
                      dyn_string 		dsText,
                      dyn_string		dsAlarmType,
                      dyn_float		dfParameters)
{
		
		dyn_string exceptionInfo;			
		fwAlertConfig_deactivateMultiple(dpList, exceptionInfo);
		fwAlertConfig_setMultiple(dpList,
                            iCode,
                            dsText,
                            dfParameters,
                            dsAlarmType,
                            makeDynString(), 
                            "", 
                            makeDynString(),  
                            "", 
                            exceptionInfo);
		fwAlertConfig_activateMultiple(dpList, exceptionInfo);													
}

//=========================================================
bool isAlertConfiguredForProperty(string propertyAddress) 
{
        int iConfigType;
        
        dpGet(dpSubStr(propertyAddress,DPSUB_SYS_DP_EL) + ":_alert_hdl.._type", iConfigType);
        return (iConfigType != 0);
}

//=========================================================
string textForProperty(string propertyAddress, string alertStatus="", anytype value="")
{
        string text;
        
        if (alertStatus=="") dpGet(dpSubStr(propertyAddress,DPSUB_SYS_DP_EL) + ":_alert_hdl.._act_state_text",
                                   alertStatus);
        text=alertStatus;
        if (value=="") dpGet(propertyAddress,value);
        int iType = dpElementType (propertyAddress);
        if (iType == DPEL_INT || iType == DPEL_FLOAT) {
                string str = value;
                dyn_string dsStrLen = strsplit (str,".");
                int iLenVal = strlen(dsStrLen[1]);
                int iLenDec = 0;
                if (dynlen(dsStrLen) >1 ) iLenDec = strlen(dsStrLen[2]);
                int iLen = iLenVal;
                //DebugTN($sPropertyName +" " +iLenVal +" " + iLenDec +" " + dsStrLen);
                if (iLenDec){
                        iLen = iLen + iLenVal +1;
                        iLenDec =1;
                }
                text+= " [" +strformat ("\\left{%"+ iLen +"."+iLenDec +"}",iLen-iLenDec,value)  + "]";
        }
        return text;
}

//=========================================================
void fwRack_createTurbine (string sRackName, string sBus, string sELMB, string sPort , string sNode, 
                           string label = "Turbine", int position = 0, int height =4, string side ="Front" )
{
  string      sCard= ELMB_CAN_CARD_KVASER;    
  int         busSpeed = 125000;
  bool        bUseDefaultAddressing =1;
  dyn_string  dsException;
  string      dpELMB;  
  
  //First we check if the CAN bus is existing. If not, it is created with the default 125000 spees and the
  //name provided
  if (!dpExists("ELMB/" + sBus)) {
    fwElmbUser_createCANbus(sBus,   "Rack Monitoring BUS. "+  sRackName,     
                            sPort,  sCard,  busSpeed,  
                            bUseDefaultAddressing, dsException);  
    if (dynlen(dsException)) {DebugTN(dsException);return;}
  }
  //The ELMB dp name
  dpELMB = "ELMB/" +sBus  + "/" + sELMB; 
  //Check now if the ELMB exists. If it doesnt it is created in the requested BUS with the requested name
  if (!dpExists(dpELMB)) {
      fwElmbUser_createElmb( sELMB,  "Rack Monitoring ELMB. " +  sRackName,  
                             sBus ,  sNode, 
                             bUseDefaultAddressing, dsException);   
      if (dynlen(dsException)) {DebugTN(dsException); return;}
  }
  //Creates the sensors for the monitoring ELMB       
  _fwRack_createSensorsWithAlert (dpELMB, sBus,sELMB);  
  
  string sRackDp;
  fwRack_getRackDp(sRackName,sRackDp);
  sRackDp= dpSubStr(sRackDp,DPSUB_DP);
  //Create the crate dp if it doesnt exist  
  if (!dpExists(sRackDp)) dpCreate (sRackDp + "/" + label, "FwRackCrate");
  
  dpSetWait(sRackDp + "/" + label + ".dp",       dpELMB,
            sRackDp + "/" + label + ".panel",    "fwRack/fwRackTurbine.pnl",
            sRackDp + "/" + label + ".Type",     "Turbine",
            sRackDp + "/" + label + ".Position", position,
            sRackDp + "/" + label + ".Side",     side, 
            sRackDp + "/" + label + ".Height",   height);
}

//=========================================================
void fwRack_deleteTurbine (string sELMBNode, string sRack, string label = "Turbine")
{
  dyn_string    ex;
  dyn_string	dsPropertyAddresses = makeDynString(sELMBNode + "/AI/RC_Temperature_0.value",
                                                 sELMBNode + "/AI/RC_Temperature_1.value",
						                                           sELMBNode + "/AI/RC_Humidity_2.value",																									
						                                           sELMBNode + "/AI/RC_SupplyVoltage_3.value",
						                                           sELMBNode + "/AI/RC_TurbineCurrent_4.value",
						                                           sELMBNode + "/AI/RC_TurbineCurrent_5.value",
						                                           sELMBNode + "/AI/RC_AirFlow_6.value",																									
						                                           sELMBNode + "/AI/RC_ThermoSwitch_10.value",
						                                           sELMBNode + "/AI/RC_Door_11.value",
					                                            sELMBNode + "/AI/RC_Card_12.value");
  string sRackName;
  fwDevice_getName(sRack,sRackName,ex);
  if (dpExists(sELMBNode)) {
        fwRackInterface_deleteRackProperties ("","", sRackName, dsPropertyAddresses);
  }
  //dpSet(sRack + ".Crates.ElmbDp", "");
  if (dpExists(sRack + "/" + label)) dpDelete(sRack + "/" + label);
}



//=========================================================
void _fwRack_createSensorsWithAlert (string dpELMB, string sBUS, string sELMB)
{
  dyn_string dsException;
  bool bUseDefaultAddressing= true;  
  
  /// ----> FIRST TEMPERATURE SENSOR ///
  if (!dpExists(dpELMB+"/AI/RC_Temperature_0.value")) {
    fwElmbUser_createSensor("RC_Temperature_0", sBUS, sELMB, "Temperature Rack Sensor", makeDynString("0"),  
                            "RC_Temperature",   makeDynFloat(),  bUseDefaultAddressing,	dsException);
    fwRack_setAlarm(makeDynString ( dpELMB+"/AI/RC_Temperature_0.value"),13,
                    makeDynString ( "OK","TOO HIGH"), makeDynString ("","_fwErrorNack."), makeDynFloat(40.0));		
  }	
  dpSetUnit(dpELMB+"/AI/RC_Temperature_0.value","ºC");					
  
  /// ----> SECOND TEMPERATURE SENSOR ///
  if (!dpExists(	dpELMB+"/AI/RC_Temperature_1.value")) {
    fwElmbUser_createSensor("RC_Temperature_1", sBUS, sELMB, "Temperature Rack Sensor",	makeDynString("1"),
                            "RC_Temperature", makeDynFloat(), bUseDefaultAddressing, dsException);
    fwRack_setAlarm(makeDynString (  dpELMB+"/AI/RC_Temperature_1.value"), 13,
		                  makeDynString ( "OK","TOO HIGH"), makeDynString ("","_fwErrorNack."),makeDynFloat(40.0));	 
  }
  dpSetUnit(dpELMB+"/AI/RC_Temperature_1.value","ºC");
  
  /// ----> HUMIDITY SENSOR ///		  
  if (!dpExists(dpELMB+"/AI/RC_Humidity_2.value")) {
    fwElmbUser_createSensor("RC_Humidity_2", sBUS, sELMB, "Humidity Rack Sensor", makeDynString("2"),			
                            "RC_Humidity", makeDynFloat(), bUseDefaultAddressing, dsException);
    fwRack_setAlarm (makeDynString ( dpELMB+"/AI/RC_Humidity_2.value"), 13,
		                   makeDynString("OK","TOO HIGH"), makeDynString("","_fwErrorNack."), makeDynFloat(80.0));	
  }
  dpSetUnit(dpELMB+"/AI/RC_Humidity_2.value","%");
		
  /// ----> SUPLY VOLTAGE ///
  if (!dpExists(dpELMB+"/AI/RC_SupplyVoltage_3.value")) {
    fwElmbUser_createSensor("RC_SupplyVoltage_3", sBUS, sELMB, "Supply voltage", makeDynString("3"),			
                            "RC_SupplyVoltage", makeDynFloat(), bUseDefaultAddressing, dsException);	
    fwRack_setAlarm(makeDynString ( dpELMB+"/AI/RC_SupplyVoltage_3.value"), 13,
			 makeDynString("TOO LOW","OK"), makeDynString ("_fwErrorNack.",""), makeDynFloat(9));	
  }
  dpSetUnit(dpELMB+"/AI/RC_SupplyVoltage_3.value","V");
  
  ///  ----> FIRST TURBINE CURRENT SENSOR///
  if (!dpExists(dpELMB+"/AI/RC_TurbineCurrent_4.value")) {
    fwElmbUser_createSensor("RC_TurbineCurrent_4", sBUS, sELMB, "Turbine Current 1", makeDynString("4"),			
                            "RC_TurbineCurrent", makeDynFloat(), bUseDefaultAddressing, dsException);	
    fwRack_setAlarm(makeDynString ( dpELMB+"/AI/RC_TurbineCurrent_4.value"), 13,
			                 makeDynString ("TOO LOW","OK","TOO HIGH"),makeDynString ("_fwErrorNack.","","_fwErrorNack."),
                    makeDynFloat(0.7,1.8));						
  }
  dpSetUnit(dpELMB+"/AI/RC_TurbineCurrent_4.value","A");

  ///  ----> SECOND TURBINE CURRENT SENSOR///		
  if (!dpExists(dpELMB+"/AI/RC_TurbineCurrent_5.value")) {
    fwElmbUser_createSensor("RC_TurbineCurrent_5", sBUS, sELMB, "Turbine Current 2", makeDynString("5"),			
                            "RC_TurbineCurrent", makeDynFloat(), bUseDefaultAddressing, dsException);			
    fwRack_setAlarm (makeDynString(dpELMB+"/AI/RC_TurbineCurrent_5.value"),13,
			                  makeDynString ("TOO LOW","OK","TOO HIGH"), makeDynString ("_fwErrorNack.","","_fwErrorNack."),
                     makeDynFloat(0.7,1.8));		
  }
  dpSetUnit(dpELMB+"/AI/RC_TurbineCurrent_5.value","A");

  /// ----> THERMO SWITCH SENSOR///
  if (!dpExists(dpELMB+"/AI/RC_ThermoSwitch_10.value")) {
    fwElmbUser_createSensor("RC_ThermoSwitch_10", sBUS, sELMB, "Thermo-Switch Rack Sensor", makeDynString("10"),		
                            "Direct ADC Voltage",makeDynFloat(1.0), bUseDefaultAddressing, dsException);
    fwRack_setAlarm(makeDynString (dpELMB+"/AI/RC_ThermoSwitch_10.value"),13,
			                 makeDynString ( "CLOSED","OPEN"),makeDynString ("","_fwErrorNack.") ,makeDynFloat(1000000.0));	
  }
  
  /// ----> CARD IN SITU SENSOR ///
  if (!dpExists(dpELMB+"/AI/RC_Card_12.value")) {
    fwElmbUser_createSensor("RC_Card_12", sBUS, sELMB, "Card Rack Sensor",makeDynString("12"),          
                            "Direct ADC Voltage", makeDynFloat(1.0),bUseDefaultAddressing, dsException);	
    fwRack_setAlarm(makeDynString(dpELMB+"/AI/RC_Card_12.value"),	13,
			                 makeDynString ("REMOVED","IN SITU"),makeDynString ("_fwErrorNack.","") ,makeDynFloat(1000.0));	
  }	
}

//=========================================================
string fwRack_getTurbine(string dpRackName)
{
	
	dyn_string dsChildren;
	dyn_string dsChildrenTypes;
	//DebugTN("Rack dp: " +dpRackName);
	dpGet(dpRackName + ".Crates.Type", dsChildrenTypes);
	dpGet(dpRackName + ".Crates.Label", dsChildren);
	//DebugTN(dsChildrenTypes,dsChildren);
	int iTurbinePos =	dynContains (dsChildrenTypes, "Turbine 4U");
	//DebugTN(iTurbinePos);
	return "fwRCA_" + dsChildren[iTurbinePos];
}





//=========================================================
void fwRack_addCrates( string	sDpRackName,
                       dyn_string	dsCrateNames,
                       dyn_string	dsCrateTypes,
                       dyn_float	dfPosition,
                       dyn_float	dfHeight,
                       dyn_string	dsLabel,
                       dyn_int	diSide,
                       dyn_string    dsDps ,
                       dyn_string    dsPanels )
{
			
  dyn_string	dsOldCrateNames, dsOldCrateTypes, dsOldLabel,dsOldDP, dsOldPanel;					
  dyn_float	dfOldPosition, dfOldHeight;
  dyn_int	diOldSide;
  int iNumber = dynlen(dsLabel);
  
  if (iNumber != dynlen(dsCrateNames) ||
      iNumber != dynlen(dsCrateTypes) ||
      iNumber != dynlen(dfPosition) ||
      iNumber != dynlen(dfHeight) ||
      iNumber != dynlen(diSide) ||
      iNumber != dynlen(dsDps) ||
      iNumber != dynlen(dsPanels )){
    DebugTN("Cannot create crates: missing paramenters");
    return; 
  }
  	
  dpGet(sDpRackName + ".Crates.Name",		dsOldCrateNames);
  dpGet(sDpRackName + ".Crates.Type",		dsOldCrateTypes);
  dpGet(sDpRackName + ".Crates.Position",	dfOldPosition);
  dpGet(sDpRackName + ".Crates.Height",		dfOldHeight);
  dpGet(sDpRackName + ".Crates.Side",		diOldSide);
  dpGet(sDpRackName + ".Crates.Label",		dsOldLabel);
  dpGet(sDpRackName + ".Crates.DP",		dsOldDP);
  dpGet(sDpRackName + ".Crates.Panel",		dsOldPanel);
			
  dynAppend(	dsOldCrateNames,	dsCrateNames);
  dynAppend(	dsOldCrateTypes,	dsCrateTypes);
  dynAppend(	dfOldPosition,  	dfPosition);
  dynAppend(	dfOldHeight,		dfHeight);
  dynAppend(	diOldSide,		diSide);				
  dynAppend(	dsOldLabel,		dsLabel);
  dynAppend(	dsOldDP,		dsDps);				
  dynAppend(	dsOldPanel,		dsPanels);	
							
  dpSet(sDpRackName + ".Crates.Name",		dsOldCrateNames);
  dpSet(sDpRackName + ".Crates.Type",		dsOldCrateTypes);
  dpSet(sDpRackName + ".Crates.Position",	dfOldPosition);
  dpSet(sDpRackName + ".Crates.Height",		dfOldHeight);
  dpSet(sDpRackName + ".Crates.Side",		diOldSide);
  dpSet(sDpRackName + ".Crates.Label",		dsOldLabel);
  dpSet(sDpRackName + ".Crates.DP",		dsOldDP);
  dpSet(sDpRackName + ".Crates.Panel",		dsOldPanel);
}

//=========================================================
void fwRack_createCrate(dyn_string 	dsCrateInfo,
                        string		    sDpName,
                        dyn_string 	&dsExceptionInfo,
                        string      dp="",
                        string      panel="")
{

	const int	iLabel = 1,	
           iPosition= 2,
           iHeight	= 3,
           iType	= 4,
           iName	= 5,
           iSide = 6;						
						
	string		sName, alias, deviceName;		
	
  //sDpName = dpSubStr(sDpName, DPSUB_DP);
  if (!dpExists(sDpName)) dpCreate (sDpName, "FwRackCrate");
        
  dpSetWait(sDpName + ".Label",	  dsCrateInfo[1],
            sDpName + ".Position",dsCrateInfo[2],
            sDpName + ".Height",  dsCrateInfo[3],
            sDpName + ".Type",	   dsCrateInfo[4],
            sDpName + ".Name",	   dsCrateInfo[5],
            sDpName + ".Side",	   dsCrateInfo[6],
            sDpName + ".dp",	     dp,
            sDpName + ".panel",	  panel);            					
}


//=========================================================
bool fwRack_rackExists(string sRackLabel, string sDpName)
{
  string  sName = sDpName 	+ fwDevice_HIERARCHY_SEPARATOR 	+ sRackLabel;		
  if (dpExists(sName)) return true;
  else return false;	
}




//=========================================================
void fwRack_createRack(dyn_string 	dsZoneInfo,
                       dyn_string	dsSides,
                       string 	sDpName,
                       dyn_string 	&dsExceptionInfo)
{			
  
  const int   
        iDBLabel     = 1,
        iLabel       = 2,						
        iWidth	      = 3,
        iDepth	      = 4,
        iHeight      = 5,
        iX	          = 6,
        iY	          = 7,
        iOrientation  = 8,
        iRackNumber   = 9,
        iRackRow	    = 10,
        iWeight	    = 11,
        iDetector	    = 12,
        iResponsible  = 13,
        iUsage	       = 14,
        iComment	      = 15,
        iItemID	      = 16;
	
  string  sName, 
          alias;

  if (!dpExists( sDpName+fwDevice_HIERARCHY_SEPARATOR + dsZoneInfo[iLabel])) {		
      fwDevice_create(makeDynString(dsZoneInfo[iLabel], "FwRack"), 
                      makeDynString(sDpName, ""), dsExceptionInfo);
  }
  else {
      DebugTN("RCA" + "Rack already exists, updating data...");
  }
  sName = sDpName	+ fwDevice_HIERARCHY_SEPARATOR 	+ dsZoneInfo[iLabel];		
  dpSet(sName + ".Label",			dsZoneInfo[iLabel]);	
  if (dsZoneInfo[iDBLabel] !="Lock") dpSet(sName + ".DBLabel",	dsZoneInfo[iDBLabel]);	
  dpSet(sName + ".Geometry.Width",		dsZoneInfo[iWidth]);
  dpSet(sName + ".Geometry.Depth",		dsZoneInfo[iDepth]);
  dpSet(sName + ".Geometry.Height",		dsZoneInfo[iHeight]);
  dpSet(sName + ".Position.X",			dsZoneInfo[iX]);
  dpSet(sName + ".Position.Y",			dsZoneInfo[iY]);
  dpSet(sName + ".Position.Orientation",	dsZoneInfo[iOrientation]);
  dpSet(sName + ".Description.RackNumber",	dsZoneInfo[iRackNumber]);
  dpSet(sName + ".Description.RackRow",		dsZoneInfo[iRackRow]);
  dpSet(sName + ".Description.Weight",		dsZoneInfo[iWeight]);
  dpSet(sName + ".Description.Sides",		dsSides);
  dpSet(sName + ".Origin.Detector",		dsZoneInfo[iDetector]);
  dpSet(sName + ".Origin.Responsible",		dsZoneInfo[iResponsible]);
  dpSet(sName + ".Origin.Usage",		dsZoneInfo[iUsage]);
  dpSet(sName + ".Origin.Comments",		dsZoneInfo[iComment]);	
  dpSet(sName + ".ItemID",			dsZoneInfo[iItemID]);
  
  //alias = dpGetAlias(sDpName + ".") + fwDevice_HIERARCHY_SEPARATOR + dsZoneInfo[iLabel] ;
  //dpSetAlias(sDpName + fwDevice_HIERARCHY_SEPARATOR + dsZoneInfo[iLabel] + ".", alias); 	
}


//=========================================================
bool fwRack_zoneExists(	string 	sZoneLabel,string 	sDpName)
{
  string sName = sDpName 			+ fwDevice_HIERARCHY_SEPARATOR 	+ sZoneLabel;		
  if (dpExists(sName)) return true;
  else return false;
}


//=========================================================
void fwRack_getRackDp (string sRackName , string &sDpRack)
{
  dyn_string dsRacks = dpNames("RCA/*/" + sRackName,"FwRack");
  if (dynlen(dsRacks)>0) sDpRack = dsRacks[1];
}


//=========================================================
void fwRack_createRackZone(dyn_string 	dsZoneInfo,
                           string 		sDpName,
                           dyn_string 	&dsExceptionInfo)
{									
									
  const int	iLabel	 	= 1,
		iDBLabel	= 2,
		iItemID		= 3,
		iDescription	= 4,
		iWidth 	        = 5,
		iDepth		= 6,
		iDrawing	= 7,
		iHeight		= 8;
	
  string	sName;
  
  if (!dpExists(sDpName+fwDevice_HIERARCHY_SEPARATOR + dsZoneInfo[iLabel])) {		
    fwDevice_create(makeDynString(dsZoneInfo[iLabel], "FwRackZone"), 
                    makeDynString(sDpName, ""), dsExceptionInfo);
  }
  else{
    DebugTN("Rack zone already exists, updating data...");
  }
  sName = sDpName	+ fwDevice_HIERARCHY_SEPARATOR 	+ dsZoneInfo[iLabel];		

  if (dsZoneInfo[iDBLabel] !="Lock") dpSet(sName + ".Info.DBLabel", dsZoneInfo[iDBLabel]);	
  dpSet(sName + ".Info.Item_id",	dsZoneInfo[iItemID]);
  dpSet(sName + ".Info.Description",	dsZoneInfo[iDescription]);		
  dpSet(sName + ".Info.Drawing",	dsZoneInfo[iDrawing]);														
  dpSet(sName + ".Geometry.Width",	(int)dsZoneInfo[iWidth]);
  dpSet(sName + ".Geometry.Depth",	(int)dsZoneInfo[iDepth]);	
  dpSet(sName + ".Geometry.Height",	(int)dsZoneInfo[iHeight]);			
}


//=========================================================
void fwRack_tip(  string	sMessage)
{
	dyn_float	df;
	dyn_string	ds;
 
 ChildPanelOnCentralModalReturn("fwRack/fwRackHelp.pnl", "Rack Message",
                                 makeDynString(	"$sMessage:" + sMessage),df, ds);	
}


//=========================================================
void fwRackInterface_deleteRackProperties(string Source,
                                          string ZoneName, 
                                          string RackName, 
                                          dyn_string PropertyAddress)
{
  dyn_string	dsFinalProperties,
             dsFinalDescription,
		           dsFinalAddresses,
		           dsFinalTypes,
		           dsFinalRackVisible,
		           dsFinalZoneVisible;
  string 	dpRackName;
	
  //DebugTN("Hello, this is fwRackInterface_deleteRackProperties.");
  //DebugTN(Source, ZoneName,  RackName,  PropertyAddress);
  strreplace(RackName, "/","_");		
  RackName = strrtrim(RackName,"UPS");	
  ZoneName =  fwRack_deleteChars (ZoneName   , " .-/=:");
  RackName =  fwRack_deleteChars (RackName   , " .-/=:");
	
  dyn_string dsRackDps = dpNames("RCA/*/"+RackName);  
  if (dynlen(dsRackDps)<=0) return;
  
  dpRackName = dsRackDps[1];	
  
  if (dpExists(dpRackName)) {
    dpGet(dpRackName + ".Operation.PropertyAddress", dsFinalAddresses);
    dpGet(dpRackName + ".Operation.PropertyDescription", dsFinalDescription);
    dpGet(dpRackName + ".Operation.PropertyName", dsFinalProperties);
    dpGet(dpRackName + ".Operation.ZoneVisible", dsFinalZoneVisible);
	      				
    while(dynlen(PropertyAddress)) {
      //deletes the porperties of the rack	
      while (dynContains(dsFinalAddresses,PropertyAddress[dynlen(PropertyAddress)])) {
        int iDeletedElement;
        iDeletedElement = dynContains(dsFinalAddresses,PropertyAddress[dynlen(PropertyAddress)]);
	       dynRemove(dsFinalAddresses,    iDeletedElement);
	       dynRemove(dsFinalDescription,  iDeletedElement);
	       dynRemove(dsFinalZoneVisible,  iDeletedElement);
	       dynRemove(dsFinalProperties,   iDeletedElement);
      }
      dynRemove(PropertyAddress,dynlen(PropertyAddress));
    }
    dpSet(dpRackName + ".Operation.PropertyAddress",dsFinalAddresses);
    dpSet(dpRackName + ".Operation.PropertyDescription",dsFinalDescription);
    dpSet(dpRackName + ".Operation.PropertyName",dsFinalProperties);
    dpSet(dpRackName + ".Operation.ZoneVisible",dsFinalZoneVisible);
    
  }
  else {
    ;//rack doesnt exist
  }								
}

//=========================================================
void  fwRack_deleteRackZone(string	sDpName)
{
    dyn_string	dsRacks, dsException ;
    int		iError;			

    if (dpExists(sDpName)) {		
         fwDevice_getChildren(sDpName, fwDevice_HARDWARE, dsRacks, dsException);
         fwRack_deleteRacks(dsRacks);
         dpDelete(sDpName);	      
    }
    else DebugTN("Zone " + sDpName +" does not exist. Unable to remove :-O");

}

//=========================================================
void fwRack_deleteRacks(dyn_string dsDpName)
{
  string sDpName;
  dyn_string dsCrates,dsException;
  int i;
  
  for (i = 1; i <= dynlen(dsDpName); i++) {
      sDpName = dsDpName[i];
      fwDevice_getChildren( sDpName, fwDevice_HARDWARE, dsCrates, dsException);
      //DebugTN(sDpName + " children: " + dsCrates);
      fwRack_deleteCrates(dsCrates);    
      if (dpExists(sDpName)) if (dpTypeName(sDpName) == "FwRack")	dpDelete(sDpName);	      
  }
}


//=========================================================
void fwRack_deleteCrates(dyn_string dsDpName)
{
  int i;
  string sDpName;
  
  for(i=1; i<=dynlen(dsDpName); i++) {
      sDpName = dsDpName[i];
      if (dpExists(sDpName)) if (dpTypeName(sDpName) == "FwRackCrate") dpDelete(sDpName);	                  
  }
}  
  



//=========================================================
dyn_string fwRack_decodeRacks (string encodedRacks)
{
  /*Reported Cases:
    LHCb: 
    When you meet TDM names with a length of 2+3*n, our suggestion is, to decode it, 
    and link it to the specified crates (???I guess they mean racks), if you meet TDM names with a length 
    of 2+2+3*n, leave it alone. It's for controlling the Marathons. Of course this method doesnt take
    into account ALICE cases... The software can not distinguish between an ALICE or an LHCb TDM.

    ALICE: 
      CR1-X00
      CR2-X00X01
      CR3-X00-X01  
      R-I16-I17
      R-I15
      R-A18
      
    The use of "-" from Alice is very unfortunate since it is well known that that characted can not 
    be used for dpNames    
  */
  dyn_string dsRacks;
  
  if (strpos(encodedRacks, "-") >= 0) {
    //ALICE  
    dyn_string dsParts = strsplit(encodedRacks,"-");
    string Zone = dsParts[1];
    for (int i = 2; i <= dynlen(dsParts);i++){
      int iLen = strlen(dsParts[i]);
      switch (iLen) {
        case 3: 
          dynAppend (dsRacks,dsParts[i]); break;
        case 6:
          dynAppend (dsRacks,substr(dsParts[i],0,3)); 
          dynAppend (dsRacks,substr(dsParts[i],3,3)); 
          break;
        case 5:
          dynAppend (dsRacks,substr(dsParts[i],0,5)); 
          break;
      default:
        DebugTN("Error, case: " + encodedRacks + " not reported")  ;
        return makeDynString();
      }
    }    
  } else {
    //LHCb    
    if (((strlen (encodedRacks)-2) %3) == 0){
      int zoneNameSize =  (strlen(encodedRacks)) %3;
      if (zoneNameSize == 0) zoneNameSize = 3;
      for (int i =zoneNameSize+1; i <= strlen(encodedRacks); i= i+3) {
         dynAppend (dsRacks, substr(encodedRacks, 0, zoneNameSize) + substr (encodedRacks, i-1, 3));		
      }
    } else DebugTN("TDM " +  encodedRacks +" doesn't belong to group 2+3*n, ignoring");  
  }
  
  return dsRacks;
}


//=========================================================
bool fwRack_RackObjectExists(string RackName)
{
  string origRackName =RackName ;
  RackName = strrtrim(RackName,"UPS");
  strreplace(RackName, "/","_");
  RackName =  fwRack_deleteChars (RackName   , " .-/=:");
  
  
  dyn_string dsRackDps = dpNames("RCA/*/"+RackName, "FwRack");
  if (dynlen(dsRackDps) != 1) {
      //try decoding racks
      dyn_string decRacks = fwRack_decodeRacks(origRackName);
      for (int i=1; i<=dynlen(decRacks);i++) {
        dyn_string dsDecRackDps = dpNames("RCA/*/"+decRacks[i], "FwRack");
        if (dynlen(dsDecRackDps)==1) return true;
      }
      return false;    
  }   else return true;     
}



//=========================================================
void fwRackInterface_addRackProperties(string       Source,
                                       string       ZoneName,
                                       string       RackName, 
                                       dyn_string   PropertyNames, 
                                       dyn_string   PropertyAddresses,
                                       dyn_string   PropertyDescriptions,
                                       dyn_bool     ZoneVisible)
{
	int i;
	dyn_string	oldPropertyNames,
			         oldPropertyAddresses,
			         oldPropertyTypes,
			         oldPropertyDescriptions;
 
	dyn_bool	oldRackVisible, oldZoneVisible;				
	RackName = strrtrim(RackName,"UPS");
	strreplace(RackName, "/","_");
	//Had to write the property to the appropriate Rack in the appropriate Zone. 
  //Sometimes there will be a relation zone-source so one of them can be missing.	
	
	string 		dpRackName;
	dyn_string	dsExceptionInfo;		
                                         
	ZoneName =  fwRack_deleteChars (ZoneName   , " .-/=:");
	RackName =  fwRack_deleteChars (RackName   , " .-/=:");
	
 //DebugTN("RCA/*/"+RackName);
 dyn_string dsRackDps = dpNames("RCA/*/"+RackName);
 //DebugTN(dsRackDps);
 if (dynlen(dsRackDps)<=0) return;
 
	//dpRackName = "RCA/" + ZoneName + "/" + RackName;
	dpRackName = dsRackDps[1];
        //DebugTN(dpRackName,dpExists(dpRackName));
	if (dpExists(dpRackName)) {			
	      dpGet(dpRackName + ".Operation.PropertyName",		oldPropertyNames);
	      dpGet(dpRackName + ".Operation.PropertyAddress",	oldPropertyAddresses);
	      dpGet(dpRackName + ".Operation.PropertyDescription",	oldPropertyDescriptions);
	      dpGet(dpRackName + ".Operation.ZoneVisible",		oldZoneVisible);		
	      if (dynlen(PropertyAddresses) != dynlen(ZoneVisible)) {
	        for(i = dynlen(ZoneVisible) + 1; i < dynlen(PropertyAddresses) + 1;i++) {
	          dynAppend(ZoneVisible, FALSE);		
	       }
	      }
	      if (dynlen(PropertyAddresses) != dynlen(PropertyNames)) {
	        for(i = dynlen(PropertyNames) + 1; i < dynlen(PropertyAddresses) + 1;i++){
	          dynAppend(PropertyNames, "UnknownName");	
	        }
	      }
	  		
        for (i = 1; i <= dynlen(PropertyNames); i++){
            if (!dynContains(oldPropertyNames, PropertyNames[i])) {
                        dynAppend(oldZoneVisible, 	   ZoneVisible[i]);		
	                       dynAppend(oldPropertyNames, 	   PropertyNames[i]);		
	                       dynAppend(oldPropertyDescriptions,   PropertyDescriptions[i]);		
	                       dynAppend(oldPropertyAddresses, 	   PropertyAddresses[i]);          
            }
        }
        dpSet(dpRackName + ".Operation.PropertyName",	      oldPropertyNames);
	       dpSet(dpRackName + ".Operation.PropertyAddress",    oldPropertyAddresses);	  
	       dpSet(dpRackName + ".Operation.PropertyDescription",oldPropertyDescriptions);	  
	       dpSet(dpRackName + ".Operation.ZoneVisible",	      oldZoneVisible);		
	} else DebugTN( RackName + " does not exist.");	
 	
}



//=========================================================
void fwRackInterface_deleteRackCommands(string Source, string ZoneName,  string RackName, 
                                        string CommandAddress, dyn_string CommandNames,
                                        string ResponseAddress)
{
	
	//DebugTN("Hello, this is fwRackInterface_deleteRackCommands.");
	dyn_string	dsFinalCommands,
			dsFinalAddress,
			dsFinalDescription;
	string 		dpRackName;
	
	RackName = strrtrim(RackName,"UPS");	
	strreplace(RackName, "/","_");
	
	
	ZoneName =  fwRack_deleteChars (ZoneName   , " .-/=:");
	RackName = 	fwRack_deleteChars (RackName   , " .-/=:");
	
	//DebugTN("RCA/*/"+RackName);
        dyn_string dsRackDps = dpNames("RCA/*/"+RackName);
  //DebugTN(dsRackDps);
        if (dynlen(dsRackDps)<=0) return;                                         
        dpRackName = dsRackDps[1];	
					 
		//The Zone already exits. What about the rack
		if (dpExists(dpRackName)) {
		   	dpGet(dpRackName + ".Operation.CommandName",dsFinalCommands);
		   	dpGet(dpRackName + ".Operation.CommandAddress",dsFinalAddress);
		   	dpGet(dpRackName + ".Operation.CommandDescription",dsFinalDescription);
			
		   	while(dynlen(CommandNames))	{
			//deletes the commands of the rack	
		   		  while (dynContains(dsFinalCommands,CommandNames[dynlen(CommandNames)])) {
			   		  int iDeletedElement;
			   	  	iDeletedElement = dynContains(dsFinalCommands,CommandNames[dynlen(CommandNames)]);
				   	  dynRemove(dsFinalCommands,iDeletedElement);
				     	dynRemove(dsFinalAddress,iDeletedElement);
				   	  dynRemove(dsFinalDescription,iDeletedElement);
					
			   	  }
			     	dynRemove(CommandNames,dynlen(CommandNames));
			   }
			   dpSet(dpRackName + ".Operation.CommandName",dsFinalCommands);
			   dpSet(dpRackName + ".Operation.CommandAddress",dsFinalAddress);
			   dpSet(dpRackName + ".Operation.CommandDescription",dsFinalDescription);
		} else {
			//rack doesnt exists
			;
		}
						
				
}

//=======================================================
void fwRackInterface_addRackCommands(string Source, string ZoneName, string RackName, 
                                     string CommandAddress, dyn_string CommandNames,
                                     dyn_string CommandDescriptions, string ResponseAddress)
{
  strreplace(RackName, "/","_");	
  string 	    dpRackName;
  dyn_string	    dsExceptionInfo;
  dyn_string        dsCommandAddress;	
  
  dyn_string	oldCommandNames, oldCommandAddresses, oldCommandDescriptions;
  
  RackName = strrtrim(RackName,"UPS");
  ZoneName =  fwRack_deleteChars (ZoneName   , " .-/=:");
  RackName =  fwRack_deleteChars (RackName   , " .-/=:");
  
  dyn_string dsRackDps = dpNames("RCA/*/"+RackName);
  if (dynlen(dsRackDps)<=0)
    return;
  dpRackName = dsRackDps[1];
  	
  dpGet(dpRackName + ".Operation.CommandName",        oldCommandNames);
  dpGet(dpRackName + ".Operation.CommandAddress",     oldCommandAddresses);
  dpGet(dpRackName + ".Operation.CommandDescription", oldCommandDescriptions);
  
  //Check if the Zone exists or not
  if (dpExists(dpRackName)) {			
  //Both the Rack and Zone exists. It assignates the properties to the existing rack.
    for(int i =  1; i < dynlen(CommandNames) + 1;i++) dsCommandAddress[i] = CommandAddress;	
    
    for(int i=1; i <=dynlen(CommandNames); i++){
      if (!dynContains(oldCommandNames,CommandNames[i])) {
        dynAppend(oldCommandNames,        CommandNames[i]);
        dynAppend(oldCommandAddresses,    CommandAddress);
        dynAppend(oldCommandDescriptions, CommandDescriptions[i]);
      }
    }
    dpSet(dpRackName + ".Operation.CommandName",		oldCommandNames);
    dpSet(dpRackName + ".Operation.CommandAddress",		oldCommandAddresses);		
    dpSet(dpRackName + ".Operation.CommandDescription",	        oldCommandDescriptions);			
  } else { DebugTN(RackName + " does not exist. "); }				
}

//=========================================================
void fwRack_insertCrateIntoRack(string 	sName, 
                                int 	iPosition,
                                string 	sLabel, 
                                string 	sDpName,
                                string	sSide)
{
	//DebugTN("$sDpName:" + $sDpName,
	//			"$sRackName:" + $sRackName,
	//			"$iPosition:" +$iPosition );

	dyn_string	dsCrates, dsType, dsLabel, dsSide;
	dyn_float		dfPosition, dfHeight;	
	float fPosition = (float)iPosition;
	
	dpGet(sDpName + ".Crates.Name", dsCrates);
	dpGet(sDpName + ".Crates.Type", dsType);
	dpGet(sDpName + ".Crates.Position", dfPosition);
	dpGet(sDpName + ".Crates.Height", dfHeight);
	dpGet(sDpName + ".Crates.Label", dsLabel);
	dpGet(sDpName + ".Crates.Label", dsSide);

	dynAppend(dsCrates,sName);
	dynAppend(dsType,"power distribution");
	dynAppend(dfPosition,fPosition);
	dynAppend(dfHeight,1.0f);
	dynAppend(dsLabel,sLabel);
	dynAppend(dsSide,sSide);
	
	dpSet(sDpName + ".Crates.Name", dsCrates);
	dpSet(sDpName + ".Crates.Type", dsType);
	dpSet(sDpName + ".Crates.Position", dfPosition);
	dpSet(sDpName + ".Crates.Height", dfHeight);
	dpSet(sDpName + ".Crates.Label", dsLabel);
	dpSet(sDpName + ".Crates.Label", dsSide);
}

//=========================================================
void fwRack_findAgentNumber(int	&agentN)
{
	for(int i=1;i < 256; i++){
		if (!dpExists("_3_SNMPAgent_"+i)) {
			//DebugTN("Encontrado agente libre " + i);										
			agentN = i;
			break;
		}
	}	
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//=========================================================
void fwRack_createRackFsmRoot()
{
  dyn_string dsExInfo;
  fwFsmTree_addNode( "FSM", "RackControl", "RackRootType",1);			  
}

//=========================================================
void fwRack_createRackFsmZone(string sZoneName)
{
  dyn_string dsExInfo;
  fwFsmTree_addNode( "RackControl", sZoneName, "RackZone",1);	 
}


//=========================================================
void fwRack_createRackFsmRack(string sRackDp, string sZoneName)
{
  dyn_string dsExInfo;
  string     sRackName;
  fwDevice_getName(sRackDp,sRackName,dsExInfo);
  fwFsmTree_addNode( sZoneName, sRackName,"RACK",0);	 
}	

//=========================================================
void fwRack_addRackFSMdevices(string sRackDp)
{
  dyn_string dsExInfo;
  string     sRackName;
  string     sPremiumType;
  fwDevice_getName(sRackDp,sRackName,dsExInfo);
  //if lucky 
  if (dpExists("PremiumObject_"+sRackName)) {
    sPremiumType = dpTypeName("PremiumObject_"+sRackName);
    if (sPremiumType=="FwRackTwidoBoxSimple")
    fwFsmTree_addNode( sRackName, "PremiumObject_"+sRackName,"FwRackTwidoBoxSimpleCMS",0);
  }
  //No luck --> browse TDM to see if the rack belongs to any of them
  dyn_string dsTDM = dpNames("PremiumObject_*","FwRackTDM");
  for(int i =1; i<= dynlen(dsTDM);i++){    
    DebugTN("Decoding : " + substr(dsTDM[i],16,strlen(dsTDM[i])-16));    
    dyn_string dsRacks = fwRack_decodeRacks(substr(dsTDM[i],16,strlen(dsTDM[i])-16));
    if (dynContains(dsRacks,sRackName)) {
       fwFsmTree_addNode( sRackName, dsTDM[i],"fwRackTDMALICE",0);	        
    }
  }
}
