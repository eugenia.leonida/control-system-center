#uses "fwGeneral/fwGeneral.ctl"
#uses "fwDevice/fwDevice.ctl"

//============================
/** deprecated CTC3 Exercise by FVR on 2018-07-31 */  
void fwAuxil_Error(string message)
{
dyn_string params;

dynAppend(params,"$1:"+message);
ChildPanelOnCentralModal("vision/MessageWarning","Error",params);
}
//==========================================================================
/** deprecated CTC3 Exercise by FVR on 2018-07-31 */  
int fwAuxil_dpBlobSetValue_int(string dpe,dyn_int iarr, int nelem = 0, int pos = 0)
{
  int error;
  blob myblob;
  int i;
  
  error = fwAuxil_blobSetValue_int(myblob,iarr,nelem,pos); 
  i = dpSetWait(dpe,myblob);
  return(error+i);
}


//==========================================================
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */  
string BlobToBool(blob &Input, int Position, int Length, int BitNumber)
{
  FWDEPRECATED();
  
int 		Intermed;
bit32 	Pattern;
int 		Output;
string 	Answer;

blobGetValue(Input, Position*2, Intermed, Length*2, TRUE);
Pattern = Intermed;
Output = getBit(Pattern, BitNumber);
Answer = Output;
return Answer;
}


//==========================================================
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */  
unsigned BlobToBCD(blob &Input, int Position, int Byte)
{
  FWDEPRECATED();
  
unsigned 	Intermed;
unsigned 	a, b, Answer;
bit32 		Pattern, A, B;
int i;

blobGetValue(Input, Position*2+Byte, Intermed, 1, TRUE);
Pattern = Intermed;

for (i = 0; i < 4; i++)   setBit(A, i, Pattern[i]);
for (i = 0; i < 4; i++)   setBit(B, i, Pattern[i+4]);

a = A;
b = B;
Answer = B*10 + A;
return Answer;
}


//================================================================================
// fwRackTool_DebugLevel
//================================================================================
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */  
void fwRackTool_DebugLevel(int value)
{
  FWDEPRECATED();
  
  _RT_DebugLevel = value;
}

//=============================================================
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */  
void _fwRackConnections_rawsChanged (string ident, dyn_dyn_anytype val)
{
  FWDEPRECATED();
  
  ;//DebugTN(ident,val);
  
}
//=============================================================
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */  
string _fwRackConnections_getModbusDriverStatus (int ModbusNumber)
{
  FWDEPRECATED();
  
string         host;
int            port;
int            i, opcIdx;
string         command, command3;
dyn_dyn_string queryResult;
bool           querySuccess, querySuccess3;
dyn_dyn_mixed queryResult2;  
  
  i = paGetProjHostPort(PROJ,host, port);
  command = "##MGRLIST:LIST";
  querySuccess = pmon_query(command, host, port, queryResult, 1, 1);
  
  //DebugTN(queryResult);
  for (i = 1; i <= dynlen (queryResult); i++) {  
    //DebugTN(queryResult[i]);   
    //if ((queryResult[i][1] == "PVSS00mod") || (queryResult[i][1] == "WCCOAmod")) {           
    if (queryResult[i][1] == "WCCOAmod") {
      if (queryResult[i][6] == "-num " +  ModbusNumber){
        DebugTN[queryResult[i]];
        opcIdx = i-1;
        command3 = "##MGRLIST:STATI";
        querySuccess3 = pmon_query(command3, host, port, queryResult2, 1, 1);        
        return queryResult2[i][1];
      }
    }
  }  
}



//=========================================================
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */  
void  getZoneFromSource(string 	Source,string 	&ZoneName)
{
  FWDEPRECATED();
  
	ZoneName = "Unlocated";			       	
}

//=========================================================
// IS THIS FUNCTION WITH A WRONG NAME EVER USED ??? (GM)
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */  
void fwRacl_deleteCrates(string  sRackDP, dyn_string  dsLabel)
{
  FWDEPRECATED();
  
  dyn_string  dsActualLabels;
  dyn_string	dsOldCrateNames, dsOldCrateTypes, dsOldLabel, dsOldDP, dsOldPanel;					
  dyn_float	dfOldPosition, dfOldHeight;
  dyn_int	diOldSide;

  if (dpExists(sRackDP)) {
    dpGet(sRackDP + ".Crates.Label", dsActualLabels);
    dpGet(sRackDP + ".Crates.Name",	dsOldCrateNames);
    dpGet(sRackDP + ".Crates.Type",	dsOldCrateTypes);
    dpGet(sRackDP + ".Crates.Position",	dfOldPosition);
    dpGet(sRackDP + ".Crates.Height",	dfOldHeight);
    dpGet(sRackDP + ".Crates.Side",	diOldSide);   
    dpGet(sRackDP + ".Crates.DP",	dsOldDP);
    dpGet(sRackDP + ".Crates.Panel",	dsOldPanel);
    for(int i =1; i <= dynlen(dsLabel); i++){
      int iPosition = dynContains(dsActualLabels,dsLabel[i]);
      if (iPosition > 0 ){
        dynRemove(dsActualLabels,iPosition);
        dynRemove(dsOldCrateNames,iPosition);
        dynRemove(dsOldCrateTypes,iPosition);
        dynRemove(dfOldPosition,iPosition);
        dynRemove(dfOldHeight,iPosition);
        dynRemove(diOldSide,iPosition);
        dynRemove(dsOldDP,iPosition);
        dynRemove(dsOldPanel,iPosition);                
      }
    }
    dpSet(sRackDP + ".Crates.Name",	dsOldCrateNames);
    dpSet(sRackDP + ".Crates.Type",	dsOldCrateTypes);
    dpSet(sRackDP + ".Crates.Position",	dfOldPosition);
    dpSet(sRackDP + ".Crates.Height",	dfOldHeight);
    dpSet(sRackDP + ".Crates.Side",	diOldSide);
    dpSet(sRackDP + ".Crates.Label",	dsActualLabels);
    dpSet(sRackDP + ".Crates.DP",	dsOldDP);
    dpSet(sRackDP + ".Crates.Panel",	dsOldPanel);  
  }
}

//=========================================================
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */  
void fwRackInterface_addRackObjectCommands(	string 		Source, 
                                            string 		sCrateName, 
                                            dyn_string	dsCommandAddress, 
                                            dyn_string 	CommandNames,
                                            dyn_string 	CommandDescriptions, 
                                            dyn_string	ResponseAddress)
{
  FWDEPRECATED();
  
	dyn_string	dsPropNames;
	dpGet(	sName + ".Operation.PropertyName", dsPropNames);
	dynAppend(	dsPropNames, dsPropertyName);
	dpSet(	sName + ".Operation.PropertyName", dsPropNames);
	
}


//=========================================================
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */
void fwRack_reconnectPremiumObjects(string IP)
{
  FWDEPRECATED();
  
  string IP_ = IP;
  strreplace(IP_,".","_");
  string ModPLCDp = "Mod_PLC_" + IP_;
  dyn_string dsPremiumObjectsDPs;
  
  dpGet (ModPLCDp + ".Objects.ObjectDPs", dsPremiumObjectsDPs);
  for (int i =1 ; i<= dynlen (dsPremiumObjectsDPs); i++) {
    if (dpExists(dsPremiumObjectsDPs[i])) fwRackPremium_connectPremiumObject(dsPremiumObjectsDPs[i]);
  }  
}


//=========================================================
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */
void fwRack_RackObjectAddProperties(	string sName, dyn_string	dsPropertyName)
{
  FWDEPRECATED();
	dyn_string	dsPropNames;
	dpGet(	sName + ".Operation.PropertyName", dsPropNames);
	dynAppend(	dsPropNames, dsPropertyName);
	dpSet(	sName + ".Operation.PropertyName", dsPropNames);
	
}

//=========================================================
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */
  
void fwRack_getZoneDp (string sZoneName , string &sDpZone)
{
  FWDEPRECATED();
  dyn_string dsZones = dpNames("RCA/" + sZoneName,"FwZone");
  if (dynlen(dsZones)>0) sDpZone = dsZones[1];
}

//=========================================================
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */
void fwRack_getTurbineDp (string sRackDp, string &sTurbineDp)
{ 
  FWDEPRECATED();
  
 dpGet (sRackDp + ".Crates.ElmbDp", sTurbineDp);  
  
}
//=========================================================
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */
void fwRack_deletePremiumObject (string dpRackName)
{
  FWDEPRECATED();
    
  dyn_string  dsException;
  string      sRackName;
  
  fwDevice_getName(dpRackName, sRackName, dsException);
  if (dpExists("PremiumObject_" + sRackName)) dpDelete("PremiumObject_" + sRackName);  
}


//=========================================================
/** deprecated CTC2 Exercise by FVR on 2018-07-25 */

void fwRack_checkExistingObjects(dyn_string &ObjectNames,
                                 dyn_string &ObjectStatusAddresses,
                                 dyn_string &ObjectTypes,
                                 dyn_string &ObjectTypesObjectDPs)
{
    FWDEPRECATED();
    
    for (int i = 1; i <= dynlen(ObjectNames); i++ ) {
      if (ObjectTypes[i] != RCA_OT_TDM){
          string RackName = ObjectNames[i];
          RackName = strrtrim(RackName,"UPS");
          strreplace(RackName, "/","_");
          RackName =  fwRack_deleteChars (RackName   , " .-/=:");
	         dyn_string dsRackDps = dpNames("RCA/*/"+RackName, "FwRack");  
          DebugTN("dps: " + dsRackDps);
          if (dynlen(dsRackDps)!=1){
              dynRemove(ObjectNames,i);
              dynRemove(ObjectStatusAddresses,i);
              dynRemove(ObjectTypes,i);
              dynRemove(ObjectTypesObjectDPs,i);
          }            
      }       
    } 
}

