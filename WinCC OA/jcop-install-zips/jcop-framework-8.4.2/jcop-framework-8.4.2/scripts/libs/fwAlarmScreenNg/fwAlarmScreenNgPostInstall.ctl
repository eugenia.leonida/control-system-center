#uses "fwInstallation/fwInstallation"
#uses "AlarmScreenNg/AlarmScreenNg"  // common definitions
#uses "AlarmScreenNg/classes/AsNgHelp"  // set default help file types
#uses "AlarmScreenNg/classes/AsNgPopupMenu"  // set default popup menu
#uses "AlarmScreenNg/AlarmScreenNgActions"  // the names of actions for popup menu

private const string AS_NG_DPE_CONFIG_TEST = ":_original.._stime";  ///< The name of DPE config and attribute to check if DPE value was set

/**
 * Check if DP(s) with basic configuration exist, and have 'initialized' DPE.
 * If not - import corresponding file using ASCII import.
 * @param dsFileNames list of file names which optentialy need to be imported
 * @param dsDpsNames List of DPE names, corresponding to every file in dsFileNames:
 *                  if corresponding DPE does not exist, or the content of DPE is not
 *                  initialized, then corresponding file shall be imported using ASCII manager
 * @note Number of items in both lists must be the same
 */
void _fwAlarmScreenNgPostInstall_importBasicConfigs(const dyn_string &dsFileNames, const dyn_string &dsDpsNames, const string &component)
{
  dyn_string dsSkippedFiles;
  for(int n = 0 ; n < dsDpsNames.count() ; n++)
  {
    string sDpeName = dsDpsNames.at(n);
    if(_fwAlarmScreenNgPostInstall_isDpeNotInitialized(sDpeName, component))
    {
      _fwAlarmScreenNgPostInstall_asNgMessage("Importing default basic configuration for DPE " + sDpeName + " as it seems unset, from: " + dsFileNames.at(n), component);
      _fwAlarmScreenNgPostInstall_asciiImportDpl(dsFileNames.at(n), component);
    }
    else
    {
      dsSkippedFiles.append(dsFileNames.at(n));
    }
  }
}

/// Check if given DPE is either does not exist, or its value was never set
bool _fwAlarmScreenNgPostInstall_isDpeNotInitialized(const string &sDpeName, const string &component)
{
  if(!dpExists(sDpeName))
  {
    return true;  // DPE doesn;t exist
  }

  time tDpeTs, tEmpty;
  string sDpeAttr = sDpeName + AS_NG_DPE_CONFIG_TEST;
  dpGet(sDpeAttr, tDpeTs);
  dyn_errClass deErrors = getLastError();
  if(!deErrors.isEmpty())
  {
    _fwAlarmScreenNgPostInstall_asNgMessage("dpGet(" + sDpeAttr + ") failed", component, "ERROR");
    throwError(deErrors);
    return false;
  }

  return (tDpeTs == tEmpty);
}

/// Perform impprt of given DPL file using ASCII manager
void _fwAlarmScreenNgPostInstall_asciiImportDpl(const string &sFileName, const string &component)
{
  string sComponentDp = fwInstallation_getComponentDp(component);
  if(!dpExists(sComponentDp))
  {
    _fwAlarmScreenNgPostInstall_asNgMessage("fwInstallation component DP " + sComponentDp + " does not exist. Import of file is not possible: " + sFileName,
										  component, "ERROR");
    return;
  }

  string sSourceDirDpe = sComponentDp + ".installationDirectory";
  string sSourceDir;
  dpGet(sSourceDirDpe, sSourceDir);
  dyn_errClass deErrors = getLastError();
  if(!deErrors.isEmpty())
  {
    _fwAlarmScreenNgPostInstall_asNgMessage("dpGet(" + sSourceDirDpe + ") failed", component, "ERROR");
    throwError(deErrors);
    return;
  }

  if(0 != fwInstallation_importComponentAsciiFiles(component, sSourceDir, "", makeDynString(sFileName), false))
  {
    _fwAlarmScreenNgPostInstall_asNgMessage("Failed to import DPL file: " + sFileName, component, "ERROR");
  }
}

/// Set default types for alarm help files
void _fwAlarmScreenNgPostInstall_setDefaultHelpFileTypes(const string &component)
{
  AsNgHelp help;
  dyn_string dsExceptions;
  mapping mConfig = help.read(dsExceptions);
  if(!dsExceptions.isEmpty())
  {
    _fwAlarmScreenNgPostInstall_asNgMessage("Failed to check file types for alarm help, alarm help may not work as expected: " + dsExceptions,
										  component, "ERROR");
    return;
  }
  if(!mConfig.isEmpty())
  {
    return;
  }

  _fwAlarmScreenNgPostInstall_asNgMessage("Setting default set of file types for alarm help", component);
  if(!help.setDefaultIfEmpty(dsExceptions))
  {
    _fwAlarmScreenNgPostInstall_asNgMessage("Failed to set file types for alarm help, alarm help may not work as expected: " + dsExceptions,
										  component, "ERROR");
  }
}

/// Set default popup menu configuration for JCOP
void _fwAlarmScreenNgPostInstall_setDefaultMenuJCOP(const string &component)
{
  dyn_string dsExceptions;
  string sDpName = AlarmScreenNg_getAdminDP(dsExceptions);
  if(sDpName == "")
  {
    _fwAlarmScreenNgPostInstall_asNgMessage("Failed to get the name of admin config DP, can't check popup menu configuration: " + dsExceptions,
										  component, "ERROR");
    return;
  }
  string sDpeName = sDpName + ".PopupMenu";
  if(!_fwAlarmScreenNgPostInstall_isDpeNotInitialized(sDpeName, component))
  {
    return;
  }

  _fwAlarmScreenNgPostInstall_asNgMessage("Setting default popup menu configuration", component);

  AsNgPopupMenu menu;

  shared_ptr<AsNgPopupMenuItem> item1 = menu.addItem("FSM Panel");
  item1.setAction(AS_ACTION_VIEW_FSM_PANEL);
  item1.setCtrlLib("fwAlarmScreenNg/AlarmScreenMenuJCOP.ctl");
  item1.setCtrlFunc("AlarmScreenMenuJCOP_showFsmPanel");
  item1.setAppearanceLib("fwAlarmScreenNg/AlarmScreenMenuJCOP.ctl");
  item1.setAppearanceFunc("AlarmScreenMenuJCOP_itemAppearanceFsmPanel");

  shared_ptr<AsNgPopupMenuItem> item2 = menu.addItem("Details");
  item2.setAction(AS_ACTION_VIEW_ALARM_DETAILS);
  item2.setCtrlLib("fwAlarmScreenNg/AlarmScreenMenuJCOP.ctl");
  item2.setCtrlFunc("AlarmScreenMenuJCOP_showDetails");
  item2.setAppearanceLib("fwAlarmScreenNg/AlarmScreenMenuJCOP.ctl");
  item2.setAppearanceFunc("AlarmScreenMenuJCOP_itemAppearanceDetails");

  shared_ptr<AsNgPopupMenuItem> item3 = menu.addItem("Trend");
  item3.setAction(AS_ACTION_VIEW_ALARM_TREND);
  item3.setCtrlLib("fwAlarmScreenNg/AlarmScreenMenuJCOP.ctl");
  item3.setCtrlFunc("AlarmScreenMenuJCOP_showTrend");
  item3.setAppearanceLib("fwAlarmScreenNg/AlarmScreenMenuJCOP.ctl");
  item3.setAppearanceFunc("AlarmScreenMenuJCOP_itemAppearanceTrend");

  shared_ptr<AsNgPopupMenuItem> item4 = menu.addItem("Alarm Help");
  item4.setAction(AS_ACTION_VIEW_ALARM_HELP);
  item4.setCtrlLib("fwAlarmScreenNg/AlarmScreenMenuJCOP.ctl");
  item4.setCtrlFunc("AlarmScreenMenuJCOP_showHelp");
  item4.setAppearanceLib("fwAlarmScreenNg/AlarmScreenMenuJCOP.ctl");
  item4.setAppearanceFunc("AlarmScreenMenuJCOP_itemAppearanceHelp");

  shared_ptr<AsNgPopupMenuItem> item5 = menu.addItem("Comment Panel");
  item5.setAction(AS_ACTION_VIEW_ALARM_COMMENT);
  item5.setCtrlLib("fwAlarmScreenNg/AlarmScreenMenuJCOP.ctl");
  item5.setCtrlFunc("AlarmScreenMenuJCOP_showCommentPanel");
  item5.setAppearanceLib("fwAlarmScreenNg/AlarmScreenMenuJCOP.ctl");
  item5.setAppearanceFunc("AlarmScreenMenuJCOP_itemAppearanceCommentPanel");

  shared_ptr<AsNgPopupMenuItem> item6 = menu.addItem("Mask Alarm");
  item6.setAction(AS_ACTION_TOGGLE_MASKED);
  item6.setCtrlLib("fwAlarmScreenNg/AlarmScreenMenuJCOP.ctl");
  item6.setCtrlFunc("AlarmScreenMenuJCOP_toggleAlarmMasked");
  item6.setAppearanceLib("fwAlarmScreenNg/AlarmScreenMenuJCOP.ctl");
  item6.setAppearanceFunc("AlarmScreenMenuJCOP_itemAppearanceMaskAlarm");

  if(!menu.save(dsExceptions))
  {
    _fwAlarmScreenNgPostInstall_asNgMessage("Failed to save default menu configuration, popup menu can not work: " + dsExceptions, component, "ERROR");
  }
  else
  {
    _fwAlarmScreenNgPostInstall_asNgMessage("Successfully set up default popup menu configuration", component);
  }
}

/// Simple wrapper for fwInstallation_throw() function
void _fwAlarmScreenNgPostInstall_asNgMessage(const string sMessage, const string &component, const string sSeverity = "INFO", int iCode = 1)
{
  fwInstallation_throw(component + ": " + sMessage, sSeverity, iCode);
}
