#uses "AlarmScreenNg/AlarmScreenNg.ctl"  // Common definitions for AS NG
#uses "AlarmScreenNg/AlarmScreenNgActionProcessing.ctl"  // Common processing of AS EWO events
#uses "AlarmScreenNg/AlarmScreenNgDisplay"  // functions to open 'standard' alarm panels
#uses "AlarmScreenNg/classes/AsNgAcknowledge.ctl"  // alarm acknowledgement
#uses "AlarmScreenNg/classes/AsNgPopupMenu.ctl"  // display/process right click popup menu
#uses "fwGeneral/fwGeneral.ctl"

/** @file AlarmScreenJCOP.ctl
  JCOP-cpecific support for NextGen Alarm Screen
*/


/** @name JCOP-specific names of alarm properties. This is extension of alarm properties,
 * defined in basic configuration, see AlarmScreenNg.ctl
 */
//@{
const string ALARM_PROP_JCOP_ALIAS = "alias";                     ///< Device alias
const string ALARM_PROP_JCOP_DESCRIPTION = "deviceDescription";   ///< Device description
const string ALARM_PROP_JCOP_ONLINE_VALUE = "onlineValue";        ///< (Formatted) DPE online value
const string ALARM_PROP_JCOP_CAME_TIME = "cameTime";              ///< Came time of alarm
const string ALARM_PROP_JCOP_SCOPE = "scope";                     ///< Alarm scope, magic logic on priority
const string ALARM_PROP_JCOP_DEV_TYPE = "deviceType";             ///< Device type
//@}

//@}

/** @name Flag for activating debug messages in this library, one flag for all functions
*/
private const string ASNG_FW_DBG = "ASNG_DBG";

/** @name Public functions */
//@(

/**
 * Get list of comletions for filter on given alarm property
 *
 * @param sPropName The name of alarm property for which filter is configured
 * @param dsSelectedSystems The list of selected system names
 * @return mapping with 3 fields (keys are strings):
 *    - "type" The type of completions used for this filter, see enum FilterCompletionsType
 *    - "list" The list of completion strings for this filter
 *    - "map"  The list of completion strings for this filter in case complitions are related
 *            to connected systems. Key is string (system name), value is dyn_string = list of
 *            completions if this system is connected
 */
mapping AlarmScreenJCOP_getFilterCompletions(string sPropName, const dyn_string &dsSelectedSystems)
{
  DebugFTN(ASNG_FW_DBG, __FUNCTION__ + "(" + sPropName + ")", dsSelectedSystems);
  // Default is no completions
  mapping mResult;
  mResult[AS_FILTER_COMPLETION_KEY_TYPE] = (int)FilterCompletionsType::None;
  // Find completions depending on alarm property name
  switch(sPropName)
  {
    case ALARM_PROP_JCOP_DEV_TYPE:
      _AlarmScreenJCOP_getFilterCompletions_deviceType(dsSelectedSystems, mResult);
      break;
  }
  DebugFTN(ASNG_FW_DBG, __FUNCTION__ + "(" + sPropName + "): result", mResult);
  return mResult;
}

/**
 * Get list of comletions for slave filter on given alarm property
 *
 * @param sPropName The name of alarm property for which filter is configured
 * @param mFilters Current settings for all 'master' filters of this slave:
 *        - key: alarm property name of filter
 *        - value: current filter value for this alarm property
 * @return mapping with 1 field (key is string):
 *    - "list" The list of completion strings for this filter
 */
mapping AlarmScreenJCOP_getSlaveFilterCompletions(string sPropName, const mapping &mFilters)
{
  DebugFTN(ASNG_FW_DBG, __FUNCTION__ + "(" + sPropName + ")", mFilters);
  // Combine the result
  mapping mResult;
  mResult[AS_FILTER_COMPLETION_KEY_LIST] = makeDynString();
  DebugFTN(ASNG_FW_DBG, __FUNCTION__ + "(" + sPropName + "): result", mResult);
  return mResult;
}

/**
 * Process mouse event in alarm table cell.
 * @param mArgs The mapping with arguments, see description of AlarmScreenNgEwo_mouseEvent()
 */
void AlarmScreenJCOP_mouseTableEvent(const mapping &mArgs)
{
  const mapping mSource = mArgs["mSource"];
  DebugFTN(ASNG_FW_DBG, __FUNCTION__ + "():", mSource);
  if((mSource["type"] == 0)  // Mouse pressed event
    && (mSource["button"] == 2))  // Right mouse button
  {
    AsNgPopupMenu menu;
    dyn_string dsExceptions;
    menu.display(mArgs, dsExceptions);
    if(dynlen(dsExceptions) > 0)
    {
      fwExceptionHandling_display(dsExceptions);
    }
  }
  else if((mSource["type"] == 1)  // Mouse click event
    && (mSource["button"] == 1)  // Left mouse button
    && (mSource["columnSource"] == 0))  // this alarm
  {
    _AlarmScreenJCOP_mouseLeftClickEvent(mArgs);
  }
}

/**
 * Process event in table: left mouse button was clicked on row with alarm data.
 * There is special processing for clicking at some columns: acknowledge alarm, open details etc.
 * @param mArgs The mapping with arguments, see description of AlarmScreenNgEwo_mouseEvent()
 */
private void _AlarmScreenJCOP_mouseLeftClickEvent(const mapping &mArgs)
{
  mapping mSource = mArgs["mSource"];
  mapping mAlarmData = mArgs["mAlarm"];
  const int iMode = mArgs["sourceMode"];
  DebugFTN(ASNG_FW_DBG, __FUNCTION__ + "():", mSource, mAlarmData, iMode);

  dyn_string exceptionInfo;
  switch(mSource["columnId"])
  {
  case ALARM_PROP_ACK_STATE_SUMMARY:  // Acknowledge when clicked on 'Ack.' column
    if((iMode == AS_NG_EWO_MODE_ONLINE) && (mAlarmData[ALARM_PROP_ACK_STATE_SUMMARY] == ALARM_ACK_STATE_SUMMARY_ACKABLE_OLDEST))
    {
      dyn_mapping dmAlarms = makeDynMapping(mAlarmData);
      _AlarmScreenJCOP_ackMultiple(dmAlarms, DPATTR_ACKTYPE_SINGLE);
    }
    break;
  case ALARM_PROP_COMMENTS_COUNT:
    if(iMode == AS_NG_EWO_MODE_ONLINE)
    {
      AlarmScreenNgDisplay_showCommentPanel(mAlarmData, exceptionInfo);
    }
    break;
  case ALARM_PROP_DETAILS:
    if(iMode == AS_NG_EWO_MODE_ONLINE)
    {
      AlarmScreenNgDisplay_showDetails(mAlarmData, exceptionInfo);
    }
    break;
  case ALARM_PROP_PANEL:
    if(strlen(mAlarmData[ALARM_PROP_PANEL]) > 0)
    {
      dyn_string dsPanelParms = mAlarmData[ALARM_PROP_PANEL_PARAM];
      ChildPanelOnCentral(mAlarmData[ALARM_PROP_PANEL], "Alarm Panel", dsPanelParms);
    }
    break;
  }
  if(dynlen(exceptionInfo) > 0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}

/**
 * Process ctrlActionRequested() event of AS EWO in UNICOS-specific way
 * @param mArgs event arguments, see description of AlarmScreenNgEwo_ctrlActionRequested()
 */
void AlarmScreenJCOP_ctrlActionRequested(const mapping &mArgs)
{
  shape ewo = mArgs["ewoShape"];
  if(ewo == 0)
  {
    throwError(makeError("", PRIO_SEVERE, ERR_IMPL, 76,  // 00076,Invalid argument in function
                         __FUNCTION__ + "(): EWO shape not found in arguments"));
    return;
  }
  const int iMode = mArgs["sourceMode"];
  dyn_mapping dmAlarms;
  switch(mArgs["sAction"])
  {
  case AS_EWO_ACTION_ACK_SELECTED:
    if(iMode == AS_NG_EWO_MODE_ONLINE)
    {
      dmAlarms = ewo.getForAcknowledgementSelected();
      _AlarmScreenJCOP_ackMultiple(dmAlarms, DPATTR_ACKTYPE_MULTIPLE);
    }
    break;
  case AS_EWO_ACTION_ACK_GONE:
    if(iMode == AS_NG_EWO_MODE_ONLINE)
    {
      dmAlarms = ewo.getForAcknowledgementAllGone();
      _AlarmScreenJCOP_ackMultiple(dmAlarms, DPATTR_ACKTYPE_MULTIPLE);
    }
    break;
  case AS_EWO_ACTION_ACK_ALL:
    if(iMode == AS_NG_EWO_MODE_ONLINE)
    {
      dmAlarms = ewo.getForAcknowledgementAll();
      _AlarmScreenJCOP_ackMultiple(dmAlarms, DPATTR_ACKTYPE_MULTIPLE);
    }
    break;
  default:
    AlarmScreenNgActionProcessing_process(mArgs);  // default processing
  }
}

/**
 * Acknowledge multiple alarms from EWO (selected/all/etc.)
 * @param dmAlarms The list of alarm parameters from EWO which shall
 *                  be acknowledged. Every mapping contains:
 *                  - key = ID of alarm property (see ALARM_PROP_XXX constants)
 *                  - value = value of alarm property with this ID
 * @param iAckType Acknowledgement type, one of DPATTR_ACKTYPE_SINGLE/DPATTR_ACKTYPE_MULTIPLE
 */
private void _AlarmScreenJCOP_ackMultiple(dyn_mapping &dmAlarms, int iAckType)
{
  AlarmScreenNg_checkForPartnersToAck(dmAlarms);
  AsNgAcknowledge acker;  // The instance who will process acknowledgement
  acker.analyze(dmAlarms, iAckType);

  string sMessage = acker.getDescription();
  if(acker.canAcknowledge()) {
    if(sMessage != "") {
      dyn_string dsReturn;
      dyn_float dfReturn;
      ChildPanelOnCentralModalReturn(
          "vision/MessageWarning2",
          getCatStr("sc", "Attention"),
          makeDynString("$1:" + sMessage,
                        "$2:" + getCatStr("general", "yes"),
                        "$3:" + getCatStr("sc", "no")),
          dfReturn, dsReturn);
      bool bContinueAck = false;
      if(dynlen(dfReturn) > 0) {
        if(dfReturn[1] == 1) {
          bContinueAck = true;
        }
      }
      if(!bContinueAck) {
        return;
      }
    }
  }
  else {
    if(sMessage != "") {
      ChildPanelOnCentralModal(
          "vision/MessageWarning",
          getCatStr("sc", "attention"),
          makeDynString("$1:" + sMessage));
    }
    return;
  }
  acker.acknowledge();
  sMessage = acker.getError();
  if(sMessage != "") {
    ChildPanelOnCentralModal(
        "vision/MessageWarning",
        getCatStr("sc", "attention"),
        makeDynString("$1:" + sMessage));
  }
  else {
    ChildPanelOnCentralModal(
        "vision/MessageWarning",
        getCatStr("sc", "attention"),
        makeDynString("$1:Alarm acknowledgement completed"));
  }
}

//@}

/** @name Private functions: processing of filter completions */
//@(

/**
 * Get completions for filter on device type
 * @param dsSelectedSystems The list of selected system names
 * @param mResult mapping where result shall be placed, 3 fields are available (keys are strings):
 *    - "type" The type of completions used for this filter, see enum FilterCompletionsType
 *    - "list" The list of completion strings for this filter
 *    - "map"  The list of completion strings for this filter in case complitions are related
 *            to connected systems. Key is string (system name), value is dyn_string = list of
 *            completions if this system is connected
 */
private void _AlarmScreenJCOP_getFilterCompletions_deviceType(const dyn_string &dsSelectedSystems, mapping &mResult)
{
  mResult[AS_FILTER_COMPLETION_KEY_TYPE] = (int)FilterCompletionsType::Static;
  dyn_dyn_string ddsTypes;
  dyn_string exceptionInfo;
  fwDevice_getAllTypes(ddsTypes, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN(__FUNCTION__ + "(): got exception", exceptionInfo);
  }
  mResult[AS_FILTER_COMPLETION_KEY_LIST] = ddsTypes[1];
}
