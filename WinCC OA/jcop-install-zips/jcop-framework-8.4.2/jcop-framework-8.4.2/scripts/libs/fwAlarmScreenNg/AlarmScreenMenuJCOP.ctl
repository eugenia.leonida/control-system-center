// $License: NOLICENSE
//--------------------------------------------------------------------------------
/**
  Set of function for working with AS NG popup menu in JCOP configuration
  @file $relPath
  @copyright $copyright
  @author lkopylov
*/

//--------------------------------------------------------------------------------
// used libraries (#uses)
#uses "fwGeneral/fwException"
#uses "AlarmScreenNg/AlarmScreenNg"  // common definitions, mainly - string constants
#uses "AlarmScreenNg/AlarmScreenNgActions"  // the names of'standard' actions
#uses "fwAlarmScreenNg/AlarmScreenJCOP"  // JCOP-specific definitiona and functions
#uses "AlarmScreenNg/AlarmScreenNgDisplay"  // functions to open 'standard' alarm panels
#uses "AlarmScreenNg/classes/AsNgAccessControl"  // access control for NG AS
#uses "AlarmScreenNg/classes/AsNgHelp"  // displaying help for alarm DPE

//--------------------------------------------------------------------------------
// variables and constants

// conditionally load fwTrending used in AlarmScreenNg_createPlotDp
private const bool fwTrendingLibLoaded = fwGeneral_loadCtrlLib("fwTrending/fwTrending.ctl", false);

//--------------------------------------------------------------------------------
//@public members
//--------------------------------------------------------------------------------

/**
 * Calculate the appearance of 'FSM panel' item in popup menu. On input function receives enough information
 * to know what shall be executed by this menu item and how this item is expected to appear in menu.
 * The function may modify appearance of item (visiblitiy, label...). If item appearcne shall not be changed,
 * then function shall return empty mapping; otherwise returned mapping shall contain new values for
 * 'components' of appearances which shall be changed compared to default. See constants AS_MENU_ITEM_KEY_xxx
 * @param mArgs The mapping containing all argument foe call, value are strings:
 *    - "sourceMode": the value is integer with (enumerated) mode of alarm source: live, archived...
 *    - "mSource": the value is mapping with information on mouse event: row/column in table, (X,Y) coordinates of mouse cursor etc.
 *    - "mAlarm": the value is mapping with properties of alarm in row where mouse was clicked
 *    - "ewoShape": the value is shape - AS EWO itself, in case the function will need more information from EWO (for example, get list of all selected alarms)
 *    - "action": the value is string - the name of action, supplied in configuration of menu item
 * @param mItem The appearance of menu item, which will be used if this function will not make changes.
 *      See constants AS_MENU_ITEM_KEY_xxx = keys of this mapping. It is guranteed that all keys are present
 *      in this argument.
 * @param dsExceptions The variable where errors can be added, see fwException
 * @return mapping with components of menu item appearance modified compared to default passed in <c>mIten</c> argument.
 *   If function doesn't want to modify item appearance, then it just returns empty mapping.
 */
mapping AlarmScreenMenuJCOP_itemAppearanceFsmPanel(const mapping &mArgs, const mapping &mItem, dyn_string &dsExceptions)
{
  mapping mResult;  // Empty result == nothing to change
  mapping mAlarmData = mArgs["mAlarm"];
  string sDpeName = mAlarmData[ALARM_PROP_FULL_DPE];
  if(!dpExists(sDpeName))
  {
    mResult[AS_MENU_ITEM_KEY_ACCESS] = ALARM_SCREEN_ACCESS_ACTION_DISABLE;  // Only allowed for existing DP
  }
  return mResult;
}

/**
 * Calculate the appearance of 'Details' item in popup menu. On input function receives enough information
 * to know what shall be executed by this menu item and how this item is expected to appear in menu.
 * The function may modify appearance of item (visiblitiy, label...). If item appearcne shall not be changed,
 * then function shall return empty mapping; otherwise returned mapping shall contain new values for
 * 'components' of appearances which shall be changed compared to default. See constants AS_MENU_ITEM_KEY_xxx
 * @param mArgs The mapping containing all argument foe call, value are strings:
 *    - "sourceMode": the value is integer with (enumerated) mode of alarm source: live, archived...
 *    - "mSource": the value is mapping with information on mouse event: row/column in table, (X,Y) coordinates of mouse cursor etc.
 *    - "mAlarm": the value is mapping with properties of alarm in row where mouse was clicked
 *    - "ewoShape": the value is shape - AS EWO itself, in case the function will need more information from EWO (for example, get list of all selected alarms)
 *    - "action": the value is string - the name of action, supplied in configuration of menu item
 * @param mItem The appearance of menu item, which will be used if this function will not make changes.
 *      See constants AS_MENU_ITEM_KEY_xxx = keys of this mapping. It is guranteed that all keys are present
 *      in this argument.
 * @param dsExceptions The variable where errors can be added, see fwException
 * @return mapping with components of menu item appearance modified compared to default passed in <c>mIten</c> argument.
 *   If function doesn't want to modify item appearance, then it just returns empty mapping.
 */
mapping AlarmScreenMenuJCOP_itemAppearanceDetails(const mapping &mArgs, const mapping &mItem, dyn_string &dsExceptions)
{
  mapping mResult;  // Empty result == nothing to change
  const int iMode = mArgs["sourceMode"];
  if(iMode != AS_NG_EWO_MODE_ONLINE)
  {
    mResult[AS_MENU_ITEM_KEY_ACCESS] = ALARM_SCREEN_ACCESS_ACTION_DISABLE;  // Only allowed in live (online) mode
  }
  return mResult;
}

/**
 * Calculate the appearance of 'Trend' item in popup menu. On input function receives enough information
 * to know what shall be executed by this menu item and how this item is expected to appear in menu.
 * The function may modify appearance of item (visiblitiy, label...). If item appearcne shall not be changed,
 * then function shall return empty mapping; otherwise returned mapping shall contain new values for
 * 'components' of appearances which shall be changed compared to default. See constants AS_MENU_ITEM_KEY_xxx
 * @param mArgs The mapping containing all argument foe call, value are strings:
 *    - "sourceMode": the value is integer with (enumerated) mode of alarm source: live, archived...
 *    - "mSource": the value is mapping with information on mouse event: row/column in table, (X,Y) coordinates of mouse cursor etc.
 *    - "mAlarm": the value is mapping with properties of alarm in row where mouse was clicked
 *    - "ewoShape": the value is shape - AS EWO itself, in case the function will need more information from EWO (for example, get list of all selected alarms)
 *    - "action": the value is string - the name of action, supplied in configuration of menu item
 * @param mItem The appearance of menu item, which will be used if this function will not make changes.
 *      See constants AS_MENU_ITEM_KEY_xxx = keys of this mapping. It is guranteed that all keys are present
 *      in this argument.
 * @param dsExceptions The variable where errors can be added, see fwException
 * @return mapping with components of menu item appearance modified compared to default passed in <c>mIten</c> argument.
 *   If function doesn't want to modify item appearance, then it just returns empty mapping.
 */
mapping AlarmScreenMenuJCOP_itemAppearanceTrend(const mapping &mArgs, const mapping &mItem, dyn_string &dsExceptions)
{
  mapping mResult;  // Empty result == nothing to change
  if(!mappingHasKey(mArgs["mAlarm"], ALARM_PROP_SUM))  // the property is missing in archived alarms
  {
    mResult[AS_MENU_ITEM_KEY_ACCESS] = ALARM_SCREEN_ACCESS_ACTION_DISABLE;  // No trend for unknown alarm
  }
  else if(mArgs["mAlarm"][ALARM_PROP_SUM])
  {
    mResult[AS_MENU_ITEM_KEY_ACCESS] = ALARM_SCREEN_ACCESS_ACTION_DISABLE;  // No trend for summary alarm
  }
  else
  {
    if(fwTrendingLibLoaded)
    {
      AlarmScreenNg_createPlotDp();
    }
    else
    {
      mResult[AS_MENU_ITEM_KEY_ACCESS] = ALARM_SCREEN_ACCESS_ACTION_DISABLE;  // No trend if trending component is not available
    }
  }
  return mResult;
}

/**
 * Calculate the appearance of 'Mask Alarm' item in popup menu. On input function receives enough information
 * to know what shall be executed by this menu item and how this item is expected to appear in menu.
 * The function may modify appearance of item (visiblitiy, label...). If item appearcne shall not be changed,
 * then function shall return empty mapping; otherwise returned mapping shall contain new values for
 * 'components' of appearances which shall be changed compared to default. See constants AS_MENU_ITEM_KEY_xxx
 * @param mArgs The mapping containing all argument foe call, value are strings:
 *    - "sourceMode": the value is integer with (enumerated) mode of alarm source: live, archived...
 *    - "mSource": the value is mapping with information on mouse event: row/column in table, (X,Y) coordinates of mouse cursor etc.
 *    - "mAlarm": the value is mapping with properties of alarm in row where mouse was clicked
 *    - "ewoShape": the value is shape - AS EWO itself, in case the function will need more information from EWO (for example, get list of all selected alarms)
 *    - "action": the value is string - the name of action, supplied in configuration of menu item
 * @param mItem The appearance of menu item, which will be used if this function will not make changes.
 *      See constants AS_MENU_ITEM_KEY_xxx = keys of this mapping. It is guranteed that all keys are present
 *      in this argument.
 * @param dsExceptions The variable where errors can be added, see fwException
 * @return mapping with components of menu item appearance modified compared to default passed in <c>mIten</c> argument.
 *   If function doesn't want to modify item appearance, then it just returns empty mapping.
 */
mapping AlarmScreenMenuJCOP_itemAppearanceMaskAlarm(const mapping &mArgs, const mapping &mItem, dyn_string &dsExceptions)
{
  mapping mResult;  // Empty result == nothing to change

  // By default 'Mask Alarm' is disabled, special option shall be added to configuration in order to enable id
  bool bEnabled = false;
  shape ewo = mArgs["ewoShape"];
  mapping mOption = ewo.getSpecificOption("maskWithForceFiltered");
  if(mappingHasKey(mOption, "enabled"))
  {
    bEnabled = mOption["enabled"];
  }
  if(!bEnabled)
  {
    mResult[AS_MENU_ITEM_KEY_ACCESS] = ALARM_SCREEN_ACCESS_ACTION_HIDE;  // No masking allowed
  }
  else // Action is allowed, decide what label shall be displayed
  {
    const int iMode = mArgs["sourceMode"];
    if(iMode == AS_NG_EWO_MODE_ONLINE)
    {
      if(mArgs["mAlarm"][ALARM_PROP_FORCE_FILTERED])
      {
        mResult[AS_MENU_ITEM_KEY_LABEL] = "Unmask Alarm";
      }
      else
      {
        mResult[AS_MENU_ITEM_KEY_LABEL] = "Mask Alarm";
      }
    }
    else
    {
      mResult[AS_MENU_ITEM_KEY_ACCESS] = ALARM_SCREEN_ACCESS_ACTION_DISABLE;  // Only allowed in live (online) mode
    }
  }
  return mResult;
}

/**
 * Calculate the appearance of 'Comment' item in popup menu. On input function receives enough information
 * to know what shall be executed by this menu item and how this item is expected to appear in menu.
 * The function may modify appearance of item (visiblitiy, label...). If item appearcne shall not be changed,
 * then function shall return empty mapping; otherwise returned mapping shall contain new values for
 * 'components' of appearances which shall be changed compared to default. See constants AS_MENU_ITEM_KEY_xxx
 * @param mArgs The mapping containing all argument foe call, value are strings:
 *    - "sourceMode": the value is integer with (enumerated) mode of alarm source: live, archived...
 *    - "mSource": the value is mapping with information on mouse event: row/column in table, (X,Y) coordinates of mouse cursor etc.
 *    - "mAlarm": the value is mapping with properties of alarm in row where mouse was clicked
 *    - "ewoShape": the value is shape - AS EWO itself, in case the function will need more information from EWO (for example, get list of all selected alarms)
 *    - "action": the value is string - the name of action, supplied in configuration of menu item
 * @param mItem The appearance of menu item, which will be used if this function will not make changes.
 *      See constants AS_MENU_ITEM_KEY_xxx = keys of this mapping. It is guranteed that all keys are present
 *      in this argument.
 * @param dsExceptions The variable where errors can be added, see fwException
 * @return mapping with components of menu item appearance modified compared to default passed in <c>mIten</c> argument.
 *   If function doesn't want to modify item appearance, then it just returns empty mapping.
 */
mapping AlarmScreenMenuJCOP_itemAppearanceCommentPanel(const mapping &mArgs, const mapping &mItem, dyn_string &dsExceptions)
{
  mapping mResult;  // Empty result == nothing to change
  mapping mAlarmData = mArgs["mAlarm"];
  string sDpeName = mAlarmData[ALARM_PROP_FULL_DPE];
  if(!dpExists(sDpeName))
  {
    mResult[AS_MENU_ITEM_KEY_ACCESS] = ALARM_SCREEN_ACCESS_ACTION_DISABLE;  // Only allowed for existing DP
  }
  return mResult;
}

/**
 * Calculate the appearance of 'Help' item in popup menu. On input function receives enough information
 * to know what shall be executed by this menu item and how this item is expected to appear in menu.
 * The function may modify appearance of item (visiblitiy, label...). If item appearcne shall not be changed,
 * then function shall return empty mapping; otherwise returned mapping shall contain new values for
 * 'components' of appearances which shall be changed compared to default. See constants AS_MENU_ITEM_KEY_xxx
 * @param mArgs The mapping containing all argument foe call, value are strings:
 *    - "sourceMode": the value is integer with (enumerated) mode of alarm source: live, archived...
 *    - "mSource": the value is mapping with information on mouse event: row/column in table, (X,Y) coordinates of mouse cursor etc.
 *    - "mAlarm": the value is mapping with properties of alarm in row where mouse was clicked
 *    - "ewoShape": the value is shape - AS EWO itself, in case the function will need more information from EWO (for example, get list of all selected alarms)
 *    - "action": the value is string - the name of action, supplied in configuration of menu item
 * @param mItem The appearance of menu item, which will be used if this function will not make changes.
 *      See constants AS_MENU_ITEM_KEY_xxx = keys of this mapping. It is guranteed that all keys are present
 *      in this argument.
 * @param dsExceptions The variable where errors can be added, see fwException
 * @return mapping with components of menu item appearance modified compared to default passed in <c>mIten</c> argument.
 *   If function doesn't want to modify item appearance, then it just returns empty mapping.
 */
mapping AlarmScreenMenuJCOP_itemAppearanceHelp(const mapping &mArgs, const mapping &mItem, dyn_string &dsExceptions)
{
  mapping mResult;  // Empty result == nothing to change
  mapping mAlarmData = mArgs["mAlarm"];
  string sDpeName = mAlarmData[ALARM_PROP_FULL_DPE];
  if(!dpExists(sDpeName))
  {
    mResult[AS_MENU_ITEM_KEY_ACCESS] = ALARM_SCREEN_ACCESS_ACTION_DISABLE;  // Only allowed for existing DP
  }
  return mResult;
}


/**
 * Replacement for function _fwAlarmHandling_showFsmPanel(): shown FSM panel for
 * DP of selected alarm
 * @param mArgs The mapping containing all argument foe call, value are strings:
 *    - "mSource": the value is mapping with information on mouse event: row/column in table, (X,Y) coordinates of mouse cursor etc.
 *    - "mAlarm": the value is mapping with properties of alarm in row where mouse was clicked
 *    - "ewoShape": the value is shape - AS EWO itself, in case the function will need more information from EWO (for example, get list of all selected alarms)
 *    - "action": the value is string - the name of action, supplied in configuration of menu item
 * @param exceptionInfo The variable where details of exception will be added in case of error
 */
void AlarmScreenMenuJCOP_showFsmPanel(const mapping &mArgs, dyn_string &exceptionInfo)
{
  mapping mAlarmData = mArgs["mAlarm"];
  string dpId = mAlarmData[ALARM_PROP_FULL_DPE];
  string dpName = dpSubStr(dpId, DPSUB_SYS_DP);
  string dpSystem = dpSubStr(dpId, DPSUB_SYS);
  string nodeName;
  if(dpTypeName(dpName) == "_FwFsmObject")
  {
    dyn_string parts = strsplit(dpName, "|");
    if(dynlen(parts) > 2)
    {
      nodeName = parts[2] + "::" + parts[3];
    }
    else
    {
      nodeName = parts[2];
    }
  }
  else
  {
    dyn_dyn_anytype queryResult;
    dpQuery("SELECT '_original.._value' FROM '*.tnode' REMOTE '" + dpSystem + "' WHERE '_original.._value' == \""
				+ dpName + "\"", queryResult);

    if(dynlen(queryResult) >= 2)
    {
      nodeName = dpSubStr(queryResult[2][1], DPSUB_DP);
      strreplace(nodeName, "|", "::");
    }
  }

  if(nodeName != "")
  {
    fwCU_view(nodeName);
  }
  else
  {
    fwException_raise(exceptionInfo, "ERROR", "The corresponding FSM object could not be found for " + dpName, "");
  }
}

/**
 * Replacement for _fwAlarmHandling_showTrend() function
 * @param mArgs The mapping containing all argument foe call, value are strings:
 *    - "mSource": the value is mapping with information on mouse event: row/column in table, (X,Y) coordinates of mouse cursor etc.
 *    - "mAlarm": the value is mapping with properties of alarm in row where mouse was clicked
 *    - "ewoShape": the value is shape - AS EWO itself, in case the function will need more information from EWO (for example, get list of all selected alarms)
 *    - "action": the value is string - the name of action, supplied in configuration of menu item
 * @param exceptionInfo The variable where details of exception will be added in case of error
 */
void AlarmScreenMenuJCOP_showTrend(const mapping &mArgs, dyn_string &exceptionInfo)
{
  mapping mAlarmData = mArgs["mAlarm"];
  string dpId = mAlarmData[ALARM_PROP_FULL_DPE];
  string dpe = dpSubStr(dpId, DPSUB_SYS_DP_EL);

  ChildPanelOnCentral("vision/AlarmScreenNg/AsNgTrend.pnl", "Trend for " + dpe,
                      makeDynString("$sDpe:" + dpe));
}

/**
 * Replacement for _fwAlarmHandling_showHelp() function: show help on selected alarm
 * @param mArgs The mapping containing all argument foe call, value are strings:
 *    - "mSource": the value is mapping with information on mouse event: row/column in table, (X,Y) coordinates of mouse cursor etc.
 *    - "mAlarm": the value is mapping with properties of alarm in row where mouse was clicked
 *    - "ewoShape": the value is shape - AS EWO itself, in case the function will need more information from EWO (for example, get list of all selected alarms)
 *    - "action": the value is string - the name of action, supplied in configuration of menu item
 * @param exceptionInfo The variable where details of exception will be added in case of error
 */
void AlarmScreenMenuJCOP_showHelp(const mapping &mArgs, dyn_string &exceptionInfo)
{
  mapping mAlarmData = mArgs["mAlarm"];

  // TODO: the purpose of commenting 'open help' operation is not clear
  _AlarmScreenMenuJCOP_addHelpOpenComment(mAlarmData, exceptionInfo);
  AsNgHelp help;
  help.showHelpOnAlarmDpe(mAlarmData[ALARM_PROP_FULL_DPE], exceptionInfo);
}

/**
 * Shown details of selected alarm
 * @param mArgs The mapping containing all argument foe call, value are strings:
 *    - "mSource": the value is mapping with information on mouse event: row/column in table, (X,Y) coordinates of mouse cursor etc.
 *    - "mAlarm": the value is mapping with properties of alarm in row where mouse was clicked
 *    - "ewoShape": the value is shape - AS EWO itself, in case the function will need more information from EWO (for example, get list of all selected alarms)
 *    - "action": the value is string - the name of action, supplied in configuration of menu item
 * @param exceptionInfo The variable where details of exception will be added in case of error
 */
void AlarmScreenMenuJCOP_showDetails(const mapping &mArgs, dyn_string &exceptionInfo)
{
  mapping mAlarmData = mArgs["mAlarm"];
  AlarmScreenNgDisplay_showDetails(mAlarmData, exceptionInfo);
}

/**
 * Replacement for _fwAlarmScreen_showCommentPanel() function, which finally jusy calls aes_insertComment(),
 * or, in this library, calls a local replacement for that function
 * @param mArgs The mapping containing all argument foe call, value are strings:
 *    - "mSource": the value is mapping with information on mouse event: row/column in table, (X,Y) coordinates of mouse cursor etc.
 *    - "mAlarm": the value is mapping with properties of alarm in row where mouse was clicked
 *    - "ewoShape": the value is shape - AS EWO itself, in case the function will need more information from EWO (for example, get list of all selected alarms)
 *    - "action": the value is string - the name of action, supplied in configuration of menu item
 * @param exceptionInfo The variable where details of exception will be added in case of error
 */
void AlarmScreenMenuJCOP_showCommentPanel(const mapping &mArgs, dyn_string &exceptionInfo)
{
  mapping mAlarmData = mArgs["mAlarm"];
  AlarmScreenNgDisplay_showCommentPanel(mAlarmData, exceptionInfo);
}

/**
 * Process action 'toggle alarm mask'. The action is done by writing _force_filtered attribute of selected alarm(s).
 * what value to write (true/false) is decided based on the value of attribute in a row where mouse was clicked.
 * @param mArgs The mapping containing all argument foe call, value are strings:
 *    - "mSource": the value is mapping with information on mouse event: row/column in table, (X,Y) coordinates of mouse cursor etc.
 *    - "mAlarm": the value is mapping with properties of alarm in row where mouse was clicked
 *    - "ewoShape": the value is shape - AS EWO itself, in case the function will need more information from EWO (for example, get list of all selected alarms)
 *    - "action": the value is string - the name of action, supplied in configuration of menu item
 * @param exceptionInfo The variable where details of exception will be added in case of error
 */
void AlarmScreenMenuJCOP_toggleAlarmMasked(const mapping &mArgs, dyn_string &exceptionInfo)
{
  // Decide what will we do: mask/unmask
  mapping mAlarmData = mArgs["mAlarm"];
  bool bMask = !mAlarmData[ALARM_PROP_FORCE_FILTERED];

  // Build list of alarms to act on, based on selection in EWO
  shape ewo = mArgs["ewoShape"];
  dyn_mapping dmAlarmsToAct = ewo.getSelectedRowsData("edit");
  //DebugN(__FUNCTION__ + "(): selected alarms:", dmAlarmsToAct);
  for(int idx = dynlen(dmAlarmsToAct) ; idx > 0 ; idx--)
  {
    if(dmAlarmsToAct[idx][ALARM_PROP_FORCE_FILTERED] == bMask)
    {
      dynRemove(dmAlarmsToAct, idx);  // This alarm already has right value of _force_filtered attribute
    }
  }

  // Ask for user configuation
  int iCount = dynlen(dmAlarmsToAct);
  string sMessage = "You are going to " + (bMask ? "MASK" : "UNMASK") + " " + iCount + " alarm" +
                    (iCount > 1 ? "s" : "");
  dyn_string dsReturn;
  dyn_float dfReturn;
  ChildPanelOnCentralModalReturn(
      "vision/MessageWarning2",
      getCatStr("sc", "Attention"),
      makeDynString("$1:" + sMessage,
                    "$2:" + getCatStr("general", "yes"),
                    "$3:" + getCatStr("sc", "no")),
      dfReturn, dsReturn);
  if(dynlen(dfReturn) == 0)
  {
    return;
  }
  if(dfReturn[1] != 1)
  {
    return;
  }

  // Execute
  for(int idx = dynlen(dmAlarmsToAct) ; idx > 0 ; idx--)
  {
    alertSet(dmAlarmsToAct[idx][ALARM_PROP_TIME],
             dmAlarmsToAct[idx][ALARM_PROP_INDEX],
             AlarmScreenNg_appendDpeAttr(dmAlarmsToAct[idx][ALARM_PROP_FULL_ALARM_DPE], "_force_filtered"),
             bMask);
    dyn_errClass dsErrors = getLastError();
    if(dynlen(dsErrors) > 0)
    {
      fwException_raise(exceptionInfo, "ERROR", __FUNCTION__ + "(): alertSet(" +
                        AlarmScreenNg_appendDpeAttr(dmAlarmsToAct[idx][ALARM_PROP_FULL_ALARM_DPE], "_force_filtered") +
                        ") failed: " + getErrorText(dsErrors), "");
      // TODO: break or not?
    }
  }
}


//--------------------------------------------------------------------------------
//@private members
//--------------------------------------------------------------------------------

/**
 * Replacement for _fwAlarmHandlingScreen_addHelpOpenComment() function.<br>
 * I can't understand the logic of original function at all. Looks like what it does is just
 * writing comment with current timestamp. But:
 *  - we know exact DPE name to work with, then what is the reason for: 1) querying "FROM '*'" and then
 *    2) loop on result of query to find our DPE? For me more logical seems a query on particular DPE
 *  - what is the purpose of query? In original it looks like the main purpose if to find the time
 *    of alarm. But we know the time from Table (or AS EWO) data, so why do we need to search for it?
 *    May be popup menu (for example) could be opened for long enough, such that the data from table
 *    is already obsolete?
 *  - and finally: after all these queries we check if comment (from Table!) is empty, and only write
 *    new comment if the comment from table is empty. Why couldn't we make such check at the beginning
 *    of function, not after all dpQuery() were performed?
 *
 * So, I identified for myself the purpose of original function (undocummented) as: write to alarm
 * the comment with current time string if comment is empty.
 */
private void _AlarmScreenMenuJCOP_addHelpOpenComment(const mapping &mAlarmData, dyn_string &exceptionInfo)
{
  if(mappingHasKey(mAlarmData, ALARM_PROP_COMMENT))
  {
    string sComment = mAlarmData[ALARM_PROP_COMMENT];
    if(!sComment.isEmpty())
    {
      return;
    }
  }

  string sFullDpe = mAlarmData[ALARM_PROP_FULL_DPE];
  if(!dpExists(sFullDpe))
  {
    return;
  }
  string sDpElement = dpSubStr(sFullDpe, DPSUB_SYS_DP_EL);
  string sQuery = "SELECT ALERT '_alert_hdl.._value' FROM '" + sDpElement + "'";
  if(dpSubStr(sDpElement, DPSUB_SYS) != getSystemName())
  {
    sQuery += " REMOTE '" + dpSubStr(sDpElement, DPSUB_SYS) + "'";
  }
  dyn_dyn_anytype tab;
  dpQuery(sQuery, tab);
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0)
  {
    fwException_raise(exceptionInfo, "ERROR", __FUNCTION__ + "(): dpQuery() failed", "");
    throwError(err);
    return;
  }
//DebugN(__FUNCTION__ + "(): mAlarmData", mAlarmData);

  time alTime = mAlarmData[ALARM_PROP_TIME];
  int alIndex = mAlarmData[ALARM_PROP_INDEX];
  for(int i = dynlen(tab) ; i > 1 ; i--)
  {
    atime aTimeFromQuery = tab[i][2];
    time tFromQuery = aTimeFromQuery;
    int count = getACount(aTimeFromQuery);
    if((tFromQuery == alTime) && (count == alIndex))
    {
      //DebugN(__FUNCTION__ + "(): alarm found: " + i + " of " + dynlen(tab));
      //get timestamp of the alarm (it is in the element 2 of tab, atime with all: DPE, time and index)
      alertSet(tFromQuery, count,
               AlarmScreenNg_appendDpeAttr(mAlarmData[ALARM_PROP_FULL_ALARM_DPE], "_comment"),
               formatTime("%x %H:%M:%S", getCurrentTime()));
      err = getLastError();
      if(dynlen(err) > 0)
      {
        fwException_raise(exceptionInfo, "ERROR", __FUNCTION__ + "(): alertSet() failed", "");
        throwError(err);
      }
      break;
    }
  }
}
