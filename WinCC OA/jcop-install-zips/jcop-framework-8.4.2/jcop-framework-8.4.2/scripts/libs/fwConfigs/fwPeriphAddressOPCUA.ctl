/**
* This file contains OPCUA specific functions for fwPeriphAddress.
*/

/**
* Get a list of subscriptions on the system for the given server.
*
* @param sSystem        The system.
* @param sServer        The server.
* @param &exceptionInfo Exceptions.
*
* @return dyn_string A list of subscriptions.
*/
public dyn_string fwPeriphAddressOPCUA_getSubscriptions(string sSystem, string sServer, dyn_string &exceptionInfo)
{
  int iRetVal, iLen, iLoop;
  string sDpe;
  dyn_string dsSubscription;


  if( strlen(sSystem) == 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "No system", "");
    return makeDynString();
  }

  sDpe = sSystem + "_" + sServer + ".Config.Subscriptions:_original.._value";
  if( !dpExists(sDpe) )
  {
    if( strlen(sServer) == 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "No OPCUA server", "");
      return dsSubscription;
    }
    fwException_raise(exceptionInfo, "ERROR", "Subscriptions DPE does not exist", "");
    return dsSubscription;
  }

  iRetVal = dpGet(sDpe, dsSubscription);
  if( iRetVal == -1 )
  {
    fwException_raise(exceptionInfo, "ERROR", "Could not get subscriptions: dpGet(" + sDpe + ") failed", "");
    return dsSubscription;
  }

  iLen = dynlen(dsSubscription);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dsSubscription[iLoop] = dpSubStr(dsSubscription[iLoop], DPSUB_DP);
    dsSubscription[iLoop] = substr(dsSubscription[iLoop], 1, strlen(dsSubscription[iLoop]) - 1);
  }
  dynSortAsc(dsSubscription);

  return dsSubscription;
}



/**
* Get the type for the given subscription.
*
* @param sSys          The system.
* @param sSubscription The subscription.
*
* @return int The type or -1 on error.
*/
public int fwPeriphAddressOPCUA_getSubscriptionType(string sSystem, string sSubscription, dyn_string &exceptionInfo)
{
  int iType, iRetVal;
  string sDpe;


  if( strlen(sSystem) == 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "No system", "");
    return -1;
  }


  sDpe = sSystem + "_" + sSubscription + ".Config.SubscriptionType";
  if( !dpExists(sDpe) )
  {
    if( strlen(sSubscription) == 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "No subscription", "");
      return -1;
    }

    fwException_raise(exceptionInfo, "ERROR", "Subscription type DPE does not exist", "");
    return -1;
  }

  iRetVal = dpGet(sDpe, iType);
  if( iRetVal == -1 )
  {
    fwException_raise(exceptionInfo, "ERROR", "Could not get subscription type: dpGet(" + sDpe + ") failed", "");
    return -1;
  }

  return iType;
}





/**
* Get a list of servers on the given system.
*
* @return dyn_string List of servers.
*/
public dyn_string fwPeriphAddressOPCUA_getServers(string sys, dyn_string &exceptionInfo) {
  if (strlen(sys) == 0) {
     fwException_raise(exceptionInfo, "ERROR", "No system","");
     return makeDynString();
  }

  dyn_string dsEqu = dpNames(sys+"*", "_OPCUAServer");

  int len = dynlen(dsEqu);

  for (int i = len; i > 0; i--) {
    // don't display redundant datapoints
    if (isReduDp(dsEqu[i])) {
      dynRemove(dsEqu, i);
    }
  }
  len = dynlen(dsEqu);
  if (len > 0) {
    for (int i = 1; i <= len; i++) {
      dsEqu[i] = dpSubStr(dsEqu[i], DPSUB_DP);
      dsEqu[i] = substr(dsEqu[i], 1, strlen(dsEqu[i]) - 1);
    }
  }

  return dsEqu;
}





/* PANEL functions */

string dpe; /*!< the DPE(s) that we're working with */
bool   isMultiDpes; /*!< Is there multiple DPEs selected? */
global int fwPeriphAddressOPCUA_panelMode; /*!< panel mode - used in the the panel functions, so must be global */


public void _fwPeriphAddressOPCUA_initPanel(string sDpe, dyn_string &exceptionInfo, bool bInMultiDpes = FALSE, int iInPanelMode = -1)
{
  DebugFTN("FW_INFO", "_fwPeriphAddressOPCUA_initPanel(" + sDpe + ", " + bInMultiDpes + ", " + iInPanelMode + ")");


  dpe                            = sDpe;
  isMultiDpes                    = bInMultiDpes;
  fwPeriphAddressOPCUA_panelMode = iInPanelMode;

//  if ( iInPanelMode != fwPeriphAddress_PANEL_MODE_OBJECT )
//  {
//    fwPeriphAddressOPCUA_panel_updateSubscriptions();
//  }

  fwPeriphAddressOPCUA_panel_update();
}



/**
* Update the subscriptions.
*
* @return void
*/
public void fwPeriphAddressOPCUA_panel_updateSubscriptions()
{
  string sConnection, sSubscription;
  dyn_string dsEquipment, dsSubscription;


  sConnection   = cmbEquipment.text();
  sSubscription = cmbSubscription.text();

  fwPeriphAddressOPCUA_panel_updatePanelFillServers(dpe);
  fwPeriphAddressOPCUA_panel_updatePanelFillSubscriptions(dpe, cmbEquipment.text());

  getMultiValue("cmbEquipment",    "items", dsEquipment,
                "cmbSubscription", "items", dsSubscription);

  setMultiValue("cmbEquipment",    "selectedPos", dynContains(dsEquipment, sConnection),
                "cmbSubscription", "selectedPos", dynContains(dsSubscription, sSubscription));
}




//the following functions are derived from opcuaDrvPara.ctl

/**
* Get the direction from the direction and receive mode GUI widgets.
*
* @return int The direction.
*/
private int _getDirection() {
  int direction = einaus.number, receiveMode = modus.number;
  direction++;

  if (direction == 2) {
    if (receiveMode == 1) {
      direction = DPATTR_ADDR_MODE_INPUT_POLL;
    } else if (receiveMode == 2) {
      direction = DPATTR_ADDR_MODE_INPUT_SQUERY;
    } else if (receiveMode == 3) {
      direction = DPATTR_ADDR_MODE_IO_SQUERY;
    }
  } else if (direction == 3) {
    if (receiveMode == 0) {
      direction = DPATTR_ADDR_MODE_IO_SPONT;
    } else if (receiveMode == 1) {
      direction = DPATTR_ADDR_MODE_IO_POLL;
    } else if (receiveMode == 3) {
      direction = 9; //DPATTR_ADDR_MODE_AM_ALERT not defined in 3.11?
    }
  }

  return direction;
}

/**
* Fill the servers combo box.
*
* @return void
*/
public void fwPeriphAddressOPCUA_panel_updatePanelFillServers(string sDpe)
{
  dyn_string exceptionInfo, dsServers;


  dsServers = fwPeriphAddressOPCUA_getServers(dpSubStr(sDpe, DPSUB_SYS), exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwExceptionHandling_display(exceptionInfo);
  }
  else
  {
    setValue("cmbEquipment", "items", dsServers);
  }

}




/**
* Fill the subscriptions combo box.
*
* @return void
*/
public void fwPeriphAddressOPCUA_panel_updatePanelFillSubscriptions(string sDpe, string sServer)
{
  dyn_string dsSubscription, exceptionInfo;


  dsSubscription = fwPeriphAddressOPCUA_getSubscriptions(dpSubStr(sDpe, DPSUB_SYS), sServer, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwExceptionHandling_display(exceptionInfo);
    return;
  }
  else
  {
    dynInsertAt(dsSubscription, "", 1);

    setValue("cmbSubscription", "deleteAllItems");
    setValue("cmbSubscription", "items", dsSubscription);
  }

}





/**
* Update the view by hiding/showing widgets.
*
* @return void
*/
public void fwPeriphAddressOPCUA_panel_update()
{
  bool bVisible, bVisibleMode, bVisiblePollGroup, bVisibleEinaus, bIsNotObjectMode;
  int iDirection, iSubscription;
  string sSubscriptionText;


  DebugFTN("FW_INFO", "fwPeriphAddressOPCUA_panelMode: " + fwPeriphAddressOPCUA_panelMode);

  bVisible          = FALSE;
  bVisibleMode      = FALSE;
  bVisiblePollGroup = FALSE;
  bVisibleEinaus    = FALSE;
  bIsNotObjectMode  = FALSE;

  if( fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES )
  {
    bVisible = TRUE;
  }

  if( bVisible && modus.visible() )
  {
    bVisibleMode = TRUE;
  }

  if( bVisible && cmbPollGroup.visible() )
  {
    bVisiblePollGroup = TRUE;
  }

  if( einaus.number != 0 )
  {
    bVisibleEinaus = TRUE;
  }

  if( fwPeriphAddressOPCUA_panelMode != fwPeriphAddress_PANEL_MODE_OBJECT )
  {
    bIsNotObjectMode = TRUE;
  }

  setMultiValue("mainApplyButton",        "visible", TRUE,
                "txtPollGroup",           "visible", FALSE,
                "setOpcuaServer",         "visible", bVisible,
                "setOpcuaDriver",         "visible", bVisible,
                "setOpcuaVariant",        "visible", bVisible,
                "setOpcuaDirection",      "visible", bVisible,
                "setOpcuaMode",           "visible", bVisible,
                "setOpcuaSubscription",   "visible", bVisible,
                "setOpcuaTransformation", "visible", bVisible,
                "setOpcuaPollGroup",      "visible", bVisible,
                "setOpcuaLowLevel",       "visible", bVisible,
                "setOpcuaActive",         "visible", bVisible,
                "setOpcuaMode",           "visible", bVisibleMode,
                "setOpcuaPollGroup",      "visible", bVisiblePollGroup,
                "lowlevel",               "visible", bVisibleEinaus);

  if( fwPeriphAddressOPCUA_panelMode != fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES )
  {
    setMultiValue("setOpcuaServer",         "state", 0, TRUE,
                  "setOpcuaDriver",         "state", 0, TRUE,
                  "setOpcuaVariant",        "state", 0, TRUE,
                  "setOpcuaSubscription",   "state", 0, TRUE,
                  "setOpcuaDirection",      "state", 0, TRUE,
                  "setOpcuaMode",           "state", 0, TRUE,
                  "setOpcuaTransformation", "state", 0, TRUE,
                  "setOpcuaPollGroup",      "state", 0, TRUE,
                  "setOpcuaLowLevel",       "state", 0, TRUE,
                  "setOpcuaActive",         "state", 0, TRUE);
  }

  iDirection = _getDirection();

  // IM 104259 mjeidler: iSub contains SubscriptionType of the chosen Subscription
  iSubscription = radKind.number;
  if( !bIsNotObjectMode )
  {
    sSubscriptionText = txtSubscription.text();
  }
  else
  {
    sSubscriptionText = cmbSubscription.text();
  }

  setMultiValue("tm",       "visible", iDirection != DPATTR_ADDR_MODE_OUTPUT,
                "border1",  "visible", bIsNotObjectMode && iDirection != DPATTR_ADDR_MODE_OUTPUT,
                "modus",    "visible", iDirection != DPATTR_ADDR_MODE_OUTPUT,
                "lowlevel", "visible", /*bIsNotObjectMode && */iDirection != DPATTR_ADDR_MODE_OUTPUT);

  // IM 104259 mjeidler: Direction, Receiving Mode, LowLevel will be shown only for the correct SubscriptionType
  if( sSubscriptionText && ((iSubscription == 0) || (iSubscription == 1)) )
  {
    if( (modus.number == 1) ||
        (modus.number == 2) ||
        (modus.number == 3)  )
    {
      modus.number = 0;
    }

    setMultiValue("modus",    "itemEnabled", 0, TRUE,
                  "modus",    "itemEnabled", 1, FALSE,
                  "modus",    "itemEnabled", 2, FALSE,
                  "modus",    "itemEnabled", 3, FALSE,
                  "einaus",   "itemEnabled", 0, TRUE,
                  "einaus",   "itemEnabled", 1, TRUE,
                  "lowlevel", "visible",     TRUE);
  }
  else
    if( sSubscriptionText && (iSubscription == 2) )
    {
      if( (modus.number == 0) ||
          (modus.number == 1) ||
          (modus.number == 2)  )
      {
        modus.number = 3;
      }

      setMultiValue("modus",    "itemEnabled", 0, FALSE,
                    "modus",    "itemEnabled", 1, FALSE,
                    "modus",    "itemEnabled", 2, FALSE,
                    "modus",    "itemEnabled", 3, TRUE,
                    "einaus",   "itemEnabled", 0, FALSE,
                    "einaus",   "itemEnabled", 1, FALSE,
                    "einaus",   "number",      2,
                    "lowlevel", "visible",     FALSE);

      if( einaus.number() == 2 )
      {
        setMultiValue("tm",      "visible", TRUE,
                      "border1", "visible", TRUE,
                      "modus",   "visible", TRUE);
      }
    }
    else
    {
      if( (modus.number() == 0) ||
          (modus.number() == 3)  )
      {
        modus.number(1);
      }

      setMultiValue("modus",    "itemEnabled", 0, FALSE,
                    "modus",    "itemEnabled", 1, TRUE,
                    "modus",    "itemEnabled", 2, TRUE,
                    "modus",    "itemEnabled", 3, FALSE,
                    "einaus",   "itemEnabled", 0, TRUE,
                    "einaus",   "itemEnabled", 1, TRUE,
                    "lowlevel", "visible",     TRUE);
    }

  // IM 104259 mjeidler: view PollGroup only, if Mode PollGroup is chosen
  if( shapeExists("frmPollGroup") &&
      (einaus.number != 0)        &&
      (modus.number == 1)           )
  {
    setMultiValue("lblPollGroup", "visible", TRUE,
                  "frmPollGroup", "visible", TRUE);

    if( bIsNotObjectMode )
    {
      setMultiValue("cmbPollGroup", "visible", TRUE,
                    "cmdPollGroup", "visible", TRUE);
    }
    else
    {
      txtPollGroup.visible(TRUE);
    }
  }
  else
  {
    setMultiValue("frmPollGroup", "visible", FALSE,
                  "lblPollGroup", "visible", FALSE,
                  "cmbPollGroup", "visible", FALSE,
                  "cmdPollGroup", "visible", FALSE,
                  "txtPollGroup", "visible", FALSE);
  }

  setMultiValue("setOpcuaMode",         "visible", isMultiDpes && modus.visible(),
                "setOpcuaPollGroup",    "visible", isMultiDpes && cmbPollGroup.visible(),
                "lowlevel",             "visible", /* bIsNotObjectMode &&*/ iDirection != DPATTR_ADDR_MODE_OUTPUT,
                "setOpcuaLowLevel",     "visible", isMultiDpes && lowlevel.visible(),
                "setOpcuaSubscription", "visible", isMultiDpes,
                "setOpcuaVariant",      "visible", isMultiDpes);

  if( isMultiDpes )
  {
    setMultiValue("txtItem",     "text",    "Can't set OPC item in multiple data point element mode",
                  "txtItem",     "enabled", FALSE,
                  "buGetItemId", "enabled", FALSE);
  }

  setMultiValue("buGetItemId",      "visible", bIsNotObjectMode,
                "lblDriverNumber",  "visible", bIsNotObjectMode,
                "Treiber",          "visible", bIsNotObjectMode,
                "radVariant",       "visible", bIsNotObjectMode,
                "lblVariant",       "visible", bIsNotObjectMode,
                "cmbEquipment",     "visible", bIsNotObjectMode,
                "lblServer",        "visible", bIsNotObjectMode,
                "cboAddressActive", "visible", bIsNotObjectMode);

  if( !txtPollGroup.visible() )
  {
    txtPollGroup.text("");
  }

  //TODO is it allowed to change Kind for OPCUA (it's not editable in PARA)
  //lblKind.visible = !bIsNotObjectMode;
  //radKind.visible = !bIsNotObjectMode;

  if( !bIsNotObjectMode )
  {
    setMultiValue("lblVariant", "visible", TRUE,
                  "radVariant", "visible", TRUE);
  }

  setMultiValue("cmbSubscription", "visible", bIsNotObjectMode,
                "cmdSubscription", "visible", bIsNotObjectMode,
                "txtSubscription", "visible", !bIsNotObjectMode);
}

