/** Add a configurator to all SMI++ domains for all trees.
* @return Error code: 0 if all OK, -1 if error.
* @deprecated Function not used. CTC2: 23-07-2018
*/
 int fwFSMConfDB_addAllConfigurators()
 {
   FWDEPRECATED();
   
  dyn_string nodes;

  nodes=fwFSMConfDB_getAllCUs();

  for(int i=1;i<=dynlen(nodes);i++)
  {
   if(fwFSMConfDB_addConfigurator(nodes[i])<0){
     DebugN("ERROR:fwFSMConfDB_addConfigurator()->Configurator was not added for node "+nodes[i]);
     return fwFSMConfDB_ERROR; 
    }
  }
  
  //refresh the FSM tree in the Device Editor Navigator
  fwFsmTree_refreshTree(); 
 
 return fwFSMConfDB_OK;
}

/** The function changes the configurator settings.
* @param sDomain name of the SMI++ domain
* @param applyRecipeMode 0:device, 1:configurator, 2:configurator in simplified mode
* @param applyRecipeTimeout optional, time interval to wait before the state of the configurator is set to ERROR, default is 20 sec. 
* @param recipeLocation optional flag to indicate whether to get the Recipe from the DB or Cache. The default value is FALSE. 
* @return Error code: 0 if all OK, -1 if error.
* @deprecated Function not used. CTC2: 23-07-2018
*/
int fwFSMConfDB_changeConfigurator(string sDomain, int applyRecipeMode, int applyRecipeTimeout=20, bool useConfDB=FALSE)
{
   FWDEPRECATED();
   
 string configuratorDP;
 bool simplifiedAR;
  
 configuratorDP=fwFSMConfDB_getConfiguratorName(sDomain);
  
 if(!dpExists(configuratorDP)){
    DebugN("ERROR: configurator for domain"+ sDomain +" does not exist, create configurator first");
    return fwFSMConfDB_ERROR;
  }
  
 if(applyRecipeMode==2)
  {
    simplifiedAR=TRUE;
    applyRecipeMode=TRUE;
  }
    else
      simplifiedAR=FALSE;
    
 dpSetWait(configuratorDP+g_csApplyRecipeUsingConfigurator,applyRecipeMode,
           configuratorDP+g_csApplyRecipeSimplified,simplifiedAR,
           configuratorDP+g_csApplyRecipesTimeout,applyRecipeTimeout,
           configuratorDP+g_csUseConfDBDPE,useConfDB); 

 if(applyRecipeMode==2)
       DebugN(configuratorDP+" parameters: Apply recipes in simplified mode, "+applyRecipeTimeout+" sec. timeout.");
       else if(applyRecipeMode==1)
            DebugN(configuratorDP+" parameters: Configurator apply all recipes, "+applyRecipeTimeout+" sec. timeout.");
            else
                DebugN(configuratorDP+" parameters: Each device apply recipe to itself."); 

 if(useConfDB)
   DebugN("Load recipes from DB");
   else
     DebugN("Load recipes from cache");
 
return fwFSMConfDB_OK;
}


/** Checks that all devices specified in the second argument are defined in the corresponding recipe
 * @param sRecipe name of the recipe
 * @param dsDevices list of requested devices
 * @param dsCommonDevices sublist of devices from the requested list of devices properly defined in the recipe
 * @param useConfDB optional flag to indicate whether to get the Recipe from the DB or Cache. The default value is FALSE.
 * @return 0 if all requested devices are properly defined in the recipe or -1 if there is a mismatch
* @deprecated Function not used. CTC2: 23-07-2018
 */

int fwFSMConfDB_checkDevicesInRecipe(string sRecipe, dyn_string dsDevices, dyn_string &dsCommonDevices, bool useConfDB = false)
{
   FWDEPRECATED();
   
  dyn_dyn_mixed recipeObject;
  dyn_string exceptionInfo;
  string systemName = getSystemName();
  dyn_string recipeDevices;

  dynClear(dsCommonDevices);

  if(useConfDB){
    fwConfigurationDB_loadRecipeFromDB(sRecipe,  
                                       "",  
                                       g_sFwDevice_HIERARCHY,  //This has to change. Cannot be hardcoded 
                                       recipeObject,  
                                       exceptionInfo,
                                       systemName,
                                       "");  
  }else{
    fwConfigurationDB_loadRecipeFromCache(sRecipe,    
                                          "",  
                                          g_sFwDevice_HIERARCHY,  //This has to change. Cannot be hardcoded
                                          recipeObject,  
                                          exceptionInfo,
                                          systemName);  
  }
     
  if(dynlen(exceptionInfo) > 0){
    DebugN("ERROR: fwFSMConfDB_checkDevicesInRecipe() -> " + exceptionInfo);
    return fwFSMConfDB_ERROR;
  }
  recipeDevices =  recipeObject[2]; 

  dsCommonDevices = dynIntersect(recipeDevices, dsDevices); 

  if(dynlen(dsCommonDevices) != dynlen(dsDevices))
  {        
    DebugN("WARNING: fwFSMConfDB_checkDevicesInRecipe() -> Some of the requested devices are not defined in recipe: " + sRecipe);
    return fwFSMConfDB_ERROR;
  }
  
  return fwFSMConfDB_OK;
}


/** Gets the FW hierarchy in use for the specified control domain
* @param sDomain Name of the SMI++ domain
* @param  sFwHierarchy name of the hierarchy
* @return Error code: 0 if all OK, -1 if invalid hierarchy name
* @deprecated Function not used. CTC2: 23-07-2018
*/
int fwFSMConfDB_getFwHierarchy(string sDomain, string &sFwHierarchy)
{
   FWDEPRECATED();
   
  string hierarchy;

  dpGet("FwFSMConfDBParametrization.fwHierarchy", hierarchy);

  // dpGet(sDomain + "_ConfDB.fwHierarchy", hierarchy); : for future versions
  
  switch(hierarchy){
    case "fwDevice_HARDWARE": sFwHierarchy = fwDevice_HARDWARE;
	                          break;

    case "fwDevice_LOGICAL":  sFwHierarchy = fwDevice_LOGICAL;
	                          break;

    case "fwDevice_FSM_NAVIGATOR":      sFwHierarchy = fwDevice_FSM_NAVIGATOR;
	                          break;

    default:                  DebugN("ERROR: fwFSMConfDB_getFwHierarchy() -> Invalid FW Hierarchy name: " + sFwHierarchy);
	                          sFwHierarchy = "";
	                          return fwFSMConfDB_ERROR;
    
  
  }//end of switch
  
  return fwFSMConfDB_OK;
}

/** Sets the FW hierarchy in use for the specified control domain
* @param sDomain Name of the SMI++ domain
* @param sFwHierarchy name of the hierarchy to be set: fwDevice_HARDWARE or fwDevice_LOGICAL
* @return Error code: 0 if all OK, -1 if invalid hierarchy name
* @deprecated Function not used. CTC2: 23-07-2018
*/
int fwFSMConfDB_setFwHierarchy(string sDomain, string sFwHierarchy)
{
   FWDEPRECATED();
   
  string hierarchy;

  switch(sFwHierarchy){
    case fwDevice_HARDWARE: hierarchy = "fwDevice_HARDWARE";
	                          break;

    case fwDevice_LOGICAL:  hierarchy = "fwDevice_LOGICAL";
	                          break;

    default: DebugN("ERROR: fwFSMConfDB_setFwhierarchy() -> Invalid FW Hierarchy name: " + sFwHierarchy);
	     sFwHierarchy = "";
	     return fwFSMConfDB_ERROR;    
  
  }//end of switch
  
  dpSet("FwFSMConfDBParametrization.fwHierarchy", hierarchy);

  return fwFSMConfDB_OK;
}

/**Removes the configurator name from a dp-name
 * @param sDpName name of the dp
 * @return dp-name without the configurator name
* @deprecated Function not used. CTC2: 23-07-2018
 */
string fwFSMConfDB_removeConfiguratorName(string sDpName)
{
   FWDEPRECATED();
   
  dyn_string ds;

  ds = strsplit(sDpName, "/");
  if(dynlen(ds) > 1)
    return ds[2];
  else 
    return ds[1];  
}



/** Terminates the current action for a device in a SMI++ domain
 * @param sDomain Name of the SMI++ object
 * @param sDevice Name of the device
* @deprecated Function not used. CTC2: 23-07-2018
 */
void fwFSMConfDB_terminateCurrentCommand(string sDomain, string sDevice)
{
   FWDEPRECATED();
   
  string state = "";

  sDevice=fwFSMConfDB_removeSystemName(sDevice);

  fwDU_getState(sDomain, sDevice, state);
  fwDU_setState(sDomain, sDevice, state);
  
  return;
}   


/** Loads to PVSS caches all existing recipes in the confDB
 * @param sDomain name of the SMI++ control domain
 * @return error code: -1 in the event of error, 0 if OK
* @deprecated Function not used. CTC2: 23-07-2018
 */
int fwFSMConfDB_cacheAllRecipes(string sDomain)
{
   FWDEPRECATED();
   
  string systemName = "";
  string cacheName = "";
  dyn_string exceptionInfo;
  dyn_dyn_mixed recipeObject;
  dyn_string recipeTags;
  dyn_string deviceList; 

  //1.- Get the list of recipes available in database
  fwConfigurationDB_getRecipesInDB (recipeTags,
		                    exceptionInfo,
		                    "" );                                  
   
  //2.- Find the devices in this subtree:   
  deviceList = fwFSMConfDB_getDomainDevices(sDomain);

  //3.- Create one PVSS cache per recipe
  for(int i = 1; i <=dynlen(recipeTags); i++){ 		//Loop over recipes in DB

      cacheName = recipeTags[i];  //For the time being, we create one cache per recipe

      //Load value for a device from the recipe in store it in a recipeObject
      fwConfigurationDB_loadRecipeFromDB(recipeTags[i],   
                                         deviceList,  
                                         g_sFwDevice_HIERARCHY,   
                                         recipeObject,  
                                         exceptionInfo,
                                         systemName,
                                         "");                       
      //Store recipe object in cache
      fwConfigurationDB_saveRecipeToCache(recipeObject,  
                                          g_sFwDevice_HIERARCHY,  
                                          cacheName,  
                                          exceptionInfo);  
                         
      if(dynlen(exceptionInfo) > 0) { 
        //fwExceptionHandling_display(exceptionInfo); 
        DebugN("ERROR::" + exceptionInfo); 
        return fwFSMConfDB_ERROR; 
      }
    }
  return fwFSMConfDB_OK;
}

/** Loads to PVSS caches all existing recipes in the confDB
 * @param sDomain name of the SMI++ control domain
 * @return error code: -1 in the event of error, 0 if OK
* @deprecated Function not used. CTC2: 23-07-2018
 */
bool fwFSMConfDB_checkCacheIntegrity(string cache, dyn_string &exception)
{
   FWDEPRECATED();
   
  dyn_dyn_mixed sysRecipeObject, cacheRecipeObject, diffRecipeObject;
  dyn_string deviceList;
    
  fwConfigurationDB_loadRecipeFromCache(cache, makeDynString(), "", cacheRecipeObject, exception);
  if(dynlen(exception))
    return false; 
       
  deviceList = cacheRecipeObject[fwConfigurationDB_RO_DP_NAME];
  dynUnique(deviceList);
  
  fwConfigurationDB_extractRecipe(deviceList, "",
                  sysRecipeObject, exception);

  if(dynlen(exception))
    return false;

  bool same = fwConfigurationDB_compareRecipes(sysRecipeObject, cacheRecipeObject, diffRecipeObject, "", exception);
  
  if(dynlen(exception))
    return exception;
  
  if(same)
    return false;
  
  return true;    
}



/**Get all CU's from all Tree's nodes
 * @return list of CUs
* @deprecated Function not used. CTC2: 23-07-2018
 */
dyn_string fwFSMConfDB_getAllCUs()
{
   FWDEPRECATED();
   
  dyn_string cuList;
  dyn_string exInfo;
  dyn_string nodes; 
  string domain;
 
  dynClear(cuList);

  //get all FSM nodes  
  fwTree_getAllNodes(nodes, exInfo);
  if(dynlen(exInfo) > 0){
    DebugN("ERROR: fwFSMConfDB_getAllCUs() -> " + exInfo);
    return makeDynString("");
  }
        
  //loop over the nodes in a tree and identify the CUs:
  for(int j = 1; j <= dynlen(nodes); j++){
    fwTree_getCUName(nodes[j], domain, exInfo);
    if(fwFsm_isCU(domain, nodes[j]) == 1)
    dynAppend(cuList, nodes[j]);
  }
  return cuList;
}


/** This function waits for the configurator to apply the recipes. 
 * Is intended to be called from the DU to pause their transitions while the configurator 
 * applies all recipes to the devices. Should only be used when the option:Configurator apply recipe is active.
 * @param sDomain Name of the SMI++ object
 * @param sConfigurator Name of the FSMConfDB Configurator
 * @return 0 if the recipe was applied otherwise -1* @deprecated Function not used. CTC2: 23-07-2018
 */

int fwFSMConfDB_waitForConfiguratorApplyRecipe(string sDomain, string sConfigurator) 
{  
   FWDEPRECATED();
   
  dyn_string dpNamesWait, dpNamesReturn; 
  dyn_anytype conditions, returnValues;  
  int status;
  string  configuratorState;
  
  if(fwFSMConfDB_getApplyRecipeUsingConfigurator(sDomain)!=1)
    return fwFSMConfDB_OK;
  
  dpNamesWait = makeDynString(sDomain +"|"+sConfigurator + ".fsm.executingAction:_online.._value"); 

  // indication of the condition that must be fulfilled 
  conditions[1] = ""; 
 
  // assignment of the datapoints for dpNamesReturn
  dpNamesReturn = makeDynString(sDomain +"|"+sConfigurator + ".fsm.executingAction:_online.._value"); 
 
  status = dpWaitForValue(dpNamesWait, conditions, dpNamesReturn, returnValues,0); 

  dpGet(sDomain +"|"+sConfigurator + ".fsm.currentState", configuratorState);
  
  if(configuratorState=="READY")
    return fwFSMConfDB_OK;
    else
        return fwFSMConfDB_ERROR;
} 
