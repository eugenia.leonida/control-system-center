#include "ReduMan.hxx"
#include <DpIdentifier.hxx>
#include <WaitForAnswer.hxx>
#include <HotLinkWaitForAnswer.hxx>
#include <DpMsgAnswer.hxx>
#include <IntegerVar.hxx>

class SplitModeListener : public HotLinkWaitForAnswer 
{
public:
	SplitModeListener(char* name, ReduHandler *handler) ;

	bool isSplitMode() {
			return (splitMode == 1);
	}

	bool isSplitActive() {
			return (splitActive == 1);
	}


	bool isInitialized() {
			return ((splitMode != -1) && (splitActive != -1));
	}

  	void callBack(DpMsgAnswer &answer);
  	void hotLinkCallBack(DpMsgAnswer &answer);
	void hotLinkCallBack(DpHLGroup &group);

protected:
	


private:
	char* dpName;
	void setSplitMode (int theSplitMode);
	void setSplitActive (int theSplitActive);
	void changed(DpVCItem* itemPtr);
	int getDpId(char* element, DpIdentifier& dpId);
	int connect();

	ReduHandler* itsHandler;
	int splitMode  ;
	int splitActive ;
	DpIdentifier splitModeId;
	DpIdentifier splitActiveId;
};

SplitModeListener::SplitModeListener(char* name, ReduHandler *handler)   
{
		dpName = name;
		itsHandler = handler;
		splitMode = -1;
		splitActive = -1;
		getDpId((char *)".SplitMode", splitModeId);
		getDpId((char *)".SplitActive", splitActiveId);
		connect();
}

int SplitModeListener::getDpId(char* element, DpIdentifier& dpId) {
	char tmp[256];
	strcpy(tmp,dpName);
	strcat(tmp,element);
	strcat(tmp,":_online.._value");

  if (Manager::getId(tmp, dpId) == PVSS_FALSE)
    {

      ErrHdl::error(ErrClass::PRIO_SEVERE,     
		    ErrClass::ERR_PARAM,       
		    ErrClass::UNEXPECTEDSTATE,  
		    "Redundant Manager",          
		    "run",                   
		    CharString("Datapoint ") + CharString(tmp) + CharString(" not found"));
	  return -1;
    }
	return 0;
}

int SplitModeListener::connect() {
	DpIdentList list = DpIdentList ();
	list.append(splitModeId);
	list.append(splitActiveId);
	
	if (Manager::dpConnect(list, this, false) == PVSS_FALSE) {

      ErrHdl::error(ErrClass::PRIO_SEVERE,      // It is a severe error
		    ErrClass::ERR_PARAM,        
		    ErrClass::UNEXPECTEDSTATE,  
		    "Redundant Manager",        
		    "run",                      
		    CharString("Impossible to connect to split mode dp"));
	  return -1;
	}

	return 0;

}
void SplitModeListener::setSplitActive(int theSplitActive) {
#ifdef DEBUG
	cout << "Set Split Active " << theSplitActive << endl;
#endif
	splitActive = theSplitActive;
	

}

void SplitModeListener::setSplitMode(int theSplitMode) {
#ifdef DEBUG
	cout << "Set Split Mode " << theSplitMode << endl;
#endif
	splitMode = theSplitMode;	

}

void SplitModeListener::changed(DpVCItem* itemPtr) {
	 DpIdentifier dpId = itemPtr->getDpIdentifier();

	 BitVar *bitVar = (BitVar*)itemPtr->getValuePtr();
	 PVSSboolean value = bitVar->getValue();

	if(dpId == splitModeId)
	{
		if (value == PVSS_TRUE) setSplitMode(1); else setSplitMode(0);
	}

	if(dpId == splitActiveId)
	{
		if (value == PVSS_TRUE) setSplitActive(1); else setSplitActive(0);
	}
}

void SplitModeListener::hotLinkCallBack(DpHLGroup &group)
{
  for (DpVCItem * itemPtr = group.getFirstItem();itemPtr; itemPtr = group.getNextItem())
   {
	   changed(itemPtr);

  }
  itsHandler->computeReduStatus();
}


void SplitModeListener::hotLinkCallBack(DpMsgAnswer &answer)
{
}

void SplitModeListener::callBack(DpMsgAnswer &answer) {
	for (AnswerGroup *grpPtr = answer.getFirstGroup(); grpPtr; grpPtr = answer.getNextGroup())
	{
	  if (grpPtr->getError())
	    {
	      cout << "Error in callback!" << endl;
	      ErrHdl::error(*(grpPtr->getError()));
	    }
	  else
	    {
		 if (answer.isA() == DP_MSG_ANSWER)
		{
		  for (DpVCItem * itemPtr = grpPtr->getFirstItem();itemPtr; itemPtr = grpPtr->getNextItem())
		    {
				changed(itemPtr);
		  }
		 }
	  }
	}
	 itsHandler->computeReduStatus();
}

ReduManager::ReduManager (const ManagerIdentifier &manId) : Manager(manId) {
	init();

}

ReduManager::ReduManager (const ManagerIdentifier &manId, CommonNameService *customCns) : Manager(manId, customCns) {
	init();
}

ReduManager::~ReduManager() {
		delete itsReduHandler;
}


void ReduManager::init() {
	itsReduHandler = new ReduHandler(this);
}

ReduHandler::ReduHandler(ReduCallback* myManager) {
	reduActive = -1;
	status = REDUMANAGER_UNDEFINED;
	itsMan = myManager;
	itsSplitModeListener = NULL;

}

ReduHandler::~ReduHandler() {
		if (itsSplitModeListener) {
				delete itsSplitModeListener;
		}
}

void ReduHandler::initializeRedu() {
	int redunum;
	if (! Resources::isRedundant()) {	
		itsSplitModeListener = NULL;	
	} else {
		redunum = Resources::getReplica();
		char* reduManagerName;
		if (redunum == 2) {
			reduManagerName = (char *)"_ReduManager_2";
		} else {
			reduManagerName = (char *)"_ReduManager";
		}
		itsSplitModeListener = new SplitModeListener(reduManagerName, this);
	}
	
}


bool ReduHandler::isActive() { 
	if (reduActive == -1) computeReduStatus();
	return (status == REDUMANAGER_REDUACTIVE) || (status == REDUMANAGER_SPLITACTIVE);
}

bool ReduHandler::isActiveOrInSplitMode() {
	if (reduActive == -1) computeReduStatus();
	return (status == REDUMANAGER_REDUACTIVE) || (status == REDUMANAGER_SPLITACTIVE) ||  (status == REDUMANAGER_SPLITPASSIVE);
}


void ReduHandler::computeReduStatus() {
	
	REDUMANAGER_STATUS oldStatus = status;
	if (reduActive == -1) {
		if (Resources::isRedundant()) {								
			if (Resources::isRedActive()) reduActive = 1; else reduActive = 0;
		} else {
			reduActive = 1;
		}
	}
	bool split = false;
	if (itsSplitModeListener) {
		if (itsSplitModeListener->isInitialized()) {
			if (itsSplitModeListener->isSplitMode()) {
				status = (itsSplitModeListener->isSplitActive())?(REDUMANAGER_SPLITACTIVE):(REDUMANAGER_SPLITPASSIVE);	
				split = true;
			}
		} 
	} else {
		if (Resources::isRedundant()) {
		cout << "Handling of the split mode is not initialized properly. Please call method initializeRedu() in your manager/driver" << endl;
		 ErrHdl::error(ErrClass::PRIO_WARNING ,      // It is a severe error
		    ErrClass::ERR_PARAM,        
		    ErrClass::UNEXPECTEDSTATE,  
		    "Redundant Manager",        
		    "run",                      
		    CharString("Handling of the split mode is not initialized properly. Please call method initializeRedu() in your manager/driver"));
		}
	}

	//cout << "Compute Redu Status : reduActive = " << reduActive << " split " << split << endl << flush;
	// if we are not in split mode check the redu status (passive or active)
	if (! split) {
		if (reduActive == 1) {
			status = REDUMANAGER_REDUACTIVE;
		} else {
			status = REDUMANAGER_REDUPASSIVE;
		}
	}
//	cout << "Compute Redu Status : Old status " << oldStatus << " (" << reduStatusName(oldStatus) << ") new status " << status << " (" << reduStatusName(status) << ")" << endl << flush;
	if ((oldStatus != REDUMANAGER_UNDEFINED) && (oldStatus != status)) {
		cout << "Redu Status changed: " << reduStatusName(oldStatus) << " -> " << reduStatusName(status) << endl << flush;
		itsMan->reduStatusChanged(oldStatus, status);
	}
}
/**
* This method receives system messages, including the redundancy ones.
* Here is the interest, if this kind of message is received, then the active/passive state i
* immediatly processed
*/
void ReduHandler::doReceive( SysMsg &sysMsg )
	{

#ifdef DEBUG
	cout <<  "Received a System message:" <<  SysMsg::getSysMsgTypeName(sysMsg.getSysMsgType()) << endl;
#endif


	switch ( sysMsg.getSysMsgType() )
		{
		case REDUNDANCY_SYS_MSG :
			{
			// messages come from Data and Event. Data is ignored
			// only Event can set the Status
			if ( sysMsg.getSource() != Manager::eventId )
				break;
			// A Redu-SysMsg has sub-types.
			// see enum RedundancySubType
			switch ( ( RedundancySubType ) sysMsg.getSysMsgSubType() )
				{
				case REDUNDANCY_ACTIVE :   // Here we become active
					{
#ifdef DEBUG
					cout << "Received a redundancy system message: REDUNDANCY_ACTIVE." << endl;
#endif
					reduActive = 1;			
					computeReduStatus();
					break;
					}
				case REDUNDANCY_PASSIVE :  // Here we are passive
					{
#ifdef DEBUG
					cout <<  "Received a redundancy system message: REDUNDANCY_PASSIVE." << endl;
#endif
					reduActive = 0;
					computeReduStatus();
					break;
					}
				/*case REDUNDANCY_REFRESH :  
					{
					PVSSINFO("Received a redundancy system message: REDUNDANCY_REFRESH.");
					break;
					}
				case REDUNDANCY_DISCONNECT :  
					{
					PVSSINFO("Received a redundancy system message: REDUNDANCY_DISCONNECT.");
					break;
					}
				case DM_START_TOUCHING :  
					{
					PVSSINFO("Received a redundancy system message: DM_START_TOUCHING.");
					break;
					}
				case DM_STOP_TOUCHING :  
					{
					PVSSINFO("Received a redundancy system message: DM_STOP_TOUCHING.");
					break;
					}
					*/
				default : ;
				}
			break;
			}

		default : ;
		}
	
	}

void ReduManager::doReceive(SysMsg& sysMsg ) {
		//Call the original method so that messages are forwarded.
	Manager::doReceive( sysMsg );

	// delegate the handling of the message to the hander
	itsReduHandler->doReceive(sysMsg);

}
