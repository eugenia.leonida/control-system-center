#include "ReduMan.hxx"
#include <DrvManager.hxx>

class ReduDrvManager : public DrvManager , public ReduCallback {
public:
	
	ReduDrvManager ();
 		
	virtual ~ReduDrvManager();

	/*
		Return true if in status REDU ACTIVE or SPLIT ACTIVE
	*/
	bool isActive() {
		return itsReduHandler->isActive();
	}

	/*
		Return true if in status REDU_ACTIVE or in SPLIT mode
	*/
	bool isActiveOrInSplitMode() {
		return itsReduHandler->isActiveOrInSplitMode();
	}
	


	/*
		Get the current status
	*/
	REDUMANAGER_STATUS getReduStatus() {
		return itsReduHandler->getReduStatus();
	}
	
	
	/*
		Initialize the handling of split mode
		This method requires the Resources to be properly initialized so it should be called in the 
		run() method of the manager
	*/
	void initializeRedu() {
		itsReduHandler->initializeRedu();
	}


	
	/*
		Return the human readable name for a given status
	*/
	static char* reduStatusName(REDUMANAGER_STATUS status) {
		return ReduHandler::reduStatusName(status);
	
			
	}
	void doReceive( SysMsg &sysMsg ) ;
			
private:
	ReduHandler* itsReduHandler;
};