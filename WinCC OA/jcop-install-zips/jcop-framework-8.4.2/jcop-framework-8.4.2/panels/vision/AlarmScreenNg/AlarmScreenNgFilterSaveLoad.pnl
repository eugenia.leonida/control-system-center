<?xml version="1.0" encoding="UTF-8"?>
<panel version="14">
 <properties>
  <prop name="Name">
   <prop name="en_US.utf8"></prop>
  </prop>
  <prop name="Size">600 402</prop>
  <prop name="BackColor">_3DFace</prop>
  <prop name="RefPoint">0 10</prop>
  <prop name="InitAndTermRef">True</prop>
  <prop name="SendClick">False</prop>
  <prop name="RefFileName"></prop>
  <prop name="DPI">95.91608391608392</prop>
  <prop name="layoutType">VBox</prop>
  <prop name="layoutLeftMargin">3</prop>
  <prop name="layoutRightMargin">3</prop>
  <prop name="layoutTopMargin">3</prop>
  <prop name="layoutBottomMargin">3</prop>
 </properties>
 <events>
  <script name="ScopeLib" isEscaped="1"><![CDATA[#uses &quot;AlarmScreenNg/AlarmScreenNgFilters.ctl&quot;  // DP operations with filters
#uses &quot;AlarmScreenNg/classes/AsNgAccessControl.ctl&quot;  // Access control
#uses &quot;AlarmScreenNg/AlarmScreenNgActions.ctl&quot;  // Action names for access control

#uses &quot;fwGeneral/fwExceptionHandling.ctl&quot;

bool bSaveMode = false;      ///&lt; true if panel is opened to save filter, false - to load filter
AsNgAccessControl m_access;  ///&lt; the instance holding access control information
string sFilterJson;          ///&lt; string with filter definition in JSON format
string sBasicConfig;         ///&lt; The name of basic configuration DP where filter is used

/// Analyze $-parameter to determing the mode for this panel (Load or Save)
void determinePanelMode()
{
  if(isDollarDefined(&quot;$bSaveMode&quot;))
  {
    bSaveMode = $bSaveMode;
  }
  else
  {
    throwError(makeError(&quot;&quot;, PRIO_SEVERE, ERR_IMPL, 76,  // 00076,Invalid argument in function
                         myModuleName() + &quot;.&quot; + myPanelName() + &quot;.&quot; + __FUNCTION__ +
                         &quot;(): $-parameter is not defined $bSaveMode&quot;));
  }

  if(bSaveMode)
  {
    if(isDollarDefined(&quot;$sFilter&quot;))
    {
      sFilterJson = $sFilter;
      if(isDollarDefined(&quot;$sBasicConfig&quot;))
      {
        sBasicConfig = $sBasicConfig;
      }
    }
    else
    {
      bSaveMode = false;  // nothing to save
      throwError(makeError(&quot;&quot;, PRIO_SEVERE, ERR_IMPL, 76,  // 00076,Invalid argument in function
                           myModuleName() + &quot;.&quot; + myPanelName() + &quot;.&quot; + __FUNCTION__ +
                           &quot;(): $-parameter for save mode is not defined $sFilter&quot;));
    }
  }

  string sTitle = bSaveMode ? &quot;Save Filter&quot; : &quot;Load Filter&quot;;
  labelTitle.text = sTitle;
  setWindowTitle(myModuleName(), myPanelName(), sTitle);
  if(!bSaveMode)
  {
    LayerOff(2);  // Layer 2 contains elements related to 'Save' functionality
  }
}

/// Load existing filters, display them in table. The list of filters to load/display can be
/// filtered by filter name and 'My Filters' checkbox
void loadDisplayFilters()
{
  tableFilters.deleteAllLines();
  dyn_dyn_anytype ddaFilterData = AlarmScreenNgFilters_loadNames(&quot;&quot;, checkBoxOptions.state(0));
  if(dynlen(ddaFilterData) &gt; 0)
  {
    tableFilters.appendLines(dynlen(ddaFilterData[NGAS_FILTER_INFO_DP]),
                             &quot;dp&quot;, ddaFilterData[NGAS_FILTER_INFO_DP],
                             &quot;name&quot;, ddaFilterData[NGAS_FILTER_INFO_NAME],
                             &quot;date&quot;, ddaFilterData[NGAS_FILTER_INFO_DATE],
                             &quot;user&quot;, ddaFilterData[NGAS_FILTER_INFO_USER]);
  }
  adjustButtons();
}

/// Process selection event in table of existing filters
void processTableSelection()
{
  if(bSaveMode)
  {
    dyn_int diSelectedRows = tableFilters.getSelectedLines;
    if(dynlen(diSelectedRows) &gt; 0)
    {
      string sFilterName;
      getValue(&quot;tableFilters&quot;, &quot;cellValueRC&quot;, diSelectedRows[1], &quot;name&quot;, sFilterName);
      textNewFilterName.text = sFilterName;
    }
  }
  adjustButtons();
}

/// Adjust availability of buttons depending on current state of panel elements
void adjustButtons()
{
  dyn_int diSelectedRows = tableFilters.getSelectedLines;
  bool bHaveTableSelection = (dynlen(diSelectedRows) &gt; 0);
  if(bSaveMode)
  {
    buttonOK.enabled = strltrim(strrtrim(textNewFilterName.text)) != &quot;&quot;;
  }
  else
  {
    buttonOK.enabled = bHaveTableSelection;
  }

  bool bSelectedMyFilter = false;
  if(bHaveTableSelection)
  {
    string sFilterOwner;
    getValue(&quot;tableFilters&quot;, &quot;cellValueRC&quot;, diSelectedRows[1], &quot;user&quot;, sFilterOwner);
    bSelectedMyFilter = (getUserName() == sFilterOwner);
  }
  buttonDelete.enabled = bHaveTableSelection
                         &amp;&amp; (bSelectedMyFilter
                             || (m_access.getAccessLevel(AS_ACTION_DELETE_FILTER) == ALARM_SCREEN_ACCESS_ACTION_ENABLE));
}

/// Process clikc event for OK button, the action to be executed depends on mode
void buttonOkClicked()
{
  if(bSaveMode)
  {
    executeSaveAction();
  }
  else
  {
    executeLoadAction();
  }
}

/// Load filter definition from DP in selected table line, return to caller of this panel
void executeLoadAction()
{
  dyn_int diSelectedRows = tableFilters.getSelectedLines;
  if(dynlen(diSelectedRows) == 0)
  {
    DebugN(&quot;executeLoadAction() without selection&quot;);
    return;  // can be called by double-click on table outside the filer row(s)
  }
  string sFilterDp;
  getValue(&quot;tableFilters&quot;, &quot;cellValueRC&quot;, diSelectedRows[1], &quot;dp&quot;, sFilterDp);

  dyn_string exceptionInfo;
  sFilterJson = AlarmScreenNgFilters_loadFilter(sFilterDp, exceptionInfo);
  if(dynlen(exceptionInfo) &gt; 0)
  {
    fwExceptionHandling_display(exceptionInfo);
    return;
  }

  PanelOffReturn(makeDynFloat(1), makeDynString(sFilterJson));
}

// Save filter defintion to DP
void executeSaveAction()
{
  // Filter name must be non-empty
  string sFilterName = strltrim(strrtrim(textNewFilterName.text));
  if(sFilterName == &quot;&quot;)
  {
    ChildPanelOnCentralModal(&quot;vision/MessageWarning&quot;, &quot;WARNING: Filter name is mandatory&quot;,
                                   makeDynString(&quot;Filter name must be specified&quot;));
    setInputFocus(myModuleName(), myPanelName(), &quot;textNewFilterName&quot;);
    return;
  }

  // Looks like infinite loop, but the function is expected to be called at most twice:
  // first time with bForce = false, second time with bForce = truye
  if(repeatSaveAction(sFilterName, false))
  {
    repeatSaveAction(sFilterName, true);
  }
}

/**
 * Save filter definition with given filter name.
 * @param sFilterName The name of filter to save
 * @bForce &lt;c&gt;true&lt;/c&gt; if existing filter with this name shall be overwritten
 * @return &lt;c&gt;true&lt;/c&gt; if it is expected that one more call with bForce = true can succeed.
 * Note that in case of success this function closes this panel.
 */
bool repeatSaveAction(const string &amp;sFilterName, bool bForce)
{
  dyn_string exceptionInfo;
  AlarmScreenNgSaveResult result = AlarmScreenNgFilters_saveFilter(sFilterName, sFilterJson, sBasicConfig,
                                                                      exceptionInfo, bForce);
  switch(result)
  {
  case AlarmScreenNgSaveResult::Success:
    PanelOffReturn(makeDynFloat(1), makeDynString(sFilterName));
    break;
  case AlarmScreenNgSaveResult::Exists:
    return askUserConfirmation(&quot;WARNING: Filter name exists&quot;,
                               &quot;Filter '&quot; + sFilterName +
                               &quot;' already exists.\nDo You want to overwrite it ?&quot;);
  case AlarmScreenNgSaveResult::ExistsAlien:
    ChildPanelOnCentralModal(&quot;vision/MessageWarning&quot;, &quot;WARNING: Filter name is not unique&quot;,
                             makeDynString(&quot;Filter '&quot; + sFilterName + &quot;' was written by another user&quot;));
    setInputFocus(myModuleName(), myPanelName(), &quot;textNewFilterName&quot;);
    break;
  case AlarmScreenNgSaveResult::Failure:
    fwExceptionHandling_display(exceptionInfo);
    break;
  }
  return false;
}

// Execute requested deletion of selected filter
void executeFilterDelete()
{
  dyn_int diSelectedRows = tableFilters.getSelectedLines;
  if(dynlen(diSelectedRows) == 0)
  {
    DebugN(&quot;executeFilterDelete() without selection&quot;);
    return;  // can be called by double-click on table outside the filer row(s)
  }
  string sFilterDp, sFilterName;
  getMultiValue(&quot;tableFilters&quot;, &quot;cellValueRC&quot;, diSelectedRows[1], &quot;dp&quot;, sFilterDp,
                &quot;tableFilters&quot;, &quot;cellValueRC&quot;, diSelectedRows[1], &quot;name&quot;, sFilterName);
  if(sFilterDp == &quot;&quot;)
  {
    throwError(makeError(&quot;&quot;, PRIO_SEVERE, ERR_IMPL, 75,  // 00075,Argument missing in function
                         myModuleName() + &quot;.&quot; + myPanelName() + &quot;.&quot; + __FUNCTION__ +
                         &quot;(): empty DP name in table row &quot; + diSelectedRows[1]));
  }

  // Ask for user's confirmation, but text can be different
  string sQuestion;
  if(AlarmScreenNgFilters_filterAlreadyUsed(sFilterDp))
  {
    sQuestion = &quot;Filter can be already used in filter sets.\n&quot;;
  }
  sQuestion += &quot;Do Yo really want to remove filter'&quot; + sFilterName + &quot;' ?&quot;;
  if(!askUserConfirmation(&quot;CONFIRM: Filter removal&quot;, sQuestion))
  {
    return;
  }

  // Delete
  dyn_string exceptionInfo;
  AlarmScreenNgFilters_deleteFilter(sFilterDp, exceptionInfo);
  if(dynlen(exceptionInfo) &gt; 0)
  {
    fwExceptionHandling_display(exceptionInfo);
    loadDisplayFilters();
  }
  else
  {
    tableFilters.deleteLineN(diSelectedRows[1]);
    adjustButtons();
  }
}

/**
 * Show dialog asking for confirmation by user
 * @param sTitle Title for confirmation dialog
 * @patam sMessage message to be shown in dialog, the question assuming one of answers Yes/No is expected
 * @return &lt;c&gt;true&lt;/c&gt; if uesr has clicked Yes in dialo, presented to him
 */
bool askUserConfirmation(const string &amp;sTitle, const string &amp;sMessage)
{
  dyn_float dfReturn;
  dyn_string dsReturn;
  ChildPanelOnCentralModalReturn(&quot;vision/MessageInfo&quot;, sTitle,
                                 makeDynString(&quot;$1:&quot; + sMessage,
                                               &quot;$2:Yes&quot;, &quot;$3:No&quot;),
                                   dfReturn, dsReturn);
  if(dynlen(dfReturn) &gt; 0)
  {
    if(dfReturn[1] == 1)
    {
      return true;
    }
  }
  return false;
}

/// Process (potential) change of acces rights for current user
void processAccessRightsChange()
{
  adjustButtons();
}
]]></script>
  <script name="Initialize" isEscaped="1"><![CDATA[main()
{
  // Prepare table
  setMultiValue(
      &quot;tableFilters&quot;, &quot;selectByClick&quot;, TABLE_SELECT_LINE,
      &quot;tableFilters&quot;, &quot;tableMode&quot;, TABLE_SELECT_BROWSE,
      &quot;tableFilters&quot;, &quot;alternatingRowColors&quot;, makeDynString(&quot;white&quot;, &quot;{233,233,233}&quot;),
      &quot;tableFilters&quot;, &quot;blankNonExistingRows&quot;, true);
  tableFilters.sort(false, &quot;date&quot;);

  // Determine mode: save or load
  determinePanelMode();

  // Prepare label for selection on configuration
  /* not used for the moment
  if(isDollarDefined(&quot;$sBasicConfig&quot;)) {
    CheckBoxOptions.text(1) = CheckBoxOptions.text(1) + $sBasicConfig;
  }
  else {
    throwError(makeError(&quot;&quot;, PRIO_SEVERE, ERR_IMPL, 76,  // 00076,Invalid argument in function
                         myModuleName() + &quot;.&quot; + myPanelName() + &quot;.&quot; + __FUNCTION__ +
                         &quot;(): $-parameter is not defined $sBasicConfig&quot;));
    CheckBoxOptions.state(1) = false;
    CheckBoxOptions.itemEnabled(1, false);
  }
  */
  loadDisplayFilters();
  m_access.setNotifier(&quot;textNotifier&quot;);
  if(!m_access.connect()) {
      ChildPanelOnCentralModal(
          &quot;vision/MessageWarning&quot;,
          getCatStr(&quot;sc&quot;, &quot;attention&quot;),
          makeDynString(&quot;$1:&quot; + m_access.getError()));
  }
}
]]></script>
 </events>
 <shapes>
  <shape Name="checkBoxOptions" shapeType="CHECK_BOX" layerId="0">
   <properties>
    <prop name="serialId">1</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">12 42</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_3DText</prop>
    <prop name="BackColor">_3DFace</prop>
    <prop name="TabOrder">0</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">12 42</prop>
    <prop name="Size">576 21</prop>
    <prop name="Vertical">False</prop>
    <prop name="Listitems">
     <prop name="Item">
      <prop name="Text">
       <prop name="en_US.utf8">My Filters</prop>
      </prop>
      <prop name="Select">True</prop>
     </prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(int button, int state)
{
  loadDisplayFilters();
}]]></script>
   </events>
  </shape>
  <shape Name="tableFilters" shapeType="TABLE" layerId="0">
   <properties>
    <prop name="serialId">2</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">3 71</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">1</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">3 71</prop>
    <prop name="Size">594 270</prop>
    <prop name="ShowColumnHeader">True</prop>
    <prop name="ShowRowHeader">False</prop>
    <prop name="SortOnClick">True</prop>
    <prop name="RowHeight">15</prop>
    <prop name="RowHeaderWidth">0</prop>
    <prop name="GridType">Cross</prop>
    <prop name="VScrollBarMode">Auto</prop>
    <prop name="HScrollBarMode">Auto</prop>
    <prop name="Column">
     <prop name="Name">name</prop>
     <prop name="Width">280</prop>
     <prop name="Visible">True</prop>
     <prop name="Editable">False</prop>
     <prop name="Label">
      <prop name="en_US.utf8">Name</prop>
     </prop>
     <prop name="ToolTipText">
      <prop name="en_US.utf8">Filter name supplied by user</prop>
     </prop>
    </prop>
    <prop name="Column">
     <prop name="Name">date</prop>
     <prop name="Width">180</prop>
     <prop name="Visible">True</prop>
     <prop name="Editable">False</prop>
     <prop name="Label">
      <prop name="en_US.utf8">Date</prop>
     </prop>
     <prop name="ToolTipText">
      <prop name="en_US.utf8">Last update of this filter</prop>
     </prop>
    </prop>
    <prop name="Column">
     <prop name="Name">user</prop>
     <prop name="Width">120</prop>
     <prop name="Visible">True</prop>
     <prop name="Editable">False</prop>
     <prop name="Label">
      <prop name="en_US.utf8">User</prop>
     </prop>
     <prop name="ToolTipText">
      <prop name="en_US.utf8"></prop>
     </prop>
    </prop>
    <prop name="Column">
     <prop name="Name">dp</prop>
     <prop name="Width">120</prop>
     <prop name="Visible">False</prop>
     <prop name="Editable">False</prop>
     <prop name="Label">
      <prop name="en_US.utf8">DP</prop>
     </prop>
     <prop name="ToolTipText">
      <prop name="en_US.utf8"></prop>
     </prop>
    </prop>
   </properties>
   <events>
    <script name="SelectionChanged" isEscaped="1"><![CDATA[synchronized main()
{
  processTableSelection();
}]]></script>
    <script name="DoubleClicked" isEscaped="1"><![CDATA[main(int row, string column)
{
  buttonOkClicked();  // Treat double-click as OK button
}]]></script>
   </events>
  </shape>
  <shape Name="buttonOK" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">3</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">395 381.8666666666664</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">2</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">395 376</prop>
    <prop name="Size">80 23</prop>
    <prop name="BorderStyle">Styled</prop>
    <prop name="Text">
     <prop name="en_US.utf8">OK</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(mapping event)
{
  buttonOkClicked();
}]]></script>
   </events>
  </shape>
  <shape Name="buttonCancel" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">4</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">496 381.8666666666664</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">3</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">496 376</prop>
    <prop name="Size">80 23</prop>
    <prop name="BorderStyle">Styled</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Cancel</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(mapping event)
{
  PanelOffReturn(makeDynFloat(), makeDynString());
}]]></script>
   </events>
  </shape>
  <shape Name="SPACER1" shapeType="SPACER" layerId="0">
   <properties>
    <prop name="serialId">5</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">60 376</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">{0,0,0}</prop>
    <prop name="BackColor">{255,255,255}</prop>
    <prop name="TabOrder">4</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="TransparentForMouse">True</prop>
    <prop name="sizePolicy">
     <sizePolicy vertical="Ignored" horizontal="Expanding"/>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Location">60 376</prop>
    <prop name="Size">142 23</prop>
    <prop name="Orientation">horizontal</prop>
   </properties>
  </shape>
  <shape Name="backgroundRectangle" shapeType="RECTANGLE" layerId="0">
   <properties>
    <prop name="serialId">9</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">10 10</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">{0,0,0}</prop>
    <prop name="BackColor">FwCorporateColor</prop>
    <prop name="TabOrder">8</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapProjecting,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[solid]</prop>
    <prop name="Geometry">0.898484848484851 0 0 1.000000000000036 -5.984848484848509 -7.000000000000359</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Location">10 10</prop>
    <prop name="Size">661 31</prop>
    <prop name="CornerRadius">0</prop>
    <prop name="Transformable">True</prop>
   </properties>
  </shape>
  <shape Name="labelTitle" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">10</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">10 10</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">white</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">9</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="sizePolicy">
     <sizePolicy vertical="Minimum" horizontal="Preferred"/>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapProjecting,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Geometry">3.788037634455165 0 0 1.010492039141 -18.84158614660975 -3.388205448217156</prop>
    <prop name="Location">10 10</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,19,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">Save Filter</prop>
    </prop>
    <prop name="Distance">2</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">False</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
  </shape>
  <shape Name="buttonDelete" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">16</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">223 381.8666666666664</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">13</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Delete selected filter</prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">223 376</prop>
    <prop name="Size">80 23</prop>
    <prop name="BorderStyle">Styled</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Delete</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(mapping event)
{
  executeFilterDelete();
}]]></script>
   </events>
  </shape>
  <shape Name="SPACER2" shapeType="SPACER" layerId="0">
   <properties>
    <prop name="serialId">17</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">324 376</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">{0,0,0}</prop>
    <prop name="BackColor">{255,255,255}</prop>
    <prop name="TabOrder">14</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="TransparentForMouse">True</prop>
    <prop name="maximumSize">50 -1</prop>
    <prop name="sizePolicy">
     <sizePolicy vertical="Ignored" horizontal="Expanding"/>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Location">324 376</prop>
    <prop name="Size">50 23</prop>
    <prop name="Orientation">horizontal</prop>
   </properties>
  </shape>
  <shape Name="textNotifier" shapeType="TEXT_FIELD" layerId="0">
   <properties>
    <prop name="serialId">19</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">24 374.5999999999999</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">False</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">red</prop>
    <prop name="TabOrder">15</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="maximumSize">15 15</prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">24 376</prop>
    <prop name="Size">15 15</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Editable">True</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
   <events>
    <script name="TextChanged" isEscaped="1"><![CDATA[main(string newText)
{
  processAccessRightsChange();
}]]></script>
   </events>
  </shape>
  <shape Name="labelName" shapeType="PRIMITIVE_TEXT" layerId="1">
   <properties>
    <prop name="serialId">7</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">130 310</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">6</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapProjecting,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Geometry">1 0 0 1.285714285714276 -125 -40.57142857142574</prop>
    <prop name="Location">130 310</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">New filter name:</prop>
    </prop>
    <prop name="Distance">2</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">False</prop>
    <prop name="TextFormat">[0s,,,AlignLeft|AlignVCenter]</prop>
   </properties>
  </shape>
  <shape Name="textNewFilterName" shapeType="TEXT_FIELD" layerId="1">
   <properties>
    <prop name="serialId">8</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">116 351.1904761904761</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">7</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Name (description) for filter to save</prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">116 347</prop>
    <prop name="Size">481 23</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Editable">True</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
   <events>
    <script name="TextChanged" isEscaped="1"><![CDATA[main(string newText)
{
  adjustButtons();
}]]></script>
   </events>
  </shape>
 </shapes>
 <groups>
  <layout parentSerial="2" Name="LAYOUT_GROUP1" serial="0">
   <properties>
    <prop idx="0" name="shapeSerial">19</prop>
    <prop idx="1" name="shapeSerial">5</prop>
    <prop idx="2" name="shapeSerial">16</prop>
    <prop idx="3" name="shapeSerial">17</prop>
    <prop idx="4" name="shapeSerial">3</prop>
    <prop idx="5" name="shapeSerial">4</prop>
    <prop name="isContainerShape">False</prop>
    <prop name="layoutType">HBox</prop>
    <prop name="layoutLeftMargin">21</prop>
    <prop name="layoutRightMargin">21</prop>
    <prop name="layoutTopMargin">0</prop>
    <prop name="layoutBottomMargin">0</prop>
    <prop name="layoutSpacing">21</prop>
   </properties>
  </layout>
  <layout parentSerial="2" Name="LAYOUT_GROUP2" serial="1">
   <properties>
    <prop idx="0" name="shapeSerial">7</prop>
    <prop idx="1" name="shapeSerial">8</prop>
    <prop name="isContainerShape">False</prop>
    <prop name="layoutType">HBox</prop>
   </properties>
  </layout>
  <group parentSerial="2" Name="SHAPE_GROUP1" serial="3">
   <properties>
    <prop name="shapeSerial">10</prop>
    <prop name="shapeSerial">9</prop>
    <prop name="sizePolicy">
     <sizePolicy vertical="Fixed" horizontal="Preferred"/>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
   </properties>
  </group>
  <layout parentSerial="2" Name="LAYOUT_GROUP4" serial="4">
   <properties>
    <prop idx="0" name="shapeSerial">1</prop>
    <prop name="isContainerShape">False</prop>
    <prop name="layoutType">HBox</prop>
    <prop name="layoutLeftMargin">9</prop>
    <prop name="layoutRightMargin">9</prop>
    <prop name="layoutTopMargin">2</prop>
    <prop name="layoutBottomMargin">2</prop>
    <prop name="layoutSpacing">0</prop>
   </properties>
  </layout>
  <layout parentSerial="-1" Name="LAYOUT_GROUP3" serial="2">
   <properties>
    <prop idx="2" name="shapeSerial">2</prop>
    <prop idx="4" name="groupSerial">0</prop>
    <prop idx="3" name="groupSerial">1</prop>
    <prop idx="0" name="groupSerial">3</prop>
    <prop idx="1" name="groupSerial">4</prop>
    <prop name="isContainerShape">False</prop>
    <prop name="layoutType">VBox</prop>
   </properties>
  </layout>
 </groups>
</panel>
