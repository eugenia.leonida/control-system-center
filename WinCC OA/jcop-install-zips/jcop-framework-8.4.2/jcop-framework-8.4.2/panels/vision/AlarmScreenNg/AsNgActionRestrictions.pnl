<?xml version="1.0" encoding="UTF-8"?>
<panel version="14">
 <properties>
  <prop name="Name">
   <prop name="en_US.utf8"></prop>
  </prop>
  <prop name="Size">620 330</prop>
  <prop name="BackColor">_3DFace</prop>
  <prop name="RefPoint">-2.000000000000007 8</prop>
  <prop name="InitAndTermRef">True</prop>
  <prop name="SendClick">False</prop>
  <prop name="RefFileName"></prop>
  <prop name="DPI">95.91608391608392</prop>
  <prop name="layoutType">VBox</prop>
  <prop name="layoutLeftMargin">6</prop>
  <prop name="layoutRightMargin">6</prop>
  <prop name="layoutTopMargin">6</prop>
  <prop name="layoutBottomMargin">6</prop>
 </properties>
 <events>
  <script name="ScopeLib" isEscaped="1"><![CDATA[#uses &quot;fwAccessControl/fwAccessControl&quot;
#uses &quot;fwGeneral/fwExceptionHandling&quot;

/** During panel initialization, names of domains and privileges are hached in the following variables */
//@{
dyn_string m_allDomains;  /// List of all domains with non-empty list of privileges in domain
dyn_dyn_string m_domainPrivileges;  /// List of privilege names for every domain
//@)

/// Initialize UI elements when panel starts
void initUI()
{
  if(isDollarDefined(&quot;$sActionName&quot;))
  {
    labelTitle.text = labelTitle.text + &quot;: &quot; + $sActionName;
  }

  dyn_string exceptionInfo;
  if(!initDomainPrivilegeCache(exceptionInfo))
  {
    fwExceptionHandling_display(exceptionInfo);
    setMultiValue(&quot;buttonAdd&quot;, &quot;enabled&quot;, false,
                  &quot;buttonDelete&quot;, &quot;enabled&quot;, false,
                  &quot;buttonOk&quot;, &quot;enabled&quot;, false);
    return;
  }

  // Prepare table
  setMultiValue(
      &quot;tablePrivileges&quot;, &quot;selectByClick&quot;, TABLE_SELECT_LINE,
      &quot;tablePrivileges&quot;, &quot;tableMode&quot;, TABLE_SELECT_BROWSE,
      &quot;tablePrivileges&quot;, &quot;blankNonExistingRows&quot;, true);

  dyn_mapping dmRestrictions;
  if(isDollarDefined(&quot;$sRestrictions&quot;))
  {
    dmRestrictions = jsonDecode($sRestrictions);
  }
  int iTotal = dynlen(dmRestrictions);
  for(int n = 1 ; n &lt;= iTotal ; n++)
  {
    displayRestriction(dmRestrictions[n]);
  }
  setButtons();
}

/// Display single restriction: either in table row, or in combo box beneath the table
void displayRestriction(mapping &amp;mRestriction)
{
  if(mappingHasKey(mRestriction, ALARM_SCREEN_RESTRICTION_DOMAIN)
    &amp;&amp; mappingHasKey(mRestriction, ALARM_SCREEN_RESTRICTION_PRIVILEGE))
  {
    displayRestrictionInTableRow(mRestriction[ALARM_SCREEN_RESTRICTION_DOMAIN],
                                 mRestriction[ALARM_SCREEN_RESTRICTION_PRIVILEGE]);
  }
  else if(mappingHasKey(mRestriction, ALARM_SCREEN_RESTRICTION_OTHER))
  {
    displayRestrictionInCombo(mRestriction[ALARM_SCREEN_RESTRICTION_OTHER]);
  }
  else
  {
    DebugN(__FUNCTION__ + &quot;(): unrecongnized restriction&quot;, mRestriction);
  }
}

/// Add row with given domain and privilege to table, but first check if such
/// domain and privilege exist in the system
void displayRestrictionInTableRow(const string &amp;sDomain, const string &amp;sPrivilege)
{
  int iDomainIdx = dynContains(m_allDomains, sDomain);
  if(iDomainIdx &lt; 1)
  {
    DebugN(__FUNCTION__ + &quot;(): domain '&quot; + sDomain + &quot;' is either uunknown, or does not have privileges&quot;);
    return;
  }

  // Add row to table and fill it
  tablePrivileges.appendLine(&quot;domain&quot;, sDomain, &quot;privilege&quot;, sPrivilege);
  int iRow = tablePrivileges.lineCount() - 1;
  setCellCombo(iRow, &quot;domain&quot;, m_allDomains);
  setCellCombo(iRow, &quot;privilege&quot;, m_domainPrivileges[iDomainIdx]);
}

/// Display restriction for 'all other' users in combo box beneath the table
void displayRestrictionInCombo(int iAccessLevel)
{
  if((0 &lt; iAccessLevel) &amp;&amp; (iAccessLevel &lt;= comboOtherUsers.itemCount()))
  {
    comboOtherUsers.selectedPos = iAccessLevel;
  }
  else
  {
    comboOtherUsers.selectedPos = 1;
  }
}

/// Collect names of all domains with privileges and names of all privileges in every domain
bool initDomainPrivilegeCache(dyn_string &amp;exceptionInfo)
{
  dyn_string dsDomainNames, dsFullDomainNames;
  fwAccessControl_getAllDomains(dsDomainNames, dsFullDomainNames, exceptionInfo);
  if(dynlen(exceptionInfo) &gt; 0)
  {
    return false;
  }
  int iTotal = dynlen(dsDomainNames);
  if(iTotal == 0)
  {
    fwException_raise(exceptionInfo, &quot;ERROR&quot;, __FUNCTION__ + &quot;(): no domains found in system&quot;, &quot;&quot;);
    return false;
  }

  // Collect privileges in every domain
  for(int idx = 1 ; idx &lt;= iTotal ; idx++)
  {
    string sDomainName = dsDomainNames[idx];
    dyn_string dsPrivilegeNames;
    dyn_int diPrivilegeIds;
    fwAccessControl_getPrivilegeNames(sDomainName, dsPrivilegeNames, diPrivilegeIds, exceptionInfo);
    if(dynlen(exceptionInfo) &gt; 0)
    {
      return false;
    }
    if(dynlen(dsPrivilegeNames) &gt; 0)
    {
      dynAppend(m_allDomains, sDomainName);
      dynAppend(m_domainPrivileges, dsPrivilegeNames);
    }
  }

  if(dynlen(m_allDomains) == 0)
  {
    fwException_raise(exceptionInfo, &quot;ERROR&quot;, __FUNCTION__ + &quot;(): no domains with privileges&quot;, &quot;&quot;);
    return false;
  }
  return true;
}


/// Add new row to table, set default values
void addTableRow()
{
  // We'll display row with first domain and first privilege in domain
  tablePrivileges.appendLine(&quot;domain&quot;, m_allDomains[1], &quot;privilege&quot;, m_domainPrivileges[1][1]);
  setCellCombo(tablePrivileges.lineCount() - 1, &quot;domain&quot;, m_allDomains);
  setCellCombo(tablePrivileges.lineCount() - 1, &quot;privilege&quot;, m_domainPrivileges[1]);

  setButtons();
}

/// Delete currently selected row from table
void deleteTableRow()
{
  dyn_int diSelectedLines = tablePrivileges.getSelectedLines();
  if(dynlen(diSelectedLines) &gt; 0)
  {
    tablePrivileges.deleteLineN(diSelectedLines[1]);
  }
  setButtons();
}

/// Process the change of domain name in given row, this can lead to change of list of available privileges
void processDomainSelection(int iRow, const string &amp;sDomainName)
{
  /// Fix current privilege in this row;
  string sPrivilege;
  getValue(&quot;tablePrivileges&quot;, &quot;cellValueRC&quot;, iRow, &quot;privilege&quot;, sPrivilege);

  // Find the index of new domain, check if this privilege is also available in new domain
  int iDomainIdx = dynContains(m_allDomains, sDomainName);
  int iPrivilegeIdx = dynContains(m_domainPrivileges[iDomainIdx], sPrivilege);
  if(iPrivilegeIdx &lt; 1)
  {
    setValue(&quot;tablePrivileges&quot;, &quot;cellValueRC&quot;, iRow, &quot;privilege&quot;, m_domainPrivileges[iDomainIdx][1]);
  }
  setCellCombo(iRow, &quot;privilege&quot;, m_domainPrivileges[iDomainIdx]);
}

/**
 * Set combo box for editing cell in table.
 * @param iRow Row number in table
 * @param sColumn The name of column in table where combo box shall be set
 * @param dsNames The names to be shown in combo box
 */
void setCellCombo(int iRow, const string &amp;sColumn, const dyn_string &amp;dsNames)
{
  mapping mComboDef;
  mComboDef[&quot;editable&quot;] = false;
  mComboDef[&quot;items&quot;] = dsNames;
  mComboDef[&quot;permanent&quot;] = false;
  tablePrivileges.cellWidgetRC(iRow, sColumn, &quot;ComboBox&quot;, mComboDef);
}

/// Set availability of buttons in the panel
void setButtons()
{
  dyn_int diSelectedLines = tablePrivileges.getSelectedLines();
  setMultiValue(&quot;buttonDelete&quot;, &quot;enabled&quot;, dynlen(diSelectedLines) &gt; 0,
                &quot;comboOtherUsers&quot;, &quot;enabled&quot;, tablePrivileges.lineCount() &gt; 0);
}
]]></script>
  <script name="Initialize" isEscaped="1"><![CDATA[main()
{
  initUI();
}]]></script>
 </events>
 <shapes>
  <shape Name="backgroundRectangle" shapeType="RECTANGLE" layerId="0">
   <properties>
    <prop name="serialId">1</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">10 10</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">{0,0,0}</prop>
    <prop name="BackColor">FwCorporateColor</prop>
    <prop name="TabOrder">0</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapProjecting,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[solid]</prop>
    <prop name="Geometry">0.9183814839958576 0 0 0.9999999998330917 -2.315594277237016 -2.999999993322384</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Location">10 10</prop>
    <prop name="Size">661 31</prop>
    <prop name="CornerRadius">0</prop>
    <prop name="Transformable">True</prop>
   </properties>
  </shape>
  <shape Name="labelTitle" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">2</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">10 10</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">white</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">1</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="sizePolicy">
     <sizePolicy vertical="Minimum" horizontal="Preferred"/>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapProjecting,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Geometry">1.131779437278446 0 0 0.999999999833056 -3.31779437278446 -1.99999999348896</prop>
    <prop name="Location">10 10</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,19,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">Alarm Screen: restrictions for action </prop>
    </prop>
    <prop name="Distance">2</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">False</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
  </shape>
  <shape Name="labelTableHead" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">3</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">238 41.00000000000107</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">2</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapProjecting,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Geometry">1.435714285714281 0 0 1 -32.19999999999986 4.999999999998927</prop>
    <prop name="Location">238 41.00000000000107</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,16,5,50,0,0,0,0,0,Κανονικά</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">The action is allowed for users with the following privileges:</prop>
    </prop>
    <prop name="Distance">2</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">False</prop>
    <prop name="TextFormat">[0s,,,AlignHCenter]</prop>
   </properties>
  </shape>
  <shape Name="tablePrivileges" shapeType="TABLE" layerId="0">
   <properties>
    <prop name="serialId">4</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">6 73</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">3</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">6 73</prop>
    <prop name="Size">570 193</prop>
    <prop name="ShowColumnHeader">True</prop>
    <prop name="ShowRowHeader">False</prop>
    <prop name="SortOnClick">True</prop>
    <prop name="RowHeight">21</prop>
    <prop name="RowHeaderWidth">0</prop>
    <prop name="GridType">Cross</prop>
    <prop name="VScrollBarMode">Auto</prop>
    <prop name="HScrollBarMode">Auto</prop>
    <prop name="Column">
     <prop name="Name">domain</prop>
     <prop name="Width">180</prop>
     <prop name="Visible">True</prop>
     <prop name="Editable">True</prop>
     <prop name="Label">
      <prop name="en_US.utf8">Domain</prop>
     </prop>
     <prop name="ToolTipText">
      <prop name="en_US.utf8"></prop>
     </prop>
    </prop>
    <prop name="Column">
     <prop name="Name">privilege</prop>
     <prop name="Width">300</prop>
     <prop name="Visible">True</prop>
     <prop name="Editable">True</prop>
     <prop name="Label">
      <prop name="en_US.utf8">Privilege</prop>
     </prop>
     <prop name="ToolTipText">
      <prop name="en_US.utf8"></prop>
     </prop>
    </prop>
   </properties>
   <events>
    <script name="SelectionChanged" isEscaped="1"><![CDATA[synchronized main()
{
  setButtons();
}]]></script>
    <script name="Clicked" isEscaped="1"><![CDATA[main(int row, string column, string value)
{
  if(column == &quot;domain&quot;)
  {
    processDomainSelection(row, value);
  }
}]]></script>
   </events>
  </shape>
  <shape Name="labelOtherUsers" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">5</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">205 306.0000000000011</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">4</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapProjecting,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Geometry">3.21739130434771 0 0 1.285714285714257 -355.5652173912912 -110.4285714285628</prop>
    <prop name="Location">205 306.0000000000011</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0,Κανονικά</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">For other users:</prop>
    </prop>
    <prop name="Distance">2</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">False</prop>
    <prop name="TextFormat">[0s,,,AlignRight|AlignVCenter]</prop>
   </properties>
  </shape>
  <shape Name="comboOtherUsers" shapeType="COMBO_BOX" layerId="0">
   <properties>
    <prop name="serialId">6</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">313 277.0769230769233</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">5</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">313 272</prop>
    <prop name="Size">301 23</prop>
    <prop name="Listitems">
     <prop name="Item">
      <prop name="Text">
       <prop name="en_US.utf8">Disable control</prop>
      </prop>
      <prop name="Select">False</prop>
     </prop>
     <prop name="Item">
      <prop name="Text">
       <prop name="en_US.utf8">Hide control</prop>
      </prop>
      <prop name="Select">False</prop>
     </prop>
    </prop>
    <prop name="Editable">False</prop>
   </properties>
  </shape>
  <shape Name="buttonOk" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">7</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">448 306.8666666666664</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">6</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">448 301</prop>
    <prop name="Size">80 23</prop>
    <prop name="BorderStyle">Styled</prop>
    <prop name="Text">
     <prop name="en_US.utf8">OK</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(mapping event)
{
  dyn_mapping dmRestrictions;
  for(int iRow = 0 ; iRow &lt; tablePrivileges.lineCount() ; iRow++)
  {
    string sDomain, sPrivilege;
    getMultiValue(&quot;tablePrivileges&quot;, &quot;cellValueRC&quot;, iRow, &quot;domain&quot;, sDomain,
                  &quot;tablePrivileges&quot;, &quot;cellValueRC&quot;, iRow, &quot;privilege&quot;, sPrivilege);
    mapping mRestriction;
    mRestriction[ALARM_SCREEN_RESTRICTION_DOMAIN] = sDomain;
    mRestriction[ALARM_SCREEN_RESTRICTION_PRIVILEGE] = sPrivilege;
    dynAppend(dmRestrictions, mRestriction);
  }
  if(dynlen(dmRestrictions) &gt; 0)
  {
    mapping mForOtherUsers;
    mForOtherUsers[ALARM_SCREEN_RESTRICTION_OTHER] = (int)comboOtherUsers.selectedPos;
    dynAppend(dmRestrictions, mForOtherUsers);
  }
  PanelOffReturn(makeDynFloat(1), makeDynString(jsonEncode(dmRestrictions)));
}]]></script>
   </events>
  </shape>
  <shape Name="buttonCancel" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">8</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">534 306.8666666666664</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">7</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">534 301</prop>
    <prop name="Size">80 23</prop>
    <prop name="BorderStyle">Styled</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Cancel</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(mapping event)
{
  PanelOff();
}]]></script>
   </events>
  </shape>
  <shape Name="buttonAdd" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">9</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">582 149.125</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">8</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">582 141</prop>
    <prop name="Size">32 26</prop>
    <prop name="BorderStyle">Styled</prop>
    <prop name="Image" SharedPixmap="1">
     <prop name="BackgroundPixmap">StandardIcons/Insert_after_20.png</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(mapping event)
{
  addTableRow();
}]]></script>
   </events>
  </shape>
  <shape Name="buttonDelete" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">10</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">582 179.6666666666664</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">9</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Sans Serif,-1,12,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">582 173</prop>
    <prop name="Size">32 26</prop>
    <prop name="BorderStyle">Styled</prop>
    <prop name="Image" SharedPixmap="1">
     <prop name="BackgroundPixmap">StandardIcons/delete_20.png</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(mapping event)
{
  deleteTableRow();
}]]></script>
   </events>
  </shape>
  <shape Name="SPACER1" shapeType="SPACER" layerId="0">
   <properties>
    <prop name="serialId">11</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">582 73</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">{0,0,0}</prop>
    <prop name="BackColor">{255,255,255}</prop>
    <prop name="TabOrder">10</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="TransparentForMouse">True</prop>
    <prop name="sizePolicy">
     <sizePolicy vertical="Expanding" horizontal="Ignored"/>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Location">582 73</prop>
    <prop name="Size">32 62</prop>
    <prop name="Orientation">vertical</prop>
   </properties>
  </shape>
  <shape Name="SPACER2" shapeType="SPACER" layerId="0">
   <properties>
    <prop name="serialId">12</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">582 205</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">{0,0,0}</prop>
    <prop name="BackColor">{255,255,255}</prop>
    <prop name="TabOrder">11</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="TransparentForMouse">True</prop>
    <prop name="sizePolicy">
     <sizePolicy vertical="Expanding" horizontal="Ignored"/>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Location">582 205</prop>
    <prop name="Size">32 61</prop>
    <prop name="Orientation">vertical</prop>
   </properties>
  </shape>
  <shape Name="SPACER3" shapeType="SPACER" layerId="0">
   <properties>
    <prop name="serialId">13</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">6 301</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">{0,0,0}</prop>
    <prop name="BackColor">{255,255,255}</prop>
    <prop name="TabOrder">12</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
    </prop>
    <prop name="TransparentForMouse">True</prop>
    <prop name="sizePolicy">
     <sizePolicy vertical="Ignored" horizontal="Expanding"/>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Location">6 301</prop>
    <prop name="Size">436 23</prop>
    <prop name="Orientation">horizontal</prop>
   </properties>
  </shape>
 </shapes>
 <groups>
  <group parentSerial="5" Name="SHAPE_GROUP1" serial="0">
   <properties>
    <prop name="shapeSerial">2</prop>
    <prop name="shapeSerial">1</prop>
    <prop name="sizePolicy">
     <sizePolicy vertical="Fixed" horizontal="Preferred"/>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
   </properties>
  </group>
  <layout parentSerial="4" Name="LAYOUT_GROUP1" serial="1">
   <properties>
    <prop idx="3" name="shapeSerial">12</prop>
    <prop idx="2" name="shapeSerial">10</prop>
    <prop idx="1" name="shapeSerial">9</prop>
    <prop idx="0" name="shapeSerial">11</prop>
    <prop name="isContainerShape">False</prop>
    <prop name="layoutType">VBox</prop>
   </properties>
  </layout>
  <layout parentSerial="5" Name="LAYOUT_GROUP2" serial="2">
   <properties>
    <prop idx="1" name="shapeSerial">6</prop>
    <prop idx="0" name="shapeSerial">5</prop>
    <prop name="isContainerShape">False</prop>
    <prop name="layoutType">HBox</prop>
   </properties>
  </layout>
  <layout parentSerial="5" Name="LAYOUT_GROUP3" serial="3">
   <properties>
    <prop idx="0" name="shapeSerial">13</prop>
    <prop idx="1" name="shapeSerial">7</prop>
    <prop idx="2" name="shapeSerial">8</prop>
    <prop name="isContainerShape">False</prop>
    <prop name="layoutType">HBox</prop>
   </properties>
  </layout>
  <layout parentSerial="5" Name="LAYOUT_GROUP4" serial="4">
   <properties>
    <prop idx="0" name="shapeSerial">4</prop>
    <prop idx="1" name="groupSerial">1</prop>
    <prop name="isContainerShape">False</prop>
    <prop name="layoutType">HBox</prop>
   </properties>
  </layout>
  <layout parentSerial="-1" Name="LAYOUT_GROUP5" serial="5">
   <properties>
    <prop idx="1" name="shapeSerial">3</prop>
    <prop idx="3" name="groupSerial">2</prop>
    <prop idx="4" name="groupSerial">3</prop>
    <prop idx="2" name="groupSerial">4</prop>
    <prop idx="0" name="groupSerial">0</prop>
    <prop name="isContainerShape">False</prop>
    <prop name="layoutType">VBox</prop>
   </properties>
  </layout>
 </groups>
</panel>
