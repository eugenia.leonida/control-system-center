JCOP Framework Developer Guide		{#mainpage}
==============================

@tableofcontents

# Introduction	{#main_introduction}

The purpose of this document is to introduce the WinCC OA JCOP Framework tools, libraries and architecture to the developers
of Control systems. This guide is a *work in progress*, with chapters being added successively.

## Content ##	{#main_something}

The following content is available:

 - [Use of CTRL libraries](@ref Libraries) : mechanisms, conventions and WinCC OA's specifics.

