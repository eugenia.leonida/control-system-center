Use of CTRL Libraries   			{#Libraries}
=====================

In this document we explain how to use CTRL libraries with the JCOP Framework;
outlining conventions, describing mechanisms for loading, detailing limitations and
the specifics of WinCC OA mechanisms.
This documentation should be considered as a complement of information delivered
by the WinCC OA online documentation.

[TOC]


## CTRL Libraries in WinCC OA ##		{#LibsInWinCCOA}

The code of functions and classes, as well as declarations of const or global variables in WinCC OA's CTRL languages may be stored in dedicated library files. These files typically have the `.ctl` filename extension and are stored in  `scripts/libs` subfolders of the project (or its subfolders).

It is worth observing that the vast majority of considerations presented here also apply to CTRL Extensions, i.e. binary modules developed using the WinCC OA C++ API, which when _loaded_ provide new functions to the CTRL language.

The functions, code and definitions of variables declared in the CTRL libraries (as well as CTRL extensions) may be used by panels, CTRL scripts or other libraries, provided that the library is _loaded_ and hence available in the execution scope. This allows the separation of the commonly used functionality or code that is often reused and hence the maintenance of that code in a single place, rather than for example, copying that code to all the panels where it is needed; this enhances the clarity and maintainability of the code.

### Overparameterizarion of libraries

Following the WinCC OA's convention, it is the main project path, the components path and WinCC OA installation paths (or in general, all the valid project paths declared in the project config file) that are searched for the `scripts/libs` CTRL libraries. If a file with the same file name is present in more than one of these paths the precedence is defined by the reverse order of paths in the project config file; i.e. the last entry (the main project path) will take precedence over
the component installation path and over the WinCC OA installation path.

 This way, the original version of the library remains unmodified, while allowing customized versions to be used. This mechanism is particularly useful for hotfixes or debugging: a temporary _overparameterized_ copy could be created with additional debugging statements for diagnostic purposes, and then removed after debugging is finished, thus restoring the system to the original state -- remember that the affected managers require restart to cause them to load the unmodified library again.

### Visibility of constructs defined in libraries.

As stated in the WinCC OA online help, the definitions of functions declared in the library are global to the manager that loads it.
In particular, once the functions (except those declared as private to the library) have been loaded (e.g. with the `#uses` statement in a child-panel) they become visible in all the other scopes of the UI manager (including the parent panel and other modules).
The definitions of classes, structures, enums (CTRL++ constructs) have similar _global_ scopes of visibility.

A particularity of WinCC OA CTRL language is its approach to the variables declared inside libraries.
The following cases should be considered:

- **constants**: the variables declared with the `const` keyword, which should be initialized by either a const literal or any statement (including a call to any arbitrary function). Its scope of visibility is _global_ to the manager, and the value may obviously not be modified. Once the library is loaded, all its constants are visible to all other execution contexts.

- **library-global variables**: the variables declared with the `global` keyword in the library, or variables added at runtime using the `addGlobal()` function of CTRL. Similarly to the case above, their visibility is _global_ to the manager, and the same instance is referred to from all the execution threads or contexts. A particular case is a `private global` variable, which can be referred to only by the functions declared in the same library, yet otherwise its scope is global (i.e. the same instance referred to by all threads)

- **library-local variables**: all the variables declared in the library, without either the `const` or `global` keyword; even though syntactically correct, their use is very limited and their behavior somewhat particular; they are also not documented in the WinCC OA online help. These variables have important limitations when the library needs to be dynamically loaded (topic covered further in this document). Every time a new thread is started (i.e. a new callback from a `dpConnect` is received, or every time a button is pressed), new instances of *all* library-local variables are created and initialized, regardless of whether they are usable or not and with evident impact on performance. And at the end of the execution of the script, they are discarded. For this reason the only use pattern is to refer to the same variable from two functions, with no need to pass it explicitly in the parameter list, yet only if these two functions are called _from the same thread_ (which should be considered a  programming pattern with questionable readability, even more obscure than global variables). Their use in existing code is typically a result of an omission (e.g. missing `const` specifier) or could easily be replaced by a global variable, by passing the variable explicitly to the function, or by declaring the variable in the scope of its use (e.g. the panel _ScopeLib_ or the CTRL Script variable) rather than the library-local variables. Their use is therefore strongly **discouraged**.

- **panel-global variables**: variables declared in the _Scope Lib_ of panels (we document them here for completeness, even though they do not belong to CTRL libraries); they are visible (and shared across) all the objects in the panel; however when used inside _reference panels_, each reference has its own copy, which allows for proper and intuitive encapsulation and hence reuse of parameterizable reference panels.  It is worth realizing that such variables become visible in library functions that are called inside the panel, and therefore this has potential to replace the problematic use of library-local variables mentioned above.  However this makes such library not "self-contained", i.e. it requires the assumption of the existence of the variables in the scope that calls the function. Consistent and clear documentation of such library functions, mentioning the dependency on the existence of such variables (as well as shapes, etc) would be highly recommended.



## JCOP Fw Conventions for CTRL libraries## 				{#LibsConventions}

The following conventions should be followed (their implications and technical details will be outlined in
further chapters):

1. JCOP Framework libraries are stored in the dedicated "component installation" folder.
This is to 
    - differentiate their location from other libraries (e.g. project-specific ones)
	- enable the possibility of having a customized version of a library (stored in the
   main project folder, which has higher precedence), e.g. to include a hotfix or
   a local modification, without loosing the original version distributed by the Framework

2. The library names (as well as names of functions, variables, classes, etc) should follow the naming conventions
defined in the [JCOP Framework Guidelines](https://edms.cern.ch/file/1100577/latest/jcopFrameworkGuidelines.pdf), namely
the prefixes (such as "fw", "al", "cms", "un") should be respected by all the libraries in the project.

3. There should be a "main" library, which defines the API for the whole component; the file name of this library should be equal to the component name,
e.g. for fwComp1 it would be `fwComp1.ctl`.

	The main library is responsible for loading all other libraries it makes use of, including
the libraries it distributes. From the perspective of a developer using a component, it
should be sufficient to declare loading of the main library only (without need to care about
its dependencies).

4. The libraries are stored in dedicated subfolders that correspond to the name of the component,
   rather than being stored directly under the `scripts/libs` of the project folder.
   For instance, the libraries of the component fwComp1 would be stored as
	- `scripts/libs/fwComp1/fwComp1.ctl`
	- `scripts/libs/fwComp1/fwComp1_tools.ctl`

5. For classes, the corresponding folder and the .ctl files should be placed following
   the WinCC OA recommendations, under scripts/libs/classes e.g. for component fwComp1:
	- `scripts/libs/classes/fwComp1/FwComp1BaseClass.ctl`
	- `scripts/libs/classes/fwComp1/FwComp1MainClass.ctl`

6. The file name of other libraries provided by the component *should* contain the name of the component, e.g.
	- `fwComp1_tools.ctl`

7. Encrypted libraries have the `.ctc` as filename extension.

8. When the `#uses` statement is used, it is technically possible to omit the .ctl (or .ctc) filename extension.

	This is strongly discouraged. initially the `#uses` statement in such form was  referring to control extension, which had different extensions on Linux and Windows, that had
	to be resolved when a panel or script was executed on an actual machine. We do not want these
	semantics to be overloaded, and we reserve the notation of a library name without an extension
	only for control extensions.


## Library loading mechanisms {#LibsLoadingMechanism}

From WinCC OA perspective, there are two mechanisms through which a library can be loaded into the
UI and CTRL Manager:

1. using the `LoadCtrlLib` statement in the project's config file, in the `[ui]` or `[ctrl]` section.
	The libraries are loaded *at the manager startup*, before any other code is executed. For example:
~~~
[ui]
LoadCtrlLib="fwComp1/fwComp1.ctl"
~~~

For completeness: to load a CTRL Extension (binary module), the config file statement to be used  is  `LoadCtrlDLL`.


2. through the `#uses` statement, specifying the library (or a CTRL Extension) to be loaded. The `#uses` statement may be placed in either a CTL script or in a panel (scope lib), and contrary to above, the library is loaded at the moment when the `#uses` statement is encountered (e.g. when a panel that makes use of the library is opened for the first time). For example:

`#uses "fwComp1/fwComp1.ctl"`

The former method was the only one available in early versions of WinCC OA, and for a long time it was the standard way through which all the JCOP Framework libraries were loaded: during the component installation entries were added to the project's config file.

The `#uses` statement for the same library in successive panels (scripts) that are started have no effect for that the library is already loaded.

Once the library is loaded by a manager, it cannot be "unloaded", and the functions/classes declared in it may not be "_reloaded_" (replaced) at runtime. A notable exception from this is GEDI, which under certain circumstances is capable of "reloading CTRL Libs" with a menu entry. This convenience mechanism makes the quick development/testing easier yet it comes with some limitations: in some cases the library is considered to be "in use" (by GEDI itself, or by a panel that remained open) and may not be safely "reloaded" - GEDI signals it with a popup error message. Notably, libraries that contain definition of classes with static members of static methods *may not* be reloaded once such member was referred to.

On the technical ground, the functionality of "reloading" the implementation of a function could be achieved by the mechanism of _function pointers_, yet at the moment we do not recommend this solution
as it would likely make the code more complex to understand or debug.


### Preloaded libraries in JCOP Framework
To minimize the effort of declaring the loading of the most widely used libraries, it was decided that the libraries of the FwCore as well as fwInstallation will always be loaded by every UI and CTRL manager, using the appropriate LoadCtrlLib statement in the config file.

This way it is not necessary to put, for instance, the `#uses "fwGeneral/fwException.ctl` on every single panel of library that needs to make use of standard framework's exception handling mechanism, etc.

At the technical level, the set of preloaded libraries for fwCore is declared in
`scripts/libs/fwCore/fwCore.ctl` and `scripts/libs/fwCore/fwCore_UI.ctl` ; the former is used
for all CTRL managers, while the latter is used by UI managers (it includes the former). Both of these libraries only contain a set of appropriate `#uses` statements.

In addition, it is important to assure that these libraries are loaded first, so that in case any of the function is delivered by another library (typically due to a mistake or misconfiguration),
the implementation provided by the Framework Core takes precedence. This is implemented by having
"wrapper" libraries: `AAA_FwLibStartup.ctl` and `AAA_FwLibStartup_UI.ctl`, which in turn
contain the `#uses` statements for fwCore.ctl and fwCore_UI.ctl as explained before.
As the libraries to be loaded are sorted alphabetically on the file names, a name starting
with triple capital 'A' characters effectively guarantees that they will be loaded. Interestingly,
the sorting order used by library-loading mechanism at manager startup is such that characters
such as underscore, or digits, have less precedence as the first character in the name than a capital
'A'. Therefore these two libraries need to be stored directly in the `scripts/libs` project folder,
and not in a dedicated subfolder.


It is then the `AAA_FwLibStartup.ctl` (and `AAA_FwLibStartup_UI.ctl`) that are actually declared
in the project config file to be loaded. The line is appended to the project config file during the
installation of the fwCore component -- the fwCore.config file defines them.

Loading of the libraries provided by fwInstallation is done separately through the config file entry
~~~
LoadCtrlLibs = "fwInstallation/fwInstallation.ctl"
~~~
which is appended by the installation tool during its first run.


### Additional Libraries loaded by GEDI parser

When a UI manager is used in GEDI mode, you may observe that additional libraries are loaded and their
symbols are visible. This is a side effect of GEDI's ScriptEditor parsing some of the non-core
framework libraries, so that it could suggest syntax (and offer to inject a relevant #uses statement) for functions from other components.
The libraries that are declared for parsing are in the config/config.gedi file.
The fact that they actually become fully loaded and available for execution e.g. when running a panel
in a "_Quick Run_" module seems to be a coincidence rather than a desired behavior, and hence it may disappear.

It is important to understand the importance of putting appropriate #uses statements in the panels' scope lib: due to the effect mentioned above, a panel that calls a framework function for which the `#uses` statement was not explicitly listed may still work in the QuickRun module of GEDI, but it will fail when running in the standard non-GEDI UI!

### Loading CTL libraries to EventManager (dpFunctions)

The code that calculates dpFunctions in WinCC OA is executed in the Event Manager, and the evaluation is done through the very same CTRL language interpreter as used by the CTRL and UI managers (with some restrictions). It is therefore possible to load a CTRL library library into the EventManager, so that it could be used in the dpFunction (or alarm action) formula. A lot of care and consideration is recommended when embarking on such solutions, because the performance and stability of a core component of WinCC OA (the Event Manager) may be impacted significantly. The framework makes use of this mechanism to provide custom dpFunctions used by the JCOP Finite State Machine component.

The way to load a library is to use the same `LoadCtrlLib` statement in the project config file inside the `[event]` section.

### Emergency "load all libs" mechanism.
In an "emergency" scenario where one cannot easily identify the framework library(ies) that contain definitions of unresolved functions, and one cannot edit ones own libraries and panels to inject the appropriate #uses statements, the framework provides a dedicated library `includes_legacy.ctl`, which is in the *main project folder* (not the components' folder); the library contains a list of #uses statement for all the main libraries of all components that were installed. It is sufficient to declare this library to be loaded by all managers using the LoadCtrlLib in the project config file, and restart the manager that misses the symbols, to have it load effectively all the public libraries of all installed components:
~~~
[ui]
LoadCtrlLib="includes_legacy.ctl"
[ctrl]
LoadCtrlLib="includes_legacy.ctl"
~~~
Note that this differs significantly from starting the manager with the `-LoadAllCtrlLibs` flag in that it will only load the libraries that were declared by the components, and it will therefore skip any stray copies or test libraries that may be present in your project's folders for any reason.


## Dynamic (on-demand) loading of CTRL libraries

WinCC OA does not provide any dedicated mechanism to load a CTRL library "on demand" or "under a certain condition" or for constructing its file name at runtime.
In particular, when a `#uses` statement for a non-existing library is encountered, a severe runtime error is generated, with no way to handle it.
However, the JCOP Framework provides this functionality through the `bool fwGeneral_loadCtrlLib()` function, notably to address the use cases of having a hook functions with customizations, and conditional loading of libraries that may or may not exist depending on whether another component is installed or not (see examples below).

The signature of the function is

~~~
bool fwGeneral_loadCtrlLib(string libRelPath, bool excOnNotFound = true, bool useBeyondInit = false)
~~~

The function employs the `evalScript()` mechanism of the CTRL to effectively execute the `#uses` statement at the time of execution; however, prior to this it checks if the indicated library exists.

With default setting of excOnNotFound the function will throw a CTRL exception if the specified library is not found. However, if one sets this parameter to false, then instead of throwing an exception the function will simply return false (without any attempt to load the library). This is particularly useful for conditional loading of libraries (i.e. load if exists) - see an example in the next subchapter

### Using the initializer pattern

To cover the use case explained above, a compact and efficient programming pattern may be used: initialization
of a `const` (or `global`) variable through the call to a function - see an example below.

The code of the function that provides the initial value is executed *at the time* of initialization (opening a panel, starting a script, loading a library), *before* any other code is executed. As it is run in a special execution context, certain restrictions apply (e.g. no possibility of calling blocking functions such as `dpGet`), yet for the `fwGeneral_loadCtrlLib()` it is particularly efficient.

For instance, we want to load a library plugin.ctl, from another library `myLib.ctl`, but only if the `plugin.ctl` exists:

~~~
private const bool myLib_pluginLibLoaded = fwGeneral_loadCtrlLib("plugin.ctl",false);
//
// any code of any function below may check the variable and try to call
// a function from plugin.ctl
~~~

The code of the myLib.ctl above could be loaded through the LoadCtrlLib, through the #uses statement, or through `fwGeneral_loadCtrlLib()` and in each case it will work as expected.

Particular attention must be paid when using the initialization pattern to declare the variable either as a `const` or a `global` (see also discussion on the top of the document). Failing to observe this may result in very misleading results as the non-global and non-const variables will be initialized (and hence code will be executed) for every thread that is started. Consider an example:

~~~
int _init = myInit(); // neither global nor const
void myInit()
{
 dpSet("ExampleDP_Arg1.",999);
}
~~~
Putting this kind of code into a library will result in having the DPE set at every start of any thread or any opening or event on a panel! In particular, if you use  the PARA module (which employs internal panels) this code will be executed many times - also in response to setting a new value; the end result may be very misleading!

The initialization pattern could be particularly for many use cases where any kind of initialization needs to be performed (such as deriving some values from constants, etc), without putting the effort on the programmer to explicitly call the initializer function. The limitations of not being able to call the "waiting" functions (such as dpGet) may partially be oversome by starting a separate (asynchronous) script where this kind of initialization may be performed. To ensure that such script runs independently on the current ones (and in particular that it won't terminate prematurely when the current script finishes) one may use `startScript()` functionality of the WinCC OA CTRL interpreter. Certainly, asynchronous execution of the latter script may present additional challenges that may need to be dealt with - the topic is out of scope for this document.

### Dynamic loading of library at runtime

**Attention** must be paid when the dynamic-loading needs to be performed at runtime, from an already running script (as opposed to the case of the library-initialization phase described above).
Due to the very particular behavior of the local variables declared in scripts, this may trigger an error, as such variables may **not** be instantiated in an already running script, and errors will be reported whenever the code will attempt to refer to such a local variable (complaining that the variable does not exist).

This has been discussed in length with ETM, and even though such variables are syntactically possible in CTRL, their use should absolutely be **avoided**. When declared in a library that is loaded, each time a new script execution starts (e.g. every click of every button, every callback of a dpConnect), separate instances of all of these variables need to be created; and these instances become out of scope when the script ends, and their values are lost.

To ensure that no mistake is made, and the loading of libraries "at runtime" in such circumstances is done intentionally (i.e. after having reviewed the loaded library for absence of such lib-local variables), the `useBeyondInit` optional parameter must be set to `true`; otherwise the function will deny loading the library when called from beyond the library-initializer context (as explained above).


## Examples

### Weak dependency (load/use if installed)
A common use case is to execute a piece of code (or enable a part of a panel) depending on whether another component is installed or not in the project. For instance, the alarm screen configuration panel may or may not have an option to configure access control.

In the past releases of the framework, where all the libraries of all components were loaded at manager startup, it was sufficient to check if a component of interest is installed (using the functions of `fwInstallation.ctl`), and then simply call the appropriate function.

As of the 8.4.0 release of the JCOP framework, where the requirement on explicit loading of libraries was placed, this is not sufficient: in the example below, code would still yield `true` because the component is installed:

~~~
string ver;
bool hasAC=fwInstallation_isComponentInstalled("fwAccessControl",ver);
~~~

However, as `fwAccessControl/fwAccessControl.ctc` library was not loaded  explicitly, the functions provided by it are not in the scope for execution. On the other hand, putting a `#uses` statement to load this library unconditionally in the panel being developed, would *trigger an error* in systems where the fwAccessControl component (and its libraries) are not installed.

Another approach is to check whether a particular function is declared in the scope, using the
`isFunctionDefined` function, and then conditionally calling it:

~~~
if (isFunctionDefined("fwAccessControl_isGranted")) {
	dyn_string exceptionInfo;
	bool enable = fwAccessControl_isGranted("SYSTEM:Visualize",exceptionInfo);
	ModifyButton.enabled=enable;
}
~~~

The code inside the `if` block will not be executed if component is installed, although the library is not loaded (as above).

The solution to this problem is to use the `fwGeneral_loadCtrlLib()` function described above, and conditionally load the necessary library "if it exists" (we achieve it by passing the value `false` to `excOnNotFound` parameter):

~~~
// load the library if it exists
private const bool _fwAccessControl_lib_loaded = fwGeneral_loadCtrlLib("fwAccessControl/fwAccessControl.ctc",false);

void configureAccessControl()
{
	// one way of conditional execution: check if lib loaded
	if (!_fwAccessControl_lib_loaded) return; // not loaded or not available - nothing more to do

	// another way of conditional execution: check if a function is in scope
	if (isFunctionDefined("fwAccessControl_isGranted")) {
		dyn_string exceptionInfo;
		bool enable = fwAccessControl_isGranted("SYSTEM:Visualize",exceptionInfo);
		ModifyButton.enabled=enable;
	}
}
~~~

In the example above, which should be considered as a part of a library used by a panel
(or placed in the scope lib), we attempt loading of the fwAccessControl library as the very
first thing before any other code is executed. The `_fwAccessControl_lib_loaded` variable will be
initialized to indicate whether the library was loaded or not, but if it wasn't then
there will be no error message, no failed attempt to load it and hence no undefined behavior.

