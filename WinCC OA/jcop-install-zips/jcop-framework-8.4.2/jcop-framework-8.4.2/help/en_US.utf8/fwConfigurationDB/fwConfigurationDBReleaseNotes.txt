		    JCOP Framework 
	    Configuration Database Tool 
		    version 8

31 Jul 2021: version 8.4.3
-------------------------------------------------
Resolved issues:
- FWCDB-1208: fix a problem with Connection-editing panel when using with CDBSI (typo error in panel name)
- FWCDB-1209: set input focus when opening the DB-connection edit panel
- FWCDB-1212: don't fail component installation if CDBSI connection params extraction fails

01 Sep 2020: version 8.4.2
-------------------------------------------------
Resolved issues:
- FWCDB-1204 Recipe loaded from DB with recent "validAt" yields duplicated values; 
             Similar issue was affecting getting the historical logical-views
- FWCDB-1204 Cannot load devices with OPC DA addresses from confDB

This release REQUIRES UPGRADE OF THE DB SCHEMA TO VERSION 3.06


26 Jun 2020: version 8.4.1
-------------------------------------------------
Resolved issues:
 - FWCDB-1186 Proper treatment of poll groups for SNMP addresses (also other types)
 - FWCDB-1187 GEDI or UNICOS menu entry for fwConfigurationDB
 - FWCDB-1190 Wrong recipe handling for DPE's being dyn_strings of length 1 with an empty string
 - FWCDB-1194 Correct inconsistent ISO/UTF strings in panels
 - FWCDB-1196 Recipes (cache) not working for dyn_long and dyn_ulong data types
 - FWCDB-1197 Recipe cache: truncated milliseconds for time-typed elements
 - FWCDB-1198 Unit tests for recipe caches
 - FWCDB-1201 Variable not declared in fwConfigurationDB_DeviceConfiguration.ctl

There is no need to upgrade the DB schema in this release

27 Nov 2019: version 8.4.0
-------------------------------------------------
* New schema version 3.05, and new schema create/update scripts.
  Note that the migration scripts for schemas earlier than 3.04
  are deprecated and have been removed from the tool - they are
  available in earlier version, and they could also be provided
  if necessary by the developers.
* FWCDB-1155: fix to the V_ITEM_PROPERTIES view so that for long
  string one displays first 4000 chars, rather than have an error.
  The view is not used by the tool directly, hence no fear of truncation.
* FWCDB-1133: granting access to other users using roles does not work,
  and it is disabled. Still, it is possible to grant the objects' roles
  directly to user, and this works correctly.



27 September 2019: version 8.3.1
-------------------------------------------------
Compatibility bridge with the newer DB schemas (FWCDB-1183).

As of this version the mechanism that checks the DB schema compatibility
was made less strict. The tool will now permit to use a DB schema
which has a higher *minor* version than the one required
(up to 8.3.0 it required the exact version).
In particular, with this release of fwConfigurationDB-8.3.1,
which requires schema version 3.04, it will be possible to connect 
to a schema with version 3.05 (as used with fwConfigurationDB-8.4.0+),
and other 3.X (X>=04) but not 4.00, etc.

This should make it easier to proceed with migration in setups where
multiple system use the same DB schema, and not all of them are
upgraded to the same version of fwConfigurationDB at the same time.

The only changes included in this release affect the libraries
and panels, which may be safely copied-over ("patched") to the 
component installation library with no need for full component 
upgrade. The affected files are:
- panels/fwConfigurationDB/fwConfigurationDB.pnl
- panels/fwConfigurationDB/fwConfigurationDB_Setup.pnl
- scripts/libs/fwConfigurationDB/fwConfigurationDB.ctl
- scripts/libs/fwConfigurationDB/fwConfigurationDB_DeviceConfiguration.ctl
- scripts/libs/fwConfigurationDB/fwConfigurationDB_Setup.ctl


28 January 2019: version 8.3.0
-------------------------------------------------
Release included in JCOP Framework 8.3.0.
No major changes.


25 May 2018: version 8.2.1
-------------------------------------------------
* FWCDB-1156: restore backward-compatibility in recipe-loading
  functions (altered by FWCDB-1107), when systemName parameter
  is used; in this case, the parameter should be a comma-separated
  list of systems on which logical devices should be looked up.
  In addition, for recipes loaded from a remote system, it is this
  system that will be searched for logical names.
* Workaround for FWCDB-1154 issue with truncation of long
  strings when inserted with CtrlRDBAccess-8.8 and older.
  When an older version is used, bulk-insert is disabled.
* FWCDB-1159: fwConfigurationDB_resolveDevices function crashing 
  with empty dyn_string deviceSystems argument; documentation
  of function updated
* panels corrected to be compatible with WinCC OA 3.16/UTF encoding

26 February 2018: version 8.2.0
-------------------------------------------------
* FWCDB-1143, performance improvements in +fwConfigurationDB_loadApplyAliases
* FWCDB-1146, workaround for performance issue with load configuration panel

08 December 2017: version 8.1.0
-------------------------------------------------
* fwConfigurationDB-8.1.0
 - major enhancement to the recipes, allowing to use aliases from
    remote dist systems (FWCDB-1107); by default, a compatibility mode
    is kept in which remote aliases are not resolved (ie. a recipe
    containing them may not be applied); a new mode is activated by
    setting the global variable fwConfigurationDB_allowResolveRemote to
    true (to be effecive one also needs to set the optional parameter
    allowApplyRemote to true in the fwConfigurationDB_applyRecipe()
    function. The functionality is also exposed in the Advanced Recipe
    Operations panel, through the new checkbox "Resolve remote aliases".
    Care should be taken when using the new mode in large distributed
    systems as the lookup of aliases may take significantly longer.
    The feature was originally required for UNICOS projects yet may find
    its application for JCOP Projects too. It is a natural extension of
    the already existing option for applying recipes remotely, which
    worked only for devices from the hardware tree so far.
 - it is now possible to store/restore the Mod_Plc internal datapoints
    into static configuration (blob/dyn_blob DPEs are treated correctly
    now) (FWCDB-1125)

28 March 2017: version 8.0.1
-------------------------------------------------
 * FWCDB-1125 handling of blob-typed datapoint elements

14 November 2016: version 8.0.0
-------------------------------------------------
 * First release for WinCC OA 3.15 and JCOP Framework-8.0

8 July 2016: version 7.0.0
-------------------------------------------------
 * First release for WinCC OA 3.14 and JCOP Framework-7.0
 * FWCDB-1114 Error when loading static configuration ORA-24333: zero iteration count



15 January 2016: version 5.2.0
-------------------------------------------------
* FWCDB-769  thread-safe setRecipeType()
* FWCDB-909  Method for getting the recipe versions' history
* FWCDB-1101 fwConfigurationDB: New API functions to get meta-data from a recipe stored in Oracle DB
* FWCDB-1102 Mispelled name for function fwConfigurattionDB_deleteDeviceConfiguration
             -> old name deprecated, kept for compatibility with a warning printed to the log
* FWCDB-1105 Function _fwConfigurationDB_loadCleanupAlerts not defined
* FWCDB-1106 Static configurations should not store in parameterization history
* FWCDB-1109 Modifying a recipe does not change the modification time/user
* FWCDB-1110 Panel and functions to get the history of selected recipe

14 August 2015: version 5.1.2
-------------------------------------------------
* remove the test library from being explicitely installed
* FWCDB-1099: _fwConfigurationDB_getRecipeClassDP() did not work with remote systems

25 September 2014: version 5.1.1
-------------------------------------------------
* Enhanced panel look to match JCOP Fw 5.1
* FWCDB-1096: Truncated data in recipes saved to database.
* FWCDB-1097: DPE unit/format sometimes not stored to static configuration in DB
* FWCDB-1098: Updating recipe in DB without merging of recipe devices:
               - new function fwConfigurationDB_resetRecipe(), exposed in Advanced Recipe Operations panel.
                 allows to reset the content of a recipe before saving a new version.

10 September 2014: version 5.1.0
-------------------------------------------------
* FWCDB-931:  Store aliases for non-framework dpelements in hardware hierarchy
* FWCDB-1036, FWCDB-1053, FWCDB-1063: tool to remove or clean up unused static configurations
* FWCDB-1041: fwConfigurationDB panel with wrong reference point
* FWCDB-1054: Recipes: DPENames of the recipe instance are not alphabetically ordered after calling fwConfigurationDB_modifyRecipeClass(...)
* FWCDB-1056: When new devices are added to a recipe class, take the online values for the recipe instances
* FWCDB-1057, FWCDB-1082, FWCDB-960: Cannot store recipes in DB after Datapoint Type change
* FWCDB-1058: configuration db limits exceed db column length when saving alerts
* FWCDB-1060: FwRecipeCache is not updated if any of the previous devices don't exist
* FWCDB-1061: Cannot load static configurations ("don't know how to cast to type 1" error)
* FWCDB-1066: Errors in the log when browsing static configuration (logical view)
* FWCDB-1068: Cannot store/restore dyn_blob elements in static configurations (_Mod_Plc)
* FWCDB-1073: Support storing/restoring the parameters of alarm hysteresis
* FWCDB-1080: Improve resolving the problems with ITEMS table
* FWCDB-1083: Cannot store recipes when neither alarm nor value declared; mismatch of type reported
* FWCDB-1085: Various rare syntax errors detected by DDT tool
* FWCDB-1086: Configuration descriptions not saved
* FWCDB-1089: Enable deletion of alert handler if no alert is defined
* FWCDB-1090: Enable the possibility to update devices' comment through fwConfigurationDB
* FWCDB-1091: Saving summary alerts in ConfigDB configuration including alerts DPEs saved before
* FWCDB-1092: Problem saving DIP addresses with ConfigurationDB
* FWCDB-1093: fwConfigurationDB save Recipe To Db fails for dpnames with |
* FWCDB-1094: Cannot load recipes from cache if a device was deleted or had its (logical) name changed
* FWCDB-1095: Improve performance of DP Alias selector
* Upgrades from schema versions lower than 3.0 are deprecated (ie. fwConfigurationDB used with PVSS 3.6); 
  use intermediate version of fwConfigurationDB to firstly upgrade the schema to version 3.X, if needed.
* Removal of deprecated fwConfigurationDB_Deprecated.ctl library and deprecated functions:
   fwConfigurationDB_GetRecipesInDB
   fwConfigurationDB_GetDBRecipesForNode
   fwConfigurationDB_GetDBRecipeMetaInfo
   fwConfigurationDB_GetRecipeVersionsInDB
   fwConfigurationDB_GetCacheRecipesForNode
   fwConfigurationDB_storeDevicesInDB
   fwConfigurationDB_saveReferences
   fwConfigurationDB_reconnectDevices
   fwConfigurationDB_updateDeviceHierarchyFromDB
   fwConfigurationDB_findDevicesInDB, 
   fwConfigurationDB_extractHierarchyFromDB, 
   fwConfigurationDB_getDeviceConfigurationFromDB
   _fwConfigurationDB_checkCreateDeviceConfiguration
   _fwConfigurationDB_DBCheckCreateSystemNode

14 November 2013: version 5.0.3
-------------------------------------------------
* FWCDB-1081, FWCDB-1072: Cannot save modified recipe to DB

10 June 2013: version 5.0.2
-------------------------------------------------
* FWCDB-1074: no popup menu in DeviceEditor&Navigator and no entries shown after logical devices loaded

04 June 2013: version 5.0.1
-------------------------------------------------
* Release for WinCC-OA 3.11SP1
* Requires schema upgrade to version 3.03 (fix for FWCDB-1065)
* FWCBD-1059 Inconsistent handling of alert limits (inclusive ranges) when handling static configurations
* FWCDB-1065 "Branches" of logical tree not restored while loading the static configuration
* FWCDB-1067 Problem loading alerts from static configurations (alarm classes containing system name)
* FWCDB-1068 Cannot store/restore dyn_blob elements in static configurations (_Mod_Plc)
* FWCDB-1069 Warnings in the log when no configuration to store
* Initial work to throw non-aborting warnings to the log file with throwError(), adding message catalogue

22 November 2012: version 5.0.0
-------------------------------------------------
* Initial release for WinCC-OA 3.11;
  Note that version 5.X may not be compatible any longer with PVSS 3.8SP2
* FWCDB-975: use bulk-insert to improve performance
* FWCDB-1048, FWCDB-1052 fwConfigurationDB_modifyRecipeClass(...) does not save meta info

23 April 2012: version 3.5.6
-------------------------------------------------
* FWCDB-1040: cannot upgrade schema from 2.05

02 March 2012: version 3.5.5
-------------------------------------------------
# FWCDB-1038 Static Configurations: values may be stored inconsistently; 
  values should not be stored for DPEs with INPUT address
# FWCDB-1039 Schema upgrade scripts fails (complaining about bad SQL statement)

27 February 2012: version 3.5.4
-------------------------------------------------
# ENS-4916 Problems with schema upgrades in projects with long history

24 February 2012: version 3.5.3
-------------------------------------------------
# ENS-4890 Alarm texts empty after restoring devices configuration from DB
# ENS-3763 Storing/restoring DIP addresses
# ENS-4865 Problems with creating datapoints
# additional check whether DP-Type exists when importing devices

02 February 2012: version 3.5.2
-------------------------------------------------
* ENS-4452 : progress-bar panels not working
* ENS-4481 : numeric overflow error while storing PVRange with maxFLOAT()
* ENS-4454 : updateConfigurationFromDB() throws error when called in CTRL scripts
* ENS-4453, ENS-4452, ENS-4471 : problem with schema upgrades
* homogenization in schemas updated from previous versions of the tool

16 December 2011: version 3.5.1
-------------------------------------------------
* #87161: Don't create roles in DB for Reader and Writer
* #89616: missing return value in findRecipesInDB
* #89932: Recipe class editor does not save the elements
* #88743: Static Configurations->Manage: cannot see configurations from other systems
* #88774: allow to load the devices saved on another system (functional replacement for fwConfigurationDB_reconnectDevices)
* #43050: allows to select the devices which should be loaded
* the list of deprecated functions available on the component's help page
* Recipe-create panel now save minimal configuration for missing devices by default
* Load/Save devices panels close after completing, to avoid confusion (progress-bars still not working)

07 September 2011: version 3.5.0
-------------------------------------------------
* official release
* improvements in schema-upgrade/drop scripts

29 August 2011: version 3.4.91 (3.5 beta2)
-------------------------------------------------
* various improvements and bugfixes
* put requirement on fwConfigs 3.4.1

25 August 2011: version 3.4.90 (3.5 beta1)
-------------------------------------------------
First beta release of new 3.5 version - highlights
 - complete rewrite of static configurations,
    huge improvement in performance (speed, memory, DB)
 - requires ConfDB Schema upgrade (to version "3.00")
 - requires newest CtrlRDBAccess 1.5
 - support for READER and WRITER ACCOUNT,
    implemented through DB roles
 - use new fwConfigs features: support for 
    discrete alerts, statistical dpfunctions
 - New "Recipe Classes", as implemented in the 3.3.66 release
 - a lot of old functions become deprecated and not functional
    requires careful cleanup
 - "Move" operation on static configurations not supported
 - Progress bar completely not functional for static configs

-------------------------------------------------
Piotr Golonka, CERN BE/ICS
