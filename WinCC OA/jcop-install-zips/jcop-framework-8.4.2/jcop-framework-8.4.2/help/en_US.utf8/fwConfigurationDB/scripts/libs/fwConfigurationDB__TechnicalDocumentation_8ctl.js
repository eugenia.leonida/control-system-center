var fwConfigurationDB__TechnicalDocumentation_8ctl =
[
    [ "getDevicesInRecipe", "group__PLSQLAPI.html#gabe41ab806ab9d03507f7b4806f807d43", null ],
    [ "getDevicesInConfiguration", "group__PLSQLAPI.html#ga9e11b412979453780f113d042f11b0b6", null ],
    [ "getRecipe", "group__PLSQLAPI.html#ga40489c9a6dcb1fd27f579811ff9af7eb", null ],
    [ "getRecipesList", "group__PLSQLAPI.html#ga07e4c170727d29af26cc35b841cd2c29", null ],
    [ "getConfigurationsList", "group__PLSQLAPI.html#ga0cdf9e94de66eddf2975e42b22f3f2d3", null ],
    [ "createRecipe", "group__PLSQLAPI.html#gabdb0d645887faffd6d711fc2799f014b", null ],
    [ "storeRecipe", "group__PLSQLAPI.html#ga9f7a0c7948782ccf6dcde7be1cf62605", null ],
    [ "getReferences", "group__PLSQLAPI.html#gae6405810846d53df7e0d9683e82f644c", null ],
    [ "setReferences", "group__PLSQLAPI.html#gadf8b2a5415b776e5a53360fb026666d7", null ],
    [ "getReferenceHistory", "group__PLSQLAPI.html#ga41489d6acfbac012c0e78635624f0c34", null ],
    [ "checkInputTable", "group__PLSQLAPI.html#ga021631cf4e85861d0c478ded36507957", null ]
];