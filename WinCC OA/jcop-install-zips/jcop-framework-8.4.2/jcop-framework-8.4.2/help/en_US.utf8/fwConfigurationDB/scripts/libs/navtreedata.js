/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "fwConfigurationDB", "index.html", [
    [ "JCOP Framework Configuration Database component", "index.html", [
      [ "Getting started", "index.html#GettingStarted", [
        [ "General Remarks", "index.html#qstart_generalRemarks", [
          [ "Exception Handling", "index.html#qstart_ExceptionHandling", null ],
          [ "Initialization", "index.html#qstart_initialization", null ],
          [ "Device lists", "index.html#qstart_devLists", null ]
        ] ],
        [ "Recipes", "index.html#qstart_recipes", [
          [ "Remote recipes and Remote Devices", "index.html#qstart_recipe_remote", null ],
          [ "Getting the list of available recipes", "index.html#qstart_getAvailableRecipes", null ],
          [ "Loading stored Recipes", "index.html#qstart_recipe_load", null ],
          [ "Apply Recipes", "index.html#qstart_recipe_apply", null ],
          [ "Create Recipes", "index.html#qstart_recipe_create", null ],
          [ "Storing recipes", "index.html#quickstart_recipe_store", null ],
          [ "Recipe templates", "index.html#qstart_recipe_templates", null ],
          [ "\"Manual\" creation of recipes", "index.html#qstart_recipe_adhoc", null ],
          [ "Other Functions", "index.html#qstart_recipe_otherFunctions", null ]
        ] ],
        [ "Hierarchies", "index.html#qstart_hierarchies", null ]
      ] ],
      [ "Data Structures", "index.html#dataStructures", [
        [ "recipeObject", "index.html#recipeObject", null ],
        [ "connectionInfo", "index.html#connectionInfo", null ],
        [ "RTData", "index.html#RTData", null ]
      ] ],
      [ "Conventions", "index.html#conventions", [
        [ "Conventions for recipe data storage", "index.html#recipeDataStorageConventions", null ]
      ] ],
      [ "Others", "index.html#others", null ]
    ] ],
    [ "Technical Documentation", "TechnicalDocumentation.html", null ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Reviewed List", "reviewed.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"TechnicalDocumentation.html",
"group__RecipeFunctions.html#gadafb27a60dc02862e5017876d4c229df"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';