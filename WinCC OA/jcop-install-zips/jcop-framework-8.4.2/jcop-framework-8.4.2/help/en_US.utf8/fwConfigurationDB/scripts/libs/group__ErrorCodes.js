var group__ErrorCodes =
[
    [ "fwConfigurationDB_checkInit", "group__ErrorCodes.html#gabfe9731975c38c37302fbfe101a2be58", null ],
    [ "fwConfigurationDB_initialize", "group__ErrorCodes.html#ga43d05490668849e2a99e320f93ed38cf", null ],
    [ "fwConfigurationDB_ERROR_GENERAL", "group__ErrorCodes.html#ga89b201b02dda2e01ead7670b0bc6aa36", null ],
    [ "fwConfigurationDB_ERROR_DPTNotExist", "group__ErrorCodes.html#ga127b56d4f021fe48d6812d3cec9cdf75", null ],
    [ "fwConfigurationDB_ERROR_OperationAborted", "group__ErrorCodes.html#gabec9c96c3f0a6303c4611f3b2fe379d0", null ],
    [ "fwConfigurationDB_ERROR_SeparatorCharInStringList", "group__ErrorCodes.html#ga08439e37e4ca7841e0943a2c135d1b97", null ],
    [ "fwConfigurationDB_ERROR_NoValuesDataInRT", "group__ErrorCodes.html#ga856fec3f7b0f0da2affa9f07c3e80ed5", null ]
];