var group__GlobalVariables =
[
    [ "fwConfigurationDB_currentSetupName", "group__GlobalVariables.html#ga676118bae9020aa17bf8feed33f86ec3", null ],
    [ "fwConfigurationDB_initialized", "group__GlobalVariables.html#gaa0cb6d1e9092075e0effa3c1bfb0b7db", null ],
    [ "fwConfigurationDB_DBConfigured", "group__GlobalVariables.html#ga202bab768fdd34fa77d6f8c80384dafa", null ],
    [ "GfwConfigurationDB_currentRecipeType", "group__GlobalVariables.html#gaa5e66c4b6898765e57f8a19192bc51a5", null ],
    [ "g_fwConfigurationDB_DBConnectionOpen", "group__GlobalVariables.html#ga37365fd45a062f3d239acb7d24c1f4ea", null ],
    [ "fwConfigurationDB_hasDBConnectivity", "group__GlobalVariables.html#ga28317180a9845b4341429bda0ee03418", null ],
    [ "g_fwConfigurationDB_DBConnection", "group__GlobalVariables.html#ga0cd06f5e1ec5329dc08ec91efc01a044", null ]
];