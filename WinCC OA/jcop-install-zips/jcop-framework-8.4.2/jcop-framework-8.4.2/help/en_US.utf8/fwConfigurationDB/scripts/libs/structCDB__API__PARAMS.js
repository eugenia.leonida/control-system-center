var structCDB__API__PARAMS =
[
    [ "I1", "structCDB__API__PARAMS.html#a823f99a60c3bb7377e7484080cd17cec", null ],
    [ "I2", "structCDB__API__PARAMS.html#a2c179f87bb756878d901a2842edaaef4", null ],
    [ "I3", "structCDB__API__PARAMS.html#ab95b97919048de26e2552c7d2092eb12", null ],
    [ "I4", "structCDB__API__PARAMS.html#ae619162a185f35f72daba119f245a252", null ],
    [ "I5", "structCDB__API__PARAMS.html#a518c93f9b7db2893f462e73775b1fecd", null ],
    [ "I6", "structCDB__API__PARAMS.html#acf920994d7023449580575ca9477eec1", null ],
    [ "I7", "structCDB__API__PARAMS.html#a3caf7093d65ec6fad2f26ceb97f5a39c", null ],
    [ "I8", "structCDB__API__PARAMS.html#a4b58eea6ae14640bd388148733a3e020", null ],
    [ "S1", "structCDB__API__PARAMS.html#a50abb9a56e7d5803cdd790e83fea1718", null ],
    [ "S2", "structCDB__API__PARAMS.html#aaafb8feba71c8d32185086792579d654", null ],
    [ "S3", "structCDB__API__PARAMS.html#a79a77c54a4e9e4cd1e0b9bbbe9c5d468", null ],
    [ "S4", "structCDB__API__PARAMS.html#a6dd09461552a591fbfefc7042d704fa1", null ],
    [ "S5", "structCDB__API__PARAMS.html#a1795c0618716be26be5e2d5667556533", null ],
    [ "S6", "structCDB__API__PARAMS.html#a21e9a82f70f73496d564311b518d703d", null ],
    [ "S7", "structCDB__API__PARAMS.html#a9b1586800313cfe47b0b62e0e7e4b02c", null ],
    [ "S8", "structCDB__API__PARAMS.html#a6606ffb5d7d5d40fe6f6a5c7c84c3c97", null ],
    [ "S9", "structCDB__API__PARAMS.html#af5fb4f73abb1ca4b433b32dc7a41e880", null ],
    [ "S10", "structCDB__API__PARAMS.html#a821732c67a2fe4544f2b32d39e011615", null ],
    [ "D1", "structCDB__API__PARAMS.html#a0b1b73ec658729086f3ee3a78f5ced94", null ],
    [ "D2", "structCDB__API__PARAMS.html#afe736301a9f746006daa62f7858b63ca", null ],
    [ "B1", "structCDB__API__PARAMS.html#a5e2c4d6aa86f0d54144dcc74ef8fe26b", null ]
];