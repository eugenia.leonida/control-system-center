This document reflects the driver numbers booked for JCOP devices:

- Caen devices:                     6
- Iseg devices:                     8
- Elmb / ElmbPsu devices:           9
- Lxi devices:                     10
- Wiener devices:                  11, 14
- Pdus devices:                    12
- DIP driver:                      13
- Cooling and Ventilation devices: 15
- Mpod devices:                    15, 17
- Atca devices:                    18

It is recommended that for custom device integrations the driver numbers in the range 30-39 are used.
