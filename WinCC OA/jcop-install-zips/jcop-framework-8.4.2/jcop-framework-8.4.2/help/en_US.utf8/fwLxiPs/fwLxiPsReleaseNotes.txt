             JCOP Framework - fwLxiPs


29 September 2021: fwLxiPs-0.0.8
---------------------------

- FWLXIPS-12: Review device definitions. Set "canHave" field TRUE for address, archive, alarm, format and unit (add defaults to unit).



31 May 2021: fwLxiPs-0.0.7
---------------------------

- Fix a datapoint problem introduced in fwLxiPs-0.0.5



31 May 2021: fwLxiPs-0.0.6
---------------------------

- FWLXIPS-11: add units display in Operation panels



14 April 2021: fwLxiPs-0.0.5
-----------------------------

- FWLXIPS-10: add PSConnected OPCUA address and Actual.Status.Connected DP (compatible with OpcUaLxiServer-1.0.9 server)



12 November 2020: fwLxiPs-0.0.1
--------------------------------

- First component release



-------------------------------------------------
Enrico Gamberini, CERN EP/DT

Support: icecontrols.support@cern.ch
