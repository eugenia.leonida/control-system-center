/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "fwCtrlUtils", "index.html", [
    [ "JCOP Framework CtrlUtils component", "index.html", [
      [ "Introduction", "index.html#Introduction", null ],
      [ "Using the extensions", "index.html#HowToUse", null ]
    ] ],
    [ "CtrlOOUtils Programmer Manual", "CtrlOOUtilsManual.html", null ],
    [ "CtrlJsonSerialize Programmer Manual", "CtrlJsonSerializeManual.html", [
      [ "JSON format and its limitations", "CtrlJsonSerializeManual.html#JsonFormat", null ],
      [ "CtrlJsonSerialize and WinCC OA JSON support", "CtrlJsonSerializeManual.html#ComparisonToWinccFunctions", null ],
      [ "WinCC OA types serialization", "CtrlJsonSerializeManual.html#WinccTypesEncoding", null ],
      [ "Remarks on JSON deserialization", "CtrlJsonSerializeManual.html#DeserializationBehavior", null ],
      [ "Optional inline type information", "CtrlJsonSerializeManual.html#InlineTypeInfo", null ],
      [ "Deserialization if object properties are missing", "CtrlJsonSerializeManual.html#AcceptIncompleteObject", null ]
    ] ],
    [ "CtrlXmlSerialize Programmer Manual", "CtrlXmlSerializeManual.html", null ],
    [ "Modules", "modules.html", "modules" ]
  ] ]
];

var NAVTREEINDEX =
[
"CtrlJsonSerializeManual.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';