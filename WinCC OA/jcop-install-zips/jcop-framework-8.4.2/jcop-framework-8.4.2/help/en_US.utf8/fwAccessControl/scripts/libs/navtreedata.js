/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "fwAccessControl", "index.html", [
    [ "JCOP Framework Access Control component", "index.html", [
      [ "Introduction", "index.html#Introduction", null ],
      [ "Integrating access control mechanism into a panel", "index.html#ACImplementInPanel", null ],
      [ "Setting up integrated Access Control in a distributed system", "index.html#IntegratedACSetup", null ],
      [ "Exporting Access Control data for use with the Installation Tool", "index.html#ExportACData", [
        [ "Using the exported Access Control data.", "index.html#ImportACData", null ]
      ] ],
      [ "The use of API (advanced)", "index.html#UseOfAPI", [
        [ "Groups of functions", "index.html#groups_fun_sec", [
          [ "Exception Handling", "index.html#ExceptionHandling", null ]
        ] ]
      ] ],
      [ "Technical details of the AC Server", "index.html#IntegratedACTechnical", null ],
      [ "Customizing the Access Control component", "index.html#customization", [
        [ "How to change the default \"logo\" picture in the login an password-change panels?", "index.html#ACLogo", null ],
        [ "How to customize the authentication method?", "index.html#AuthMethod", null ]
      ] ]
    ] ],
    [ "Modules", "modules.html", "modules" ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"files.html",
"group__UserFunctions.html#ga61cd60f644761f20ef7e3d5696a7fc50"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';