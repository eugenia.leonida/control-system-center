          JCOP Framework - fwWiener

18 December 2020: fwWiener-8.4.3
---------------------------------
- Fixes:
  - FWWIENER-268: Increase the version number in the DPT for 8.4.0 modifications


09 December 2020: fwWiener-8.4.2
---------------------------------
- Fixes:
  - FWWIENER-266: Fixed recovery panel bug in OPC UA in Linux OS
  - FWWIENER-265: Added redundant settings for OPC DA


26 June 2020: fwWiener-8.4.1
------------------------------

- OPC UA support:
  - FWWIENER-258: Remove useless code from migration panel

- Basic component updates:
  - FWWIENER-259: Update fwWiener-8.4.1 release notes

Full details of fwWiener-8.4.0 available at: https://its.cern.ch/jira/projects/FWWIENER/versions/33394




18 August 2019: fwWiener-8.4.0
------------------------------

        ONLY FOR TESTING PROPOUSES !!!
        ------------------------------
        ------------------------------

- Basic component updates:
  - FWWIENER-218: Update component version and dependencies to WinCC OA 3.16, fwInstallation-8.4.0 and fwCore-8.4.0
  - FWWIENER-208: Added release notes
  - FWWIENER-231: Update fwWiener-8.4.0 release notes

- Migration to UTF-8 character encoding:
  - FWWIENER-220: Translation of special characters in the dpList files
  - FWWIENER-225: Change character encoding in the html help files

- Panel migration to XML format keeping .pnl extension:
  - FWWIENER-226: Migrate panels to XML

- Component fixes:
  - FWWIENER-203: Channel operational panels drop errors if any channel of the crate is missing.
  - FWWIENER-227: Errors in the advanced panel for Wiener TCP crates
  - FWWIENER-232: PL508-DO crates had an obsolete inhibitions settings panel. New was available, but not installed. To keep the consistency of the JCOP device definitions and models, panel has moved to objects/fwWienner/fwWienerInhibitionsSettings.pnl
  - FWWIENER-242 (ENS-25720): Improved the code of the preInit, init and postInstall scripts. Specially when an address needs to be set and its driver / simulator is not connected.
  - FWWIENER-244: time counters defined as DATA in OPC DA, now are exposed in OPC UA as unsigned long. New DPE and dpFunctions created to keep the backcompatibility.

- OPC UA support:

  - FWWIENER-189: Migration panel from Wiener OPC DA setups to Wiener OPC UA.
  - FWWIENER-190: Migration of the fwDeviceDefinitions and fwDeviceModels for Wiener equipment. No DPT changes.
  - FWWIENER-191: Extraction and comparison of Wiener OPC DA and UA namespace (items, properties and values), tested with PL512 (TCP), PL508(CAN) and VME(CAN).
  - FWWIENER-192: Review of panels and dplist to make them compatible with Wiener OPC UA addressing.
  - FWWIENER-195: Add default OPC UA driver number 14.
  - FWWIENER-196: Validation of Wiener OPC UA native instantiation.
  - FWWIENER-197: Validation of Wiener OPC UA migration panel.

  - Issues reported in the 8.3.1 betas and fixed:
    - FWWIENER-198: OpcUaMigration files are packaged in the component but not installed.
    - FWWIENER-204: Migration panel doesn't detect properly devices already migrated to Wiener OPC UA.
    - FWWIENER-206 (ENS-25350): Migration panel fails to backup due to issues in latest patches of WinCC OA due to DB MultiUserMode DPE that it is not working as expected.
    - FWWIENER-209: Fix error on ShowError() function inside migration panel.
    - FWWIENER-210 (ENS-25350): Installation fails due to -13 error in a dplist import. New DPE introduced by ETM, broke back compatibility with previous WinCC OA patches.
    - FWWIENER-214 (ENS-25350): fwWiener.postInstall is migrating directly PL512 crates to OPC UA wrongly.
    - FWWIENER-217: Typo fixed in the recovery panel, that didn't allow to display the crates to recover.
    - FWWIENER-228 (ENS-25583): Wiener OPCUA migration panel didn't backup and migrate FwWienerMarathonGroup DPs
    - FWWIENER-219: OPC UA device definition migration panel doesn't work in linux.

  - Improvements in the migration panel:
    - FWWIENER-201: Avoid multiple popups asking the user for credentials.
    - FWWIENER-202: Re-enable table for selection of more crates after the previous migration is done.
    - FWWIENER-211: Improved the look and feel of the shapes of migration panel when is run on Linux.
    - FWWIENER-229: Wiener OPCUA migration panel had a race condition between dpSet and dpList export processes

  FwWiener component now supports native OPC UA and migration of current setups in to OPC UA.
  Nevertheless, there are a set of known issues related with the Wiener OPC UA that needs to be clarify and fixed.
  This could create some incompatibilities with devices migrated or instantiated with this release.
  For this reason this release is done for testing propouses and it is not recommended to migrate, instantiate newer setups in production.

  - Current Wiener OPC UA issues on work are:
    - OPCWIENER-51: Missing OPC UA items for server information (Server.UpTime), TCP/CAN interface statistical information.
    - OPCWIENER-52: Improvement of publications like System.UpTime to be human readable.
    - OPCWIENER-53: Data type is float and should be int.
    - OPCWIENER-55: WriteOnly OPC UA properties are seen as Read / Write properties from OPC UA clients
    - OPCWIENER-56: PL512 crates expose different OPC UA items, as consequence will appear addressing errors on the WinCC OA logviewer.

- Documentation:
  - FWWIENER-212: Added Wiener OPC DA - UA documentation
  - FWWIENER-233: Added documentation for fwWiener.ctl library. It will be available in two formats: HTML and QT integrated inside GEDI.

Full details of fwWiener-8.4.0 available at: https://its.cern.ch/jira/projects/FWWIENER/versions/32069




For the older versions of the component release notes are not available.
Please refer to the history of issues in the "FWCAEN" Jira project:
https://its.cern.ch/jira/projects/FWWIENER


-------------------------------------------------
Ben FARNHAM,    CERN BE-ICS-FD
Jonas ARROYO,   CERN BE-ICS-FD

Support: icecontrols.support@cern.ch

