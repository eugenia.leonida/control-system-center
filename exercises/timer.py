import gpiozero  
import time  

led_gre = gpiozero.LED(17) 
led_red = gpiozero.LED(27) 

while True:
  led_gre.on() 
  time.sleep(5)
  led_gre.off() 
  time.sleep(1)  
  
  led_red.on() 
  time.sleep(3)
  led_red.off() 
  time.sleep(2)  