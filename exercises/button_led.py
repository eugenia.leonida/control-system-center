
from gpiozero import LED, Button

led = LED(17)
button = Button(2)

while 1:

  button.when_pressed = led.on
  button.when_released = led.off
