# Control System Center

Control System Center is an under-development data logging project that aims to gather environmental data using multiple barometric (BME280) and magnetometer sensors. This project also uses WinCC OA to display all the data. The system is located at the Test Beam area of CERN to track environmental data such as temperature, humidity, pressure, magnetic field.

## Project Overview

### Directories

- **RASBEAM-Panel:**
  - Contains the WinCC OA panel file for visualization.

- **CSC Hardware:**
  - Contains KiCAD files for the PCB design of the Raspberry Pi HAT.

- **CSC Software:**
  - Includes Python scripts and software for each sensor.

## Tools Used

- **WinCC OA:** Utilized for automation, control and data visualization. Version 3.16

- **KiCAD:** Used for designing the hardware, including schematic and layout for the Raspberry Pi HAT. KiCAD Version 6.0

- **Python:** The project leverages Python for implementing the software for each sensor.

## Sensors Included

- **BME280 Sensor:** Measures temperature, humidity, and pressure.
  
- **Hall Effect Sensor (CYSJ362A):** Detects and measures magnetic fields.

