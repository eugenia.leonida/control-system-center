var fwArchive_8ctl =
[
    [ "fwArchive_deleteMultiple", "group__fwArchiveConfig.html#gad794c2f1685ac26363153a7d8a55e655", null ],
    [ "fwArchive_deleteMany", "group__fwArchiveConfig.html#gaf05488c6de59b87b7e80c0e634ca8f79", null ],
    [ "fwArchive_delete", "group__fwArchiveConfig.html#gadda131d7cba9677c8fd675b60a07db4b", null ],
    [ "_fwArchive_setOrConfig", "group__fwArchiveConfig.html#ga3d3db374903adbdb369ba018a339c48c", null ],
    [ "fwArchive_setMultiple", "group__fwArchiveConfig.html#gaa63e3364aae1bbbf6c23f5ec37b1bb43", null ],
    [ "fwArchive_setMany", "group__fwArchiveConfig.html#gabeb766c540f79fab8ef344d2ae9e970a", null ],
    [ "_fwArchive_convertManyArchiveClassNamesToDpNames", "group__fwArchiveConfig.html#gaf33f77fd9b25f8ef08158f1b29fd319f", null ],
    [ "fwArchive_set", "group__fwArchiveConfig.html#gafb9e50a3acf8e15ce902c9293a5dd687", null ],
    [ "fwArchive_configMultiple", "group__fwArchiveConfig.html#gad041b7e0d9329415ef56ffc86f738501", null ],
    [ "fwArchive_configMany", "group__fwArchiveConfig.html#ga01fc68f9eb455d623fb5eb0113b50fb9", null ],
    [ "fwArchive_config", "group__fwArchiveConfig.html#ga246ca35b69a5e2b3d7c3e30ddc2261e9", null ],
    [ "fwArchive_getMany", "group__fwArchiveConfig.html#gab465cefba12466ee6261437c2bab2eb8", null ],
    [ "fwArchive_get", "group__fwArchiveConfig.html#gaca95fedf7b695af9af8f1ca34a70a95f", null ],
    [ "fwArchive_startMultiple", "group__fwArchiveConfig.html#ga63ddbcc6b93b6b6c2281d3012c045285", null ],
    [ "fwArchive_start", "group__fwArchiveConfig.html#ga9174f196378fe815e95d68aab0c3d3f3", null ],
    [ "fwArchive_stopMultiple", "group__fwArchiveConfig.html#ga0ff2a9ec3c664e3d740b8949418b6e50", null ],
    [ "fwArchive_stop", "group__fwArchiveConfig.html#ga0d86c1e82a16dbc6d589d80f6a52c8f0", null ],
    [ "_fwArchive_set", "group__fwArchiveConfig.html#ga666ee7f61a2442ce3efb0d548bc2c0de", null ],
    [ "_fwArchive_setMany", "group__fwArchiveConfig.html#gaa79dcad330a670d6b7cb265dab453522", null ],
    [ "fwArchive_convertClassNameToDpName", "group__fwArchiveConfig.html#gae3e4607497b9e650d30d4f877f6f9dce", null ],
    [ "fwArchive_convertDpNameToClassName", "group__fwArchiveConfig.html#ga0f453170b46f3158905eabe08ebcd7ec", null ],
    [ "fwArchive_checkClass", "group__fwArchiveConfig.html#gab671c8d51e05269ab48c0e882d91136f", null ],
    [ "fwArchive_getClassState", "group__fwArchiveConfig.html#gaab86660e89a40516193cab59bfd9ad92", null ],
    [ "fwArchive_getClassStatistics", "group__fwArchiveConfig.html#ga4b79a05a5fe14a30616b358f696a8f73", null ],
    [ "fwArchive_getClassFreeSpace", "group__fwArchiveConfig.html#gae59041f2769c51e1fdb529d5e48781b8", null ],
    [ "fwArchive_checkDpesArchived", "group__fwArchiveConfig.html#ga4da008db2ef4b191d5c9cdf2c5f74268", null ],
    [ "_fwArchive_flagEndOfRefresh", "group__fwArchiveConfig.html#ga2a0aa71df9a294c9f2ee076971eaa422", null ],
    [ "fwArchive_convertDpNameToRDBClassName", "group__fwArchiveConfig.html#ga3480e066bd251829a3eb9713a439ae1c", null ],
    [ "fwArchive_convertRDBClassNameToDpName", "group__fwArchiveConfig.html#gaee4158f4839cf4a0831f4a5b0633049e", null ],
    [ "fwArchive_getAllValueArchiveClasses", "group__fwArchiveConfig.html#gaf2a85f29bdeff13645c716821f56222a", null ],
    [ "fwArchive_getAllRDBArchiveClasses", "group__fwArchiveConfig.html#ga33538220069ab0e65f89423f2d6e305a", null ],
    [ "fwArchive_useNGA", "group__fwArchiveConfig.html#ga72494083ae2210e250e576c863346267", null ],
    [ "fwArchive_checkIfSystemUsesNGA", "group__fwArchiveConfig.html#gaa09b2481c9a3fe7c3533c3305ca71ded", null ],
    [ "_fwArchive_checkIfSameArchivingIsUsed", "group__fwArchiveConfig.html#ga2cdad0b7959deaeebc614c4a7a8fe022", null ],
    [ "fwArchive_getAllNGAArchiveClasses", "group__fwArchiveConfig.html#gab23c22b75e71100f719bb71a4197061e", null ],
    [ "fwArchive_convertDpNameToNGAClassName", "group__fwArchiveConfig.html#gabd6f3d506b680f8a36c815abd752c14b", null ],
    [ "fwArchive_convertNGAClassNameToDpName", "group__fwArchiveConfig.html#ga9c4f2d809c4cab889b3ed9de2fc03b57", null ],
    [ "_fwArchive_convertNGAClassNameToInternalName", "group__fwArchiveConfig.html#ga99e37563d4fb6f52cf002ed86c80ef4e", null ],
    [ "_fwArchive_isArchiveClassNameNga", "group__fwArchiveConfig.html#ga7cebfc45592074cc460edc314e97bc23", null ],
    [ "_fwArchive_getNumberOfArchivingProcedures", "group__fwArchiveConfig.html#ga90285efa10ad98a03fb37bd0bb1f1121", null ],
    [ "_fwArchive_getDpeArchiveConfigsPresent", "group__fwArchiveConfig.html#ga04bd9e2fd8a762ccb1d5ff65e54fd13b", null ],
    [ "_fwArchive_getDpeArchiveConfigsEnabled", "group__fwArchiveConfig.html#gabbc0edf53ffbe0b4dbf844c3c8f9b2ca", null ],
    [ "_fwArchive_getDpeArchivingConfiguredActiveProcedureNumbers", "group__fwArchiveConfig.html#gab5eee36acc74cc9926d1698a5fb80599", null ],
    [ "_fwArchive_checkSettings", "group__fwArchiveConfig.html#ga9a591033701390d2bfe16e88c415dfd6", null ],
    [ "fwArchive_setManyArchivingProcedures", "group__fwArchiveConfig.html#ga617024736ed572678aad52ac37461560", null ],
    [ "fwArchive_replaceManyArchivingProcedures", "group__fwArchiveConfig.html#gae9240550be10fc28f8a93e39d3d21bd4", null ],
    [ "fwArchive_appendManyArchivingProcedures", "group__fwArchiveConfig.html#ga37a40467803c40d3fe71148eb9093aaa", null ],
    [ "fwArchive_deleteManyArchivingProcedures", "group__fwArchiveConfig.html#ga8258b3d3d7a155c13abe7ccb1779859f", null ],
    [ "fwArchive_getManyArchivingProcedures", "group__fwArchiveConfig.html#gaf336d0f54a29c9241f805c336761e696", null ],
    [ "_fwArchive_convertManyArchiveClassNamesToDpNamesMultipleArchiveProcedures", "group__fwArchiveConfig.html#ga03a9b36d983dc77c68e4f31a4e923762", null ],
    [ "_fwArchive_convertManyArchiveClassDpsToNamesMultipleArchiveProcedures", "group__fwArchiveConfig.html#ga777b5a159ff49477a9871761b1165450", null ]
];