import matplotlib.pyplot as plt
import numpy as np

reference_sensor = np.array([24, 34, 44, 84, 100, 120, 130])
new_sensor_readings = np.array([0.0195, 0.0391, 0.0586, 0.0781, 0.1220, 0.1520, 0.1820])

# Perform linear regression (calibration)
calibration_params = np.polyfit(new_sensor_readings, reference_sensor, 1)
calibration_slope, calibration_intercept = calibration_params

# Apply calibration to new sensor readings
calibrated_values = calibration_slope * new_sensor_readings + calibration_intercept

# Print calibration parameters
print(f'Calibration Slope: {calibration_slope}')
print(f'Calibration Intercept: {calibration_intercept}')

# Plot the calibration curve
plt.figure(figsize=(8, 4))
plt.scatter(new_sensor_readings, reference_sensor, label='Calibration Data', color='blue')
plt.plot(new_sensor_readings, calibrated_values, label='Calibration Curve', color='red', linestyle='--')
plt.xlabel('New Sensor Readings (V)')
plt.ylabel('Reference Sensor Readings (mT)')
plt.title('Sensor Calibration')
plt.legend()
plt.grid(True)
plt.show()
