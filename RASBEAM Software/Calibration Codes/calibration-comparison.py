import matplotlib.pyplot as plt
import numpy as np

voltage = [1.5, 3.0, 4.0, 4.5, 5.0, 5.4, 6.0]
current = [0.52, 1.02, 1.34, 1.53, 1.73, 1.86, 2.00]
reference_sensor = [24, 34, 44, 84, 100, 120, 130]
new_sensor_readings = [26, 39, 53, 66, 97, 117, 138]
new_sensor_error = 6  # Specify the error for the new sensor according to the datasheet

# Calculate absolute error for each measurement from the reference sensor (3.5% of the value)
reference_sensor_error_percentage = 3.5
reference_sensor_error = np.array(reference_sensor) * (reference_sensor_error_percentage / 100)

# Plot for both Reference and Original Sensor Data with Error Bars
plt.figure(figsize=(10, 6))

# Reference Sensor Data
plt.errorbar(current, reference_sensor, yerr=reference_sensor_error, fmt='o', label='Reference Sensor Data', color='blue', ecolor='gray', capsize=5)

# Original Sensor Data
plt.errorbar(current, new_sensor_readings, yerr=new_sensor_error, fmt='x', label='Original Sensor Data', color='red', ecolor='gray', capsize=5)

plt.xlabel('Current (A)')
plt.ylabel('Sensor Readings')
plt.title('Sensor Data Comparison')
plt.legend()
plt.grid(True)
plt.show()
