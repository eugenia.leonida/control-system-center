import matplotlib.pyplot as plt
import numpy as np


voltage = [1.5, 3.0, 4.0, 4.5, 5.0, 5.4, 6.0]
current = [0.52, 1.02, 1.34, 1.53, 1.73, 1.86, 2.00]
reference_sensor = [24, 34, 44, 84, 100, 120, 130]
new_sensor_readings = [0.0195, 0.0391, 0.0586, 0.0781, 0.1220, 0.1520, 0.1820]
new_sensor_error = 0.008  # Specify the error for the new sensor according to the datasheet

# Calculate absolute error for each measurement from the reference sensor (3.5% of the value)
reference_sensor_error_percentage = 3.5
reference_sensor_error = np.array(reference_sensor) * (reference_sensor_error_percentage / 100)

# Plot for Reference Sensor Data with Error Bars
plt.figure(figsize=(8, 4))
plt.errorbar(current, reference_sensor, yerr=reference_sensor_error, fmt='o', label='Reference Sensor Data', color='blue', ecolor='gray', capsize=5)
plt.xlabel('Current (A)')
plt.ylabel('Sensor Readings (mT)')
plt.title('Reference Sensor Data')
plt.legend()
plt.grid(True)
plt.show()

# Plot for Original Sensor Data with Error Bars
plt.figure(figsize=(8, 4))
plt.errorbar(current, new_sensor_readings, yerr=new_sensor_error, fmt='x', label='Original Sensor Data', color='red', ecolor='gray', capsize=5)
plt.xlabel('Current (A)')
plt.ylabel('Sensor Readings (V)')
plt.title('Original Sensor Data ')
plt.legend()
plt.grid(True)
plt.show()
