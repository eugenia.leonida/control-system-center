# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

import time
import board
import adafruit_tca9548a
from adafruit_bme280 import basic as adafruit_bme280
import RPi.GPIO as GPIO

# Define GPIO pins for LEDs
LED_PIN_1 = 17  # BCM17
LED_PIN_2 = 27  # BCM27
LED_PIN_3 = 22  # BCM22

# Setup GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED_PIN_1, GPIO.OUT)
GPIO.setup(LED_PIN_2, GPIO.OUT)
GPIO.setup(LED_PIN_3, GPIO.OUT)

i2c = board.I2C()  # uses board.SCL and board.SDA
tca = adafruit_tca9548a.TCA9548A(i2c)

bme1 = adafruit_bme280.Adafruit_BME280_I2C(tca[0])
bme2 = adafruit_bme280.Adafruit_BME280_I2C(tca[1])
bme3 = adafruit_bme280.Adafruit_BME280_I2C(tca[2])

bme1.sea_level_pressure = 1013.25
bme2.sea_level_pressure = 1013.25
bme3.sea_level_pressure = 1013.25

print("|Temp 1 |Temp 2 |Temp 3 |Pressure 1|Pressure 2|Pressure 3|Hum 1 |Hum 2 |Hum 3 |")

while True:
    temp1 = bme1.temperature
    hum1 = bme1.relative_humidity
    pressure1 = bme1.pressure

    temp2 = bme2.temperature
    hum2 = bme2.relative_humidity
    pressure2 = bme2.pressure

    temp3 = bme3.temperature
    hum3 = bme3.relative_humidity
    pressure3 = bme3.pressure

    print("|%0.1f C |%0.1f C |%0.1f C |%0.1f hPa |%0.1f hPa |%0.1f hPa |%0.1f%% |%0.1f%% |%0.1f%% |" % (
        temp1, temp2, temp3, pressure1, pressure2, pressure3, hum1, hum2, hum3))

    # Control LEDs based on BME280 presence
    GPIO.output(LED_PIN_1, GPIO.HIGH if hasattr(bme1, '_i2c') else GPIO.LOW)
    GPIO.output(LED_PIN_2, GPIO.HIGH if hasattr(bme2, '_i2c') else GPIO.LOW)
    GPIO.output(LED_PIN_3, GPIO.HIGH if hasattr(bme3, '_i2c') else GPIO.LOW)

    time.sleep(1)
