import RPi.GPIO as GPIO
import time 

# Set the GPIO pin number
FLAME_SENSOR_PIN = 4

# Set the GPIO mode to BCM
GPIO.setmode(GPIO.BCM)

# Set up the Flame Sensor pin as an input
GPIO.setup(FLAME_SENSOR_PIN, GPIO.IN)

# Continuously read the state of the Flame Sensor
while True:
    time.sleep(0.5)
    if GPIO.input(FLAME_SENSOR_PIN):
        print("No flame detected!")
    else:
        print("Flame detected.")
