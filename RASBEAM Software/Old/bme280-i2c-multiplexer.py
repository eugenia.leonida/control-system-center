# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

import time
import board
import adafruit_tca9548a
from adafruit_bme280 import basic as adafruit_bme280

i2c = board.I2C()  # uses board.SCL and board.SDA
# i2c = board.STEMMA_I2C()  # For using the built-in STEMMA QT connector on a microcontroller

# Create the TCA9548A object and give it the I2C bus
tca = adafruit_tca9548a.TCA9548A(i2c)

# Create BME280 sensors using the TCA9548A channels
bme1 = adafruit_bme280.Adafruit_BME280_I2C(tca[0])   # TCA Channel 0
bme2 = adafruit_bme280.Adafruit_BME280_I2C(tca[1])   # TCA Channel 1
bme3 = adafruit_bme280.Adafruit_BME280_I2C(tca[2])   # TCA Channel 2

# change this to match the location's pressure (hPa) at sea level
bme1.sea_level_pressure = 1013.25
bme2.sea_level_pressure = 1013.25
bme3.sea_level_pressure = 1013.25

print("|Temp 1 |Temp 2 |Temp 3 |Pressure 1|Pressure 2|Pressure 3|Hum 1 |Hum 2 |Hum 3 |")

while True:
    temp1 = bme1.temperature
    hum1 = bme1.relative_humidity
    pressure1 = bme1.pressure

    temp2 = bme2.temperature
    hum2 = bme2.relative_humidity
    pressure2 = bme2.pressure

    temp3 = bme3.temperature
    hum3 = bme3.relative_humidity
    pressure3 = bme3.pressure

    print("|%0.1f C |%0.1f C |%0.1f C |%0.1f hPa |%0.1f hPa |%0.1f hPa |%0.1f%% |%0.1f%% |%0.1f%% |" % (
        temp1, temp2, temp3, pressure1, pressure2, pressure3, hum1, hum2, hum3))

    time.sleep(1)
