from picamera import PiCamera
from time import sleep

camera = PiCamera()

# set camera resolution
camera.resolution = (1024, 768)

# start camera preview
camera.start_preview()

# wait for camera to warm up
sleep(2)

# capture image and save to file
camera.capture('/home/pi/Desktop/image.jpg')

# stop camera preview
camera.stop_preview()
