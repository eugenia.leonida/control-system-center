import time
import board
import math
import adafruit_pcf8591.pcf8591 as PCF
from adafruit_pcf8591.analog_in import AnalogIn
from adafruit_pcf8591.analog_out import AnalogOut

i2c = board.I2C()
pcf = PCF.PCF8591(i2c)

pcf_in_0 = AnalogIn(pcf, PCF.A0)
pcf_in_1 = AnalogIn(pcf, PCF.A1)
pcf_in_2 = AnalogIn(pcf, PCF.A2)
#pcf_in_3 = AnalogIn(pcf, PCF.A3)

pcf_out = AnalogOut(pcf, PCF.OUT)

header = "B(x,y,z) |{:>12}|{:>12}|{:>12}".format("Bx", "By", "Bz")
print(header)
print("-" * len(header))

while True:
    pcf_out.value = 65535

    raw_value_0 = pcf_in_0.value
    scaled_value_0 = (raw_value_0 / 65535) * 5.0

    raw_value_1 = pcf_in_1.value
    scaled_value_1 = (raw_value_1 / 65535) * 5.0

    raw_value_2 = pcf_in_2.value
    scaled_value_2 = (raw_value_2 / 65535) * 5.0

    #raw_value_3 = pcf_in_3.value
    #scaled_value_3 = (raw_value_3 / 65535) * 5.0

    bx, by, bz = 0, 0, 0  # Initialize magnetic field components

    # Update Bx, By, Bz
    for i, scaled_value in enumerate([scaled_value_0, scaled_value_1, scaled_value_2]):
        if   i == 0:
            bx = round(scaled_value, 4)
        elif i == 1:
            by = round(scaled_value, 4)
        elif i == 2:
            bz = round(scaled_value, 4)
    # Print magnetic field and Bx, By, Bz values with centered and aligned columns
    data_line = "{:.2f}  {:>12}  {:>12}  {:>12}".format(math.sqrt(bx**2 + by**2 + bz**2), bx, by, bz)
    print(data_line)

    time.sleep(1)

















