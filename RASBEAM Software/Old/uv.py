import smbus2
import time

# Define I2C address of UV sensor
UV_I2C_ADDR = 0x60

# Define UV sensor registers
UV_REG_CMD = 0x00
UV_REG_UVINDEX0 = 0x2C
UV_REG_UVINDEX1 = 0x2D

# Open I2C bus
i2c_bus = smbus2.SMBus(1)

# Configure UV sensor
i2c_bus.write_byte_data(UV_I2C_ADDR, UV_REG_CMD, 0x06)

while True:
    # Read UV data
    uv_data = 0
    for i in range(10):
        uv_data += i2c_bus.read_word_data(UV_I2C_ADDR, UV_REG_UVINDEX0)
        time.sleep(0.1)

    uv_data /= 10

    # Print UV data
    print("UV Index: %.2f" % (uv_data / 100.0))

    # Wait for 2 seconds before reading again
    time.sleep(2)
