#include "Si1145.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <bcm2835.h>


int main(int argc, char **argv)  {
    
    float UV_index;

    //bcm2835 Init
    if(!bcm2835_init()) {
        printf("bcm2835 init failed !\r\n");
        while(1);
    }

    //I2C Init
    bcm2835_i2c_begin();
    bcm2835_i2c_setSlaveAddress(SI1145_I2C_ADDRESS);
    bcm2835_i2c_setClockDivider(BCM2835_I2C_CLOCK_DIVIDER_2500);

    Si1145_Init();

    while(1)    {
        
        printf("====================\r\n");
        printf("Vis: %d\r\n", Si1145_readVisible());
        printf("IR:  %d\r\n", Si1145_readIR());

        UV_index = Si1145_readUV();
        UV_index /= 100;
        printf("UV: %.2f\r\n", UV_index);
        bcm2835_delay(1000);       
    }
}


