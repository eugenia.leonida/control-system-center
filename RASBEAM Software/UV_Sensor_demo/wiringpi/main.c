#include "Si1145.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int fd;

int main(int argc, char **argv)  {
    
    float UV_index;

    //wiringPi Init
    if(wiringPiSetupGpio() < 0) {
        printf("wiringPi init failed !\r\n");
        while(1);
    }

    //I2C Init
    fd = wiringPiI2CSetup(0x60);

    //Si1145 Init
    Si1145_Init(fd);

    while(1)    {
        
        printf("====================\r\n");
        printf("Vis: %d\r\n", Si1145_readVisible());
        printf("IR:  %d\r\n", Si1145_readIR());

        UV_index = Si1145_readUV();
        UV_index /= 100;
        printf("UV: %.2f\r\n", UV_index);
        delay(1000);       
    }
}


