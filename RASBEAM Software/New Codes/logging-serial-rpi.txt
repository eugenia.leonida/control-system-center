import serial
import time
import logging
import os
from datetime import datetime

# Create 'log' directory if it doesn't exist
log_dir = 'log'
if not os.path.exists(log_dir):
    os.makedirs(log_dir)

# Generate a log filename with the current timestamp
timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
log_filename = os.path.join(log_dir, f'{timestamp}.log')

# Set up logging to the new file
logging.basicConfig(filename=log_filename, level=logging.INFO, format='%(asctime)s - %(message)s')

# Open serial port
ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
ser.flush()

while True:
    if ser.in_waiting > 0:
        line = ser.readline().decode('utf-8').rstrip()
        print(line)  # Print to console
        logging.info(line)  # Log the output to the .log file
    time.sleep(1)
